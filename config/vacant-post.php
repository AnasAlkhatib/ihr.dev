<?php
if (env('APP_ENV') == 'local') {
    return array(
        'recipients_list' => array('ahmad@sto.com.sa', 'ahmadtarawneah@yahoo.com')
    );
}
return array(
    'recipients_list' => array(
        'fadi@sto.com.sa',
        'ahmad@sto.com.sa'
    )
);
