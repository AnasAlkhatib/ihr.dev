<?php
return [
    'width' => env('PROFILE_PICTURE_WIDTH', 200),
    'height' => env('PROFILE_PICTURE_HEIGHT', 200),
];