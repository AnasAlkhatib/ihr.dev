<?php
return array(
    /*
      |--------------------------------------------------------------------------
      | Absolute path to location where parsed swagger annotations will be stored
      |--------------------------------------------------------------------------
    */
    'doc-dir' => storage_path() . '/docs',
    /*
      |--------------------------------------------------------------------------
      | Relative path to access parsed swagger annotations.
      |--------------------------------------------------------------------------
    */
    'doc-route' => 'docs',
    /*
      |--------------------------------------------------------------------------
      | Relative path to access swagger ui.
      |--------------------------------------------------------------------------
    */
    'api-docs-route' => 'api/docs',
    /*
      |--------------------------------------------------------------------------
      | Absolute path to directory containing the swagger annotations are stored.
      |--------------------------------------------------------------------------
    */
    "app-dir" => "app",
    /*
      |--------------------------------------------------------------------------
      | Absolute path to directories that you would like to exclude from swagger generation
      |--------------------------------------------------------------------------
    */
    "excludes" => array(
        storage_path(),
        base_path() . "/tests",
        base_path() . "/resources/views",
        base_path() . "/config",
        base_path() . "/vendor",
        base_path() . "/database",
        base_path() . "/public",
        base_path() . "/storage",
        base_path() . "/app/Base",
        base_path() . "/app/Console",
        base_path() . "/app/Events",
        base_path() . "/app/Exceptions",
        base_path() . "/app/Jobs",
        base_path() . "/app/Listeners",
        base_path() . "/app/Policies",
        base_path() . "/app/Providers",
        base_path() . "/app/Repositories",
        base_path() . "/app/Requests",
        base_path() . "/app/Tasks",
        base_path() . "/app/Transformers",
        base_path() . "/app/Util",
    ),
    /*
      |--------------------------------------------------------------------------
      | Turn this off to remove swagger generation on production
      |--------------------------------------------------------------------------
    */
    "generateAlways" => true,
    "api-key" => "auth_token",
    /*
      |--------------------------------------------------------------------------
      | Edit to set the api's version number
      |--------------------------------------------------------------------------
    */
    "default-api-version" => "v1",
    /*
      |--------------------------------------------------------------------------
      | Edit to set the swagger version number
      |--------------------------------------------------------------------------
    */
    "default-swagger-version" => "2.0",
    /*
      |--------------------------------------------------------------------------
      | Edit to set the api's base path
      |--------------------------------------------------------------------------
    */
    "default-base-path" => "",
    /*
      |--------------------------------------------------------------------------
      | Edit to trust the proxy's ip address - needed for AWS Load Balancer
      |--------------------------------------------------------------------------
    */
    "behind-reverse-proxy" => false,
    /*
      |--------------------------------------------------------------------------
      | Uncomment to add response headers when swagger is generated
      |--------------------------------------------------------------------------
    */
    /*"viewHeaders" => array(
        'Content-Type' => 'text/plain'
    ),*/
    /*
      |--------------------------------------------------------------------------
      | Uncomment to add request headers when swagger performs requests
      |--------------------------------------------------------------------------
    */
    /*"requestHeaders" => array(
        'TestMe' => 'testValue'
    ),*/
);