<?php
if (env('APP_ENV') == 'production') {
    return [
        'gcm' => [
            'priority' => env('GCM_PRIORITY', 'normal'),
            'dry_run' => env('GCM_DRY_RUN', false),
            'apiKey' => env('GCM_API_KEY'),
        ],
        'fcm' => [
            'priority' => env('FCM_PRIORITY', 'normal'),
            'dry_run' => env('FCM_DRY_RUN', false),
            'apiKey' => env('FCM_API_KEY'),
        ],
        'apn' => [
            'certificate' => storage_path() . '/certificate/production/production.pem',
            'priority' => env('APN_PRIORITY', 'normal'),
            'passPhrase' => env('APN_PASS_PHRASE'),
            'dry_run' => env('APN_DRY_RUN', false)
        ]
    ];
}
/*
 * local configurations
 */
return [
    'gcm' => [
        'priority' => env('GCM_PRIORITY', 'normal'),
        'dry_run' => env('GCM_DRY_RUN', false),
        'apiKey' => env('GCM_API_KEY'),
    ],
    'fcm' => [
        'priority' => env('FCM_PRIORITY', 'normal'),
        'dry_run' => env('FCM_DRY_RUN', false),
        'apiKey' => env('FCM_API_KEY'),
    ],
    'apn' => [
        'certificate' => storage_path() . '/certificate/development/development.pem',
        'priority' => env('APN_PRIORITY', 'normal'),
        'passPhrase' => env('APN_PASS_PHRASE'),
        'dry_run' => env('APN_DRY_RUN', false)
    ]
];

