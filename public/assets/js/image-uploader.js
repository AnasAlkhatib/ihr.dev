function profileImageUploader(form, status, imageSelector) {
    form.ajaxForm({
        success: function (data) {
            status.empty();
            imageSelector.attr("src", data.pic_url);
        },
        error: function (xhr) {
            if (xhr.status == '422') {
                var errors = xhr.responseJSON.message;
                status.addClass('text-danger').html(errors.file[0]);
            }
        }
    });
}
