@extends('layouts.login_master')
@section('title')STO | {{trans('activate.activate_account')}}@stop
@section('content')
        <!--=== Content Part ===-->
<div class="container">
    <!--Reg Block-->
    <div class="reg-block light-shadow">
        <div class="reg-block-header">
            <h2>
                <img src="{{ asset('assets/img/logo1-default.png') }}"/></h2>
        </div>
        @if (session('message'))
            <div class="alert {{session('alert-class')}}">
                {{ session('message') }}
            </div>
        @endif
        <div class="checkbox margin-top-20 text-center">
            <p><a href="" onclick="goBack()">{{ trans('reset_password.back')}}</a></p>
        </div>
    </div>
    <!--End Reg Block-->
</div>
@stop
@section('custom-script')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });</script>
    <script type="text/javascript">
        $.backstretch([
            "{{ asset('assets/img/bg/login-1.jpg') }}",
            "{{ asset('assets/img/bg/login-2.jpg') }}",
            "{{ asset('assets/img/bg/login-3.jpg') }}"
        ], {
            fade: 1000,
            duration: 7000
        });
        function goBack() {
            window.history.back();
        }
    </script>
@stop
