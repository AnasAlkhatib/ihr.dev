@extends('layouts.login_master')
@section('title'){{trans('app.sto')}}@stop
@section('content')
    <!--=== Content Part ===-->
    <div class="container">
        <!--Reg Block-->
        <div class="reg-block light-shadow">
            <div class="reg-block-header">
                <h2>
                    <img src="{{ asset('assets/img/logo1-default.png') }}"/></h2>
                <br/>
                <h2>STO iHR</h2>
            </div>
            <div class=" row">
                <div class="col-md-10 col-md-offset-1" style="text-align: center;">
                    <h5 class="center">حمّل تطبيقنا من - Download our App from</h5>
                </div>
                <div class="col-md-8 col-md-offset-2 margin-bottom-5">
                    <a href="https://play.google.com/store/apps/details?id=com.sto.ihrapp5" target="_blank"
                       class="btn btn-block btn-android-inversed">
                        <i class="fa fa-android"></i> آندرويد - Android
                    </a>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <a href="https://itunes.apple.com/sa/app/sto-ihr/id1078064730" target="_blank"
                       class="btn  btn-block btn-tumblr-inversed">
                        <i class="fa fa-apple"></i> آيفون - iPhone
                    </a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <a href="{{ url('employer/login') }}" type="submit" class="btn-u btn-block">Employer Section<br>قسم أصحاب العمل</a>
                </div>
            </div>
        </div>
        <!--End Reg Block-->
    </div>
    <!--/container-->
@stop
@section('custom-script')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });</script>
    <script type="text/javascript">
        $.backstretch([
            "{{ asset('assets/img/bg/login-0.jpg') }}",
            "{{ asset('assets/img/bg/login-1.jpg') }}",
            "{{ asset('assets/img/bg/login-2.jpg') }}",
            "{{ asset('assets/img/bg/login-3.jpg') }}"
        ], {
            fade: 1000,
            duration: 7000
        });
    </script>
@stop
