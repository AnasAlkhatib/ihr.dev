@extends('layouts.admin_master')
@section('title')STO | {{trans('app.dashboard')}}@stop
@section('content')
    <!--=== Profile ===-->
    <div class="container content profile">
        <div class="row">
            <!--Left Sidebar-->
        @include('includes.admin_profile')
        <!-- Dashboard Content -->
            <div class="col-md-9">
                @if(session('status'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        {{ session('status') }}
                    </div>
                @endif
                <div class="profile-body">

                    <div class="row margin-bottom-20">

                        <!--News Posts-->
                        <div class="col-sm-6 md-margin-bottom-20">
                            <div class="panel panel-profile no-bg light-shadow">
                                <div class="panel-heading overflow-h bg-color-light-grey">
                                    <h2 class="panel-title heading-sm pull-left">
                                        <i class="fa  fa-bullhorn"></i>{{ trans('app.last_news') }}</h2>
                                </div>
                                <div id="scrollbar" class="panel-body no-padding mCustomScrollbar"
                                     data-mcs-theme="minimal-dark">
                                    @if(count($news) > 0)
                                        <?php $i = 0; ?>
                                        @foreach($news as $News)
                                            <?php $i++; ?>
                                            <div class="profile-post color-five">
                                                <span class="profile-post-numb">{{ sprintf("%02d", $i) }}</span>

                                                <div class="profile-post-in">
                                                    <h3 class="heading-xs">
                                                        <a href="dashboard/news/{{ $News->ID }}">{{ str_limit($News->news_title, $limit =30, $end = '...') }}</a>
                                                    </h3>

                                                    <p>{{ str_limit($News->news_body, $limit = 120, $end = '...') }}</p>
                                                </div><!--profile-post-in-->
                                            </div><!--profile-post-->
                                        @endforeach
                                    @else
                                        <h3 class="heading-xs">
                                            {{trans('app.no_records_found')}}
                                        </h3>
                                    @endif
                                </div><!--scrollbar-->
                            </div> <!--panel-->
                        </div>
                        <!--End News Posts-->
                        <!--Latest Jobs-->
                        <div class="col-sm-6">
                            <div class="panel panel-profile no-bg light-shadow">
                                <div class="panel-heading overflow-h bg-color-dark-red">
                                    <h2 class="panel-title heading-sm pull-left">
                                        <i class="fa fa-file-text-o"></i>{{ trans('app.last_vacancies') }}</h2>
                                </div>
                                <div id="scrollbar2" class="panel-body no-padding mCustomScrollbar"
                                     data-mcs-theme="minimal-dark">
                                    @if(count($vacancies) > 0)
                                        @foreach($vacancies as $vacant)
                                            <div class="profile-event">
                                                <div class="date-formats">
                                                    <span>{{ date('d', strtotime($vacant->created_at)) }}</span>
                                                    <small>{{ date('m', strtotime($vacant->created_at)) }},
                                                        {{ date('Y', strtotime($vacant->created_at)) }}</small>
                                                </div>
                                                <div class="overflow-h">
                                                    <h3 class="heading-xs">
                                                        <a href="dashboard/vacant/{{ $vacant->ID }}">{{ $vacant->job_title }} </a>
                                                    </h3>

                                                    <p>{{ str_limit($vacant->job_discription, $limit = 120, $end = '...') }}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <h3 class="heading-xs">
                                            {{trans('app.no_records_found')}}
                                        </h3>
                                    @endif
                                </div><!--/scrollbar2-->
                            </div><!--panel-->
                        </div><!--/col-sm-6-->
                        <!--end latest jobs-->
                    </div><!--/end row-->
                    <hr/>
                    <!--Statistics Block-->
                    <div class="row margin-bottom-10">
                        <div class="col-md-12 no-padding">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 sm-margin-bottom-20">
                                <div class="service-block-v3 service-block-u">
                                    <i class="icon-flag"></i><span
                                            class="service-heading">{{trans('app.vacancies')}}</span>
                                    <span class="counter">{{ number_format($vacant_count) }}</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 sm-margin-bottom-20">
                                <div class="service-block-v3 service-block-u">
                                    <i class="icon-users"></i><span
                                            class="service-heading">{{trans('app.seekers_count')}}</span>
                                    <span class="counter">{{ number_format($seekers_count) }}</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                <div class="service-block-v3 service-block-u">
                                    <i class="icon-user-following"></i><span
                                            class="service-heading">{{trans('app.applied_vacant')}}</span>
                                    <span class="counter">{{ number_format($applied_vacant_count) }}</span>
                                </div>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--/end Statistics Block-->
                </div><!--/end row-->
            </div><!--/end Dashboard Content -->
        </div><!--/row -->
    </div><!--/container-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".alert").fadeTo(2000, 500).slideUp(500, function () {
                $(".alert").alert('close');
            });
        });
    </script>
@endsection
