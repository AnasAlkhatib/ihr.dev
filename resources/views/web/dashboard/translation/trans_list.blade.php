@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.translation_list')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i>{{trans('app.translation_list')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('dashboard') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.translation_list')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert {{session('alert-class')}}">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            @endif
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="f2-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th colspan="4"><h3>{{trans('app.translation_Group_1')}}</h3></th>
                    </tr>
                    <tr>
                        <th>{{trans('app.translation_id')}}</th>
                        <th colspan="3"></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($areas as $area)
                        <tr>
                            <td>{{$area->ID}}</td>
                            <td colspan="3">
                                <form class="form-inline" action={{url('/dashboard/translations/area/'.$area->ID.'/edit')}} method="POST">
                                    <input type="hidden" name="ID" value="{{$area->ID}}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                        <input type="text" value="{{$area->name}} " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                        <input type="text" value="{{$area->name_en}} " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                    </div>

                                    <button type="submit" class="btn btn-default">Edit</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--End Table Striped-->

                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="f2-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th colspan="4"><h3>{{trans('app.translation_Group_2')}}</h3></th>
                    </tr>
                    <tr>
                        <th>{{trans('app.translation_id')}}</th>
                        <th colspan="3"></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cities as $city)
                        <tr>
                            <td>{{$city->ID}}</td>
                            <td colspan="3">
                                <form class="form-inline" action={{url('/dashboard/translations/city/'.$city->ID.'/edit')}} method="POST">
                                    <input type="hidden" name="ID" value="{{$city->ID}}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                        <input type="text" value="{{$city->name}} " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                        <input type="text" value="{{$city->name_en}} " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                    </div>

                                    <button type="submit" class="btn btn-default">Edit</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--End Table Striped-->

                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="f2-table" cellspacing="0" width="100%">
                    <thead>

                    <tr>
                        <th colspan="4"><h3>{{trans('app.translation_Group_3')}}</h3></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th colspan="3">
                            <form class="form-inline" action={{url('/dashboard/translations/ci/create')}} method="POST">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                    <input type="text" value=" " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                    <input type="text" value=" " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                </div>

                                <button type="submit" class="btn btn-default">Add New</button>
                            </form>
                        </th>
                    </tr>
                    <tr>
                        <th>{{trans('app.translation_id')}}</th>
                        <th colspan="3"></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companyIndustries as $companyIndustry)
                        <tr>
                            <td>{{$companyIndustry->ID}}</td>
                            <td colspan="3">
                                <form class="form-inline" action={{url('/dashboard/translations/ci/'.$companyIndustry->ID.'/edit')}} method="POST">
                                    <input type="hidden" name="ID" value="{{$companyIndustry->ID}}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                        <input type="text" value="{{$companyIndustry->name}} " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                        <input type="text" value="{{$companyIndustry->name_en}} " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                    </div>

                                    <button type="submit" class="btn btn-default">Edit</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--End Table Striped-->


                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="f2-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th colspan="4"><h3>{{trans('app.translation_Group_4')}}</h3></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th colspan="3">
                            <form class="form-inline" action={{url('/dashboard/translations/jr/create')}} method="POST">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                    <input type="text" value=" " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                    <input type="text" value=" " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                </div>

                                <button type="submit" class="btn btn-default">Add New</button>
                            </form>
                        </th>
                    </tr>
                    <tr>
                        <th>{{trans('app.translation_id')}}</th>
                        <th colspan="3"></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($jobRoles as $jobRole)
                        <tr>
                            <td>{{$jobRole->ID}}</td>
                            <td colspan="3">
                                <form class="form-inline" action={{url('/dashboard/translations/jr/'.$jobRole->ID.'/edit')}} method="POST">
                                    <input type="hidden" name="ID" value="{{$jobRole->ID}}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label class="sr-only" for="name">{{ trans('app.translation_name') }}</label>
                                        <input type="text" value="{{$jobRole->name}} " class="form-control" id="name" name="name" placeholder="{{ trans('app.translation_name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="name_en">{{ trans('app.translation_name_en') }}</label>
                                        <input type="text" value="{{$jobRole->name_en}} " class="form-control" id="name_en" name="name_en" placeholder="{{ trans('app.translation_name_en') }}">
                                    </div>

                                    <button type="submit" class="btn btn-default">Edit</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
    <!-- delete Modal -->
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('app.delete_vacant')}}</h4>
                </div>
                <div class="modal-body">
                    {{trans('app.sure_delete_vacant')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger ok">{{trans('app.delete')}}</button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">{{trans('app.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container content profile -->
@stop
@section('custom-script')

@stop
