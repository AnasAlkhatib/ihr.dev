@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.edit_employer')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_employer') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/employer') }}">{{trans('app.employer_list')}} </a></li>
            <li class="active">{{trans('app.edit_employer')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <form action="{{ url('/dashboard/employer/').'/'.$employer->id }}" id="sky-form1" class="sky-form"
                      method="POST">
                    {{ method_field('PUT') }}
                    {!! csrf_field() !!}
                    {{--<div class="alert alert-warning fade in">--}}
                        {{--{{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .--}}
                    {{--</div>--}}
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-4">
                                    <label>{{trans('app.name')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}"
                                               value="{{ old('name',$employer->name) }}"/>
                                        @if ($errors->has('name'))
                                            <span class="text-danger"> {{ $errors->first('name') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.mobile')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="mobile" autocomplete="off"
                                               placeholder="* {{ trans('app.mobile') }}"
                                               value="{{ old('mobile',$employer->mobile) }}"/>
                                        @if ($errors->has('mobile'))
                                            <span class="text-danger"> {{ $errors->first('mobile') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.email')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email" value="{{ old('email',$employer->email) }}"
                                               placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                        @if ($errors->has('email'))
                                            <span class="text-danger"> {{ $errors->first('email') }}</span>
                                        @endif
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label>{{trans('app.company_name')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-building-o"></i>
                                        <input type="text" name="company_name" autocomplete="off"
                                               placeholder="* {{trans('app.company_name') }}"
                                               value="{{ old('company_name',$employer->company_name) }}"/>
                                        @if ($errors->has('company_name'))
                                            <span class="text-danger"> {{ $errors->first('company_name') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.company_type')}} :</label>
                                    <label for="company_type" class="select">
                                        <select name="company_type">
                                            <option selected disabled hidden>* {{trans('app.company_type')}}</option>
                                            @if (old('company_type',$employer->company_type) !='' )
                                                <option value="{{ old('company_type',$employer->company_type) }}"
                                                        selected>{{ trans('app.'.old('company_type',$employer->company_type)) }}</option>
                                            @endif
                                            @if (count($company_types) > 0)
                                                @foreach ($company_types as $company_type)
                                                    @if(old('company_type',$employer->company_type) != $company_type['value'] )
                                                        <option value="{{ $company_type['value'] }}">{{ $company_type['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i></i>
                                        @if ($errors->has('company_type'))
                                            <span class="text-danger"> {{ $errors->first('company_type') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.job_title')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" name="job_title"
                                               value="{{ old('job_title',$employer->job_title) }}"
                                               placeholder="* {{trans('app.job_title')}}" autocomplete="off"/>
                                        @if ($errors->has('job_title'))
                                            <span class="text-danger"> {{ $errors->first('job_title') }}</span>
                                        @endif
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label>{{trans('app.password')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password" value="{{ old('password') }}"
                                               placeholder="* {{ trans('app.password') }}"/>
                                        @if ($errors->has('password'))
                                            <span class="text-danger"> {{ $errors->first('password') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.plan')}} :</label>
                                    <label for="plan" class="select">
                                        <select name="plan">
                                            <option selected disabled hidden> {{trans('app.plan')}}</option>
                                            @if (count($plans) > 0)
                                                @foreach ($plans as $plan)
                                                    @if (old('plan',$employerPlan['plan_id']) == $plan->id)
                                                        <option value="{{ $plan->id }}"
                                                                selected>{{$plan->name}}</option>
                                                    @else
                                                        <option value="{{$plan->id }}">{{ $plan->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i></i>
                                    </label>
                                    @if ($errors->has('plan'))
                                        <span class="text-danger"> {{ $errors->first('plan') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.website')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-globe"></i>
                                        <input type="text" name="website" autocomplete="off"
                                               placeholder="{{ trans('app.website') }}"
                                               value="{{ old('website',$employer->website) }}"/>
                                        @if ($errors->has('website'))
                                            <span class="text-danger"> {{ $errors->first('website') }}</span>
                                        @endif
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                    <section class="col-md-4 pull-right">
                                        <div class="btn-group" data-toggle="buttons">
                                            @if($employer->active == 1)
                                                <label class="btn btn-default active">
                                                    <input checked type="radio" name="active"
                                                           value="1">{{trans('app.active')}}
                                                </label>
                                            @else
                                                <label class="btn btn-default">
                                                    <input type="radio" name="active" value="1">{{trans('app.active')}}
                                                </label>
                                            @endif
                                            @if($employer->active == 0)
                                                <label class="btn btn-default active">
                                                    <input checked type="radio" name="active"
                                                           value="0">{{trans('app.inactive')}}
                                                </label>
                                            @else
                                                <label class="btn btn-default">
                                                    <input type="radio" name="active"
                                                           value="0">{{trans('app.inactive')}}
                                                </label>
                                            @endif
                                        </div>
                                    </section>
                            </div>
                            <section class="col-md-12">
                                <button id="submit" type="submit"
                                        class="btn-u">{{ trans('app.update')}}</button>
                            </section>
                        </fieldset>
                    </div><!--row-->
                </form>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop