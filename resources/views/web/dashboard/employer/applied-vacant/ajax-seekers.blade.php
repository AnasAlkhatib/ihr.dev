<div class="row margin-bottom-20">
    @foreach($seekers as $seeker)
        <div class="col-md-4 sm-margin-bottom-20">
            <div class="profile-blog light-shadow margin-bottom-20">
                <div class="btn-group pull-right">
                    <div class="favorite">
                        <favorite :seeker={{ $seeker->ID }}
                                :favorited={{ $seeker->isFavorited(Auth::guard('employer')->user()) ? 'true' : 'false' }}
                        >
                        </favorite>
                    </div>
                </div>
                <img class="rounded-x" src="{{ $seeker->pic_url }}" alt="">
                <div class="name-location">
                    <a href="{{ url('employer/seeker/'.$seeker->ID) }}">
                        <strong>{{ $seeker->full_name }}</strong>
                    </a>
                    <span>{{trans('app.seeker_education')}}
                        :{{ $seeker->edu }}</span>
                </div>
                <div class="clearfix margin-bottom-20">
                </div>
                <hr/>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-clock-o"></i>{{trans('app.experience')}}
                        :{{$seeker->exp}}</li>
                </ul>
                <hr/>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-birthday-cake"></i>{{trans('app.age')}}
                        : {{ $seeker->age }}
                    </li>
                    <li><i class="fa fa-venus-mars"></i>{{trans('app.gender')}}
                        : {{ $seeker->gender }}</li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-calendar"></i>{{trans('app.birth_date')}}
                        : {{ $seeker->birth_date }}
                    </li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-map-marker"></i>{{trans('app.area')}}
                        : {{ $seeker->area}}
                    </li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-home"></i>{{trans('app.city')}}
                        : {{ $seeker->city}}
                    </li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-globe"></i>{{trans('app.nationality')}}
                        : {{ $seeker->nationality }}</li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-file-text-o"></i>{{trans('app.national_num')}}
                        : {{ $seeker->national_num }}
                    </li>
                </ul>
                <hr/>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-phone"></i>{{trans('app.mobile')}}
                        : {{ $seeker->mobile }}</li>
                </ul>
                <ul class="list-inline share-list">
                    <li><i class="fa fa-envelope"></i>{{trans('app.mail')}}
                        <a href="mailto:{{$seeker->email}}">: {{$seeker->email}}</a>
                    </li>
                </ul>
                <hr/>
                <ul class="list-inline share-list">
                    <li>
                        <i class="fa fa-suitcase"></i>{{trans('app.employment_status')}}
                        @if($seeker->is_employed)
                            :  <b><span class="text-danger">{{trans('app.employed')}}</span></b>
                        @else
                            :  <b><span class="text-success">{{trans('app.not_employed')}}</span></b>
                        @endif
                    </li>
                </ul>
                @if($seeker->isCandidate)
                    <a href="#{{$seeker->candidate_id}}"
                       class="delete-candidate btn btn-u  btn-u-sm btn-u-light-grey btn-block text-center margin-top-20">
                        <i class="fa fa-user-times" aria-hidden="true"></i> {{trans('app.cancel_nominate')}}</a>
                @else
                    <a href="{{url('employer/candidate/select/'.$seeker->ID.'/vacant/'.$vacant->ID)}}"
                       class="btn btn-u btn-u-sm btn-u-red-dark btn-block text-center margin-top-20 ">
                        <i class="fa fa-user-plus" aria-hidden="true"></i> {{trans('app.nominate')}}</a>
                @endif
            </div>
        </div>
    @endforeach
</div>
<div>
    <!--End Profile Blog-->
    <!--Pagination Centered-->
    <div class="text-center">
        {!! $seekers->appends(Request::except('page'))->links() !!}
    </div>
    <!--End Pagination Centered-->
</div>

