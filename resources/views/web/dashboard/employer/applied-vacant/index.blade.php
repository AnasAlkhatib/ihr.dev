@extends('layouts.employer_master')
@section('title')STO | {{trans('app.dashboard')}}@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs light-shadow">
        <div class="container">
            <h1 class="pull-left">
                <i class="fa fa-suitcase"></i>{{trans('app.applied_vacant')}}
                <small>( {{$vacant->job_title}} )</small>
            </h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{ url('employer') }}">{{ trans('app.home') }}</a></li>
                <li><a href="{{ url('employer/vacant') }}">{{ trans('app.vacant_list') }}</a></li>
                <li class="active">{{trans('app.applied_vacant')}}</li>
            </ul>
        </div>
    </div>
    <!--/breadcrumbs-->
    <!--=== Profile ===-->
    <div class="container content profile" id="app">
        <div class="row">
            <!-- Profile Content -->
            <div class="col-md-12">
                <div class="profile-body margin-bottom-20">
                    <form class="sky-form" id="search" method="get"
                          action="{{ url('employer') }}/vacant/{{$vacant->ID}}/applied-vacant">
                        <fieldset class="margin-bottom-20">
                            <div class="headline">
                                <h4>{{trans('app.search')}} :</h4>
                            </div>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name='gender' id='gender' data-placeholder='{{trans('app.gender')}}'>
                                        <option></option>
                                        @foreach ($genders as $gender)
                                            @if( Input::get('gender') == $gender['value'] )
                                                <option value="{{ $gender['value']}}"
                                                        selected>{{ $gender['name'] }}</option>
                                            @else
                                                <option value="{{ $gender['value']}}">{{ $gender['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="nationality" id="nationality"
                                            data-placeholder='{{trans('app.nationality')}}'>
                                        <option></option>
                                        @foreach ($nationalities as $nationality)
                                            @if( Input::get('nationality') == $nationality['value'] )
                                                <option value="{{ $nationality['value']}}"
                                                        selected>{{ $nationality['name'] }}</option>
                                            @else
                                                <option value="{{ $nationality['value']}}">{{ $nationality['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="area" id="area" data-placeholder="{{trans('app.area')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="city" id="city" data-placeholder="{{trans('app.city')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>
                            <button id="submit_search"
                                    class="btn btn-danger btn-sm col-lg-1 col-md-1 col-sm-3 col-xs-12 margin-bottom-10 pull-right margin-left-10 margin-right-10">
                                <i class="fa fa-search margin-left-10"></i> {{trans('app.search_btn')}}
                            </button>
                        </fieldset>
                    </form>
                    <!--Profile Blog-->
                    <div class="dataTables_processing">
                        {{trans('app.please_wait')}} ...
                    </div>
                    <div id="seekers-ajax">
                        @if($seekers->count() > 0 )
                            @include('web.dashboard.employer.applied-vacant.ajax-seekers')
                        @else
                            <div class="text-center alert alert-warning fade in">
                                <h4>{{trans('app.no_result_to_display')}}</h4>
                            </div>
                        @endif
                    </div>
                </div>
                <!--/end row-->
            </div>
            <!--=== End Profile ===-->
        </div>
    </div>
    <!-- delete Modal -->
    <div id="delete_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('app.delete_candidate')}}</h4>
                </div>
                <div class="modal-body">
                    {{trans('app.sure_delete_candidate')}}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger ok">{{trans('app.delete')}}</button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">{{trans('app.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- delete Modal -->
@stop
@section('custom-script')
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /* delete  candidate */
            $(document).on('click', '.delete-candidate', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                var id = link.substring(link.indexOf('#') + 1);
                $("#delete_modal").modal({show: true});
                var AjaxRunning = false;
                $('.ok').click(function () {
                    if (!AjaxRunning) {
                        AjaxRunning = true;
                        $.ajax({
                            type: "POST",
                            url: "{{ url('/employer/candidate/') }}" + "/" + id,
                            data: {_method: "DELETE", _token: "{{  csrf_token() }}"},
                            success: function () {
                                $('#delete').modal('hide');
                                location.reload();
                            }
                        });
                    }
                });
            });
            /* delete  candidate  end */
            $('.dataTables_processing')
                    .hide()
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxStop(function () {
                        $(this).hide();
                    });
            <!--pagination-->
            $(document).on('click', '.pagination a', function (e) {
                getSeekers($(this).attr('href'));
                e.preventDefault();
            });
        });
        function getSeekers(page) {
            $.ajax({
                url: page,
                dataType: 'json',
                cache: false
            }).done(function (data) {
                $('#seekers-ajax').html(data);
                new Vue({el: '#app'});
                $('.dataTables_processing').hide();
            }).fail(function () {
                console.log('Seekers could not be loaded');
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true

            });
            /* Area Drop Down */
            var $AreaElement = $("#area");
            var $areaRequest = $.ajax({
                url: '/employer/area'
            });
            $areaRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $AreaElement.append(option);
                }
                $AreaElement.val('{{Input::get('area')}}').trigger('change');
            });
            /* city drop down */
            var $cityElement = $("#city");
            var $cityElementOption = $("#city option[value]");
            var $cityRequest = $.ajax({
                url: '/employer/city'
            });
            $cityRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $cityElement.append(option);
                }
                $cityElement.val('{{Input::get('city')}}').trigger('change');
            });

            /* relation area city */
            $AreaElement.on("change", function () {
                var id = $(this).val();
                $cityElement.prop("disabled", true);
                if (id !== "") {
                    $cityElementOption.remove();
                    var cities = $.ajax({
                        url: '/employer/area/' + id + '/city'
                    });
                    cities.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                } else {
                    $cityElementOption.remove();
                    var $cityRequest = $.ajax({
                        url: '/employer/city'
                    });
                    $cityRequest.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                }
                $cityElement.prop("disabled", false);
            });
        });
    </script>
@stop

