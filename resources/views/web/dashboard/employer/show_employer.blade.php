@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.view_employer')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i>{{trans('app.view_employer')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('dashboard/employer') }}">{{trans('app.employer_list')}}</a></li>
            <li class="active">{{trans('app.view_employer')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="employer-table">
                    <tbody>
                    <tr>
                        <td><i class="fa fa-user"></i> {{trans('app.name')}} : {{$employer->name}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-mobile-phone"></i> {{trans('app.mobile')}} : {{$employer->mobile}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-building-o"></i> {{trans('app.company_name')}}
                            : {{$employer->company_name}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-file-text-o"></i> {{trans('app.company_type')}}
                            : {{trans('app.'.$employer->company_type)}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-briefcase"></i> {{trans('app.job_title')}} : {{$employer->job_title}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-envelope"></i> {{trans('app.email')}} : {{$employer->email}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-globe"></i> {{trans('app.website')}} : {{$employer->website}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-check"></i> {{trans('app.status')}} :
                            @if($employer->active === 1)
                                <span class="text-success">{{trans('app.active')}}</span>
                            @endif
                            @if($employer->active === 0)
                                <span class="text-danger">{{trans('app.inactive')}}</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
</div> <!-- container content profile -->
@stop

