@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.edit_vacant')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i>{{ trans('app.edit_vacant') }} ( {{ $vacant->job_title }} )</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('employer') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('employer/vacant') }}">{{trans('app.vacant_list')}} </a></li>
            <li class="active">{{trans('app.edit_vacant')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<!--=== Profile ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <!-- Form -->
                {!! Form::open(['method' => 'PUT', 'url' => url('employer/vacant/'.$vacant->ID ),'class' => 'sky-form','id' => 'sky-form1' ]) !!}
                <div class="alert alert-warning fade in">
                    {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                </div>
                <div class='row'>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.company_info') }}</header>
                        <section>
                            <label for="company_location" class="label">* {{trans('app.company_location')}} </label>
                            <div class="inline-group">
                                @if (count($area) > 0)
                                    @foreach ($area as $key => $value)
                                        @if( !empty(old('company_location')))
                                            @if ( in_array($value['id'],old('company_location') ) )
                                                <label class="checkbox">
                                                    {!! Form::checkbox('company_location[]', $value['id'], true ,['class' => 'checkbox'])!!}
                                                    <i></i>{{$value['name'] }}
                                                </label>
                                            @else
                                                <label class="checkbox">
                                                    {!! Form::checkbox('company_location[]', $value['id'] , null ,['class' => 'checkbox'])!!}
                                                    <i></i>{{ $value['name']}}
                                                </label>
                                            @endif
                                        @elseif (in_array($value['id'], explode(',',$vacant->company_location) ) )
                                            <label class="checkbox">
                                                {!! Form::checkbox('company_location[]', $value['id'] , true ,['class' => 'checkbox'])!!}
                                                <i></i>{{ $value['name'] }}
                                            </label>
                                        @else
                                            <label class="checkbox">
                                                {!! Form::checkbox('company_location[]',$value['id'] , null ,['class' => 'checkbox'])!!}
                                                <i></i>{{ $value['name'] }}
                                            </label>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            @if ($errors->has('company_location'))
                                <span class="text-danger"> {{ $errors->first('company_location') }}</span>
                            @endif
                        </section>
                        <section>
                            <label for="company_industry" class='select'>
                                <select name='company_industry' id='company_industry'
                                        data-placeholder='* {{trans('app.company_industry')}}'>
                                    <option></option>
                                    @if (count($company_industry) > 0)
                                        @foreach ($company_industry as $company)
                                            @if (old('company_industry',$vacant->company_industry) && in_array(old('company_industry',$vacant->company_industry),$company) )
                                                <option value="{{ $company['id'] }}" selected>
                                                    {{ $company['name'] }}</option>
                                            @else
                                                <option value="{{ $company['id'] }}">{{ $company['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                            @if ($errors->has('company_industry'))
                                <span class="text-danger"> {{ $errors->first('company_industry') }}</span>
                            @endif
                        </section>
                        <section>
                            <label for='job_rules' class="select">
                                <select name='job_rules' id='job_rules'
                                        data-placeholder='* {{trans('app.job_rules')}}'>
                                    <option></option>
                                    @if (count($job_rule) > 0)
                                        @foreach ($job_rule as $rule)
                                            @if (old('job_rules',$vacant->job_rules) && in_array(old('job_rules',$vacant->job_rules),$rule) )
                                                <option value="{{ $rule['id'] }}" selected>
                                                    {{ $rule['name'] }}</option>
                                            @else
                                                <option value="{{ $rule['id'] }}">{{ $rule['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                            @if ($errors->has('job_rules'))
                                <span class="text-danger"> {{ $errors->first('job_rules') }}</span>
                            @endif
                        </section>
                        <section>
                            <label for='job_status' class="select">
                                <select name='job_status' id='job_status'
                                        data-placeholder='* {{trans('app.job_status')}}'>
                                    <option></option>
                                    @if (count($job_status) > 0)
                                        @foreach ($job_status as $status)
                                            @if (old('job_status',$vacant->job_status) == $status['value'])
                                                <option value="{{ $status['value']  }}"
                                                        selected>{{ $status['name'] }}</option>
                                            @else
                                                <option value="{{ $status['value']}}">{{ $status['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                            @if ($errors->has('job_status'))
                                <span class="text-danger"> {{ $errors->first('job_status') }}</span>
                            @endif
                        </section>
                        <section>
                            <label class="select">
                                <select multiple="multiple" name="tags[]" id="tags"
                                        data-placeholder='* {{trans('app.tags')}}'>
                                    <option></option>
                                    @foreach($tags as $tag)
                                        @if(old('tags',in_array($tag,$vacantTags)))
                                            <option value="{{ $tag }}"
                                                    selected>{{ $tag }}</option>
                                        @else
                                            <option value="{{$tag}}">{{ $tag }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </label>
                            @if ($errors->has('tags'))
                                <span class="text-danger"> {{ $errors->first('tags') }}</span>
                            @endif
                        </section>
                        <section>
                            <label for="close_date" class="input">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" name="close_date" id="close_date" autocomplete="off"
                                       placeholder="{{trans('app.close_date')}}"
                                       value="{{ old('close_date',($vacant->close_date)== '0000-00-00' ? '':$vacant->close_date ) }}">
                            </label>
                        </section>
                    </fieldset>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.vacant_info') }}</header>
                        <section>
                            <label for="job_title" class="input  ">
                                <i class="icon-append fa fa-briefcase"></i>
                                <input type='text' name='job_title' autocomplete="off"
                                       placeholder='* {{ trans('app.job_title') }}'
                                       value="{{ old('job_title',$vacant->job_title) }}"/>
                            </label>
                            @if ($errors->has('job_title'))
                                <span class="text-danger"> {{ $errors->first('job_title') }}</span>
                            @endif
                        </section>
                        <section>
                            <label for="job_description" class="textarea">
                                <i class="icon-append fa fa-file-text-o"></i>
                                <textarea rows="10" name='job_discription' id='job_description'
                                          placeholder="* {{ trans('app.job_description') }}">{{ old('job_discription',$vacant->job_discription) }}</textarea>
                            </label>
                            @if ($errors->has('job_discription'))
                                <span class="text-danger"> {{ $errors->first('job_discription') }}</span>
                            @endif

                        </section>
                    </fieldset>
                </div>
                <div class='row'>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.qualifications') }}</header>
                        <section>
                            <label for='education' class="select">
                                <select name='education' id='education' data-placeholder='{{trans('app.education')}}'>
                                    <option></option>
                                    @if (count($education) > 0)
                                        @foreach ($education as $edu)
                                            @if (old('education',$vacant->education) == $edu['value'])
                                                <option value="{{ $edu['value']  }}"
                                                        selected>{{ $edu['name']}}</option>
                                            @else
                                                <option value="{{ $edu['value'] }}">{{ $edu['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                        </section>
                        <section>
                            <label for="certification" class="textarea">
                                <i class="icon-append fa fa-bookmark"></i>
                                <textarea rows="5" name='certification'
                                          placeholder="{{ trans('app.certification') }}">{{ old('certification',$vacant->certification) }}</textarea>
                            </label>
                        </section>
                    </fieldset>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.experience') }}</header>
                        <section>
                            <label for="exp_years" class="select">
                                <select name='exp_years' id='exp_years' data-placeholder='{{trans('app.exp_years')}}'>
                                    <option></option>
                                    @if (count($exp_years) > 0)
                                        @foreach ($exp_years as $years)
                                            @if (old('exp_years',$vacant->exp_years) == $years)
                                                <option value="{{ $years  }}" selected>{{ $years}}</option>
                                            @else
                                                <option value="{{ $years }}">{{ $years }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>

                            </label>
                        </section>
                        <section>
                            <label for="exp_level" class="select">
                                <select name='exp_level' id='exp_level' data-placeholder='* {{trans('app.exp_level')}}'>
                                    <option></option>
                                    @if (count($exp_level) > 0)
                                        @foreach ($exp_level as $level)
                                            @if (old('exp_level',$vacant->exp_level) == $level['value'])
                                                <option value="{{ $level['value']  }}"
                                                        selected>{{ $level['name']}}</option>
                                            @else
                                                <option value="{{ $level['value'] }}">{{ $level['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                            @if ($errors->has('exp_level'))
                                <span class="text-danger"> {{ $errors->first('exp_level') }}</span>
                            @endif

                        </section>
                    </fieldset>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.languages') }}</header>
                        <section>
                            <div class="inline-group">
                                @if (count($language) > 0)
                                    @foreach ($language as $lang)
                                        @if( !empty(old('language')))
                                            @if ( in_array($lang['value'] ,old('language') ) )
                                                <label class="checkbox">
                                                    {!! Form::checkbox('language[]', $lang['value'] , true ,['class' => 'checkbox'])!!}
                                                    <i></i>{{ $lang['name']}}
                                                </label>
                                            @else
                                                <label class="checkbox">
                                                    {!! Form::checkbox('language[]', $lang['value'] , null ,['class' => 'checkbox'])!!}
                                                    <i></i>{{ $lang['name']}}
                                                </label>
                                            @endif
                                        @elseif (strstr($vacant->language, $lang['value']) )
                                            <label class="checkbox">
                                                {!! Form::checkbox('language[]', $lang['value'] , true ,['class' => 'checkbox'])!!}
                                                <i></i>{{ $lang['name']}}
                                            </label>
                                        @else
                                            <label class="checkbox">
                                                {!! Form::checkbox('language[]', $lang['value'] , null ,['class' => 'checkbox'])!!}
                                                <i></i>{{ $lang['name']}}
                                            </label>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </section>
                    </fieldset>
                </div>
                <div class='row'>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.skills') }}</header>
                        <section>
                            <label for="skills" class="textarea">
                                <i class="icon-append fa fa-star"></i>
                                <textarea rows="5" name='skills'
                                          placeholder="{{ trans('app.skills') }}">{{ old('skills',$vacant->skills) }}</textarea>
                            </label>
                        </section>
                    </fieldset>
                    <fieldset class="col-md-6">
                        <header>{{ trans('app.additional_vacant_info') }} </header>
                        <section>
                            <label for="additional_vacant_info" class="textarea">
                                <i class="icon-append fa fa-plus-square"></i>
                                <textarea rows="5" name='additional_vacant_info'
                                          placeholder="{{ trans('app.additional_vacant_info') }}">{{ old('additional_vacant_info',$vacant->additional_vacant_info) }}</textarea>
                            </label>
                        </section>
                    </fieldset>
                </div>
                <div class='row'>
                    <fieldset class="col-md-12">
                        <header> {{ trans('app.preferred_info') }} </header>
                        <section class="col-md-4 padding-right-0">
                            <label class="select">
                                <select name='gender' id='gender' data-placeholder='{{trans('app.gender')}}'>
                                    <option></option>
                                    @if (count($Genders) > 0)
                                        @foreach ($Genders as $gender)
                                            @if (old('gender',$vacant->gender) == $gender['value'])
                                                <option value="{{ $gender['value'] }}"
                                                        selected>{{ $gender['name'] }}</option>
                                            @else
                                                <option value="{{ $gender['value'] }}">{{ $gender['name']  }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </label>
                        </section>
                        <section class="col-md-4">
                            <label class="input">
                                <i class="icon-append fa fa-user"></i>
                                <input type='text' name='candidate_age'
                                       value="{{ old('candidate_age',$vacant->candidate_age) }}"
                                       placeholder='{{ trans('app.candidate_age') }}'/>
                            </label>
                        </section>
                        <section class="col-md-4 padding-left-0">
                            <label class="input">
                                <i class="icon-append fa fa-home"></i>
                                <input type='text' name='candidate_address' class='form-control'
                                       value="{{ old('candidate_address',$vacant->candidate_address) }}"
                                       placeholder='{{ trans('app.candidate_address') }}'/>
                            </label>
                        </section>
                        <section class="col-md-12 no-padding">
                            <label class="textarea">
                                <i class="icon-append fa fa-plus-square"></i>
                                <textarea rows="5" name='additional_prefered_info' class='form-control'
                                          placeholder="{{ trans('app.additional_preferred_info') }}">{{ old('additional_prefered_info',$vacant->additional_prefered_info) }}</textarea>
                            </label>
                        </section>
                    </fieldset>
                </div>
                <footer>
                    <button type="submit" class="btn btn-danger" id="submit">
                        {{ trans('app.update')  }}
                    </button>
                    <div class="progress"></div>
                </footer>
            {!! Form::close() !!}
            <!-- End Form -->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
</div>
<!--/container-->
<!--=== End Profile ===-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select:not(#tags)").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                }
            });
            /* vacancy tags  options */
            $("#tags").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                tags: true
            }).focusin(function () {
                $(this).data("select2").$container.find(".select2-search__field").focus();
            });

            /* date picker position */
            $.extend($.datepicker, {
                _checkOffset: function (inst, offset, isFixed) {
                    return offset;
                }
            });
            /* date picker*/
            $('#close_date').datepicker({
                dateFormat: 'yy-mm-dd',
                showAnim: 'slideDown'
            });
            $("#close_date_icon").click(function () {
                $("#close_date").datepicker("show");
            });
        });</script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop