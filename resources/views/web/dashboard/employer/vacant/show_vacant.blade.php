@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.view_vacant')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-plus-square-o"></i>{{ trans('app.view_vacant') }} ( {{ $vacant->job_title }} )</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('employer') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('employer/vacant') }}">{{trans('app.vacant_list')}} </a></li>
            <li class="active">{{trans('app.view_vacant')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<!--=== Profile ===-->
<div class="container content profile">
    <div class="row">
        <!-- Profile Content -->
        <div class="col-md-12">

            <div class="profile-body margin-bottom-20">
                <!--Job-->
                <div class="row margin-bottom-5">
                    <div class="col-md-6 sm-margin-bottom-20">
                        <div class="profile-blog light-shadow margin-bottom-20">
                            <div class="name-location">
                                <strong class="red">{{ trans('app.company_info') }}</strong>
                            </div>
                            <div class="clearfix margin-bottom-20">
                            </div>
                            <ul class="list-inline">
                                <li><b>{{ trans('app.company_industry') }}
                                        : </b><span>{{$companyIndustry}}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.company_location') }}
                                        : </b><span>{{$companyLocation}}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.job_rules') }} : </b><span>{{$jobRules}}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.job_status') }}
                                        : </b><span>{{ trans('app.'.$vacant->job_status) }}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.created_at') }}
                                        : </b><span>{{$vacant->created_at->format('Y-m-d')}}</span></li>
                                @if($vacant->close_date != "0000-00-00")
                                    <hr/>
                                    <li><b>{{ trans('app.close_date') }} : </b><span>{{$vacant->close_date}}</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 sm-margin-bottom-20">
                        <div class="profile-blog light-shadow margin-bottom-20">
                            <div class="name-location">
                                <strong>{{ trans('app.vacant_info') }}</strong>
                            </div>
                            <div class="clearfix margin-bottom-20">
                            </div>
                            <ul class="list-inline">
                                <li><b>{{ trans('app.job_title') }} : </b><span>{{$vacant->job_title}}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.reference_number') }} : </b><span>{{$vacant->ref_num}}</span></li>
                                <hr/>
                                <li><b>{{ trans('app.job_description') }}
                                        : </b><span>{{$vacant->job_discription}}</span></li>
                            </ul>
                        </div>
                    </div>
                    @if ( $vacant->education || $vacant->certification)
                        <div class="col-md-6 sm-margin-bottom-20">
                            <div class="profile-blog light-shadow margin-bottom-20">
                                <div class="name-location">
                                    <strong>{{ trans('app.qualifications') }}</strong>
                                </div>
                                <div class="clearfix margin-bottom-20">
                                </div>
                                @if ( $vacant->education )
                                    <ul class="list-inline">
                                        <li><b>{{ trans('app.education') }}
                                                : </b><span>{{ trans('app.'.$vacant->education) }}</span>
                                        </li>
                                    </ul>
                                @endif
                                @if ( $vacant->certification )
                                    <ul class="list-inline">
                                        <li><b>{{ trans('app.certification') }}
                                                : </b><span>{{$vacant->certification}}</span></li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
                @if ( $vacant->exp_years || $vacant->exp_level)
                    <div class="row margin-bottom-5">
                        <div class="col-md-6 sm-margin-bottom-20">
                            <div class="profile-blog light-shadow margin-bottom-20">
                                <div class="name-location">
                                    <strong>{{ trans('app.experience') }}</strong>
                                </div>
                                <div class="clearfix margin-bottom-20">
                                </div>
                                <ul class="list-inline">
                                    @if ( $vacant->exp_years )
                                        <li><b>{{ trans('app.exp_years') }} : </b><span>{{$vacant->exp_years}}</span>
                                        </li>
                                        <hr/>
                                    @endif
                                    @if ( $vacant->exp_level )
                                        <li><b>{{ trans('app.exp_level') }}
                                                : </b><span>{{trans('app.'.$vacant->exp_level)}}</span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @endif
                        @if ( $vacant->skills )
                            <div class="col-md-6 sm-margin-bottom-20">
                                <div class="profile-blog light-shadow margin-bottom-20">
                                    <div class="name-location">
                                        <strong>{{ trans('app.skills') }}</strong>
                                    </div>
                                    <div class="clearfix margin-bottom-20">
                                    </div>
                                    <ul class="list-inline">
                                        <li>{{$vacant->skills}} </li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div><!--here-->
                    <div class="row margin-bottom-5">
                        @if ( $vacant->language )
                            <div class="col-md-6 sm-margin-bottom-20">
                                <div class="profile-blog light-shadow margin-bottom-20">
                                    <div class="name-location">
                                        <strong>{{ trans('app.languages') }}</strong>
                                    </div>
                                    <div class="clearfix margin-bottom-20">
                                    </div>
                                    <ul class="list-inline">
                                        <?php $language = explode(',', $vacant->language)?>
                                        <li>
                                            @for ($i = 0; $i < count($language); $i++)
                                                {{ trans('app.'.$language[$i]) }}
                                            @endfor
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-6 sm-margin-bottom-20">
                            @if ( $vacant->gender ||  $vacant->candidate_age || $vacant->candidate_address)
                                <div class="profile-blog light-shadow margin-bottom-20">
                                    <div class="name-location">
                                        <strong>{{ trans('app.preferred_info') }}</strong>
                                    </div>
                                    <div class="clearfix margin-bottom-20">
                                    </div>
                                    <ul class="list-inline">
                                        @if ( $vacant->gender )
                                            <li><b>{{ trans('app.gender') }}
                                                    : </b><span>{{trans('app.'.$vacant->gender)}}</span>
                                            </li>
                                        @endif
                                        @if ( $vacant->candidate_age )
                                            <hr/>
                                            <li><b>{{ trans('app.candidate_age') }}
                                                    : </b><span>{{$vacant->candidate_age}}</span>
                                            </li>
                                        @endif
                                        @if ( $vacant->candidate_address )
                                            <hr/>
                                            <li><b>{{ trans('app.candidate_address') }}
                                                    : </b><span>{{$vacant->candidate_address}}</span></li>
                                        @endif
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!--start-->
                    @if ( $vacant->additional_vacant_info || $vacant->additional_prefered_info )
                        <div class="row margin-bottom-5">
                            @if ( $vacant->additional_vacant_info )
                                <div class="col-md-6 sm-margin-bottom-20">
                                    <div class="profile-blog light-shadow margin-bottom-20">
                                        <div class="name-location">
                                            <strong>{{trans('app.additional_vacant_info')}}</strong>
                                        </div>
                                        <div class="clearfix margin-bottom-20">
                                        </div>
                                        <ul class="list-inline">
                                            <li>{{$vacant->additional_vacant_info}}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @if ( $vacant->additional_prefered_info )
                                <div class="col-md-6 sm-margin-bottom-20">
                                    <div class="profile-blog light-shadow margin-bottom-20">
                                        <div class="name-location">
                                            <strong>{{trans('app.additional_preferred_info')}}</strong>
                                        </div>
                                        <div class="clearfix margin-bottom-20">
                                        </div>
                                        <ul class="list-inline">
                                            <li>{{$vacant->additional_prefered_info}}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @endif

                        </div><!--here-->
                        <!--end-->
            </div>
            <!--End Job-->
        </div>
    </div>
    <!-- End Profile Content -->
</div>
<!--/end row-->
</div>
@stop
