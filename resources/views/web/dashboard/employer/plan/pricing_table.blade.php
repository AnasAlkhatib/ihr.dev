@extends('layouts.employer_front_master')
@section('title'){{trans('app.sto')}} | {{trans('app.login')}}@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">{{trans('app.plan_list')}}</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
                <li class="active">{{trans('app.plan_list')}}</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
    <!--=== Content Part ===-->
    <div class="container content">
    {{--<div class="heading heading-v1 margin-bottom-0">--}}
    {{--<h2>Pricing Table</h2>--}}
    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the--}}
    {{--industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and--}}
    {{--scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap--}}
    {{--into electronic typesetting, remaining essentially unchanged</p>--}}
    {{--</div>--}}
    <!-- Pricing Mega v1 -->
        <div class="row no-space-pricing pricing-mega-v1">
            <div class="col-md-3 col-sm-6 hidden-sm hidden-xs">
                <div class="pricing hidden-area">
                    <div class="pricing-head">
                        <h4 class="price"></h4>
                    </div>
                    <!--start features loop-->
                    <ul class="pricing-content list-unstyled">
                        <?php $bgColor = 'bg-color'; ?>
                        @foreach($features as $feature)
                            @if($bgColor == 'bg-color')
                                <li class="{{$bgColor}}">
                                    {{trans('app.plansFeatures.'.$feature)}}
                                </li>
                                <?php $bgColor = ''; ?>
                            @elseif($bgColor == '')
                                <li>
                                    {{trans('app.plansFeatures.'.$feature)}}
                                </li>
                                <?php $bgColor = 'bg-color'; ?>
                            @endif
                        @endforeach
                    </ul>
                    <!--end features loop-->
                </div>
            </div>
            <!--start plans loop-->
            @foreach($plans as $plan)
                <div class="col-md-2 col-sm-6 block">
                    <div class="pricing">
                        <div class="pricing-head">
                            <h3 style="color:{{$plan->color}};background: {{$plan->background}};text-shadow: {{$plan->text_shadow}}">
                                {{trans('app.plans.'.$plan->id.'.name')}}
                                <span class="">{{trans_choice('app.interval_description.' . $plan->interval, $plan->interval_count)}}</span>
                                {{--<span>{{trans('app.plans.'.$plan->id.'.description')}}</span>--}}
                            </h3>
                            <h4 class="price">
                                @if($plan->price=='0.00')
                                    <i></i>{{trans('app.free')}}
                                @else
                                    <i>{{trans('app.SAR')}}</i>{{$plan->price}}
                                @endif
                                {{--<span class="">{{trans_choice('app.interval_description.' . $plan->interval, $plan->interval_count)}}</span>--}}
                            </h4>
                        </div><!--pricing-head-->
                        <ul class="pricing-content list-unstyled ">
                            <!--start plan features loop-->
                            <?php $bgColor = 'bg-color'; ?>
                            @foreach($plan->features as $planFeatures)
                                @if($bgColor == 'bg-color')
                                    <li class="{{$bgColor}}">
                                    <?php $bgColor = ''; ?>
                                @elseif($bgColor == '')
                                    <li>
                                        <?php $bgColor = 'bg-color'; ?>
                                        @endif
                                        @if($planFeatures->value=='Y')
                                            <i class="fa fa-check green"></i>
                                        @elseif($planFeatures->value=='N' || $planFeatures->value=='0')
                                            <i class="fa fa-times"></i>
                                        @elseif($planFeatures->value=='UNLIMITED')
                                            {{trans('app.UNLIMITED')}}
                                        @elseif($planFeatures->code=='ads_on_site' && $planFeatures->value==7)
                                            {{trans('app.week')}}
                                        @elseif($planFeatures->code=='ads_on_site' && $planFeatures->value==30)
                                            {{trans('app.month')}}
                                        @else
                                            {{$planFeatures->value}}
                                        @endif
                                        <span class="hidden-md hidden-lg"> {{trans('app.plansFeatures.'.$planFeatures->code)}}</span>
                                    </li>
                                    @endforeach
                        </ul>
                        <div class="btn-group btn-group-justified">
                            @if($plan->price=='0.00')
                                <button type="button" class="btn-u btn-block btn-u-default">
                                    {{trans('app.buy_now')}}
                                </button>
                            @else
                                <a href="{{ url('/employer/register?plan=') }}{{$plan->id}}"
                                   class="btn-u btn-block btn-u-default"
                                   style="text-shadow: 0 1px 0 #999">
                                    <i class="fa fa-shopping-cart"></i>
                                    {{trans('app.buy_now')}}
                                </a>
                            @endif
                        </div>
                    </div><!--pricing-->
                </div><!--block-->
        @endforeach
        <!--end plans loop-->
        </div>
        <!-- End Pricing Mega v1 -->
        <div class="margin-bottom-40"></div>
    </div><!--/container-->
    <!--=== End Content Part ===-->
@stop

