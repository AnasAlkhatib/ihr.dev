@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.view_seeker')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i> {{ trans('app.view_seeker') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.view_seeker')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                <div class="sky-form">
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <section class="col-md-12">
                                <header>{{trans('app.personal_info')}}</header>
                                {{ Form::hidden('seeker_url', url('/dashboard/candidate/selectCandidate/'.$seeker->ID), array('id' => 'seeker_url')) }}
                            </section>
                        </fieldset>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <tr>
                                        <th>{{ trans('app.name') }}</th>
                                        <th>{{ trans('app.email') }}</th>
                                        <th>{{ trans('app.mobile') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$seeker->full_name}}</td>
                                        <td>{{$seeker->email}}</td>
                                        <td>{{$seeker->mobile}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.birth_date') }}</th>
                                        <th>{{ trans('app.nationality') }}</th>
                                        <th>{{ trans('app.national_num') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$seeker->birth_date}}</td>
                                        <td>{{$seeker->nationality}}</td>
                                        <td>{{$seeker->national_num}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.area') }}</th>
                                        <th>{{ trans('app.city') }}</th>
                                        <th>{{ trans('app.address') }}</th>
                                    </tr>
                                    <tr>
                                        @if($seekerArea !=='' && $seekerCity !=='')
                                            <td>{{$seekerArea}}</td>
                                            <td>{{$seekerCity}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                        <td>{{$seeker->address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!--/row-->
                        @if (count($seeker->experience ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.experience') }}</header>
                                    <table class="table table-striped  table-bordered  table-responsive">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('app.job_title') }} </th>
                                            <th>{{ trans('app.company_name') }} </th>
                                            <th>{{ trans('app.company_industry') }} </th>
                                            <th>{{ trans('app.job_description') }} </th>
                                            <th>{{ trans('app.period') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($seeker->experience as $experience)
                                            <tr>
                                                <td>{{$experience->job_title}}</td>
                                                <td>{{$experience->company_name}}</td>
                                                <td>{{$experience->company_industry}}</td>
                                                <td>{{$experience->job_description}}</td>
                                                <td>
                                                    @if($experience->end_date =='0000-00-00' ||  $experience->end_date =='')
                                                        {{trans('app.from')}} {{$experience->start_date}} {{trans('app.until_now')}}
                                                    @else
                                                        {{trans('app.from')}} {{$experience->start_date}} {{trans('app.to')}} {{$experience->end_date}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="4">{{ trans('app.total_experience') }}</th>
                                            <td>{{$seekerExperience}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($seeker->education ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.qualifications') }}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{ trans('app.edu_level') }}</th>
                                            <th>{{ trans('app.edu_major') }}</th>
                                            <th>{{ trans('app.edu_name') }}</th>
                                            <th>{{ trans('app.edu_grade') }}</th>
                                            <th>{{ trans('app.edu_year') }}</th>
                                        </tr>
                                        @foreach ($seeker->education as $education)
                                            <tr>
                                                <td>{{$education->edu_level}}</td>
                                                <td>{{$education->edu_major}}</td>
                                                <td>{{$education->edu_name}}</td>
                                                <td>{{$education->edu_grade}}</td>
                                                <td>{{$education->edu_year}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($seeker->course ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.courses') }}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{trans('app.course_name')}}</th>
                                            <th>{{trans('app.center_name')}}</th>
                                            <th>{{trans('app.course_date')}}</th>
                                            <th>{{trans('app.course_hours')}}</th>
                                        </tr>
                                        @foreach ($seeker->course as $course)
                                            <tr>
                                                <td>{{$course->course_name}}</td>
                                                <td>{{$course->center_name}}</td>
                                                <td>{{$course->course_date}}</td>
                                                <td>{{$course->course_hours}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($seeker->language ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{trans('app.languages')}}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{trans('app.language')}}</th>
                                            <th>{{trans('app.level')}}</th>
                                        </tr>
                                        @foreach ($seeker->language as $language)
                                            <tr>
                                                <td>{{$language->language_name}}</td>
                                                <td>{{$language->language_level}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                    </div>
                </div><!--div sky-form-->
            </div>
        </div><!--row-->
    </div><!--profile-body-->
</div><!--col-md-12-->
@stop
