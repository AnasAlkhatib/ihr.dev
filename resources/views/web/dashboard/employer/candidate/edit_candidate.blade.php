@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.edit_candidate')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_candidate') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/employer/candidate') }}">{{trans('app.candidate_list')}} </a></li>
            <li class="active">{{trans('app.edit_candidate')}}</li>
        </ul>
    </div>
</div>
<style type="text/css">
    td > a {
        color: #333 !important;
        cursor: pointer;
    }

    td > a:hover {
        color: #d82e35 !important;
    }
</style>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                {!! Form::open(['method' => 'PUT', 'url' => url('employer/candidate/'.$candidatables['id'] ),'class' => 'sky-form']) !!}
                <input type="hidden" name='seeker_id' value="{{$candidatables['seeker_id']}}">
                <input type="hidden" name='vacant_id' value="{{$candidatables['vacant_id']}}">
                <div class="row">
                    <fieldset class="col-md-12 no-padding margin-top-20">
                        <section class="col-md-12">
                            <header>{{trans('app.personal_info')}}</header>
                        </section>
                    </fieldset>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <tr>
                                        <th>{{ trans('app.name') }}</th>
                                        <th>{{ trans('app.email') }}</th>
                                        <th>{{ trans('app.mobile') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->full_name}}</td>
                                        <td><a href="mailto:{{$candidate->email}}">{{$candidate->email}}</a></td>
                                        <td>{{$candidate->mobile}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.birth_date') }}</th>
                                        <th>{{ trans('app.nationality') }}</th>
                                        <th>{{ trans('app.national_num') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->birth_date}}</td>
                                        <td>{{$candidate->nationality}}</td>
                                        <td>{{$candidate->national_num}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.area') }}</th>
                                        <th>{{ trans('app.city') }}</th>
                                        <th>{{ trans('app.address') }}</th>
                                    </tr>
                                    <tr>
                                        @if($address['area'] !=='' && $address['city'] !=='')
                                            <td>{{$address['area']}}</td>
                                            <td>{{$address['city']}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                        <td>{{$candidate->address}}</td>
                                    </tr>
                                </table>
                            </div><!--/row-->
                        </div><!--/row-->
                        @if (count($candidate->experience ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.experience') }}</header>
                                    <table class="table table-striped  table-bordered  table-responsive">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('app.job_title') }} </th>
                                            <th>{{ trans('app.company_name') }} </th>
                                            <th>{{ trans('app.company_industry') }} </th>
                                            <th>{{ trans('app.job_description') }} </th>
                                            <th>{{ trans('app.period') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($candidate->experience as $experience)
                                            <tr>
                                                <td>{{$experience->job_title}}</td>
                                                <td>{{$experience->company_name}}</td>
                                                <td>{{$experience->company_industry}}</td>
                                                <td>{{$experience->job_description}}</td>
                                                <td>
                                                    @if($experience->end_date =='0000-00-00' ||  $experience->end_date =='')
                                                        {{trans('app.from')}} {{$experience->start_date}} {{trans('app.until_now')}}
                                                    @else
                                                        {{trans('app.from')}} {{$experience->start_date}} {{trans('app.to')}} {{$experience->end_date}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="4">{{ trans('app.total_experience') }}</th>
                                            <td>{{$seekerExperience}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($candidate->education ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.qualifications') }}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{ trans('app.edu_level') }}</th>
                                            <th>{{ trans('app.edu_major') }}</th>
                                            <th>{{ trans('app.edu_name') }}</th>
                                            <th>{{ trans('app.edu_grade') }}</th>
                                            <th>{{ trans('app.edu_year') }}</th>
                                        </tr>
                                        @foreach ($candidate->education as $education)
                                            <tr>
                                                <td>{{$education->edu_level}}</td>
                                                <td>{{$education->edu_major}}</td>
                                                <td>{{$education->edu_name}}</td>
                                                <td>{{$education->edu_grade}}</td>
                                                <td>{{$education->edu_year}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($candidate->course ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{ trans('app.courses') }}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{trans('app.course_name')}}</th>
                                            <th>{{trans('app.center_name')}}</th>
                                            <th>{{trans('app.course_date')}}</th>
                                            <th>{{trans('app.course_hours')}}</th>
                                        </tr>
                                        @foreach ($candidate->course as $course)
                                            <tr>
                                                <td>{{$course->course_name}}</td>
                                                <td>{{$course->center_name}}</td>
                                                <td>{{$course->course_date}}</td>
                                                <td>{{$course->course_hours}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        @if (count($candidate->language ) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <header>{{trans('app.languages')}}</header>
                                    <table class="table table-striped  table-bordered table-responsive">
                                        <tr>
                                            <th>{{trans('app.language')}}</th>
                                            <th>{{trans('app.level')}}</th>
                                        </tr>
                                        @foreach ($candidate->language as $language)
                                            <tr>
                                                <td>{{$language->language_name}}</td>
                                                <td>{{$language->language_level}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div><!--/row-->
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <header>{{trans('app.status')}}</header>
                            </div>
                            @if ($errors->has('status'))
                                <div class="row">
                                    <section class="col-md-2">
                                        <span class="text-danger"> {{ $errors->first('status') }}</span>
                                    </section>
                                </div>
                            @endif
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'accept_candidate', (old('status',$candidatables['status']) == 'accept_candidate'), ['class'=>'showNotes','id'=>'accept_candidate']) !!}
                                    <i></i>{{trans('app.accept_candidate')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'reject_candidate', (old('status',$candidatables['status']) == 'reject_candidate'), ['class'=>'showNotes','id'=>'reject_candidate']) !!}
                                    <i></i>{{trans('app.reject_candidate')}}
                                </label>
                            </section>
                            {{--<section class="col-md-2">--}}
                                {{--<label class="radio @if($errors->has('status')){{'state-error'}}@endif">--}}
                                    {{--{!! Form::radio('status', 'rehabilitation_candidate', (old('status',$candidatables['status']) == 'rehabilitation_candidate'), ['class'=>'showNotes','id'=>'rehabilitation_candidate']) !!}--}}
                                    {{--<i></i>{{trans('app.rehabilitation_candidate')}}--}}
                                {{--</label>--}}
                            {{--</section>--}}
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'telephone_interview', (old('status',$candidatables['status']) == 'telephone_interview'), ['class'=>'showNotes','id'=>'telephone_interview']) !!}
                                    <i></i>{{trans('app.telephone_interview')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'preselection', (old('status',$candidatables['status']) == 'preselection'), ['class'=>'showNotes','id'=>'preselection']) !!}
                                    <i></i>{{trans('app.preselection')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="notes" style="display: none;">
                            <section class="col-md-12">
                                <label class="textarea">
                                    <i class="icon-append fa fa-file-text-o"></i>
                                    <textarea rows="2" name="note" id="notes_text"
                                              placeholder="{{trans('app.notes')}}">{{old('note',$candidatables['note'])}}</textarea>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'offer_vacancies', (old('status',$candidatables['status']) == 'offer_vacancies'), ['class'=>'offer_vacancies_status','id'=>'offer_vacancies_status']) !!}
                                    <i></i>{{trans('app.offer_vacancies')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="offer_vacancies_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.vacancies')}}</label>
                                <label class="select">
                                    <select name="offer_vacancies[]" id="offer_vacancies" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'final_interview', (old('status',$candidatables['status']) == 'final_interview'), ['class'=>'final_interview','id'=>'final_interview']) !!}
                                    <i></i>{{trans('app.final_interview')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="required_documents_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.required_documents')}}</label>
                                <label class="select">
                                    <select name="required_documents[]" id="required_documents" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                    </div>
                    <footer class="col-md-12" style="padding: 15px 15px;">
                        <button type="submit" class="btn-u">{{trans('app.update')}}</button>
                    </footer>
                </div>
                </form><!--form-->
            </div><!--row-->
        </div><!--profile-body-->
    </div><!--col-md-12-->
</div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true,
                width: '100%'
            });
            /* offer_vacancies Drop Down */
            var $offeredVacanciesElement = $("#offer_vacancies");
            var $offeredVacanciesRequest = $.ajax({
                url: '/employer/candidate/getAllVacancies'
            });
            $offeredVacanciesRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $offeredVacanciesElement.append(option);
                }
                        @if(old('offer_vacancies',unserialize($candidatables['offer_vacancies'])))
                var offerVacanciesArray = [<?php echo '"' . implode('","',
                                        old('offer_vacancies', unserialize($candidatables['offer_vacancies']))) . '"' ?>];
                $offeredVacanciesElement.val(offerVacanciesArray).trigger('change');
                @else
                    $offeredVacanciesElement.val('').trigger('change');
                @endif
            });
            /* requiredDocuments Drop Down */
            var $requiredDocumentsElement = $("#required_documents");
            var $requiredDocumentsRequest = $.ajax({
                url: '/employer/candidate/requiredDocuments'
            });
            $requiredDocumentsRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $requiredDocumentsElement.append(option);
                }
                        @if(old('required_documents',unserialize($candidatables['required_documents'])))
                var documentsArray = [<?php echo '"' . implode('","',
                                        old('required_documents', unserialize($candidatables['required_documents']))) . '"' ?>];
                $requiredDocumentsElement.val(documentsArray).trigger('change');
                @else
                    $requiredDocumentsElement.val('').trigger('change');
                @endif
            });
            //show notes if  accept_candidate or reject_candidate
            $('.showNotes').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $requiredDocumentsElement.select2("val", "");
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").hide();
                $('#notes').show();

            });
            if ($('.showNotes:checked').length > 0) {
                $("#notes").show();
            }
// show offer_vacancies if offer_vacancies
            $('.offer_vacancies_status').click(function () {
                $requiredDocumentsElement.select2("val", "");
                $("#required_documents_div").hide();
                $("#offer_vacancies_div").show();
                $('#notes_text').val('')
                $('#notes').hide();
            });
            if ($('#offer_vacancies_status:checked').length > 0) {
                $("#offer_vacancies_div").show();
            }
            //show required_documents_div if final_interview
            $('.final_interview').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $('#notes_text').val('')
                $('#notes').hide();
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").show();
            });
            if ($('#final_interview:checked').length > 0) {
                $("#required_documents_div").show();
            }
        });
    </script>
@stop