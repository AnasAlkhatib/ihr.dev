@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.candidate_list')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-user-plus"></i> {{ trans('app.candidate_list') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.candidate_list')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert {{session('alert-class')}}">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            @endif
            <div id="ajaxMessage"></div>
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="user-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('app.name')}}</th>
                        <th>{{trans('app.vacancy')}}</th>
                        <th>{{trans('app.status')}}</th>
                        <th> {{trans('app.created_at')}}</th>
                        <th>{{trans('app.options')}}</th>
                    </tr>
                    </thead>
                </table>
                <!--End Table Striped-->
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
    <!-- delete Modal -->
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('app.delete_candidate')}}</h4>
                </div>
                <div class="modal-body">
                    {{trans('app.sure_delete_candidate')}}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger ok">{{trans('app.delete')}}</button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">{{trans('app.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Share Modal -->
    <div id="share" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('app.share_candidate')}}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method' => 'POST', 'url' => '#','class' => 'sky-form','id'=>'shareProfile']) !!}
                    <section>
                        <label></label>
                        <label class="input email_input">
                            <i class="icon-append fa fa-envelope"></i>
                            <input name="email" type="text" id="email"
                                   placeholder="* {{trans('app.email')}}"/>
                        </label>
                        <div class="errorMessage text-danger"></div>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger ok">{{trans('app.send')}}</button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">{{trans('app.cancel')}}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                "aLengthMenu": [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, '{{trans('app.all')}}']],
                "iDisplayLength": 5,
                order: [[0, 'desc']],
                "language": {
                    "url": "{{ ($locale) == 'en' ? asset('js/locals/english.json'): asset('js/locals/arabic.json') }}"
                },
                ajax: "{{ url('/employer/candidate/list') }}",
                columns: [
                    {data: "id", name: "id", visible: false, searchable: false},
                    {data: "name", name: "name", orderable: false},
                    {data: "vacant_id", name: "vacant_id", orderable: false},
                    {data: "status", name: "status", orderable: false},
                    {data: "created_at", name: "created_at", orderable: false, searchable: false},
                    {data: "action", name: "action", sWidth: "20%", orderable: false, searchable: false}
                ]
                ,
                "fnRowCallback": function (nRow) {
                    /* delete button */
                    $('.btn-delete', nRow).on('click', function (e) {
                        e.preventDefault();
                        var link = $(this).attr('href');
                        var id = link.substring(link.indexOf('#') + 1);
                        $("#delete").modal({show: true});
                        var AjaxRunning = false;
                        $('.ok').click(function () {
                            if (!AjaxRunning) {
                                AjaxRunning = true;
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('/employer/candidate/') }}" + "/" + id,
                                    data: {_method: "DELETE", _token: "{{  csrf_token() }}"},
                                    success: function () {
                                        $('#delete').modal('hide');
                                        location.reload();
                                    }
                                });
                            }
                        });
                        $('.cancel').click(function () {
                            location.reload();
                        });
                    });
                    $(".alert").fadeTo(1000, 500).slideUp(500, function () {
                        $(".alert").alert('close');
                    });
                },
                "fnPreDrawCallback": function () {
                    $(".dataTables_filter label").addClass('input').unwrap().wrap("<section class='col col-3 pull-right'>");
                    $(".dataTables_length label").append("<i></i>");
                    $(".dataTables_length label").addClass('select').unwrap().wrap("<section class='col col-2 no-padding pull-right'>");
                    $("div#warp_tools").unwrap().wrap("<form class='sky-form'>");
                    if ($(".search_box").val() != '') {
                        $(".search_box").focus();
                    }
                }
            });
        });
    </script>
@stop
