@extends('layouts.employer_master')
@section('content')
@section('title'){{trans('app.personal_profile')}}@stop
<style>
    .img-holder {
        width: 200px;
        height: 200px;
        position: relative;
    }

    .button-link {
        position: absolute;
        left: 0;
        top: 0;
        padding: 2px 0px 2px 2px;
    }

    #loading {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -25px;
        margin-left: -25px;
    }

</style>
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-user"></i> {{ trans('app.personal_profile') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.personal_profile')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        @if(session('message'))
            <div class="col-md-12">
                <div class="alert {{ session('alert-class') }}" id="message">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            </div><!--/alert message div -->
    @endif
    <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">

                <form class="profile-image sky-form"
                      action="{{ url('/employer/settings/profile/'.$employer->id.'/UpdatePicture') }}"
                      method="post"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="row">
                        <section class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-4">
                                    <label for="file" class="input input-file">
                                        <div class="img-holder">
                                            <div class="button button-div" style="background: rgba(54, 25, 25, .00004)">
                                                <span class="btn-u button-link"><i
                                                            class="fa fa-2x fa-pencil-square-o"></i></span>
                                                <input type="file" id="file" name="file">
                                            </div>
                                            <img class="img-thumbnail rounded-3x" id="employer-image"
                                                 src="{{$employerFileName}}" alt="">
                                            <div id="loading" style="display:none">
                                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                                <span class="sr-only">{{trans('app.please_wait')}} ...</span>
                                            </div>
                                        </div>
                                    </label>
                                    <span id="status"></span>
                                </section>
                            </div><!--/row-->
                        </section>
                    </div><!--/row-->
                </form>
                <form action="{{ url('/employer/settings/profile/'.$employer->id) }}" id="sky-form1"
                      class="sky-form"
                      method="POST">
                    {{ method_field('PUT') }}
                    {!! csrf_field() !!}
                    <div class="row">
                        <fieldset class="col-md-12 no-padding ">
                            <div class="row">
                                <section class="col-md-4">
                                    <label>{{trans('app.name')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}"
                                               value="{{ old('name',$employer->name) }}"/>
                                        @if ($errors->has('name'))
                                            <span class="text-danger"> {{ $errors->first('name') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.mobile')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="mobile" autocomplete="off"
                                               placeholder="* {{ trans('app.mobile') }}"
                                               value="{{ old('mobile',$employer->mobile) }}"/>
                                        @if ($errors->has('mobile'))
                                            <span class="text-danger"> {{ $errors->first('mobile') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.email')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email" value="{{ old('email',$employer->email) }}"
                                               placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                        @if ($errors->has('email'))
                                            <span class="text-danger"> {{ $errors->first('email') }}</span>
                                        @endif
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label>{{trans('app.company_name')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-building-o"></i>
                                        <input type="text" name="company_name" autocomplete="off"
                                               placeholder="* {{trans('app.company_name') }}"
                                               value="{{ old('company_name',$employer->company_name) }}"/>
                                        @if ($errors->has('company_name'))
                                            <span class="text-danger"> {{ $errors->first('company_name') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.company_type')}} :</label>
                                    <label for="company_type" class="select">
                                        <select name="company_type">
                                            <option selected disabled hidden>
                                                * {{trans('app.company_type')}}</option>
                                            @if (old('company_type',$employer->company_type) !='' )
                                                <option value="{{ old('company_type',$employer->company_type) }}"
                                                        selected>{{ trans('app.'.old('company_type',$employer->company_type)) }}</option>
                                            @endif
                                            @if (count($companyTypes) > 0)
                                                @foreach ($companyTypes as $companyType)
                                                    @if(old('company_type',$employer->company_type) != $companyType['value'] )
                                                        <option value="{{ $companyType['value'] }}">{{ $companyType['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i></i>
                                        @if ($errors->has('company_type'))
                                            <span class="text-danger"> {{ $errors->first('company_type') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.job_title')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" name="job_title"
                                               value="{{ old('job_title',$employer->job_title) }}"
                                               placeholder="* {{trans('app.job_title')}}" autocomplete="off"/>
                                        @if ($errors->has('job_title'))
                                            <span class="text-danger"> {{ $errors->first('job_title') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.website')}} :</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-globe"></i>
                                        <input type="text" name="website"
                                               value="{{ old('website',$employer->website) }}"
                                               placeholder="* {{trans('app.website')}}" autocomplete="off"/>
                                        @if ($errors->has('website'))
                                            <span class="text-danger"> {{ $errors->first('website') }}</span>
                                        @endif
                                    </label>
                                </section>
                                <section class="col-md-4">
                                    <label>{{trans('app.plan')}} :</label>
                                    <label for="plan" class="select">
                                        <select name="plan">
                                            <option selected disabled hidden>* {{trans('app.plan')}}</option>
                                            @if (count($plans) > 0)
                                                @foreach ($plans as $plan)
                                                    @if (old('plan',$employerPlan['plan_id']) == $plan->id)
                                                        <option value="{{ $plan->id }}"
                                                                selected>{{$plan->name}}</option>
                                                    @else
                                                        <option value="{{$plan->id }}">{{ $plan->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i></i>
                                    </label>
                                    @if ($errors->has('plan'))
                                        <span class="text-danger"> {{ $errors->first('plan') }}</span>
                                    @endif
                                </section>
                            </div>
                            <section class="col-md-12">
                                <button id="submit" type="submit"
                                        class="btn-u">{{ trans('app.update')}}</button>
                            </section>
                        </fieldset>
                    </div><!--row-->
                </form>
            </div><!--/profile-body" -->
        </div> <!--/col-md-12 div -->
    </div> <!--/row div -->
</div> <!--/container div content profile -->
@stop
@section('custom-script')
    <script src="{{ asset('assets/js/image-uploader.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#message").fadeTo(2000, 500).slideUp(500, function () {
                $("#message").alert('close');
            });
            <!--loading icon-->
            $('#loading')
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxStop(function () {
                        $(this).hide();
                    });
            <!--Upload employer image-->
            var form = $(".profile-image");
            var status = $("#status");
            var imageSelector = $("#employer-image");
            $("#file").change(function () {
                form.submit();
            });
            profileImageUploader(form, status, imageSelector);
        });
    </script>
@stop