@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.edit_candidate')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_candidate') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/candidate') }}">{{trans('app.candidate_list')}} </a></li>
            <li class="active">{{trans('app.edit_candidate')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                {!! Form::open(['method' => 'PUT', 'url' => url('/dashboard/candidate/'.$candidate->id ),'class' => 'sky-form','id' => 'sky-form1' ]) !!}
                <div class="alert alert-warning fade in">
                    {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                </div>
                <div class="row">
                    <fieldset class="col-md-12 no-padding margin-top-20">
                        <section class="col-md-12">
                            <header>{{trans('app.personal_info')}}
                                <div class="pull-right">
                                    <small>{{trans('app.reference_number')}} :</small>
                                    <small>
                                        <mark>{{$candidate->ref_num}}</mark>
                                    </small>
                                </div>
                            </header>
                        </section>
                    </fieldset>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-4">
                                <section>
                                    <label class="control-label">* {{ trans('app.name') }}</label>
                                    <label class="input @if($errors->has('name')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}"
                                               value="{{ old('name',$candidate->name) }}"/>
                                    </label>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </section><!--/name section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>* {{trans('app.email')}}</label>
                                    <label class="input @if($errors->has('email')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email"
                                               value="{{ old('email',$candidate->email) }}"
                                               placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                    </label>
                                    @if ($errors->has('email'))
                                        <span class="text-danger"> {{ $errors->first('email') }}</span>
                                    @endif
                                </section><!--/email section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>* {{trans('app.mobile')}}</label>
                                    <label class="input @if($errors->has('mobile')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="mobile"
                                               value="{{ old('mobile',$candidate->mobile) }}"
                                               placeholder="* {{trans('app.mobile')}}" autocomplete="off"/>
                                    </label>
                                    @if ($errors->has('mobile'))
                                        <span class="text-danger"> {{ $errors->first('mobile') }}</span>
                                    @endif
                                </section><!--/mobile section -->
                            </div>
                        </div><!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <section>
                                    <label>* {{ trans('app.birth_date') }}</label>
                                    <label class="input @if($errors->has('birth_date')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input type="text" readonly name="birth_date" id="birth_date"
                                               value="{{ old('birth_date',$candidate->birth_date) }}"
                                               placeholder="* {{ trans('app.birth_date') }}"/>
                                    </label>
                                    @if ($errors->has('birth_date'))
                                        <span class="text-danger"> {{ $errors->first('birth_date') }}</span>
                                    @endif
                                </section><!--/birth_date section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>* {{ trans('app.nationality') }}</label>
                                    <label class="input @if($errors->has('nationality')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-tag"></i>
                                        <input type="text" name="nationality"
                                               value="{{ old('nationality',$candidate->nationality) }}"
                                               placeholder="* {{ trans('app.nationality') }}"/>
                                    </label>
                                    @if ($errors->has('nationality'))
                                        <span class="text-danger"> {{ $errors->first('nationality') }}</span>
                                    @endif
                                </section><!--/nationality section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>* {{ trans('app.national_num') }}</label>
                                    <label class="input @if($errors->has('national_num')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-asterisk"></i>
                                        <input type="text" name="national_num"
                                               value="{{ old('national_num',$candidate->national_num)}}"
                                               placeholder="* {{ trans('app.national_num') }}"/>
                                    </label>
                                    @if ($errors->has('national_num'))
                                        <span class="text-danger"> {{ $errors->first('national_num') }}</span>
                                    @endif
                                </section><!--/national_num section -->
                            </div>
                        </div><!--/row-->
                        <div class="row">
                            <div class="col-md-4">
                                <section>
                                    <label>{{ trans('app.area') }}</label>
                                    <label class="select @if($errors->has('area')){{'state-error'}}@endif">
                                        <select name="area" id="area"
                                                data-placeholder="{{trans('app.area')}}">
                                            <option></option>
                                        </select>
                                    </label>
                                    @if ($errors->has('area'))
                                        <span class="text-danger"> {{ $errors->first('area') }}</span>
                                    @endif
                                </section><!--/area section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>* {{ trans('app.city') }}</label>
                                    <label class="select @if($errors->has('city')){{'state-error'}}@endif">
                                        <select name="city_id" id="city"
                                                data-placeholder="{{trans('app.city')}}">
                                            <option></option>
                                        </select>
                                    </label>
                                    @if ($errors->has('city_id'))
                                        <span class="text-danger"> {{ $errors->first('city_id') }}</span>
                                    @endif
                                </section><!--/city section -->
                            </div>
                            <div class="col-md-4">
                                <section>
                                    <label>{{ trans('app.address') }}</label>
                                    <label class="input @if($errors->has('address')){{'state-error'}}@endif">
                                        <i class="icon-append fa fa-home"></i>
                                        <input type="text" name="address"
                                               value="{{ old('address',$candidate->address) }}"
                                               placeholder="{{ trans('app.address') }}"/>
                                    </label>
                                    @if ($errors->has('address'))
                                        <span class="text-danger"> {{ $errors->first('address') }}</span>
                                    @endif
                                </section><!--/city address -->
                            </div>
                        </div><!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <header></header>
                            </div>
                            <section class="col-md-6">
                                <label>* {{ trans('app.client_name') }}</label>
                                <label class="select @if($errors->has('client_id')){{'state-error'}}@endif">
                                    <select name="client_id" id="client_id"
                                            data-placeholder="{{trans('app.client_name')}}">
                                        <option></option>
                                    </select>
                                </label>
                                @if ($errors->has('client_id'))
                                    <span class="text-danger"> {{ $errors->first('client_id') }}</span>
                                @endif
                            </section><!--/client section -->
                            <section class="col-md-6">
                                <label>* {{ trans('app.vacancy') }}</label>
                                <label class="select @if($errors->has('vacant_id')){{'state-error'}}@endif">
                                    <select name="vacant_id" id="vacant_id"
                                            data-placeholder="{{trans('app.vacancy')}}">
                                        <option></option>
                                    </select>
                                </label>
                                @if ($errors->has('vacant_id'))
                                    <span class="text-danger"> {{ $errors->first('vacant_id') }}</span>
                                @endif
                            </section><!--/client section -->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <header>{{trans('app.status')}}</header>
                            </div>
                            @if ($errors->has('status'))
                                <div class="row">
                                    <section class="col-md-2">
                                        <span class="text-danger"> {{ $errors->first('status') }}</span>
                                    </section>
                                </div>
                            @endif
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'accept_candidate', (old('status',$candidatables['status']) == 'accept_candidate'), ['class'=>'showNotes','id'=>'accept_candidate']) !!}
                                    <i></i>{{trans('app.accept_candidate')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'reject_candidate', (old('status',$candidatables['status']) == 'reject_candidate'), ['class'=>'showNotes','id'=>'reject_candidate']) !!}
                                    <i></i>{{trans('app.reject_candidate')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'rehabilitation_candidate', (old('status',$candidatables['status']) == 'rehabilitation_candidate'), ['class'=>'showNotes1','id'=>'rehabilitation_candidate']) !!}
                                    <i></i>{{trans('app.rehabilitation_candidate')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'telephone_interview', (old('status',$candidatables['status']) == 'telephone_interview'), ['class'=>'showNotes1','id'=>'telephone_interview']) !!}
                                    <i></i>{{trans('app.telephone_interview')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'preselection', (old('status',$candidatables['status']) == 'preselection'), ['class'=>'showNotes1','id'=>'preselection']) !!}
                                    <i></i>{{trans('app.preselection')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="notes" style="display: none;">
                            <section class="col-md-12">
                                <label class="textarea">
                                    <i class="icon-append fa fa-file-text-o"></i>
                                    <textarea rows="2" name="notes" id="notes_text"
                                              placeholder="{{trans('app.notes')}}">{{old('notes',$candidatables['notes'])}}</textarea>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'offer_vacancies', (old('status',$candidatables['status']) == 'offer_vacancies'), ['class'=>'offer_vacancies_status','id'=>'offer_vacancies_status']) !!}
                                    <i></i>{{trans('app.offer_vacancies')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="offer_vacancies_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.vacancies')}}</label>
                                <label class="select">
                                    <select name="offer_vacancies[]" id="offer_vacancies" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'final_interview', (old('status',$candidatables['status']) == 'final_interview'), ['class'=>'final_interview','id'=>'final_interview']) !!}
                                    <i></i>{{trans('app.final_interview')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="required_documents_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.required_documents')}}</label>
                                <label class="select">
                                    <select name="required_documents[]" id="required_documents" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                    </div>
                    <footer class="col-md-12" style="padding: 15px 15px;">
                        <button type="submit" class="btn-u">{{trans('app.update')}}</button>
                    </footer>
                </div>
                </form><!--form-->
            </div><!--row-->
        </div><!--profile-body-->
    </div><!--col-md-12-->
</div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true,
                width: '100%'
            });
            /* code to add */
            /* select2 for client */
            var clientElement = $('#client_id');
            var $clientRequest = $.ajax({
                url: '/dashboard/client/getAllClients'
            });
            $clientRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    clientElement.append(option);
                }
                clientElement.val('{{old('client_id',$candidatables['client_id'])}}').trigger('change');
            });
            /* end select2 for client Vacant */
            /* select2 for vacant_id */
            var $clientVacantElement = $("#vacant_id");
            $clientVacantElement.prop("disabled", true);
            /* relation client  vacant*/
            clientElement.on("change", function () {
                var id = $(this).val();
                if (id !== "") {
                    $clientVacantElement.find('option').remove().end();
                    var vacancies = $.ajax({
                        url: '/dashboard/client/getClientVacancies/' + id
                    });
                    vacancies.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $clientVacantElement.append(option);
                        }
                        $clientVacantElement.val('{{old('vacant_id',$candidatables['vacant_id'])}}').trigger('change');
                    });
                    $clientVacantElement.prop("disabled", false);
                }
            });

            /* end select2 for client Vacant *
             /* Area Drop Down */
            var $AreaElement = $("#area");
            var $areaRequest = $.ajax({
                url: '/dashboard/area'
            });
            $areaRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $AreaElement.append(option);
                }
                $AreaElement.val('{{old('area',$candidateArea)}}').trigger('change');
            });
            /* city drop down */
            var $cityElement = $("#city");
            var $cityElementOption = $("#city option[value]");
            var $cityRequest = $.ajax({
                url: '/dashboard/city'
            });
            $cityRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $cityElement.append(option);
                }
                $cityElement.val('{{old('city_id')}}').trigger('change');
            });
            /* relation area city */
            $AreaElement.on("change", function () {
                var id = $(this).val();
                $cityElement.prop("disabled", true);
                if (id !== "") {
                    $cityElementOption.remove();
                    var cities = $.ajax({
                        url: '/dashboard/area/' + id + '/city'
                    });
                    cities.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{old('city_id',$candidate->city_id)}}').trigger('change');
                    });
                } else {
                    $cityElementOption.remove();
                    var $cityRequest = $.ajax({
                        url: '/dashboard/city'
                    });
                    $cityRequest.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{ old('city_id',$candidate->city_id) }}').trigger('change');
                    });
                }
                $cityElement.prop("disabled", false);
            });

            /* offer_vacancies Drop Down */
            var $offeredVacanciesElement = $("#offer_vacancies");
            var $offeredVacanciesRequest = $.ajax({
                url: '/dashboard/candidate/getAllVacancies'
            });
            $offeredVacanciesRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $offeredVacanciesElement.append(option);
                }
                        @if(old('offer_vacancies',unserialize($candidatables['offer_vacancies'])))
                var offerVacanciesArray = [<?php echo '"' . implode('","',
                                        old('offer_vacancies', unserialize($candidatables['offer_vacancies']))) . '"' ?>];
                $offeredVacanciesElement.val(offerVacanciesArray).trigger('change');
                @else
                    $offeredVacanciesElement.val('').trigger('change');
                @endif
            });
            /* requiredDocuments Drop Down */
            var $requiredDocumentsElement = $("#required_documents");
            var $requiredDocumentsRequest = $.ajax({
                url: '/dashboard/candidate/requiredDocuments'
            });
            $requiredDocumentsRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $requiredDocumentsElement.append(option);
                }
                        @if(old('required_documents',unserialize($candidatables['required_documents'])))
                var documentsArray = [<?php echo '"' . implode('","',
                                        old('required_documents', unserialize($candidatables['required_documents']))) . '"' ?>];
                $requiredDocumentsElement.val(documentsArray).trigger('change');
                @else
                    $requiredDocumentsElement.val('').trigger('change');
                @endif
            });
            //show notes if  accept_candidate or reject_candidate
            $('.showNotes').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $requiredDocumentsElement.select2("val", "");
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").hide();
                $('#notes').show();

            });
            if ($('.showNotes:checked').length > 0) {
                $("#notes").show();
            }
// show offer_vacancies if offer_vacancies
            $('.offer_vacancies_status').click(function () {
                $requiredDocumentsElement.select2("val", "");
                $("#required_documents_div").hide();
                $("#offer_vacancies_div").show();
                $('#notes_text').val('')
                $('#notes').hide();
            });
            if ($('#offer_vacancies_status:checked').length > 0) {
                $("#offer_vacancies_div").show();
            }
            //show required_documents_div if final_interview
            $('.final_interview').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $('#notes_text').val('')
                $('#notes').hide();
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").show();
            });
            if ($('#final_interview:checked').length > 0) {
                $("#required_documents_div").show();
            }
            /* date picker position */
            $.extend($.datepicker, {
                _checkOffset: function (inst, offset, isFixed) {
                    return offset;
                }
            });
            /* birth_date picker*/
            $('#birth_date').datepicker({
                dateFormat: 'yy-mm-dd',
                showAnim: 'slideDown',
                changeYear: true,
                yearRange: "-112:+0",
            });
        });

    </script>
@stop