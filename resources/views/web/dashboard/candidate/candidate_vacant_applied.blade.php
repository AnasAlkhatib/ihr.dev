@if ($seekerVacantApplied->count() > 0)
    <table class="table table-striped  table-bordered table-responsive">
        <thead>
        <tr>
            <th>{{trans('app.job_title')}}</th>
            <th>{{trans('app.applied_at')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($seekerVacantApplied as $vacantApplied)
            <tr>
                <td width="49%">{{$vacantApplied->job_title}}</td>
                <td width="50%">{{$vacantApplied->pivot->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div>
        {!! $seekerVacantApplied->appends(Request::except('page'))->links() !!}
    </div>
@endif
{{--Form::checkbox('name', 'value');--}}
