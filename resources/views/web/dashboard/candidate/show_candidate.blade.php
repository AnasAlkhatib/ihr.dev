@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.view_candidate')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i> {{ trans('app.view_candidate') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/candidate') }}">{{trans('app.candidate_list')}} </a></li>
            <li class="active">{{trans('app.view_candidate')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                <div class="sky-form">
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <section class="col-md-12">
                                <header>{{trans('app.personal_info')}}
                                    <div class="pull-right">
                                        <small>{{trans('app.reference_number')}} :</small>
                                        <small>
                                            <mark>{{$candidate->ref_num}}</mark>
                                        </small>
                                    </div>
                                </header>
                                <input id="seeker_url" type="hidden"
                                       value="{{ url('/dashboard/candidate/selectCandidate/'.$candidate->seeker_id) }}">
                            </section>
                        </fieldset>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <tr>
                                        <th>{{ trans('app.name') }}</th>
                                        <th>{{ trans('app.email') }}</th>
                                        <th>{{ trans('app.mobile') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->name}}</td>
                                        <td>{{$candidate->email}}</td>
                                        <td>{{$candidate->mobile}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.birth_date') }}</th>
                                        <th>{{ trans('app.nationality') }}</th>
                                        <th>{{ trans('app.national_num') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->birth_date}}</td>
                                        <td>{{$candidate->nationality}}</td>
                                        <td>{{$candidate->national_num}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.area') }}</th>
                                        <th>{{ trans('app.city') }}</th>
                                        <th>{{ trans('app.address') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidateArea}}</td>
                                        <td>{{$candidateCity}}</td>
                                        <td>{{$candidate->address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!--/row-->
                        @if($seekerAccount)
                            @if (count($seeker->experience ) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <header>{{ trans('app.experience') }}</header>
                                        <table class="table table-striped  table-bordered  table-responsive">
                                            <thead>
                                            <tr>
                                                <th>{{ trans('app.job_title') }} </th>
                                                <th>{{ trans('app.company_name') }} </th>
                                                <th>{{ trans('app.job_description') }} </th>
                                                <th>{{ trans('app.period') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($seeker->experience as $experience)
                                                <tr>
                                                    <td>{{$experience->job_title}}</td>
                                                    <td>{{$experience->company_name}}</td>
                                                    <td>{{$experience->job_description}}</td>
                                                    <td>
                                                        @if($experience->end_date =='0000-00-00' ||  $experience->end_date =='')
                                                            {{trans('app.from')}} {{$experience->start_date}} {{trans('app.until_now')}}
                                                        @else
                                                            {{trans('app.from')}} {{$experience->start_date}} {{trans('app.to')}} {{$experience->end_date}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="3">{{ trans('app.total_experience') }}</th>
                                                <td>{{$seekerExperience}}</td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div><!--/row-->
                            @endif
                            @if (count($seeker->education ) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <header>{{ trans('app.qualifications') }}</header>
                                        <table class="table table-striped  table-bordered table-responsive">
                                            <tr>
                                                <th>{{ trans('app.edu_level') }}</th>
                                                <th>{{ trans('app.edu_major') }}</th>
                                                <th>{{ trans('app.edu_name') }}</th>
                                                <th>{{ trans('app.edu_grade') }}</th>
                                                <th>{{ trans('app.edu_year') }}</th>
                                            </tr>
                                            @foreach ($seeker->education as $education)
                                                <tr>
                                                    <td>{{$education->edu_level}}</td>
                                                    <td>{{$education->edu_major}}</td>
                                                    <td>{{$education->edu_name}}</td>
                                                    <td>{{$education->edu_grade}}</td>
                                                    <td>{{$education->edu_year}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div><!--/row-->
                            @endif
                            @if (count($seeker->course ) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <header>{{ trans('app.courses') }}</header>
                                        <table class="table table-striped  table-bordered table-responsive">
                                            <tr>
                                                <th>{{trans('app.course_name')}}</th>
                                                <th>{{trans('app.center_name')}}</th>
                                                <th>{{trans('app.course_date')}}</th>
                                                <th>{{trans('app.course_hours')}}</th>
                                            </tr>
                                            @foreach ($seeker->course as $course)
                                                <tr>
                                                    <td>{{$course->course_name}}</td>
                                                    <td>{{$course->center_name}}</td>
                                                    <td>{{$course->course_date}}</td>
                                                    <td>{{$course->course_hours}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div><!--/row-->
                            @endif
                            @if (count($seeker->language ) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <header>{{trans('app.languages')}}</header>
                                        <table class="table table-striped  table-bordered table-responsive">
                                            <tr>
                                                <th>{{trans('app.language')}}</th>
                                                <th>{{trans('app.level')}}</th>
                                            </tr>
                                            @foreach ($seeker->language as $language)
                                                <tr>
                                                    <td>{{$language->language_name}}</td>
                                                    <td>{{$language->language_level}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div><!--/row-->
                            @endif
                            @if ($seekerVacantApplied->count() > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <header>{{trans('app.applied_vacant') }}
                                            ( {{$seekerVacantApplied->total() }} )
                                        </header>
                                        <div class="vacantApplied">
                                            @include('web.dashboard.candidate.candidate_vacant_applied')
                                        </div>
                                        <div class="dataTables_processing">
                                            {{trans('app.please_wait')}} ...
                                        </div>
                                    </div>
                                </div><!--/row-->
                            @endif
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                {{--<header>{{trans('app.status') }}</header>--}}
                                {{--<section>--}}
                                {{--<h4><span>{{trans('app.'.$candidate->status)}}</span></h4>--}}
                                {{--</section>--}}
                                @if($candidate->notes)
                                    <label>{{trans('app.notes')}}</label>
                                    <section>
                                        <blockquote><p>{{$candidate->notes}}</p></blockquote>
                                    </section>
                                @endif
                                @if(!empty($candidateOfferVacancies))
                                    <label>{{trans('app.vacancies')}}</label>
                                    <ul>
                                        @foreach ($candidateOfferVacancies as $vacant)
                                            <li>{{ $vacant->job_title }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                @if(!empty($candidateRequiredDocuments))
                                    <label>{{trans('app.required_documents')}}</label>
                                    <ul>
                                        @foreach ($candidateRequiredDocuments as $documentName)
                                            <li>{{ trans('app.'.$documentName)}}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div><!--/row-->
                    </div>
                </div>
            </div><!--div sky-form-->
        </div><!--row-->
    </div><!--profile-body-->
</div><!--col-md-12-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            //processing
            $('.dataTables_processing')
                    .hide()
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxComplete(function () {
                        $(this).hide();
                    });
            // ajax pagination
            $(document).on('click', '.pagination a', function (e) {
                var seeker_url = $('#seeker_url').val();
                var page = seeker_url + '?' + this.href.split('?')[1];
                getRecords(page);
                e.preventDefault();
            });
            function getRecords(page) {
                $.ajax({
                    url: page,
                    dataType: 'json'
                }).done(function (data) {
                    $('.vacantApplied').html(data);
                }).fail(function () {
                    console.log('Vacant Applied could not be loaded.');
                });
            }

        });
    </script>
@stop
