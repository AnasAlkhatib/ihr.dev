<!doctype html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table, th, td {
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
        }

        p {
            font-size: 16px;
        }
    </style>
</head>
<body>
<div dir="rtl">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <p>{{ trans('system_message.candidate_profile_pdf.dear_customer')}}..</p>
                <p>{{ trans('system_message.candidate_profile_pdf.was_nominated')}} ( {{$candidate->name}}
                    ) {{ trans('system_message.candidate_profile_pdf.according_to_resume')}} </p>
                <p>{{ trans('system_message.candidate_profile_pdf.by')}} {{ trans('system_message.candidate_profile_pdf.job_recruiter')}}
                    {{$user->name}} </p>
                <p>{{ trans('system_message.candidate_profile_pdf.contact_him')}}</p>
                <p>{{ trans('app.email')}} : {{$user->email}}</p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>