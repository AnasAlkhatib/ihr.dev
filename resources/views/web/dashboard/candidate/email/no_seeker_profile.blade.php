<!doctype html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        body {
            background-color: #FFF;
        }

        .info_table {

        }

        .name {
            font-size: 18px;
        }

        .content {
            font-size: 13px;
        }

        .mobile, .email, .birth_date, .exp_year {
            font-size: 17px;
            font-style: normal;
        }

        .gender {
            font-size: 14px;
            font-style: normal;
        }

        .number {
            font-size: 17px;
            font-style: normal;
        }

        .content_title {
            font-size: 13px;
            font-weight: bold;
        }

        a {
            text-decoration: none;
            color: #000;
        }

        table td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
        }

        .border-none {
            border-bottom: 1px solid #FFF;
            border-top: 1px solid #FFF;
            border-left: 1px solid #FFF;
            border-right: 1px solid #FFF;
        }

        .border-bottom-none {
            border-bottom: 1px solid #FFF;
        }

        .border-right-none {
            border-right: 1px solid #FFF;
        }

        .info_table, .exp_table {
            background-color: #FFF;
            padding: 0px 2px 0px 2px;
        }

        h1 {
            font-size: 16px;
        }

        h2 {
            font-size: 14px;
            background-color: #EEE;
            border-bottom: 1px solid #999;
        }

        .job_title, .education, .language {
            font-size: 13px;
            font-weight: bold;
        }

        .company {
            font-size: 12px;
            font-style: italic;
        }

        .job_description {
            font-size: 13px;
        }
    </style>
</head>
<body>
<table class="info_table" cellspacing="0" cellpadding="0">
    <tr>
        <td class="border-none"><span align="left">{{trans('app.reference_number')}} : {{$candidate->ref_num}}</span>
        </td>
    </tr>
</table>
<h1 align="right" class="name">{{$candidate->name}}</h1>
<table class="table_content" align="right" cellspacing="0" cellpadding="1">
    <tr>
        <td class="border-bottom-none content_title"><span align="right">{{trans('app.address')}}</span></td>
        <td class="border-right-none border-bottom-none content_title"><span
                    align="right">{{trans('app.contact_info')}}</span></td>


    </tr>
    <tr>
        <!-- if has education-->
        <td class="border-bottom-none content"><span align="right">{{$address['area']}}
                , {{$address['city']}}{{($candidate->address !=='')? " ,".$candidate->address:''}}</span>
            {{--<span> {{\App\City::find($seeker->city_id)->name}} ,{{$seeker->address}}</span><br/>--}}
        </td>
        <td class="border-right-none border-bottom-none"><span class="mobile"> {{$candidate->mobile}}</span><br/>
            <a class="email" href="mailto:{{$candidate->email}}"> {{$candidate->email}}</a></td>
    </tr>
</table>
<h2 align="right">{{trans('app.personal_info')}}</h2>
<table class="info_table" align="right">
    <tr>
        <td width="80%"><span class="birth_date">{{$candidate->birth_date}}</span></td>
        <td width="20%"><span class="content_title">{{trans('app.birth_date')}} :</span></td>
    </tr>
    <tr>
        <td><span class="gender">{{$candidate->nationality}}</span></td>
        <td><span class="content_title">{{trans('app.nationality')}} :</span></td>
    </tr>
    <tr>
        <td><span class="mobile" valign="middle">{{$candidate->national_num}}</span></td>
        <td><span class="content_title">{{trans('app.national_num')}} :</span></td>
    </tr>
</table>
</body>
</html>