@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.edit_plan')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_plan') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/plan') }}">{{trans('app.plan_list')}} </a></li>
            <li class="active">{{trans('app.edit_plan')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                {{Form::open(['url' => 'dashboard/plan/'.$plan->id, 'method' => 'put','class'=>'sky-form'])}}
                <div class="alert alert-warning fade in">
                    {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                </div>
                <div class="row">
                    <fieldset class="col-md-12 no-padding margin-top-20">
                        <div class="row">
                            <section class="col-md-4">
                                <label>* {{ trans('app.name') }}</label>
                                <label class="input">
                                    <i class="icon-append fa fa-briefcase"></i>
                                    <input type="text" name="name" autocomplete="off"
                                           placeholder="* {{ trans('app.name') }}"
                                           value="{{ old('name',$plan->name) }}"/>
                                </label>
                                @if ($errors->has('name'))
                                    <span class="text-danger"> {{ $errors->first('name') }}</span>
                                @endif
                            </section>
                            <section class="col-md-4">
                                <label>* {{ trans('app.price') }}</label>
                                <label class="input">
                                    <i class="icon-append  fa fa-money"></i>
                                    <input type="number" name="price" autocomplete="off"
                                           step="0.01"
                                           placeholder="* {{ trans('app.price') }}"
                                           value="{{ old('price',$plan->price) }}"/>
                                </label>
                                @if ($errors->has('price'))
                                    <span class="text-danger"> {{ $errors->first('price') }}</span>
                                @endif
                            </section>
                            <section class="col-md-4">
                                <label>* {{ trans('app.interval') }}</label>
                                <label for="interval" class="select">
                                    <select name="interval" data-placeholder="* {{ trans('app.interval') }}">
                                        <option></option>
                                        @if (count($intervals) > 0)
                                            @foreach ($intervals as $interval)
                                                @if (old('interval',$plan->interval) == $interval['value'])
                                                    <option value="{{ $interval['value']  }}"
                                                            selected>{{ $interval['name']  }}</option>
                                                @else
                                                    <option value="{{ $interval['value'] }}">{{ $interval['name'] }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </label>
                                @if ($errors->has('interval'))
                                    <span class="text-danger"> {{ $errors->first('$interval') }}</span>
                                @endif
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-4">
                                <label>* {{ trans('app.interval_count') }}</label>
                                <label for="interval_count" class="select">
                                    <select name="interval_count"
                                            data-placeholder="* {{ trans('app.interval_count') }}">
                                        <option></option>
                                        @for ($i = 1; $i <= 30; $i++)
                                            @if (old('interval_count',$plan->interval_count) == $i)
                                                <option value="{{ $i  }}" selected>{{ $i }}</option>
                                            @else
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </label>
                                @if ($errors->has('interval_count'))
                                    <span class="text-danger"> {{ $errors->first('interval_count') }}</span>
                                @endif
                            </section>
                            <section class="col-md-4">
                                <label>* {{ trans('app.trial_period_days') }}</label>
                                <label for="trial_period_days" class="select">
                                    <select name="trial_period_days"
                                            data-placeholder="* {{ trans('app.trial_period_days') }}">
                                        <option></option>
                                        @for ($i = 0; $i <= 30; $i++)
                                            @if (old('trial_period_days',$plan->trial_period_days) == $i)
                                                <option value="{{ $i  }}" selected>{{ $i }}</option>
                                            @else
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </label>
                                @if ($errors->has('trial_period_days'))
                                    <span class="text-danger"> {{ $errors->first('trial_period_days') }}</span>
                                @endif
                            </section>
                            <section class="col-md-4">
                                <label>* {{ trans('app.sort_order') }}</label>
                                <label for="sort_order" class="select">
                                    <select name="sort_order"
                                            data-placeholder="* {{ trans('app.sort_order') }}">
                                        <option></option>
                                        @for ($i = 1; $i <= $plansCount; $i++)
                                            @if (old('sort_order',$plan->sort_order) == $i)
                                                <option value="{{ $i  }}" selected>{{ $i }}</option>
                                            @else
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </label>
                                @if ($errors->has('sort_order'))
                                    <span class="text-danger"> {{ $errors->first('sort_order') }}</span>
                                @endif
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-12">
                                <label>* {{ trans('app.description') }}</label>
                                <label for="job_description" class="textarea">
                                    <i class="icon-append fa fa-file-text-o"></i>
                                    <textarea rows="2" name='description' id='description'
                                              placeholder="* {{ trans('app.description') }}">{{ old('description',$plan->description) }}</textarea>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                </div>
                <footer></footer>
                <div class="row">
                    <section class="col-md-12">
                        <button type="submit" class="btn btn-danger" id="submit">
                            {{ trans('app.update')  }}
                        </button>
                    </section>
                </div>
                {!! Form::close() !!}
            </div><!--profile-body-->
        </div>
    </div><!--col-md-12-->
</div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        /* select2 options */
        $("select").select2({
            dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
            language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
            placeholder: function () {
                $(this).data('placeholder');
            },
            allowClear: true

        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop