@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.plan_list')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i>{{trans('app.plan_list')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('dashboard') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.plan_list')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert {{session('alert-class')}}">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            @endif
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="plan-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('app.name')}}</th>
                        <th>{{trans('app.price')}}</th>
                        <th>{{trans('app.interval')}}</th>
                        <th>{{trans('app.trial_period_days')}}</th>
                        <th>{{trans('app.sort_order')}}</th>
                        <th>{{trans('app.options')}}</th>
                    </tr>
                    </thead>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
</div> <!-- container content profile -->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#plan-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                "aLengthMenu": [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, '{{trans('app.all')}}']],
                "iDisplayLength": 5,
                order: [[0, 'desc']],
                "language": {
                    "url": "{{ ($locale) == 'en' ? asset('js/locals/english.json'): asset('js/locals/arabic.json') }}"
                },
                ajax: "{{ url('dashboard/plan/list') }}",
                columns: [
                    {data: "id", name: "id", visible: false},
                    {data: "name", name: "name", orderable: false},
                    {data: "price", name: "price", orderable: false},
                    {data: "interval", name: "interval", orderable: false},
                    {data: "trial_period_days", name: "trial_period_days", orderable: false},
                    {data: "sort_order", name: "sort_order", orderable: false},
                    {data: "action", name: "action", orderable: false}
                ],
                "fnPreDrawCallback": function () {
                    $("div.dataTables_filter ,div.dataTables_length").hide();
                    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
                        $(".alert").alert('close');
                    });
                }
            });

        });
    </script>
@stop
