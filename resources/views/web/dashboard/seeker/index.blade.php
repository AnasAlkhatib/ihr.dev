@extends('layouts.admin_master')
@section('title')STO | {{trans('app.dashboard')}}@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs light-shadow">
        <div class="container">
            <h1 class="pull-left">
                <i class="fa fa-file-text-o"></i>{{trans('app.seekers_list')}}
            </h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{ url('dashboard') }}">{{ trans('app.home') }}</a></li>
                <li class="active">{{trans('app.seekers_list')}}</li>
            </ul>
        </div>
    </div>
    <!--/breadcrumbs-->
    <!--=== Profile ===-->
    <div class="container content profile">
        <div class="row">
            <!-- Profile Content -->
            <div class="col-md-12">
                @if(session('message'))
                    <div class="alert {{session('alert-class')}}">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        {{ session('message') }}
                    </div>
                @endif
                <div class="profile-body margin-bottom-20">
                    <form class="sky-form" id="search" method="get"
                          action="">
                        <fieldset class="margin-bottom-20">
                            <div class="headline">
                                <h4>{{trans('app.search')}} :</h4>
                            </div>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name='gender' id='gender' data-placeholder='{{trans('app.gender')}}'>
                                        <option></option>
                                        @foreach ($dropDownData['genders'] as $gender)
                                            @if( Input::get('gender') == $gender['value'] )
                                                <option value="{{ $gender['value']}}"
                                                        selected>{{ $gender['name'] }}</option>
                                            @else
                                                <option value="{{ $gender['value']}}">{{ $gender['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="nationality" id="nationality"
                                            data-placeholder='{{trans('app.nationality')}}'>
                                        <option></option>
                                        @foreach ($dropDownData['nationalities'] as $nationality)
                                            @if( Input::get('nationality') == $nationality['value'] )
                                                <option value="{{ $nationality['value']}}"
                                                        selected>{{ $nationality['name'] }}</option>
                                            @else
                                                <option value="{{ $nationality['value']}}">{{ $nationality['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="area" id="area" data-placeholder="{{trans('app.area')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="city" id="city" data-placeholder="{{trans('app.city')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="age" id="age" data-placeholder="{{trans('app.age')}}">
                                        <option></option>
                                        @foreach ($dropDownData['agePeriods'] as $period)
                                            @if( Input::get('age') == $period['value'] )
                                                <option value="{{ $period['value']}}"
                                                        selected>{{ $period['name'] }}</option>
                                            @else
                                                <option value="{{$period['value']}}">{{ $period['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="edu_level" id="education"
                                            data-placeholder="{{trans('app.edu_level')}}">
                                        <option></option>
                                        @foreach ($dropDownData['educationLevel']  as $edu)
                                            @if( Input::get('edu_level') == $edu['value'] )
                                                <option value="{{ $edu['value']}}"
                                                        selected>{{ $edu['name'] }}</option>
                                            @else
                                                <option value="{{$edu['value']}}">{{ $edu['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="exp_years" id="exp_years"
                                            data-placeholder="{{trans('app.exp_years')}}">
                                        <option></option>
                                        @foreach ($dropDownData['expYears']  as $exp)
                                            @if( Input::get('exp_years') == $exp['value'] )
                                                <option value="{{ $exp['value']}}"
                                                        selected>{{ $exp['name'] }}</option>
                                            @else
                                                <option value="{{ $exp['value']}}">{{ $exp['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3">
                                <label class="select">
                                    <select name="company_industry" id="company_industry"
                                            data-placeholder="{{trans('app.area_expertise')}}">
                                        <option></option>
                                        @foreach ($dropDownData['companyIndustry']  as $industry)
                                            @if( Input::get('company_industry') == $industry['value'] )
                                                <option value="{{ $industry['value']}}"
                                                        selected>{{ $industry['name'] }}</option>
                                            @else
                                                <option value="{{ $industry['value']}}">{{ $industry['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-4 col-sm-3 pull-right">
                                <div class="btn-group btn-group-justified pull-right" data-toggle="buttons">
                                    @if(Input::get('employment_status') == '')
                                        <label class="btn btn-default active">
                                            <input type="radio" checked name="employment_status"
                                                   value="">{{trans('app.all')}}
                                        </label>
                                    @else
                                        <label class="btn btn-default">
                                            <input type="radio" name="employment_status"
                                                   value="">{{trans('app.all')}}
                                        </label>
                                    @endif
                                    @if(Input::get('employment_status') == 'not_working')
                                        <label class="btn btn-default active">
                                            <input type="radio" checked name="employment_status"
                                                   value="not_working">{{trans('app.not_employed')}}
                                        </label>
                                    @else
                                        <label class="btn btn-default">
                                            <input type="radio" name="employment_status"
                                                   value="not_working">{{trans('app.not_employed')}}
                                        </label>
                                    @endif
                                    @if(Input::get('employment_status') == 'working')
                                        <label class="btn btn-default active">
                                            <input type="radio" checked name="employment_status"
                                                   value="working">{{trans('app.employed')}}
                                        </label>
                                    @else
                                        <label class="btn btn-default">
                                            <input type="radio" name="employment_status"
                                                   value="working">{{trans('app.employed')}}
                                        </label>
                                    @endif
                                </div>
                            </section>
                            <section class="col-md-12 col-sm-12">
                                <button id="submit_search"
                                        class="btn btn-danger btn-sm col-lg-1 col-md-1 col-sm-3 col-xs-12  pull-right margin-left-10">
                                    <i class="fa fa-search margin-left-10"></i> {{trans('app.search_btn')}}
                                </button>
                            </section>
                        </fieldset>
                    </form>
                    <!--Profile Blog-->
                    <div class="dataTables_processing">
                        {{trans('app.please_wait')}} ...
                    </div>
                    @if($seekers->count() > 0 )
                        <div id="seekers-ajax">
                            <div class="row margin-bottom-20">
                                @foreach($seekers as $seeker)
                                    <div class="col-md-4 sm-margin-bottom-20">
                                        <div class="profile-blog light-shadow margin-bottom-20">
                                            <div class="btn-group pull-right">
                                                <button type="button"
                                                        class="btn btn-default btn-u-split-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa fa-cog"></i>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="{{ url('dashboard/candidate/selectCandidate/'.$seeker->ID) }}"><i
                                                                    class="fa fa-check-square-o"></i> {{trans('app.nominate')}}
                                                        </a>
                                                    </li>
                                                    <li><a href="{{ url('dashboard/seeker/'.$seeker->ID) }}"><i
                                                                    class="fa fa-user"></i> {{trans('app.personal_profile')}}
                                                        </a></li>
                                                    @role('SuperAdmin')
                                                    @if($seeker->active == 1)
                                                        <li>
                                                            <a href="{{ url('dashboard/seeker/'.$seeker->ID.'/deactivateAccount') }}"><i
                                                                        class="fa fa-times"></i> {{trans('app.deactivate_account')}}
                                                            </a></li>
                                                    @else
                                                        <li>
                                                            <a href="{{ url('dashboard/seeker/'.$seeker->ID.'/activateAccount') }}"><i
                                                                        class="fa fa-check"></i> {{trans('app.activate_account')}}
                                                            </a></li>
                                                    @endif
                                                    @endrole
                                                </ul>
                                            </div>
                                            <img class="rounded-x" src="{{ $seeker->pic_url }}" alt="">
                                            <div class="name-location">
                                                <strong>{{ $seeker->full_name }}</strong>
                                                <span>
                                                    {{trans('app.seeker_education')}}
                                                    : {{ $seeker->edu }}
                                                </span>
                                            </div>
                                            <div class="clearfix margin-bottom-20"></div>
                                            <hr/>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-clock-o"></i>{{trans('app.experience')}}
                                                    :{{$seeker->exp}}</li>

                                            </ul>
                                            <hr/>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-birthday-cake"></i>{{trans('app.age')}}
                                                    : {{ $seeker->age }}
                                                </li>
                                                <li><i class="fa fa-venus-mars"></i>{{trans('app.gender')}}
                                                    : {{ $seeker->gender }}</li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-calendar"></i>{{trans('app.birth_date')}}
                                                    : {{ $seeker->birth_date }}
                                                </li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-map-marker"></i>{{trans('app.area')}}
                                                    : {{ $seeker->area}}
                                                </li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-home"></i>{{trans('app.city')}}
                                                    : {{ $seeker->city}}
                                                </li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-globe"></i>{{trans('app.nationality')}}
                                                    : {{ $seeker->nationality }}</li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-file-text-o"></i>{{trans('app.national_num')}}
                                                    : {{ $seeker->national_num }}
                                                </li>
                                            </ul>
                                            <hr/>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-phone"></i>{{trans('app.mobile')}}
                                                    : {{ $seeker->mobile }}</li>
                                            </ul>
                                            <ul class="list-inline share-list">
                                                <li><i class="fa fa-envelope"></i>{{trans('app.mail')}}
                                                    <a href="mailto:{{$seeker->email}}">: {{$seeker->email}}</a>
                                                </li>
                                            </ul>
                                            <hr/>
                                            <ul class="list-inline share-list">
                                                <li>
                                                    <i class="fa fa-suitcase"></i>{{trans('app.employment_status')}}
                                                    @if($seeker->is_employed)
                                                        :  <b><span class="text-danger">{{trans('app.employed')}}</span></b>
                                                    @else
                                                        :
                                                        <b><span class="text-success">{{trans('app.not_employed')}}</span></b>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div>
                                <!--End Profile Blog-->
                                <!--Pagination Centered-->
                                <div class="text-center">
                                    {!! $seekers->appends(Request::except('page'))->links() !!}
                                </div>
                                @else
                                    <div class="text-center alert alert-warning fade in">
                                        <h4>{{trans('app.no_result_to_display')}}</h4>
                                    </div>
                            @endif
                            <!--End Pagination Centered-->
                            </div>
                        </div>
                        <!-- End Profile Content -->
                </div>
                <!--/end row-->
            </div>
            <!--=== End Profile ===-->
        </div>
    </div>
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables_processing')
                    .hide()
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxStop(function () {
                        $(this).hide();
                    });
        });
    </script>
    <script>
        $(function () {
            $('#seekers-ajax').on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href'));
                e.preventDefault();
            });
        });
        function getPosts(page) {
            $.ajax({
                url: page,
                dataType: 'json',
                cache: false
            }).done(function (data) {
                $('#seekers-ajax').html(data);
            }).fail(function () {
                console.log('Seekers could not be loaded');
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /* flash message effect */
            $(".alert").fadeTo(700, 500).slideUp(500, function () {
                $(".alert").alert('close');
            });
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true
            });
            /* Area Drop Down */
            var $AreaElement = $("#area");
            var $areaRequest = $.ajax({
                url: '/dashboard/area'
            });
            $areaRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $AreaElement.append(option);
                }
                $AreaElement.val('{{Input::get('area')}}').trigger('change');
            });
            /* city drop down */
            var $cityElement = $("#city");
            var $cityElementOption = $("#city option[value]");
            var $cityRequest = $.ajax({
                url: '/dashboard/city'
            });
            $cityRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $cityElement.append(option);
                }
                $cityElement.val('{{Input::get('city')}}').trigger('change');
            });

            /* relation area city */
            $AreaElement.on("change", function () {
                var id = $(this).val();
                $cityElement.prop("disabled", true);
                if (id !== "") {
                    $cityElementOption.remove();
                    var cities = $.ajax({
                        url: '/dashboard/area/' + id + '/city'
                    });
                    cities.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                } else {
                    $cityElementOption.remove();
                    var $cityRequest = $.ajax({
                        url: '/dashboard/city'
                    });
                    $cityRequest.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                }
                $cityElement.prop("disabled", false);
            });
        });
    </script>
@stop
