@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.favorite_seekers_list')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-star"></i> {{ trans('app.favorite_seekers_list') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/employer') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.favorite_seekers_list')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile" id="app">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                <!--Profile Blog-->
                {{--<div class="dataTables_processing">--}}
                {{--{{trans('app.please_wait')}} ...--}}
                {{--</div>--}}
                @if($seekers->count() > 0 )
                    <div id="seekers-ajax">
                        <div class="row margin-bottom-20">
                            @foreach($seekers as $seeker)
                                <div class="col-md-4 sm-margin-bottom-20">
                                    <div class="profile-blog light-shadow margin-bottom-20">
                                        <div class="btn-group pull-right">
                                            <div class="favorite">
                                                <favorite :seeker={{ $seeker->ID }}
                                                        :favorited={{ $seeker->isFavorited(Auth::guard('admin')->user()) ? 'true' : 'false' }}
                                                >
                                                </favorite>
                                            </div>
                                        </div>
                                        <img class="rounded-x" src="{{ $seeker->pic_url }}" alt="">
                                        <div class="name-location">
                                            <a href="{{ url('dashboard/seeker/'.$seeker->ID) }}">
                                                <strong>{{ $seeker->full_name }}</strong>
                                            </a>
                                            <span>{{trans('app.seeker_education')}}
                                                :{{ $seeker->edu }}</span>
                                        </div>
                                        <div class="clearfix margin-bottom-20">
                                        </div>
                                        <hr/>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-clock-o"></i>{{trans('app.experience')}}
                                                :{{$seeker->exp}}</li>
                                        </ul>
                                        <hr/>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-birthday-cake"></i>{{trans('app.age')}}
                                                : {{ $seeker->age }}
                                            </li>
                                            <li><i class="fa fa-venus-mars"></i>{{trans('app.gender')}}
                                                : {{ $seeker->gender }}</li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-calendar"></i>{{trans('app.birth_date')}}
                                                : {{ $seeker->birth_date }}
                                            </li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-map-marker"></i>{{trans('app.area')}}
                                                : {{ $seeker->area}}
                                            </li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-home"></i>{{trans('app.city')}}
                                                : {{ $seeker->city}}
                                            </li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-globe"></i>{{trans('app.nationality')}}
                                                : {{ $seeker->nationality }}</li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-file-text-o"></i>{{trans('app.national_num')}}
                                                : {{ $seeker->national_num }}
                                            </li>
                                        </ul>
                                        <hr/>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-phone"></i>{{trans('app.mobile')}}
                                                : {{ $seeker->mobile }}</li>
                                        </ul>
                                        <ul class="list-inline share-list">
                                            <li><i class="fa fa-envelope"></i>{{trans('app.mail')}}
                                                <a href="mailto:{{$seeker->email}}">: {{$seeker->email}}</a>
                                            </li>
                                        </ul>
                                        <hr/>
                                        <ul class="list-inline share-list">
                                            <li>
                                                <i class="fa fa-suitcase"></i>{{trans('app.employment_status')}}
                                                @if($seeker->is_employed)
                                                    :  <b><span class="text-danger">{{trans('app.employed')}}</span></b>
                                                @else
                                                    :
                                                    <b><span class="text-success">{{trans('app.not_employed')}}</span></b>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div>
                            <!--End Profile Blog-->
                            <!--Pagination Centered-->
                            <div class="text-center">
                                {!! $seekers->appends(Request::except('page'))->links() !!}
                            </div>
                            @else
                                <div class="text-center alert alert-warning fade in">
                                    <h4>{{trans('app.no_result_to_display')}}</h4>
                                </div>
                        @endif
                        <!--End Pagination Centered-->
                        </div>
                    </div>
                    <!-- End Profile Content -->
            </div>
        </div><!--row-->
    </div><!--profile-body-->
</div><!--col-md-12-->
@stop
@section('custom-script')
    <script src="{{ asset('js/app-admin.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.favorite a').click(function () {
                $.ajax({
                    url: window.location.href,
                    headers: {
                        "Pragma": "no-cache",
                        "Expires": -1,
                        "Cache-Control": "no-cache"
                    }
                }).done(function () {
                    window.location.reload(true);
                });
            });
        });
    </script>
@stop
