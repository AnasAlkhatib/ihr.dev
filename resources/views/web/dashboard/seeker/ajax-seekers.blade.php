<div id="seekers-ajax">
    <div class="row margin-bottom-20">
        @foreach($seekers as $seeker)
            <div class="col-md-4 sm-margin-bottom-20">
                <div class="profile-blog light-shadow margin-bottom-20">
                    <div class="btn-group pull-right">
                        <button type="button"
                                class="btn btn-default btn-u-split-default dropdown-toggle"
                                data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('dashboard/candidate/selectCandidate/'.$seeker->ID) }}"><i
                                            class="fa fa-check-square-o"></i> {{trans('app.nominate')}}</a>
                            </li>
                            <li><a href="{{ url('dashboard/seeker/'.$seeker->ID) }}"><i
                                            class="fa fa-user"></i> {{trans('app.personal_profile')}}
                                </a></li>
                            @role('SuperAdmin')
                            @if($seeker->active == 1)
                                <li>
                                    <a href="{{ url('dashboard/seeker/'.$seeker->ID.'/deactivateAccount') }}"><i
                                                class="fa fa-times"></i> {{trans('app.deactivate_account')}}
                                    </a></li>
                            @else
                                <li>
                                    <a href="{{ url('dashboard/seeker/'.$seeker->ID.'/activateAccount') }}"><i
                                                class="fa fa-check"></i> {{trans('app.activate_account')}}
                                    </a></li>
                            @endif
                            @endrole
                        </ul>
                    </div>
                    <img class="rounded-x" src="{{ $seeker->pic_url }}" alt="">
                    <div class="name-location">
                        <strong>{{ $seeker->full_name }}</strong>
                        <span>{{trans('app.seeker_education')}}
                            :{{ $seeker->edu }}</span>
                    </div>
                    <div class="clearfix margin-bottom-20">
                    </div>
                    <hr/>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-clock-o"></i>{{trans('app.experience')}}
                            :{{$seeker->exp}}</li>
                    </ul>
                    <hr/>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-birthday-cake"></i>{{trans('app.age')}}
                            : {{ $seeker->age }}
                        </li>
                        <li><i class="fa fa-venus-mars"></i>{{trans('app.gender')}}
                            : {{ $seeker->gender }}</li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-calendar"></i>{{trans('app.birth_date')}}
                            : {{ $seeker->birth_date }}
                        </li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-map-marker"></i>{{trans('app.area')}}
                            : {{ $seeker->area}}
                        </li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-home"></i>{{trans('app.city')}}
                            : {{ $seeker->city}}
                        </li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-globe"></i>{{trans('app.nationality')}}
                            : {{ $seeker->nationality }}</li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-file-text-o"></i>{{trans('app.national_num')}}
                            : {{ $seeker->national_num }}
                        </li>
                    </ul>
                    <hr/>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-phone"></i>{{trans('app.mobile')}}
                            : {{ $seeker->mobile }}</li>
                    </ul>
                    <ul class="list-inline share-list">
                        <li><i class="fa fa-envelope"></i>{{trans('app.mail')}}
                            <a href="mailto:{{$seeker->email}}">: {{$seeker->email}}</a>
                        </li>
                    </ul>
                    <hr/>
                    <ul class="list-inline share-list">
                        <li>
                            <i class="fa fa-suitcase"></i>{{trans('app.employment_status')}}
                            @if($seeker->is_employed)
                                :  <b><span class="text-danger">{{trans('app.employed')}}</span></b>
                            @else
                                :  <b><span class="text-success">{{trans('app.not_employed')}}</span></b>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
    <div>
        <!--End Profile Blog-->
        <!--Pagination Centered-->
        <div class="text-center">
            {!! $seekers->appends(Request::except('page'))->links() !!}
        </div>
        <!--End Pagination Centered-->
    </div>
</div>