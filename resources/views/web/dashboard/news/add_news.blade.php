@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.add_news')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-plus-square-o"></i> {{ trans('app.add_news') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/news') }}">{{trans('app.news_list')}} </a></li>
            <li class="active">{{trans('app.add_news')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <form  action="{{ url('/dashboard/news') }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="alert alert-warning fade in">
                        {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                    </div>
                    <div class="row">
                        <fieldset class="col-md-12">
                            <section>
                                <label for="news_title"
                                       class="input">* {{trans('app.news_title')}} </label>
                                <input type='text' name='news_title' class='form-control' autocomplete="off" placeholder='{{ trans('app.news_title') }}' value= "{{ old('news_title') }}"/>

                                @if ($errors->has('news_title'))
                                <span class="text-danger"> {{ $errors->first('news_title') }}</span>
                                @endif
                            </section>
                            <section>
                                <label for="news_img"
                                       class="input"> {{trans('app.news_img')}} </label>
                                <input type='file' name='news_img' class='form-control' placeholder='{{ trans('app.news_img') }}' value= "{{ old('news_img') }}"/>

                                @if ($errors->has('news_img'))
                                <span class="text-danger"> {{ $errors->first('news_img') }}</span>
                                @endif
                            </section>
                            <section>
                                <label for="news_body"
                                       class="input">* {{trans('app.news_body')}} </label>
                                <textarea rows="5" name='news_body' class='form-control' placeholder="{{ trans('app.news_body') }}">{{ old('news_body') }}</textarea>

                                @if ($errors->has('news_body'))
                                <span class="text-danger"> {{ $errors->first('news_body') }}</span>
                                @endif
                            </section>
                        </fieldset>
                    </div> <!--row-->
                    <footer class="margin-top-20">
                        <button type="submit" class="btn btn-danger" id="submit">
                            {{ trans('app.add')  }}
                        </button>
                    </footer>
                </form>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop