@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.news_list')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa  fa-bullhorn"></i>{{trans('app.news_list')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('dashboard') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.news_list')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="news-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('app.news_title')}}</th>
                        <th>{{trans('app.posted_by')}}</th>
                        <th> {{trans('app.created_at')}}</th>
                        <th>{{trans('app.options')}}</th>
                    </tr>
                    </thead>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
    <!-- delete Modal -->
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('app.delete_news')}}</h4>
                </div>
                <div class="modal-body">
                    {{trans('app.sure_delete_news')}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger ok">{{trans('app.delete')}}</button>
                    <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{trans('app.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#news-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                "aLengthMenu": [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, '{{trans('app.all')}}']],
                "iDisplayLength": 5,
                order: [[0, 'desc']],
                "language": {
                    "url": "{{ ($locale) == 'en' ? asset('js/locals/english.json'): asset('js/locals/arabic.json') }}"
                },
                ajax: "{{ url('dashboard/news/list') }}",
                columns: [
                    {data: "ID", name: "ID", visible: false, searchable: false},
                    {data: "news_title", name: "news_title", orderable: false},
                    {
                        data: "news_posted_by",
                        name: "news_posted_by",
                        sWidth: "13%",
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: "news_created_at",
                        name: "news_created_at",
                        sWidth: "9%",
                        orderable: false,
                        searchable: false
                    },
                    {data: "action", name: "action", sWidth: "14%", orderable: false, searchable: false}
                ]
                ,
                "fnRowCallback": function (nRow) {
                    $('.btn-delete', nRow).on('click', function (e) {
                        e.preventDefault();
                        var link = $(this).attr('href');
                        var id = link.substring(link.indexOf('#') + 1);
                        $("#delete").modal({show: true});
                        var AjaxRunning = false;
                        $('.ok').click(function () {
                            if (!AjaxRunning) {
                                AjaxRunning = true;
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('dashboard/news/') }}" + "/" + id,
                                    data: {_method: "DELETE", _token: "{{  csrf_token() }}"},
                                    success: function () {
                                        $('#delete').modal('hide');
                                        location.reload();
                                    }
                                });
                            }
                        });
                        $('.cancel').click(function () {
                            location.reload();
                        });
                    });
                },
                "fnPreDrawCallback": function () {
                    var url = "{{ url('dashboard/news/create') }}";
                    var btn = "<a href=" + url + " ><button class='btn btn-danger col-lg-1 col-md-1 col-sm-3 col-xs-12 margin-bottom-10'>" +
                            "<i class='fa fa-plus-square-o'></i> {{trans('app.new_record')}}</button></a>";

                    $("div#new_record_btn").html(btn);
                    $(".dataTables_filter label").addClass('input').unwrap().wrap("<section class='col col-3 pull-right'>");
                    $(".dataTables_length label").append("<i></i>");
                    $(".dataTables_length label").addClass('select').unwrap().wrap("<section class='col col-2 no-padding pull-right'>");
                    $("div#warp_tools").unwrap().wrap("<form class='sky-form'>");
                    if ($(".search_box").val() != '') {
                        $(".search_box").focus();
                    }
                }
            });
        });
    </script>
@stop
