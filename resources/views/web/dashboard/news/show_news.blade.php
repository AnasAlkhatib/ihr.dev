@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.view_news')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-plus-square-o"></i> {{ trans('app.view_news') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/news') }}">{{trans('app.news_list')}} </a></li>
            <li class="active">{{trans('app.view_news')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <fieldset>
                    <div class="col-md-12 text-center">
                        <h3>  {{trans('app.news_title')}} :</h3>
                        <p>{{ $news->news_title }}</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <h3>  {{trans('app.news_img')}} :</h3>
                        <img class="img-responsive img-thumbnail" src="{{url("/uploads/news/thumbnail/" . $news->news_thumbnail)}}"/>
                    </div>
                    <div class="col-md-12 text-center">
                        <h3>  {{trans('app.news_body')}} :</h3>
                        <p>{{ $news->news_body }}</p>
                    </div>
                </fieldset>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop
