@extends('layouts.client_master')
@section('content')
@section('title'){{trans('app.edit_candidate')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_candidate') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/client') }}">{{trans('app.home')}}</a></li>
            <li>
                <a href="{{ url('/client/vacant/'.$candidatables['vacant_id'].'/candidates') }}">{{trans('app.candidate_list')}} </a>
            </li>
            <li class="active">{{trans('app.edit_candidate')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-body">
                {!! Form::open(['method' => 'PUT', 'url' => url('/client/candidate/'.$candidate->id ),'class' => 'sky-form','id' => 'sky-form1' ]) !!}
                <div class="row">
                    <fieldset class="col-md-12 no-padding margin-top-20">
                        <section class="col-md-12">
                            <header>{{trans('app.personal_info')}}
                                <div class="pull-right">
                                    <small>{{trans('app.reference_number')}} :</small>
                                    <small>
                                        <mark>{{$candidate->ref_num}}</mark>
                                    </small>
                                </div>
                            </header>
                        </section>
                    </fieldset>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <tr>
                                        <th>{{ trans('app.name') }}</th>
                                        <th>{{ trans('app.email') }}</th>
                                        <th>{{ trans('app.mobile') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->name}}</td>
                                        <td>{{$candidate->email}}</td>
                                        <td>{{$candidate->mobile}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.birth_date') }}</th>
                                        <th>{{ trans('app.nationality') }}</th>
                                        <th>{{ trans('app.national_num') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidate->birth_date}}</td>
                                        <td>{{$candidate->nationality}}</td>
                                        <td>{{$candidate->national_num}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ trans('app.area') }}</th>
                                        <th>{{ trans('app.city') }}</th>
                                        <th>{{ trans('app.address') }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{$candidateArea}}</td>
                                        <td>{{$candidateCity}}</td>
                                        <td>{{$candidate->address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!--/row-->
                        <div class="row">
                            <div class="col-md-12">
                                <header>* {{trans('app.status')}}</header>
                            </div>
                            @if ($errors->has('status'))
                                <div class="row">
                                    <section class="col-md-2">
                                        <span class="text-danger"> {{ $errors->first('status') }}</span>
                                    </section>
                                </div>
                            @endif
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'accept_candidate', (old('status',$candidatables['status']) == 'accept_candidate'), ['class'=>'showNotes','id'=>'accept_candidate']) !!}
                                    <i></i>{{trans('app.accept_candidate')}}
                                </label>
                            </section>
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'reject_candidate', (old('status',$candidatables['status']) == 'reject_candidate'), ['class'=>'showNotes','id'=>'reject_candidate']) !!}
                                    <i></i>{{trans('app.reject_candidate')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="notes" style="display: none;">
                            <section class="col-md-12">
                                <label class="textarea">
                                    <i class="icon-append fa fa-file-text-o"></i>
                                    <textarea rows="2" name="notes" id="notes_text"
                                              placeholder="{{trans('app.notes')}}">{{old('notes',$candidatables['notes'])}}</textarea>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'offer_vacancies', (old('status',$candidatables['status']) == 'offer_vacancies'), ['class'=>'offer_vacancies_status','id'=>'offer_vacancies_status']) !!}
                                    <i></i>{{trans('app.offer_vacancies')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="offer_vacancies_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.vacancies')}}</label>
                                <label class="select">
                                    <select name="offer_vacancies[]" id="offer_vacancies" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col-md-2">
                                <label class="radio @if($errors->has('status')){{'state-error'}}@endif">
                                    {!! Form::radio('status', 'final_interview', (old('status',$candidatables['status']) == 'final_interview'), ['class'=>'final_interview','id'=>'final_interview']) !!}
                                    <i></i>{{trans('app.final_interview')}}
                                </label>
                            </section>
                        </div>
                        <div class="row" id="required_documents_div" style="display: none;">
                            <section class="col-md-12">
                                <label>{{trans('app.required_documents')}}</label>
                                <label class="select">
                                    <select name="required_documents[]" id="required_documents" multiple
                                            data-placeholder="{{trans('app.please_select')}}">
                                    </select>
                                </label>
                            </section>
                        </div>
                    </div>
                    <footer class="col-md-12" style="padding: 15px 15px;">
                        <button type="submit" class="btn-u">{{trans('app.update')}}</button>
                    </footer>
                </div>
                </form><!--form-->
            </div><!--row-->
        </div><!--profile-body-->
    </div><!--col-md-12-->
</div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true,
                width: '100%'
            });
            /* Area Drop Down */
            var $AreaElement = $("#area");
            var $areaRequest = $.ajax({
                url: '/dashboard/area'
            });
            $areaRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $AreaElement.append(option);
                }
                $AreaElement.val('{{old('area',$candidateArea)}}').trigger('change');
            });
            /* city drop down */
            var $cityElement = $("#city");
            var $cityElementOption = $("#city option[value]");
            var $cityRequest = $.ajax({
                url: '/dashboard/city'
            });
            $cityRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $cityElement.append(option);
                }
                $cityElement.val('{{old('city_id')}}').trigger('change');
            });
            /* relation area city */
            $AreaElement.on("change", function () {
                var id = $(this).val();
                $cityElement.prop("disabled", true);
                if (id !== "") {
                    $cityElementOption.remove();
                    var cities = $.ajax({
                        url: '/dashboard/area/' + id + '/city'
                    });
                    cities.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{old('city_id',$candidate->city_id)}}').trigger('change');
                    });
                } else {
                    $cityElementOption.remove();
                    var $cityRequest = $.ajax({
                        url: '/dashboard/city'
                    });
                    $cityRequest.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{ old('city_id',$candidate->city_id) }}').trigger('change');
                    });
                }
                $cityElement.prop("disabled", false);
            });

            /* offer_vacancies Drop Down */
            var $offeredVacanciesElement = $("#offer_vacancies");
            var $offeredVacanciesRequest = $.ajax({
                url: '/dashboard/candidate/getAllVacancies'
            });
            $offeredVacanciesRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $offeredVacanciesElement.append(option);
                }
                        @if(old('offer_vacancies',unserialize($candidatables['offer_vacancies'])))
                var offerVacanciesArray = [<?php echo '"' . implode('","',
                                        old('offer_vacancies', unserialize($candidatables['offer_vacancies']))) . '"' ?>];
                $offeredVacanciesElement.val(offerVacanciesArray).trigger('change');
                @else
                    $offeredVacanciesElement.val('').trigger('change');
                @endif
            });
            /* requiredDocuments Drop Down */
            var $requiredDocumentsElement = $("#required_documents");
            var $requiredDocumentsRequest = $.ajax({
                url: '/dashboard/candidate/requiredDocuments'
            });
            $requiredDocumentsRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $requiredDocumentsElement.append(option);
                }
                        @if(old('required_documents',unserialize($candidatables['required_documents'])))
                var documentsArray = [<?php echo '"' . implode('","',
                                        old('required_documents', unserialize($candidatables['required_documents']))) . '"' ?>];
                $requiredDocumentsElement.val(documentsArray).trigger('change');
                @else
                    $requiredDocumentsElement.val('').trigger('change');
                @endif
            });
            //show notes if  accept_candidate or reject_candidate
            $('.showNotes').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $requiredDocumentsElement.select2("val", "");
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").hide();
                $('#notes').show();

            });
            if ($('.showNotes:checked').length > 0) {
                $("#notes").show();
            }
// show offer_vacancies if offer_vacancies
            $('.offer_vacancies_status').click(function () {
                $requiredDocumentsElement.select2("val", "");
                $("#required_documents_div").hide();
                $("#offer_vacancies_div").show();
                $('#notes_text').val('')
                $('#notes').hide();
            });
            if ($('#offer_vacancies_status:checked').length > 0) {
                $("#offer_vacancies_div").show();
            }
            //show required_documents_div if final_interview
            $('.final_interview').click(function () {
                $offeredVacanciesElement.select2("val", "");
                $('#notes_text').val('')
                $('#notes').hide();
                $("#offer_vacancies_div").hide();
                $("#required_documents_div").show();
            });
            if ($('#final_interview:checked').length > 0) {
                $("#required_documents_div").show();
            }
            /* date picker position */
            $.extend($.datepicker, {
                _checkOffset: function (inst, offset, isFixed) {
                    return offset;
                }
            });
            /* birth_date picker*/
            $('#birth_date').datepicker({
                dateFormat: 'yy-mm-dd',
                showAnim: 'slideDown'
            });
        });

    </script>
@stop