@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.add_client')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-user-plus"></i> {{ trans('app.add_client') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/client') }}">{{trans('app.client_list')}} </a></li>
            <li class="active">{{trans('app.add_client')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <form action="{{ url('/dashboard/client') }}" class="sky-form" method="POST">
                    {!! csrf_field() !!}
                    <div class="alert alert-warning fade in">
                        {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                    </div>
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}" value="{{ old('name') }}"/>
                                    </label>
                                    @if ($errors->has('name'))
                                        <span class="text-danger"> {{ $errors->first('name') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" name="mobile" autocomplete="off"
                                               placeholder="* {{ trans('app.mobile') }}" value="{{ old('mobile') }}"/>
                                    </label>
                                    @if ($errors->has('mobile'))
                                        <span class="text-danger"> {{ $errors->first('mobile') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email" value="{{ old('email') }}"
                                               placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                    </label>
                                    @if ($errors->has('email'))
                                        <span class="text-danger"> {{ $errors->first('email') }}</span>
                                    @endif
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-building-o"></i>
                                        <input type="text" name="company_name" autocomplete="off"
                                               placeholder="* {{ trans('app.company_name') }}"
                                               value="{{ old('company_name') }}"/>
                                    </label>
                                    @if ($errors->has('company_name'))
                                        <span class="text-danger"> {{ $errors->first('company_name') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label for="company_type" class="select">
                                        <select name="company_type">
                                            <option selected disabled hidden>* {{trans('app.company_type')}}</option>
                                            @if (count($companyTypes) > 0)
                                                @foreach ($companyTypes as $companyType)
                                                    @if (old('company_type') == $companyType['value'])
                                                        <option value="{{ $companyType['value']}}"
                                                                selected>{{$companyType['name']}}</option>
                                                    @else
                                                        <option value="{{$companyType['value'] }}">{{ $companyType['name']}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <i></i>
                                    </label>
                                    @if ($errors->has('company_type'))
                                        <span class="text-danger"> {{ $errors->first('company_type') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" name="job_title" value="{{ old('job_title') }}"
                                               placeholder="* {{trans('app.job_title')}}" autocomplete="off"/>
                                    </label>
                                    @if ($errors->has('job_title'))
                                        <span class="text-danger"> {{ $errors->first('job_title') }}</span>
                                    @endif
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password" value="{{ old('password') }}"
                                               placeholder="* {{ trans('app.password') }}"/>
                                    </label>
                                    @if ($errors->has('password'))
                                        <span class="text-danger"> {{ $errors->first('password') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password_confirmation"
                                               value="{{ old('password_confirmation') }}"
                                               placeholder="* {{ trans('app.password_confirmation') }}"/>
                                    </label>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="text-danger"> {{ $errors->first('password') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-4">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default active">
                                            <input checked type="radio" name="active" value="1">{{trans('app.active')}}
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="active" value="0">{{trans('app.inactive')}}
                                        </label>
                                    </div>
                                </section>
                            </div>
                            <section class="col-md-12">
                                <button id="submit" type="submit"
                                        class="btn-u">{{trans('app.add')}}</button>
                            </section>
                        </fieldset>
                    </div><!--row-->
                </form><!--end-form-->
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div><!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop