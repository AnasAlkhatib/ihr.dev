@extends('layouts.client_master')
@section('title'){{trans('app.sto')}} | {{trans('app.dashboard')}}@stop
@section('content')
    <!--=== Profile ===-->
    <div class="container content profile">
        <div class="row">
            <!--Left Sidebar-->
        @include('includes.client_profile')
        <!-- Dashboard Content -->
            <div class="col-md-9">
                <div class="profile-body">
                    <div class="row margin-bottom-20">

                        <!--Latest Jobs-->
                        <div class="col-sm-12">
                            <div class="panel panel-profile no-bg light-shadow">
                                <div class="panel-heading overflow-h bg-color-dark-red">
                                    <h2 class="panel-title heading-sm pull-left">
                                        <i class="fa fa-file-text-o"></i>{{ trans('app.last_vacancies') }}</h2>
                                </div>
                                <div id="scrollbar2" class="panel-body no-padding mCustomScrollbar"
                                     data-mcs-theme="minimal-dark">
                                    @if(count($vacancies) > 0)
                                        @foreach($vacancies as $vacant)
                                            <div class="profile-event">
                                                <div class="date-formats">
                                                    <span>{{ date('d', strtotime($vacant->created_at)) }}</span>
                                                    <small>{{ date('m', strtotime($vacant->created_at)) }},
                                                        {{ date('Y', strtotime($vacant->created_at)) }}</small>
                                                </div>
                                                <div class="overflow-h">
                                                    <h3 class="heading-xs">
                                                        <a href="client/vacant/{{ $vacant->ID }}">{{ $vacant->job_title }} </a>
                                                    </h3>

                                                    <p>{{ str_limit($vacant->job_discription, $limit = 120, $end = '...') }}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <h3 class="heading-xs">
                                            <p style="padding: 10px;">{{trans('app.no_records_found')}}</p>
                                        </h3>
                                    @endif
                                </div><!--/scrollbar2-->
                            </div><!--panel-->
                        </div><!--/col-sm-6-->
                        <!--end latest jobs-->
                    </div><!--/end row-->
                    <hr/>
                    <!--Statistics Block-->
                    <div class="row margin-bottom-10">
                        <div class="col-md-12 no-padding">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sm-margin-bottom-20">
                                <div class="service-block-v3 service-block-u">
                                    <i class="icon-flag"></i><span
                                            class="service-heading">{{trans('app.vacancies')}}</span>
                                    <span class="counter">{{ number_format($vacant_count) }}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="service-block-v3 service-block-u">
                                    <i class="icon-user-following"></i><span
                                            class="service-heading">{{trans('app.candidates')}}</span>
                                    <span class="counter">{{ number_format($candidates) }}</span>
                                </div>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--/end Statistics Block-->
                </div><!--/end row-->
            </div><!--/end Dashboard Content -->
        </div><!--/row -->
    </div><!--/container-->
@stop
