@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.view_client')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i>{{trans('app.view_client')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('dashboard/client') }}">{{trans('app.client_list')}}</a></li>
            <li class="active">{{trans('app.view_client')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="client-table">
                    <tbody>
                    <tr>
                        <td><i class="fa fa-user"></i> {{trans('app.name')}} : {{$client->name}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-mobile-phone"></i> {{trans('app.mobile')}} : {{$client->mobile}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-building-o"></i> {{trans('app.company_name')}}
                            : {{$client->company_name}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-file-text-o"></i> {{trans('app.company_type')}}
                            : {{trans('app.'.$client->company_type)}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-briefcase"></i> {{trans('app.job_title')}} : {{$client->job_title}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-envelope"></i> {{trans('app.email')}} : {{$client->email}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-check"></i> {{trans('app.status')}} :
                            @if($client->active === 1)
                                <span class="text-success">{{trans('app.active')}}</span>
                            @endif
                            @if($client->active === 0)
                                <span class="text-danger">{{trans('app.inactive')}}</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
</div> <!-- container content profile -->
@stop

