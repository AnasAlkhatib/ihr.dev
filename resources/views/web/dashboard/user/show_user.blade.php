@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.view_user')}}@stop
<!--=== Profile ===-->
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-file-text-o"></i>{{trans('app.view_user')}}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/user') }}">{{trans('app.user_list')}}</a></li>
            <li class="active">{{trans('app.view_user')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <!--Table Striped-->
                <table class="table table-striped table-responsive" id="user-table">
                    <tbody>
                    <tr>
                        <td><i class="fa fa-user"></i> {{trans('app.name')}}</td>
                    </tr>
                    <tr>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-envelope"></i> {{trans('app.email')}}</td>
                    </tr>
                    <tr>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-key"></i> {{trans('app.role')}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('app.' . $user->roles()->first()->name)}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-calendar"></i> {{trans('app.created_at')}}</td>
                    </tr>
                    <tr>
                        <td>{{$user->created_at}}</td>
                    </tr>
                    </tbody>
                </table>
                <!--End Table Striped-->
            </div>
        </div>
        <!-- End Dashboard Content -->
    </div>
</div> <!-- container content profile -->
@stop


