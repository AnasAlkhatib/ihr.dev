@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.edit_user')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-edit"></i> {{ trans('app.edit_user') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li><a href="{{ url('/dashboard/user') }}">{{trans('app.user_list')}} </a></li>
            <li class="active">{{trans('app.edit_user')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            <div class="profile-body">
                <form action="{{ url('/dashboard/user/').'/'.$user->id }}" id="sky-form1" class="sky-form"
                      method="POST">
                    {{ method_field('PUT') }}
                    {!! csrf_field() !!}
                    <div class="alert alert-warning fade in">
                        {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                    </div>
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}"
                                               value="{{  old('name',$user->name)  }}"/>
                                    </label>
                                    @if ($errors->has('name'))
                                        <span class="text-danger"> {{ $errors->first('name') }}</span>
                                    @endif
                                </section>

                                <div class="row">
                                    <section class="col-md-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-envelope"></i>
                                            <input type="text" name="email" value="{{ old('email',$user->email) }}"
                                                   placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                        </label>
                                        @if ($errors->has('email'))
                                            <span class="text-danger"> {{ $errors->first('email') }}</span>
                                        @endif
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col-md-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password" value="{{ old('password') }}"
                                                   placeholder="{{ trans('app.password') }}"/>
                                        </label>
                                        @if ($errors->has('password'))
                                            <span class="text-danger"> {{ $errors->first('password') }}</span>
                                        @endif
                                    </section>
                                    <section class="col-md-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password_confirmation"
                                                   value="{{ old('password_confirmation') }}"
                                                   placeholder="{{ trans('app.password_confirmation') }}"/>
                                        </label>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="text-danger"> {{ $errors->first('password') }}</span>
                                        @endif
                                    </section>
                                </div>
                                <section class="col-md-4">
                                    <label class="select">
                                        <select name="role" id="role">
                                            <option></option>
                                            @if (count($roles) > 0)
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}">{{ trans('app.'.$role->name ) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </label>
                                    @if ($errors->has('role'))
                                        <span class="text-danger"> {{ $errors->first('role') }}</span>
                                    @endif
                                </section>
                                <section class="col-md-12">
                                    <button id="submit" type="submit"
                                            class="btn-u">{{trans('app.update')}}</button>
                                </section>
                        </fieldset>
                    </div>
                </form>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#role").select2({
                placeholder: "* {{trans('app.role')}}",
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? "ar": 'en' }}"
            });
            $("#role").val("{{ old('role',$user->roles->first()->id) }}").trigger("change");
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop