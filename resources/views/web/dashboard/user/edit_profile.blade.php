@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.personal_profile')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-user"></i> {{ trans('app.personal_profile') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.personal_profile')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert {{ session('alert-class') }}" id="message">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            @endif
            <div class="profile-body">
                <form action="{{ url('/dashboard/user/').'/'.$user->id .'/updateProfile'}}" id="sky-form1"
                      class="sky-form"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="alert alert-warning fade in">
                        {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                    </div>
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" autocomplete="off"
                                               placeholder="* {{ trans('app.name') }}"
                                               value="{{  old('name',$user->name)  }}"/>
                                    </label>
                                    @if ($errors->has('name'))
                                        <span class="text-danger"> {{ $errors->first('name') }}</span>
                                    @endif
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-4">
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="text" name="email" value="{{ old('email',$user->email) }}"
                                               placeholder="* {{trans('app.email')}}" autocomplete="off"/>
                                    </label>
                                    @if ($errors->has('email'))
                                        <span class="text-danger"> {{ $errors->first('email') }}</span>
                                    @endif
                                </section>
                            </div>
                            <section class="col-md-12">
                                <button id="submit" type="submit"
                                        class="btn-u">{{ trans('app.update')  }}</button>
                            </section>
                        </fieldset>
                    </div>
                </form>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#message").fadeTo(2000, 500).slideUp(500, function () {
                $("#message").alert('close');
            });
        });
    </script>
@stop
