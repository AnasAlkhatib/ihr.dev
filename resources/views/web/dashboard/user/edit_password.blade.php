@extends('layouts.admin_master')
@section('content')
@section('title'){{trans('app.change_password')}}@stop
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs light-shadow">
    <div class="container">
        <h1 class="pull-left">
            <i class="fa fa-key"></i> {{ trans('app.change_password') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
            <li class="active">{{trans('app.change_password')}}</li>
        </ul>
    </div>
</div>
<!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->
<div class="container content profile">
    <div class="row">
        <!--Left Sidebar-->
        <!--End Left Sidebar-->
        <!-- Dashboard Content -->
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert {{ session('alert-class') }}" id="message">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ session('message') }}
                </div>
            @endif
            <div class="profile-body">
                <form action="{{ url('/dashboard/user/').'/'.$user->id .'/updatePassword'}}" id="sky-form1"
                      class="sky-form"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="alert alert-warning fade in">
                        {{ trans('app.field_with') }} ( <strong>*</strong> ) {{ trans('app.required_field') }} .
                    </div>
                    <div class="row">
                        <fieldset class="col-md-12 no-padding margin-top-20">
                            <div class="row">
                                <section class="col-md-5">
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="current_password" autocomplete="off"
                                               placeholder="* {{ trans('app.current_password') }}"
                                               value="{{  old('current_password')  }}"/>
                                    </label>
                                        @if ($errors->has('current_password'))
                                            <span class="text-danger"> {{ $errors->first('current_password') }}</span>
                                        @endif
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-5">
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password" autocomplete="off"
                                               placeholder="* {{ trans('app.password') }}"
                                               value="{{  old('password')  }}"/>
                                    </label>
                                        @if ($errors->has('password'))
                                            <span class="text-danger"> {{ $errors->first('password') }}</span>
                                        @endif
                                </section>
                            </div>
                            <div class="row">
                                <section class="col-md-5">
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password_confirmation" autocomplete="off"
                                               placeholder="* {{ trans('app.password_confirmation') }}"
                                               value="{{  old('password_confirmation')  }}"/>
                                    </label>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="text-danger"> {{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                </section>
                            </div>
                            <section class="col-md-12">
                                <button id="submit" type="submit"
                                        class="btn-u">{{ trans('app.update')  }}</button>
                            </section>
                        </fieldset>
                    </div>
                </form>
            </div><!--profile-body-->
        </div><!--col-md-12-->
    </div> <!--row-->
</div><!--container content profile-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#message").fadeTo(2000, 500).slideUp(500, function () {
                $("#message").alert('close');
            });
        });
    </script>
@stop
