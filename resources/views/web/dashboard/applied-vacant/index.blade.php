@extends('layouts.admin_master')
@section('title')STO | {{trans('app.dashboard')}}@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs light-shadow">
        <div class="container">
            <h1 class="pull-left">
                <i class="fa fa-suitcase"></i>{{trans('app.applied_vacant')}}
                <small>( {{$vacant->job_title}} )</small>
            </h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{ url('dashboard') }}">{{ trans('app.home') }}</a></li>
                <li><a href="{{ url('dashboard/vacant') }}">{{ trans('app.vacant_list') }}</a></li>
                <li class="active">{{trans('app.applied_vacant')}}</li>
            </ul>
        </div>
    </div>
    <!--/breadcrumbs-->
    <!--=== Profile ===-->
    <div class="container content profile" id="app">
        <div class="row">
            <!-- Profile Content -->
            <div class="col-md-12">
                <div class="profile-body margin-bottom-20">
                    <form class="sky-form" id="search" method="get"
                          action="{{ url('dashboard') }}/vacant/{{$vacant->ID}}/applied-vacant">
                        <fieldset class="margin-bottom-20">
                            <div class="headline">
                                <h4>{{trans('app.search')}} :</h4>
                            </div>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name='gender' id='gender' data-placeholder='{{trans('app.gender')}}'>
                                        <option></option>
                                        @foreach ($dropDownData['genders'] as $gender)
                                            @if( Input::get('gender') == $gender['value'] )
                                                <option value="{{ $gender['value']}}"
                                                        selected>{{ $gender['name'] }}</option>
                                            @else
                                                <option value="{{ $gender['value']}}">{{ $gender['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="nationality" id="nationality"
                                            data-placeholder='{{trans('app.nationality')}}'>
                                        <option></option>
                                        @foreach ($dropDownData['nationalities'] as $nationality)
                                            @if( Input::get('nationality') == $nationality['value'] )
                                                <option value="{{ $nationality['value']}}"
                                                        selected>{{ $nationality['name'] }}</option>
                                            @else
                                                <option value="{{ $nationality['value']}}">{{ $nationality['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="area" id="area" data-placeholder="{{trans('app.area')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="city" id="city" data-placeholder="{{trans('app.city')}}">
                                        <option></option>
                                    </select>
                                </label>
                            </section>

                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="age" id="age" data-placeholder="{{trans('app.age')}}">
                                        <option></option>
                                        @foreach ($dropDownData['agePeriods'] as $period)
                                            @if( Input::get('age') == $period['value'] )
                                                <option value="{{ $period['value']}}"
                                                        selected>{{ $period['name'] }}</option>
                                            @else
                                                <option value="{{$period['value']}}">{{ $period['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="edu_level" id="education"
                                            data-placeholder="{{trans('app.edu_level')}}">
                                        <option></option>
                                        @foreach ($dropDownData['educationLevel']  as $edu)
                                            @if( Input::get('edu_level') == $edu['value'] )
                                                <option value="{{ $edu['value']}}"
                                                        selected>{{ $edu['name'] }}</option>
                                            @else
                                                <option value="{{$edu['value']}}">{{ $edu['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="exp_years" id="exp_years"
                                            data-placeholder="{{trans('app.exp_years')}}">
                                        <option></option>
                                        @foreach ($dropDownData['expYears']  as $exp)
                                            @if( Input::get('exp_years') == $exp['value'] )
                                                <option value="{{ $exp['value']}}"
                                                        selected>{{ $exp['name'] }}</option>
                                            @else
                                                <option value="{{ $exp['value']}}">{{ $exp['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-3 col-sm-4">
                                <label class="select">
                                    <select name="company_industry" id="company_industry"
                                            data-placeholder="{{trans('app.area_expertise')}}">
                                        <option></option>
                                        @foreach ($dropDownData['companyIndustry']  as $industry)
                                            @if( Input::get('company_industry') == $industry['value'] )
                                                <option value="{{ $industry['value']}}"
                                                        selected>{{ $industry['name'] }}</option>
                                            @else
                                                <option value="{{ $industry['value']}}">{{ $industry['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                            <section class="col-md-12 col-sm-12">
                                <button id="submit_search"
                                        class="btn btn-danger btn-sm col-lg-1 col-md-1 col-sm-3 col-xs-12  pull-right margin-left-10">
                                    <i class="fa fa-search margin-left-10"></i> {{trans('app.search_btn')}}
                                </button>
                            </section>
                        </fieldset>
                    </form>
                    <!--Profile Blog-->
                    <div class="dataTables_processing">
                        {{trans('app.please_wait')}} ...
                    </div>
                    <div id="seekers-ajax">
                        @if($seekers->count() > 0 )
                            @include('web.dashboard.applied-vacant.ajax-seekers')
                        @else
                            <div class="text-center alert alert-warning fade in">
                                <h4>{{trans('app.no_result_to_display')}}</h4>
                            </div>
                        @endif
                    </div>
                    <!--End Pagination Centered-->
                </div>
            </div>
        </div>
        <!--/end row-->
    </div>
    <!--=== End Profile ===-->
@stop
@section('custom-script')
    <script src="{{ asset('js/app-admin.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables_processing')
                    .hide()
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxStop(function () {
                        $(this).hide();
                    });
        });
    </script>
    <script>
        $(function () {
            $('#seekers-ajax').on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href'));
                e.preventDefault();
            });
        });
        function getPosts(page) {
            $.ajax({
                url: page,
                dataType: 'json',
                cache: false
            }).done(function (data) {
                $('#seekers-ajax').html(data);
                new Vue({el: '#app'});
                $('.dataTables_processing').hide();
            }).fail(function () {
                console.log('Seekers could not be loaded');
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /* select2 options */
            $("select").select2({
                dir: "{{ ($locale) == 'ar' ? "rtl": 'ltr' }}",
                language: "{{ ($locale) == 'ar' ? 'ar': 'en' }}",
                placeholder: function () {
                    $(this).data('placeholder');
                },
                allowClear: true

            });
            /* Area Drop Down */
            var $AreaElement = $("#area");
            var $areaRequest = $.ajax({
                url: '/dashboard/area'
            });
            $areaRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $AreaElement.append(option);
                }
                $AreaElement.val('{{Input::get('area')}}').trigger('change');
            });
            /* city drop down */
            var $cityElement = $("#city");
            var $cityElementOption = $("#city option[value]");
            var $cityRequest = $.ajax({
                url: '/dashboard/city'
            });
            $cityRequest.then(function (data) {
                for (var d = 0; d < data.length; d++) {
                    var item = data[d];
                    var option = new Option(item.text, item.id, true, true);
                    $cityElement.append(option);
                }
                $cityElement.val('{{Input::get('city')}}').trigger('change');
            });

            /* relation area city */
            $AreaElement.on("change", function () {
                var id = $(this).val();
                $cityElement.prop("disabled", true);
                if (id !== "") {
                    $cityElementOption.remove();
                    var cities = $.ajax({
                        url: '/dashboard/area/' + id + '/city'
                    });
                    cities.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                } else {
                    $cityElementOption.remove();
                    var $cityRequest = $.ajax({
                        url: '/dashboard/city'
                    });
                    $cityRequest.then(function (data) {
                        for (var d = 0; d < data.length; d++) {
                            var item = data[d];
                            var option = new Option(item.text, item.id, true, true);
                            $cityElement.append(option);
                        }
                        $cityElement.val('{{Input::get('city')}}').trigger('change');
                    });
                }
                $cityElement.prop("disabled", false);
            });
        });
    </script>
@stop

