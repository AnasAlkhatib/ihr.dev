<!doctype html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        body {
            background-color: #FFF;
        }

        .full_name {
            font-size: 20px;
        }

        .content {
            font-size: 13px;
        }

        .mobile, .email, .birth_date, .exp_year {
            font-size: 17px;
            font-style: normal;
        }

        .gender {
            font-size: 14px;
            font-style: normal;
        }

        .number {
            font-size: 17px;
            font-style: normal;
        }

        .content_title {
            font-size: 13px;
            font-weight: bold;
        }
        a {
            text-decoration: none;
            color: #000;
        }
        table td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
        }
        .border-bottom-none {
            border-bottom: 1px solid #FFF;
        }
        .border-right-none {
            border-right: 1px solid #FFF;
        }

        .info_table, .exp_table {
            background-color: #FFF;
            padding: 0px 2px 0px 2px;
        }
        h1 {
            font-size: 16px;
        }
        h2 {
            font-size: 14px;
            background-color: #EEE;
            border-bottom: 1px solid #999;
        }
        .job_title, .education, .language {
            font-size: 13px;
            font-weight: bold;
        }

        .company {
            font-size: 12px;
            font-style: italic;
        }

        .job_description {
            font-size: 13px;
        }
    </style>
</head>
<body>
<h1 align="right" class="full_name">{{$seeker->full_name}}</h1>
<table class="table_content" align="right" cellspacing="0" cellpadding="1">
    <tr>
        <td class="border-bottom-none"><span align="right content_title">التخصص العلمي </span></td>
        <td class="border-bottom-none"><span align="right content_title">العنوان </span></td>
        <td class="border-right-none border-bottom-none"><span align="right"> بيانات الاتصال</span></td>
    </tr>
    <tr>
        <td class="border-bottom-none content"> {{ isset($seeker->education->last()->edu_level) ? $seeker->education->last()->edu_level : '' }}
            ,{{ isset($seeker->education->last()->edu_major) ? $seeker->education->last()->edu_major : '' }},
            <abbr>{{ isset($seeker->education->last()->edu_name) ? $seeker->education->last()->edu_name : '' }}</abbr>
        </td>
        <td class="border-bottom-none content">
            <span> المملكة العربية السعودية</span><br/>
            <span> {{\App\City::find($seeker->city_id)->name}} ,{{$seeker->address}}</span><br/>
        </td>
        <td class="border-right-none border-bottom-none"><span class="mobile"> {{$seeker->mobile}}</span><br/>
            <a class="email" href="mailto:{{$seeker->email}}"> {{$seeker->email}}</a></td>
    </tr>
</table>
<h2 align="right">المعلومات الشخصية</h2>
<table class="info_table" align="right">
    <tr>
        <td width="80%"><span class="birth_date">{{$seeker->birth_date}}</span></td>
        <td width="20%"><span class="content_title">تاريخ الميلاد :</span></td>
    </tr>
    <tr>
        <td><span class="gender">{{($seeker->gender) =='male' ?'ذكر' : 'أنثى'}}</span></td>
        <td><span class="content_title">الجنس :</span></td>
    </tr>
    <tr>
        <td><span class="gender">{{$seeker->nationality}}</span></td>
        <td><span class="content_title">الجنسية :</span></td>
    </tr>
    <tr>
        <td><span class="mobile" valign="middle">{{$seeker->national_num}}</span></td>
        <td><span class="content_title">الهوية الوطنية / الاقامة :</span></td>
    </tr>

    @if ($seeker->marital_status !='')
        <tr>
            <td><span class="gender">
                @if ($seeker->gender =='male' && $seeker->marital_status =='أعزب' )
                        <span class="gender">أعزب</span>
                    @elseif ($seeker->gender =='male' && $seeker->marital_status =='متزوج' )
                        <span class="gender">متزوج</span>
                    @endif
                    @if ($seeker->gender =='female' && $seeker->marital_status =='أعزب' )
                        <span class="gender">عزباء</span>
                    @elseif  ($seeker->gender =='female' && $seeker->marital_status =='متزوج' )
                        <span class="gender">متزوجة</span>
                    @endif
            </span></td>
            <td><span class="content_title">الحالة الاجتماعية :</span></td>
        </tr>
    @endif
    @if ($seeker->dependent_num !='')
        <tr>
            <td><span class="number">{{$seeker->dependent_num}}</span></td>
            <td><span class="content_title">عدد المعالين :</span></td>
        </tr>
    @endif
    @if ($seeker->driving_licence !='')
        <tr>
            <td><span class="gender">
                @if ($seeker->driving_licence =='yes')
                        <span class="gender">{{trans('app.yes')}}</span>
                    @elseif ($seeker->driving_licence =='no')
                        <span class="gender">{{trans('app.no')}}</span>
                    @endif
            </span></td>
            <td><span class="content_title">رخصة قيادة :</span></td>
        </tr>
    @endif
    @if ($seeker->owning_vehicle !='')
        <tr>
            <td><span class="gender">
                @if ($seeker->owning_vehicle =='yes')
                        <span class="gender">{{trans('app.yes')}}</span>
                    @elseif ($seeker->owning_vehicle =='no')
                        <span class="gender">{{trans('app.no')}}</span>
                    @endif
            </span></td>

            <td><span class="content_title">يمتلك سيارة :</span></td>
        </tr>
    @endif
</table>
@if (!$seeker->experience->isEmpty())
    <h2 align="right">الخبرة العملية</h2>
    @foreach ($seeker->experience as $experience)
        <table class="exp_table" align="right">
            <tr>
                <td align="left"><span class="exp_year">
                    @if ($experience->end_date =='0000-00-00' ||  $experience->end_date =='')
                            <span class="company">{{trans('app.until_now')}}</span> &laquo; {{$experience->start_date}}
                            @else
                            {{$experience->end_date}} &laquo; {{$experience->start_date}}
                        @endif
                </span>
                </td>
                <td>
                    <span class="job_title">{{$experience->job_title}}</span><br/>
                    <span class="company">{{$experience->company_name}}</span><br/>
                    <span class="job_description">{{$experience->job_description}}</span>
                </td>
            </tr>
        </table>
    @endforeach
@endif
<h2 align="right">التعليم</h2>
@foreach ($seeker->education as $education)
    <table class="exp_table" align="right">
        <tr>
            <td align="right">
                <span class="exp_year">{{date('Y', strtotime($education->edu_year))}}</span> ,
                <span class="education">{{$education->edu_level}} {{$education->edu_major}}
                    , {{$education->edu_name}}</span>
            </td>
        </tr>
    </table>
@endforeach
<h2 align="right">اللغات</h2>
@foreach ($seeker->language as $language)
    <table class="exp_table" align="right">
        <tr>
            <td align="right">
                <span class="language">{{$language->name}} , {{$language->level}}</span>
            </td>
        </tr>
    </table>
@endforeach
</body>
</html>