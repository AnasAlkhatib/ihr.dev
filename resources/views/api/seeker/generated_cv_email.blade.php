<!doctype html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table, th, td {
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
        }
    </style>
</head>
<body>
<div dir="rtl">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><h3>{{ trans('system_message.seeker_cv_pdf.message') }} {{ $seeker->full_name}}</h3></td>
        </tr>
        <tr>
            <td><h3>{{ trans('system_message.seeker_cv_pdf.find_attachment') }} .</h3></td>
        </tr>
    </table>
</div>
</body>
</html>