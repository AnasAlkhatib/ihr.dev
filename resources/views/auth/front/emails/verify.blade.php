<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
</head>
<body>
<div dir="rtl">
    <p>{{trans('activate.title')}} : </p>
    <div>
        <p>{{trans('activate.message')}}.</p>
        {{--<a href="{{  $link = link_to($url.'/activate',$activation_code)}}">{{$link}}</a>--}}
        <a href="{{ $link ='http://'.$url.'/activate/'.$activation_code}}">{{$link}}</a>
        <p>{{trans('activate.issue')}}.</p>
    </div>
</div>
</body>
</html>
