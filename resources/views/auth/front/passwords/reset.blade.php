@extends('layouts.login_master')
@section('title')STO | {{trans('reset_password.change_password')}}@stop
@section('content')
    <div class="container">
        <!--Reg Block-->
        <div class="reg-block light-shadow">
            <div class="reg-block-header">
                <h2><img alt="" src="{{ asset('assets/img/logo1-default.png') }}"/></h2>
                <br/>

                <h2>{{ trans('reset_password.change_password')}}</h2>
            </div>
            <form method="POST" action="{{ url('/password/reset') }}">
                {!! csrf_field() !!}
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        {{ session('status') }}
                    </div>
                @endif
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group margin-bottom-20 ">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" name="email" value="{{ $email or old('email') }}" class="form-control"
                               placeholder="عنوان البريد الإلكتروني">
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                    {{ $errors->first('email') }}
                </span>
                    @endif
                </div><!--form-group-->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="كلمة المرور">
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                    {{ $errors->first('password') }}
                </span>
                    @endif
                </div><!--form-group-->
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password_confirmation" class="form-control"
                               placeholder="تأكيد كلمة المرور">
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                    {{ $errors->first('password_confirmation') }}
                </span>
                    @endif
                </div><!--form-group-->
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <input class="btn-u btn-block" type="submit"
                               value="{{ trans('reset_password.change_password')}}"/>
                    </div>
                </div>
            </form><!--End form-->
        </div><!--End Reg Block-->
    </div> <!--/container-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $('form').submit(function () {
            $("input[type='submit']", this)
                    .val("انتظر فضلا ...")
                    .attr('disabled', 'disabled');

            return true;
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });</script>
    <script type="text/javascript">
        $.backstretch([
            "{{ asset('assets/img/bg/login-1.jpg') }}",
            "{{ asset('assets/img/bg/login-2.jpg') }}",
            "{{ asset('assets/img/bg/login-3.jpg') }}"
        ], {
            fade: 1000,
            duration: 7000
        });
    </script>
@stop
