@extends('layouts.login_master')
@section('title')STO | {{trans('reset_password.restore_password')}}@stop
@section('content')
<div class="container">
    <!--Reg Block-->
    <div class="reg-block light-shadow">
        <div class="reg-block-header">
            <h2>
                <img src="{{ asset('assets/img/logo1-default.png') }}" /></h2>
            <br />
            <h2>{{ trans('reset_password.restore_password') }}</h2>
        </div>
        <form  method="POST"  action="/password/email">
            {!! csrf_field() !!}
            @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ session('status') }}</p>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                @foreach ($errors->all() as $error)
                <p>{{$error}}</p>
                @endforeach
            </div>
            @endif
            <div class="input-group margin-bottom-20">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" name='email' class="form-control" placeholder="البريد الإلكتروني">
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <input class="btn-u btn-block" type="submit" value="{{ trans('reset_password.send') }}"  /> 
                </div>
            </div>
            <div class="checkbox margin-top-20 text-center">
                <p><a href="" onclick="goBack()">{{ trans('reset_password.back')}}</a></p>
            </div>
        </form><!-- /form -->
    </div><!--End Reg Block-->
</div><!--/container-->
@stop
@section('custom-script')
<script type="text/javascript">
    $('form').submit(function ()
    {
        $("input[type='submit']", this)
                .val("انتظر فضلا ...")
                .attr('disabled', 'disabled');

        return true;
    });
</script>
<script type="text/javascript">
jQuery(document).ready(function () {
        App.init();
    });</script>
<script type="text/javascript">
    $.backstretch([
        "{{ asset('assets/img/bg/login-1.jpg') }}",
        "{{ asset('assets/img/bg/login-2.jpg') }}",
        "{{ asset('assets/img/bg/login-3.jpg') }}"
    ], {
        fade: 1000,
        duration: 7000
    });
    function goBack() {
        window.history.back();
    }
</script>
@stop
