@extends('layouts.login_master')
@section('title')STO | {{trans('web.login')}}@stop
@section('content')
<div class="container">
    <!--Reg Block-->
    <div class="reg-block light-shadow">
        <div class="reg-block-header">
            <h2><img alt="" src="{{ asset('assets/img/logo1-default.png') }}" /></h2>
            <br />
            <h2>{{ trans('web.login') }}</h2>
        </div>
        <form class="form-signin" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                @foreach ($errors->all() as $error)
                <p>{{$error}}</p>
                @endforeach
            </div>
            @endif
            <div class="input-group margin-bottom-20 {{ $errors->has('email') ? ' has-error' : '' }}">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input  name='email' type="text" value="{{ old('email') }}" class="form-control" placeholder="البريد الالكتروني ">
            </div>
            <div class="input-group margin-bottom-20 {{ $errors->has('password') ? ' has-error' : '' }}">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input name="password" type="password"  class="form-control" placeholder="كلمة السر">
            </div>
            <hr>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"/>
                    <span>{{ trans('web.remember')}}</span>
                </label>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn-u btn-block"> {{ trans('web.enter') }}</button>
                </div>
            </div>
            <div class="checkbox margin-top-20 text-center">
                <p><a href="{{ url('password/email') }}">{{trans('web.forgot-password')}}</a></p>
            </div>
        </form><!-- /form -->
    </div><!--End Reg Block-->
</div><!--/container-->
@stop
@section('custom-script')
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
    });</script>
<script type="text/javascript">
    $.backstretch([
        "{{ asset('assets/img/bg/login-1.jpg') }}",
        "{{ asset('assets/img/bg/login-2.jpg') }}",
        "{{ asset('assets/img/bg/login-3.jpg') }}"
    ], {
        fade: 1000,
        duration: 7000
    });
</script>
@stop
