@extends('layouts.login_master')
@section('title'){{trans('app.sto')}} | {{trans('app.register_employee')}}@stop
@section('content')
    <div class="container">
        <!--Reg Block-->
        <div class="reg-block light-shadow">
            <div class="reg-block-header">
                <h2><img alt="" src="{{ asset('assets/img/logo1-default.png') }}"/></h2>
                <h2>{{ trans('app.register') }}  {{ trans('app.client') }}</h2>
                <div class=" margin-top-20 text-center">
                    <h5>{{ trans('app.have_account') }}<a
                                href="{{ url('/client/login') }}"><b> {{ trans('app.login') }}</b></a></h5>
                </div>
            </div>
            <form method="POST" action="{{ url('/client/register') }}">
                {!! csrf_field() !!}
                @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{ session('message') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
                <div class="input-group margin-bottom-20 {{ $errors->has('company_name') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></span>
                    <input name='company_name' type="text" value="{{ old('company_name') }}" class="form-control"
                           placeholder="{{trans('app.company_name')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('company_type') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                    <select class="form-control" name="company_type" style="padding:0 5px;">
                        <option selected hidden disabled value=""> {{trans('app.company_type')}}</option>
                        @if (count($company_types) > 0)
                            @foreach ($company_types as $company_type)
                                @if (old('company_type') == $company_type['value'])
                                    <option value="{{ $company_type['value']}}"
                                            selected>{{$company_type['name']}}</option>
                                @else
                                    <option value="{{$company_type['value'] }}">{{ $company_type['name']}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input name='name' type="text" value="{{ old('name') }}" class="form-control"
                           placeholder="{{trans('app.name')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('mobile') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-mobile-phone" aria-hidden="true"></i></span>
                    <input name='mobile' type="text" value="{{ old('mobile') }}" class="form-control"
                           placeholder="{{trans('app.mobile')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('job_title') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                    <input name='job_title' type="text" value="{{ old('job_title') }}" class="form-control"
                           placeholder="{{trans('app.job_title')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                    <input name='email' type="text" value="{{ old('email') }}" class="form-control"
                           placeholder="{{trans('app.email')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <input name="password" type="password" class="form-control" autocomplete="off"
                           placeholder="{{trans('app.password')}}">
                </div>
                <div class="input-group margin-bottom-20 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <input name="password_confirmation" type="password" class="form-control"
                           placeholder="{{trans('app.password_confirmation')}}">
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn-u btn-block"> {{ trans('app.register') }}</button>
                    </div>
                </div>
                @include('includes.login_language_switcher')
            </form><!-- /form -->
        </div><!--End Reg Block-->
    </div><!--/container-->
@stop
@section('custom-script')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });</script>
    <script type="text/javascript">
        $.backstretch([
            "{{ asset('assets/img/bg/login-1.jpg') }}",
            "{{ asset('assets/img/bg/login-2.jpg') }}",
            "{{ asset('assets/img/bg/login-3.jpg') }}"
        ], {
            fade: 1000,
            duration: 7000
        });
    </script>
@stop
