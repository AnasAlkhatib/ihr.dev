@extends('layouts.login_master')
@section('title'){{trans('app.sto')}} | {{trans('app.change_password')}}@stop
@section('content')
    <div class="container">
        <!--Reg Block-->
        <div class="reg-block light-shadow">
            <div class="reg-block-header">
                <h2><img alt="" src="{{ asset('assets/img/logo1-default.png') }}"/></h2>
                <br/>

                <h2>{{ trans('app.change_password')}}</h2>
            </div>
            {!! Form::open(['method' => 'POST', 'url' => url('client/password/reset') ]) !!}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group margin-bottom-20 ">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" name="email" value="{{ $email or old('email') }}" class="form-control"
                           placeholder="{{ trans('app.email')}}">
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div><!--form-group-->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group margin-bottom-20">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('app.password')}}">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                    {{ $errors->first('password') }}
                </span>
                @endif
            </div><!--form-group-->
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <div class="input-group margin-bottom-20">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="{{ trans('app.password_confirmation')}}">
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                    {{ $errors->first('password_confirmation') }}
                </span>
                @endif
            </div><!--form-group-->
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <input class="btn-u btn-block" type="submit"
                           value="{{ trans('app.change_password')}}"/>
                </div>
            </div>
        @include('includes.login_language_switcher')
        {!! Form::close() !!}<!--/form-->
        </div><!--End Reg Block-->
    </div> <!--/container-->
@stop
@section('custom-script')
    <script type="text/javascript">
        $('form').submit(function () {
            $("input[type='submit']", this)
                    .val("{{trans('app.please_wait')}} ...")
                    .attr('disabled', 'disabled');

            return true;
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });</script>
    <script type="text/javascript">
        $.backstretch([
            "{{ asset('assets/img/bg/login-1.jpg') }}",
            "{{ asset('assets/img/bg/login-2.jpg') }}",
            "{{ asset('assets/img/bg/login-3.jpg') }}"
        ], {
            fade: 1000,
            duration: 7000
        });
    </script>
@stop
