<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
</head>
<body>
<div dir="rtl">
    <div>
        <h3>{{trans('reset_password.message')}}:</h3>
        <a href="{{ $link = "http://".config('app.url')."/employer/password/reset/".$token."?email=".urlencode($user->getEmailForPasswordReset()) }}">{{ $link }}</a>
    </div>
</div>
</body>
</html>
