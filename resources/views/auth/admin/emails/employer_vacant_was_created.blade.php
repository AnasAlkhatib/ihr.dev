<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
    <style>
        table, th, td {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        th, td {
            padding: 10px;
        }
    </style>
</head>
<body>
<div dir="rtl">
    <div>
        <h3>{{trans('vacant_event.title')}} : </h3>
        <table border="1" style="width:100%" cellpadding="0" cellspacing="0">
            <tr>
                <th align="right">الرقم المرجعي</th>
                <th align="right">المسمى الوظيفي</th>
                <th align="right">تاريخ النشر</th>
            </tr>
            <tr>
                <td>{{ $vacant->ref_num }}</td>
                <td>{{ $vacant->job_title }}</td>
                <td>{{ $vacant->created_at }}</td>
            </tr>
        </table>
        <br/>
        <h3>بيانات صاحب العمل : </h3>
        <table border="1" style="width:100%" cellpadding="0" cellspacing="0">
            <tr>
                <th align="right">الاسم</th>
                <th align="right">الشركة</th>
                <th align="right">الجوال</th>
                <th align="right">البريد الالكتروني</th>
            </tr>
            <tr>
                <td>{{ $employer->name }}</td>
                <td>{{ $employer->company_name }}</td>
                <td>{{ $employer->mobile }}</td>
                <td>{{ $employer->email }}</td>
            </tr>
        </table>
        <br/>
        رابط التفعيل :
        <a href="{{ 'http://'.$link .'/dashboard/vacant/'.$vacant->ID.'/activate' }}"> اضغط هنا </a>
    </div>
</div>
</body>
</html>
