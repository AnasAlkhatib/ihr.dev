<html dir="{{($locale) == 'ar' ? "rtl": 'ltr' }}" lang="{{ ($locale) == 'ar' ? "ar": 'en' }}">
<head>
    <meta charset="utf-8">
</head>
<body>
<div dir="{{ ($locale) == 'ar' ? "rtl": 'ltr' }}">
    <div>
        <h3>{{trans('system_message.reset_password.message')}}</h3>
        <a href="{{ $link = url('dashboard/password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">{{ $link }}</a>
    </div>
</div>
</body>
</html>
