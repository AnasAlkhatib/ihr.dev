<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 10px;
        }
    </style>
</head>
<body>
<div dir="rtl">
    <div>
        <h3>{{ trans('system_message.employer_registered.message')}} : </h3>
        <table border="1" style="width:100%" cellpadding="0" cellspacing="0">
            <tr>
                <th>{{trans('app.name')}}</th>
                <th>{{trans('app.email')}}</th>
                <th>{{trans('app.mobile')}}</th>
            </tr>
            <tr>
                <td>{{ $employer->name }}</td>
                <td>{{ $employer->email }}</td>
                <td>{{ $employer->mobile }}</td>
                <td>{{ $employerPlan }}</td>
            </tr>
        </table>
        <br/>

        رابط التفعيل :
        <a href="{{ 'http://'.$link .'/dashboard/employer/'.$employer->id.'/edit' }}"> اضغط هنا </a>
    </div>
</div>
</body>
</html>
