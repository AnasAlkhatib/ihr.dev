<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html dir="{{ ($locale) == 'ar' ? "rtl": 'ltr' }}" lang="{{ ($locale) == 'ar' ? "ar": 'en' }}">
<!--<![endif]-->
<head>
    <title>@yield('title')</title>
    @include('includes.admin_head')
</head>
<body>
<div class="wrapper">
    @include('includes.client_header')
    @yield('content')
    @include('includes.admin_footer')
</div><!--/wrapper-->
@include('includes.admin_JavaScript')
        @yield('custom-script')
 <!--[if lt IE 9]>
<script src="{{ asset('assets/plugins/respond.js') }}"></script>
<script src="{{ asset('assets/plugins/html5shiv.js') }}"></script>
<script src="{{ asset('assets/plugins/placeholder-IE-fixes.js') }}"></script>
<![endif]-->
<!--lang switcher-->
<script type="text/javascript">
    $(document).ready(function () {
        $(".locale").click(function (e) {
            e.preventDefault();
            var url = "{{url('lang')}}";
            var link = $(this).attr('href');
            var arr = [];
            arr['locale'] = link.substring(link.indexOf('#') + 1);
            arr['_token'] = "{{ csrf_token() }}";
            arr['_method'] = 'POST';
            var data = $.extend({}, arr);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response['status_code'] == 200) {
                        location.reload();
                    }
                },
                error: function (error) {
                    console.log(error);

                }
            });
        });
    });
</script>
</body>
</html>