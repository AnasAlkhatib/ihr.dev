<div class="container">
    <footer class="footer">
        <div class="container" style="text-align: center">
            <span>{{ date("Y") }}</span>
            <span><i class="fa fa-copyright"></i></span>
            <span class="btn-link btn-danger"><a class='forgot-password' href="http://www.sto.com.sa">STO</a></span>
        </div>
    </footer>
</div>
</body>
</html>