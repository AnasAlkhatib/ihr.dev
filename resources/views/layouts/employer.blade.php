<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{{trans('web.employer')}}</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom-theme/jquery-ui-1.10.0.custom.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/style.css') }}">
        <!-- jQuery library -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- DataTables -->
        <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/datatables.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/jquery-ui-1.9.2.custom.min.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/locals/ui.datepicker-ar.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/select2.min.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/select2_ar.js') }}" type="text/javascript" ></script>
        <!--[if IE]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.3/respond.min.js"></script>
           <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/ie.css') }}">
       <![endif]-->
    </head>
    <body>
        <header>
            <div class="container">
                <a href="{{ url('employer') }}"><img src="{{ asset('img/admin/logo.png') }}" class="img-responsive"/></a>
                <nav class="hideme">
                    <ul>
                        <li class="active"><a href="{{ url('employer') }}"><i class="fa fa-home"></i> {{trans('web.home')}}</a></li>
                        <li class='newsbtn'><a href="{{ url('employer/vacant') }}"><i class="fa fa-briefcase"></i> {{trans('web.vacancies')}}</a></li>
                    </ul>
                </nav>
                <button class="btn btn-default btn-menu hideMobile" data-toggle="collapse" data-target="#demo" ><span class="glyphicon glyphicon-menu-hamburger"></span></button>
            </div>
        </header>
        <div id="demo" class="collapse ">
            <div class="container">
                <nav  class="hideMobile">
                    <ul>
                        <li class="active"><a href="index.html"> {{trans('web.home')}}</a></li>
                        <li><span  style="cursor: pointer;"  data-toggle="collapse" data-target="#subMenu">{{trans('web.vacancies')}}</span></li>
                        <ol class="collapse" id="subMenu">
                            <li class="li"><a href="{{ url('employer/vacant') }}">عرض الشواغر</a></li>
                            <li  class="li"><a href="{{ url('employer/vacant/create') }}">أضافة شاغر</a></li>
                        </ol>
                    </ul>
                </nav>
                <nav class="hideme">
                    <ul>
                        <li><a href="{{ url('employer/vacant') }}">عرض الشواغر</a></li>
                        <li><a href="{{ url('employer/vacant/create') }}">أضافة شاغر</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="container" >
            @yield('content')
            @yield('scripts')
            @include('layouts.admin_footer')
