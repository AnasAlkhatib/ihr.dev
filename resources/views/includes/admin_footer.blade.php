<!--=== Footer ===-->
<div class="footer-v1">
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="margin-0">
                        {{ date("Y") }} &copy; {{trans('app.copyright')}}. <a
                                href="http://www.sto.com.sa/">sto.com.sa</a>
                    </p>
                </div>
                <!-- Social Links -->
                <div class="col-md-6">
                    <ul class="footer-socials list-inline margin-bottom-0">
                        <li><a href="https://www.linkedin.com/company/sto-international" class="tooltips"
                               target="_blank"
                               data-toggle="tooltip" data-placement="top"
                               title="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="https://www.facebook.com/stosaudi" class="tooltips" target="_blank"
                               data-toggle="tooltip" data-placement="top"
                               title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/sto_sa" class="tooltips" target="_blank" data-toggle="tooltip"
                               data-placement="top"
                               title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCFJi6F2rb7MadA_rgUh0fzg" class="tooltips"
                               target="_blank" data-toggle="tooltip" data-placement="top"
                               title="" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div>
    </div><!--/copyright-->
</div><!--=== End Footer ===-->