<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
@if ($locale == 'ar')
    <!-- Web Fonts -->
    <link rel="stylesheet" href="{{ asset('assets/font/droidarabickufi.css') }}">
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/style-rtl.css') }}">
@else
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endif
<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{ asset('assets/plugins/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/line-icons/line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/brand-buttons/brand-buttons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/brand-buttons/brand-buttons-inversed.css') }}">
<!-- CSS Page Style -->
<link rel="stylesheet" href="{{ asset('assets/css/pages/page_log_reg_v2.css') }}">
@if ($locale == 'ar')
    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/theme-colors/dark-red.css') }}" id="style_color">
    <!-- RTL Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/rtl.css') }}">
    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/custom-rtl.css') }}">
@else
    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme-colors/dark-red.css') }}" id="style_color">
    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endif


