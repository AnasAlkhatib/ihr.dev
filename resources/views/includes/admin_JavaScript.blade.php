<!-- JS Global Compulsory -->
<script src="{{ asset('assets/plugins/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- JS Implementing Plugins -->
<script src="{{ asset('assets/plugins/back-to-top.js') }}"></script>
<script src="{{ asset('assets/plugins/smoothScroll.js') }}"></script>
<script src="{{ asset('assets/plugins/counter/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/plugins/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.min.js') }}"></script>
@if ($locale == 'ar')
    <script src="{{ asset('assets/js/plugins/select2/select2_ar.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui.datepicker-ar.js') }}"></script>
@endif
<!-- JS Customization -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<!-- JS Page Level -->
<script src="{{ asset('assets/js/app.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initCounter();
        App.initScrollBar();
    });
</script>
