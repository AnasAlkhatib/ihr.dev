<!--=== Header ===-->
<div class="header">
    <div class="container">
        <!-- Logo -->
        <a class="logo" href="{{ url('/dashboard') }}">
            <img src="{{ asset('assets/img/logo1-default.png') }}" alt="{{ trans('app.sto')}}"/>
        </a>
        <!-- End Logo -->
        <!-- Topbar -->
        <div class="topbar">
            <ul class="loginbar pull-right">
                <li class="hoverSelector">
                    <i class="fa fa-caret-down"></i>
                    <i class="fa fa-user"></i>
                    <a>{{ Auth::guard('admin')->user()->name }}</a>
                    <ul class="languages hoverSelectorBlock">
                        <li>
                            <a href="{{ url('/dashboard/user/'.Auth::guard('admin')->user()->id).'/editProfile' }}"><i
                                        class="fa fa-user"></i> {{trans('app.personal_profile')}}</a>
                        </li>
                        <li>
                            <a href="{{ url('/dashboard/user/'.Auth::guard('admin')->user()->id).'/editPassword' }}"><i
                                        class="fa fa-lock"></i> {{trans('app.change_password')}}</a>
                        </li>
                        <li>
                            <a href="{{ url('/dashboard/logout') }}"><i
                                        class="fa fa-sign-out"></i> {{trans('app.logout')}}</a>
                        </li>
                    </ul>
                </li>
                <!-- language switcher-->
                <li class="topbar-devider "></li>
                <li><i class="fa fa-globe"></i><span>
                        <a class="locale {{ ($locale) == 'en' ? 'pull-right': '' }}"
                           href="#{{ ($locale) == 'ar' ? 'en': 'ar' }}">
                           @if ($locale == 'ar')
                                {{trans('app.english_language') }}
                            @elseif ($locale == 'en')
                                &nbsp;{{ trans('app.arabic_language') }}
                            @else
                                {{trans('app.english_language') }}
                            @endif
                        </a>
                    </span></li>
                <!-- /End language switcher-->
            </ul>
        </div>
        <!-- End Topbar -->
        <!-- Toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"></span>
        </button>
        <!-- End Toggle -->
    </div><!--/end container-->
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container">
            <ul class="nav navbar-nav col-lg-9 col-md-9 padding-left-0">
                <li class="active"><a href="{{ url('/dashboard') }}">{{trans('app.home')}}</a></li>
                @permission('list-users')
                <li><a href="{{ url('/dashboard/user') }}">{{trans('app.users')}}</a></li>
                @endpermission
                {{--@permission('list-seekers')--}}
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        {{trans('app.seekers')}}
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/dashboard/seeker') }}">{{trans('app.seekers_list')}}</a></li>
                        <li><a href="{{ url('/dashboard/favoriteSeeker') }}">{{trans('app.favorite_seekers_list')}}</a></li>
                    </ul>
                </li>
                {{--<li><a href="{{ url('/dashboard/seeker') }}">{{trans('app.seekers')}}</a></li>--}}
                {{--@endpermission--}}
                {{--@permission('list-clients')--}}
                @permission('list-employers')
                <li><a href="{{ url('/dashboard/client') }}">{{trans('app.clients')}}</a></li>
                @endpermission
                {{--@endpermission--}}
                {{--@permission('list-candidates')--}}
                <li><a href="{{ url('/dashboard/candidate') }}">{{trans('app.candidates')}}</a></li>
                {{--@endpermission--}}
                @permission('list-employers')
                <li><a href="{{ url('/dashboard/plan') }}">{{trans('app.ihr_plans')}}</a></li>
                @endpermission
                @permission('list-employers')
                <li><a href="{{ url('/dashboard/employer') }}">{{trans('app.employers')}}</a></li>
                @endpermission
                @permission('list-jobs')
                <li><a href="{{ url('/dashboard/vacant') }}">{{trans('app.vacancies')}}</a></li>
                @endpermission
                @permission('list-news')
                <li><a href="{{ url('/dashboard/news') }}">{{trans('app.news')}}</a></li>
                @endpermission

            </ul>
        </div><!--/end container-->
    </div><!--/navbar-collapse-->
</div><!--=== End Header ===-->

