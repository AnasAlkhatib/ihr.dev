<div class="col-md-3 md-margin-bottom-40">
    <div class="col-md-12 blog-border light-shadow">
        <div class="panel-heading overflow-h margin-bottom-10">
            <h2 class="panel-title heading-sm text-center">
                <i class="fa fa-user"></i>{{trans('app.employer_info')}}</h2>
        </div>
        <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
            <img class="img-responsive profile-img margin-bottom-20 rounded-x" src="{{$employerFileName}}"
                 alt=""></div>
        <div class="col-md-12 no-padding profile-blog margin-bottom-10">
            <h3 class="heading-xs font-size-12">
                {{trans('app.user_name')}}<span class="pull-right bold">{{Auth::guard('employer')->user()->name}}</span>
            </h3>
            <h3 class="heading-xs font-size-12">
                {{trans('app.last_login')}}<span
                        class="pull-right bold">{{ Auth::guard('employer')->user()->last_login }}</span>
            </h3>
            {{--<div class="heading-xs font-size-12 margin-top-20">--}}
                {{--<a href="{{url('employer/settings/profile') }}"><i class="fa fa-edit"></i> تعديل المعلومات</a>--}}
            {{--</div>--}}
            <a class="btn btn-block btn-u margin-top-20" href="{{url('employer/logout') }}">
                <i class="fa fa-sign-out margin-right-5"></i>{{trans('app.logout')}}</a>
        </div>
    </div>
</div><!--End Left Sidebar-->
