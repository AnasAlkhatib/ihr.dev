<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<!-- Web Fonts -->
@if ($locale == 'ar')
    <link rel="stylesheet" href="{{ asset('assets/font/droidarabickufi.css') }}">
@endif
<!-- CSS Global Compulsory -->
@if ($locale == 'ar')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/style-rtl.css') }}">
    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/headers/header-default-rtl.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/footers/footer-v1-rtl.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sky-forms-pro/skyforms/css/sky-forms-rtl.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms-rtl.css') }}">
    <!-- CSS Page Style -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/pages/profile-rtl.css') }}">
    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/theme-colors/dark-red.css') }}" id="style_color">
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/theme-skins/dark.css') }}">
    <!-- RTL Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/rtl.css') }}">
    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/css-rtl/custom-rtl.css') }}">
@else
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="{{ asset('assets/css/headers/header-default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/footers/footer-v1.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css') }}">
    <!-- CSS Page Style -->
    <link rel="stylesheet" href="{{ asset('assets/css/pages/profile.css') }}">
    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme-colors/dark-red.css') }}" id="style_color">
    <link rel="stylesheet" href="{{ asset('assets/css/theme-skins/dark.css') }}">
    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endif
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/css/datatables.css') }}">
<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{ asset('assets/plugins/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/line-icons/line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2-bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css') }}">
<!-- JS Global Compulsory -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery.form.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/js/dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/js/datatables_custom.js') }}"></script>


