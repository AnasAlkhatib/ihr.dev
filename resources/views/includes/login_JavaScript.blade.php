<!-- JS Global Compulsory -->
<script  src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- JS Implementing Plugins -->
<script src="{{ asset('assets/plugins/back-to-top.js') }}"></script>
<script src="{{ asset('assets/plugins/backstretch/jquery.backstretch.min.js') }}"></script>
<!-- JS Customization -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<!-- JS Page Level -->
<script src="{{ asset('assets/js/app.js') }}"></script>