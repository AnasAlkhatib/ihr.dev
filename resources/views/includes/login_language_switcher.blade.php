<div class="text-center checkbox margin-top-20">
    <p>
        <a class="locale" href="#{{ ($locale) == 'ar' ? 'en': 'ar' }}">
            @if ($locale == 'ar')
                {{trans('app.english_language')}}
            @elseif ($locale == 'en')
                {{ trans('app.arabic_language')}}
            @else
                {{trans('app.english_language') }}
            @endif
        </a>
    </p>
</div>
