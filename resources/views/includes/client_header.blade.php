<!--=== Header ===-->
<div class="header">
    <div class="container">
        <!-- Logo -->
        <a class="logo" href="{{ url('client') }}">
            <img src="{{ asset('assets/img/logo1-default.png') }}" alt="{{ trans('app.sto')}}"/>
        </a>
        <!-- End Logo -->
        <!-- Topbar -->
        <div class="topbar">
            <ul class="loginbar pull-right">
                <li class="hoverSelector">
                    <i class="fa fa-caret-down"></i>
                    <i class="fa fa-user"></i>
                    <a>{{ Auth::guard('client')->user()->name }}</a>
                    <ul class="languages hoverSelectorBlock">
                        <li>
                            <a href="{{ url('client/settings/profile') }}"><i
                                        class="fa fa-user"></i> {{trans('app.personal_profile')}}</a>
                        </li>
                        <li>
                            <a href="{{ url('client/settings/password') }}"><i
                                        class="fa fa-lock"></i> {{trans('app.change_password')}}</a>
                        </li>
                        <li>
                            <a href="{{ url('/client/logout') }}"><i
                                        class="fa fa-sign-out"></i> {{trans('app.logout')}}</a>
                        </li>
                    </ul>
                </li>
                <!-- language switcher-->
                <li class="topbar-devider "></li>
                <li><i class="fa fa-globe"></i><span>
                        <a class="locale {{ ($locale) == 'en' ? 'pull-right': '' }}"
                           href="#{{ ($locale) == 'ar' ? 'en': 'ar' }}">
                           @if ($locale == 'ar')
                                {{trans('app.english_language') }}
                            @elseif ($locale == 'en')
                                &nbsp;{{ trans('app.arabic_language') }}
                            @else
                                {{trans('app.english_language') }}
                            @endif
                        </a>
                    </span></li>
                <!-- /End language switcher-->
            </ul>
        </div>
        <!-- End Topbar -->
        <!-- Toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"></span>
        </button>
        <!-- End Toggle -->
    </div><!--/end container-->
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container">
            <ul class="nav navbar-nav col-lg-9 col-md-9 padding-left-0">
                <li class="active"><a href="{{ url('client') }}">{{trans('app.home')}}</a></li>
                <li><a href="{{ url('client/vacant') }}">{{trans('app.vacancies')}}</a></li>
                {{--<li><a href="{{ url('client/candidate') }}">{{trans('app.candidates')}}</a></li>--}}
            </ul>
        </div><!--/end container-->
    </div><!--/navbar-collapse-->
</div><!--=== End Header ===-->

