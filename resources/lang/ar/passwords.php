<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Password Reminder Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are the default lines which match reasons
      | that are given by the password broker for a password update attempt
      | has failed, such as for an invalid token or invalid new password.
      |
     */

    'password' => 'يجب أن لا يقل طول كلمة المرور عن ستة أحرف، كما يجب أن تتطابق مع حقل التأكيد',
    'user' => 'لم يتم العثور على أيّ حسابٍ بهذا العنوان الإلكتروني',
    'token' => 'رمز استعادة كلمة المرور الذي أدخلته غير صحيح .',
    'sent' => 'تم إرسال تفاصيل استعادة كلمة المرور الخاصة بك إلى بريدك الإلكتروني',
    'reset' => 'تم تغير كلمة المرور بنجاح',
    'subject' => ' اعادة تعيين كلمة المرور'
];
