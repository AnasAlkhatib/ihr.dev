<?php
return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | such as the size rules. Feel free to tweak each of these messages.
      |
     */
    'accepted' => 'يجب قبول الحقل :attribute',
    'active_url' => 'الحقل :attribute لا يُمثّل رابطًا صحيحًا',
    'hash' => ':attribute  لاتتطابق مع كلمة المرور.',
    'after' => 'يجب  ان يكون  حقل :attribute بعد  :date.',
    'alpha' => 'يجب أن لا يحتوي الحقل :attribute سوى على حروف',
    'alpha_dash' => 'يجب أن لا يحتوي الحقل :attribute على حروف، أرقام ومطّات.',
    'alpha_num' => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط',
    'array' => 'يجب أن يكون الحقل :attribute ًمصفوفة',
    'before' => 'يجب على الحقل :attribute أن يكون تاريخًا سابقًا للتاريخ :date.',
    'between' => [
        'numeric' => 'يجب أن تكون قيمة :attribute محصورة ما بين :min و :max.',
        'file' => 'يجب أن يكون حجم الملف :attribute محصورًا ما بين :min و :max كيلوبايت.',
        'string' => 'يجب أن يكون عدد حروف النّص :attribute محصورًا ما بين :min و :max',
        'array' => 'يجب أن يحتوي :attribute على عدد من العناصر محصورًا ما بين :min و :max',
    ],
    'boolean' => 'يجب أن تكون قيمة الحقل :attribute إما true أو false ',
    'confirmed' => 'حقل التأكيد غير مُطابق لحقل :attribute',
    'date' => 'حقل :attribute ليس تاريخًا صحيحًا',
    'date_format' => ':format لا يتوافق حقل :attribute مع الشكل ',
    'different' => 'يجب أن يكون الحقلان :attribute و :other مُختلفان',
    'digits' => 'يجب أن يحتوي حقل :attribute على :digits رقمًا/أرقام',
    'digits_between' => 'يجب أن يحتوي الحقل :attribute ما بين :min و :max رقمًا/أرقام ',
    'email' => 'يجب أن يكون :attribute عنوان بريد إلكتروني صحيح ',
    'exists' => ' :attribute غير موجود',
    'filled' => 'الحقل :attribute إجباري',
    'image' => 'يجب أن يكون حقل :attribute صورةً',
    'in' => 'مدخلات حقل :attribute غير صحيح',
    'integer' => 'يجب أن يكون حقل :attribute عددًا صحيحًا',
    'ip' => 'يجب أن يكون الحقل :attribute عنوان IP ذي بُنية صحيحة',
    'json' => 'يجب أن يكون الحقل :attribute نصآ من نوع JSON.',
    'max' => [
        'numeric' => 'يجب أن تكون قيمة الحقل :attribute أصغر من :max.',
        'file' => 'يجب أن يكون حجم الملف :attribute أصغر من :max كيلوبايت',
        'string' => 'يجب أن لا يتجاوز طول :attribute :max حروفٍ/حرفًا',
        'array' => 'يجب أن لا يحتوي الحقل :attribute على أكثر من :max عناصر/عنصر.',
    ],
    'mimes' => 'يجب أن يكون الحقل ملفًا من نوع : :values.',
    'min' => [
        'numeric' => 'يجب أن تكون قيمة الحقل :attribute أكبر من :min.',
        'file' => 'يجب أن يكون حجم الملف :attribute أكبر من :min كيلوبايت',
        'string' => 'يجب أن يكون طول  :attribute أكبر او يساوي :min حروفٍ/حرفًا',
        'array' => 'يجب أن يحتوي الحقل :attribute على الأقل على :min عُنصرًا/عناصر',
    ],
    'not_in' => 'الحقل :attribute لاغٍ',
    'numeric' => 'يجب على الحقل :attribute أن يكون رقمًا',
    'regex' => 'الصيغة التي ادخلتها في حقل :attribute غير صحيحة',
    'required' => 'حقل :attribute مطلوب.',
    'required_if' => 'الحقل :attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless' => 'الحقل :attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with' => 'الحقل :attribute إذا توفّر :values.',
    'required_with_all' => 'الحقل :attribute إذا توفّر :values.',
    'required_without' => 'الحقل :attribute إذا لم يتوفّر :values.',
    'required_without_all' => 'الحقل :attribute إذا لم يتوفّر :values.',
    'same' => 'يجب أن يتطابق الحقل :attribute مع :other',
    'size' => [
        'numeric' => 'يجب أن تكون قيمة :attribute أكبر من :size.',
        'file' => 'يجب أن يكون حجم الملف :attribute أكبر من :size كيلو بايت.',
        'string' => 'يجب أن يحتوي النص :attribute عن ما لا يقل عن  :size حرفٍ/أحرف.',
        'array' => 'يجب أن يحتوي الحقل :attribute عن ما لا يقل عن:min عنصرٍ/عناصر',
    ],
    'string' => 'يجب أن يكون حقل :attribute نصآ.',
    'timezone' => 'يجب أن يكون :attribute نطاقًا زمنيًا صحيحًا',
    'unique' => 'قيمة الحقل :attribute مُستخدمة من قبل',
    'url' => 'صيغة الرابط :attribute غير صحيحة',
    'period' => 'خطأ في ادخال التاريخ',
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'name' => 'الاسم',
        'username' => 'اسم المُستخدم',
        'email' => 'البريد الالكتروني',
        'first_name' => 'الاسم',
        'last_name' => 'اسم العائلة',
        'password' => 'كلمة المرور',
        'password_confirmation' => 'تأكيد كلمة المرور',
        'current_password' => 'كلمة المرور الحالية',
        'new_password' => 'كلمة المرور الجديدة',
        'city' => 'المدينة',
        'city_id' => 'المدينة',
        'country' => 'الدولة',
        'residence' => 'مكان الاقامة',
        'address' => 'العنوان',
        'phone' => 'الهاتف',
        'mobile' => 'الجوال',
        'age' => 'العمر',
        'sex' => 'الجنس',
        'gender' => 'الجنس',
        'day' => 'اليوم',
        'month' => 'الشهر',
        'year' => 'السنة',
        'hour' => 'ساعة',
        'minute' => 'دقيقة',
        'second' => 'ثانية',
        'title' => 'اللقب',
        'content' => 'المُحتوى',
        'description' => 'الوصف',
        'excerpt' => 'المُلخص',
        'date' => 'التاريخ',
        'time' => 'الوقت',
        'available' => 'مُتاح',
        'size' => 'الحجم',
        'full_name' => 'الاسم الرباعي',
        'nationality' => 'الجنسية',
        'national_num' => 'الرقم الوطني',
        'birth_date' => 'تاريخ الميلاد',
        'seeker_id' => 'المستخدم',
        'level' => 'المستوى',
        'vacant_id' => 'فرصة العمل',
        'start_date' => 'تاريخ البداية من العمل',
        'end_date' => 'تاريخ الانتهاء من العمل',
        'company_name' => 'اسم الشركة',
        'company_type' => 'طبيعة عمل الشركة',
        'job_title' => 'المسمى الوظيفي',
        'close_date' => 'تاريخ انتهاء التقديم',
        'company_industry' => 'مجال الشركة',
        'company_location' => 'موقع الشركة',
        'job_rules' => 'الدور الوظيفي',
        'job_status' => 'الحالة الوظيفية',
        'job_discription' => 'الوصف الوظيفي',
        'exp_level' => 'مستوى الخبرة',
        'edu_year' => 'تاريخ الحصول على الشهادة',
        'edu_name' => 'اسم المؤسسة التعليمية',
        'edu_level' => 'الدرجة العلمية',
        'edu_major' => 'التخصص',
        'course_name' => 'اسم الدورة',
        'course_date' => 'تاريخ الحصول على شهادةالدورة',
        'center_name' => 'اسم المركز التعليمي',
        'language_name' => 'اسم اللغة',
        'language_level' => 'مستوى اللغة',
        'period' => "فتره التنبيه",
        'device_token' => 'رقم تمييز الجوال',
        'device_model' => 'موديل الجوال',
        'device_type' => 'نوع نظام التشغيل',
        'device_version' => 'نسخة اصدار الجوال',
        'notification_active' => 'تفعيل الاشعارات',
        /* news lang */
        'news_title' => 'عنوان الخبر',
        'news_img' => 'صورة الخبر',
        'news_body' => 'نص الخبر',
        'role' => 'الصلاحية',
        'status' => 'الحالة',
        'tags' => 'الوسوم',
        'locale' => 'اللغة',
        'file' => 'الملف',
        /*
         * plan
         */
        'plan' => 'الخطة',
        'country_id' => 'الدولة',
        'website'=>'الموقع الالكتروني'
    ],
];
