<?php
/*
  |--------------------------------------------------------------------------
  | Employer Register Success Message arabic
  |--------------------------------------------------------------------------
 */
$content = "<div class ='text-center'>";
$content .= "<span>شكرا لوقتكم الثمين ...</span><br/>";
$content .= "<span>تم التسجيل بنجاح ...</span>";
$content .= "</div>";
$content .= "سيقوم احد المستشارين من الشركة الدولية للموارد البشرية بالاتصال بكم وتفعيل حسابكم .‬";
$content .= "<div class ='text-center'>";
$content .= "<br/>";
$content .= "شكرا لكم ...";
$content .= "<br/>";
$content .= "ادارة خدمة كبار العملاء";
$content .= "</div>";
return [
    'content' => $content
];


