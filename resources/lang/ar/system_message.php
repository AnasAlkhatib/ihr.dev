<?php
return [
    /*
    |--------------------------------------------------------------------------
    | System Messages Arabic Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'reset_password' => array(
        'message' => 'اضغط على الرابط التالي لاعادة تعين كلمة المرور',
        'subject' => 'رابط اعادة تعيين كلمة المرور'
    ),
    'employer_registered' => array(
        'message' => 'طلب تسجيل صاحب عمل جديد',
        'subject' => 'طلب تسجيل صاحب عمل جديد'
    ),
    'seeker_cv_pdf' => array(
        'message' => 'سيرة ذاتية مرسلة اليك من',
        'subject' => 'سيرة ذاتية مرسلة اليك',
        'find_attachment' => 'تحميل الملف المرفق للإطلاع على السيرة الذاتية',
    ),
    'candidate_profile_pdf' => array(
        'dear_customer' => 'عميلنا العزيز',
        'was_nominated' => 'تم ترشيح',
        'according_to_resume' => 'حسب بياناته في السيرة لكم',
        'by' => 'من قبل',
        'job_recruiter' => 'مسئول التوظيف',
        'contact_him' => 'للتواصل معه مباشرة',
        'subject' => 'مرشح من الشركة الدولية للموارد البشرية STO',
    )
];
