<?php
return [
    /*
    |--------------------------------------------------------------------------
    | System Messages English Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'reset_password' => array(
        'message' => 'Visit The Link Below To Reset Your Password',
        'subject' => 'Reset Password Link'
    ),
    'employer_registered' => array(
        'message' => 'New Employer Registered',
        'subject' => 'New Employer Registered'
    ),
    'seeker_cv_pdf' => array(
        'message' => 'Cv sent to You From ',
        'subject' => 'Cv sent to You',
        'find_attachment' => 'please find the attachment',
    ),
    'candidate_profile_pdf' => array(
        'message' => 'Candidate Profile sent to You From ',
        'subject' => 'Candidate Profile sent to You',
        'find_attachment' => 'please find the attachment',
    )
];
