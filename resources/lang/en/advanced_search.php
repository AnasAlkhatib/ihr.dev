<?php
return [
    /*
      |--------------------------------------------------------------------------
      | advanced search  Arabic Language Lines
      |--------------------------------------------------------------------------
     */
    '50_older' => 'Older than 50 years',
    '45_50' => '45 - 50 years old',
    '40_45' => '40 - 45 years old',
    '35_40' => '35 - 40 years old',
    '30_35' => '30 - 35 years old',
    '25_30' => '25 - 30 years old',
    '20_25' => '20 - 25 years old',
    '20_younger' => '20 years old or younger',
    /*
    * Years of Experience
    */
    'experience_less_one' => 'Less than one year',
    'experience_1_2' => '1 - 2 Years',
    'experience_2_5' => '2 - 5 Years',
    'experience_5_10' => '5 - 10 Years',
    'experience_more_10' => 'More than 10 Years',
];
