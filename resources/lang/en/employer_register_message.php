<?php
/*
  |--------------------------------------------------------------------------
  | Employer Register Success Message English
  |--------------------------------------------------------------------------
 */
$content = "<div class ='text-center'>";
$content .= "<span>Thank you for your precious time</span><br/>";
$content .= "<span>Successfully registered...</span>";
$content .= "</div>";
$content .= "a consultant from International Human resources Co. will contact you and activate your account .‬";
$content .= "<div class ='text-center'>";
$content .= "<br/>";
$content .= "Thank you...";
$content .= "<br/>";
$content .= "Customer Service Management Department";
$content .= "</div>";
return [
    'content' => $content
];



