<?php
use App\Area;
$factory->define(Area::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city,
        'name_en' => $faker->city,
    ];
});
