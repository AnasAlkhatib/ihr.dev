<?php
use App\Candidate;
$factory->define(Candidate::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'ref_num' => $faker->randomLetter,
        'seeker_id' => $faker->numberBetween(10, 100),
        'email' => $faker->email,
        'mobile' => $faker->phoneNumber,
        'birth_date' => $faker->date(),
        'nationality' => $faker->randomLetter,
        'national_num' => $faker->numberBetween(10, 100),
        'city_id' => $faker->numberBetween(1, 100),
        'address' => $faker->address
    ];
});
