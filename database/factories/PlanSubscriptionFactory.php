<?php

use App\Plans\Models\Plan;
use App\Employer;
use App\Plans\Models\PlanSubscription;

$factory->define(PlanSubscription::class, function (Faker\Generator $faker) {
    return [
        'employer_id' => factory(Employer::class)->create()->id,
        'plan_id' => factory(Plan::class)->create()->id,
        'name' => $faker->word
    ];
});