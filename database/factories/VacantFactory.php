<?php
use App\Vacant;
$factory->define(Vacant::class, function (Faker\Generator $faker) {
    return [
        'job_title' => $faker->jobTitle,
        'company_industry' => $faker->randomElement([1, 2, 3, 4, 5]),
        'company_location' => $faker->randomElement([1, 2, 3, 4]),
        'job_rules' => $faker->randomElement([1, 2, 3, 4, 6, 7]),
        'job_status' => $faker->randomElement(['full_time', 'part_time', 'freelance']),
        'job_discription' => $faker->sentence,
        'education' => $faker->randomElement([
            'high_school',
            'diploma',
            'bachelor',
            'high_diploma',
            'master',
            'doctorate'
        ]),
        'certification' => $faker->word,
        'exp_level' => $faker->randomElement([
            'exp_not_required',
            'trainer',
            'beginner',
            'mid',
            'management',
            'senior_management'
        ]),
        'exp_years' => $faker->numberBetween(1, 10),
        'skills' => $faker->word,
        'language' => $faker->randomElement(['arabic', 'english', 'french']),
        'gender' => $faker->randomElement(['male', 'female', 'both']),
        'user_id' => $faker->randomElement([1, 2, 3, 4, 6, 7]),
        'employer_id' => $faker->randomElement([1, 2, 3, 4, 6, 7]),
        'candidate_age' => $faker->numberBetween(18, 45),
        'candidate_address' => $faker->word,
        'ref_num' => $faker->word,
        'close_date' => $faker->dateTime(),
    ];
});
