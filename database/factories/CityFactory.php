<?php
use App\City;
$factory->define(City::class, function (Faker\Generator $faker) {
    return [
        'area_id' => $faker->randomDigit,
        'name' => $faker->city,
        'name_en' => $faker->city,
    ];
});
