<?php
use App\Client;
use Carbon\Carbon;
$factory->define(Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail(),
        'password' => bcrypt(123456),
        'job_title' => $faker->jobTitle(),
        'company_name' => $faker->company(),
        'company_type' => $faker->randomElement(['private_sector', 'public_sector']),
        'mobile' => $faker->phoneNumber(),
        'active' => 0,
        'last_login' => Carbon::now(),
    ];
});
