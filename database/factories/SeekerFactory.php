<?php
use Carbon\Carbon;

$factory->define(App\Seeker::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'email' => $faker->email,
        'mobile' => '050' . $faker->randomNumber(7),
        'password' => bcrypt('123456'),
        'activation_code' => $faker->word,
        'gender' => $faker->randomElement(['male', 'female']),
        'nationality' => $faker->randomElement(['Saudi', 'Jordan']),
        'national_num' => '101' . $faker->randomNumber(7),
        'birth_date' => $faker->date(),
        'city_id' => $faker->numberBetween(1, 90),
        'driving_licence' => $faker->randomElement([1, 0]),
        'owning_vehicle' => $faker->randomElement([1, 0]),
    ];
});