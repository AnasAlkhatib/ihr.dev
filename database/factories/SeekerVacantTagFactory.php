<?php
$factory->define(App\SeekerVacantTag::class, function (Faker\Generator $faker) {
    return [
        'seeker_id' => $faker->randomElement([1, 2, 3, 4]),
        'tags' => 'test1,test2,test3'
    ];
});
