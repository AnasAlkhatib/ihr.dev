<?php
$factory->define(App\Notification::class, function (Faker\Generator $faker) {
    return [
        'seeker_id' => $faker->randomElement([1, 2, 3, 4]),
        'device_token' => $faker->uuid,
        'device_type' => $faker->randomElement([1, 2]),
        'device_model' => $faker->randomElement(['iphone', 'android']),
        'device_version' => $faker->numberBetween(1, 20),
        'notification_active' => $faker->randomElement([0, 1]),
    ];
});
