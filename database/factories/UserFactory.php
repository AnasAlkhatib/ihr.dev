<?php
use App\User;
use Carbon\Carbon;
$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail(),
        'password' => bcrypt(123456),
        'last_login' => Carbon::now(),
    ];
});
