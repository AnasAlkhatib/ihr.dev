<?php
use App\Employer;
use Carbon\Carbon;
$factory->define(Employer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail(),
        'password' => bcrypt(123456),
        'job_title' => $faker->jobTitle(),
        'company_name' => $faker->company(),
        'company_type' => $faker->randomElement(['private_sector', 'public_sector']),
        'mobile' => $faker->phoneNumber(),
        'active' => 1,
        'last_login' => '2017-01-08 11:38:06',
    ];
});
