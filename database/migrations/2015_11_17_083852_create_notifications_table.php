<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("seeker_id")->index();
            $table->longtext('device_token');
            $table->enum("device_type", array ('1', '2'));
            $table->longtext('device_model');
            $table->string('device_version');
            $table->enum("notification_active", array ('0', '1'));
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
