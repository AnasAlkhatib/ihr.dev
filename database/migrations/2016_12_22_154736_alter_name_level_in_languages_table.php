<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AlterNameLevelInLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('languages', 'name')) {
            Schema::table('languages', function (Blueprint $table) {
                $table->renameColumn('name', 'language_name');
            });
        }
        if (Schema::hasColumn('languages', 'language_level')) {
            Schema::table('languages', function (Blueprint $table) {
                $table->renameColumn('level', 'language_level');
            });
        }
//        Schema::table('languages', function (Blueprint $table) {
//            $table->renameColumn('name', 'language_name');
//            $table->renameColumn('level', 'language_level');
//        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('languages', 'language_name')) {
            Schema::table('languages', function (Blueprint $table) {
                $table->renameColumn('language_name', 'name');
            });
        }
        if (Schema::hasColumn('languages', 'language_level')) {
            Schema::table('languages', function (Blueprint $table) {
                $table->renameColumn('language_level', 'level');
            });
        }
//        Schema::table('languages', function (Blueprint $table) {
//            $table->renameColumn('language_name', 'name');
//            $table->renameColumn('language_level', 'level');
//        });
    }
}
