<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddCountryIdToSeekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->integer("country_id")
                ->default(0)
                ->after('mobile');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('seekers', 'country_id')) {
            Schema::table('seekers', function (Blueprint $table) {
                $table->dropColumn('country_id');
            });
        }
    }
}
