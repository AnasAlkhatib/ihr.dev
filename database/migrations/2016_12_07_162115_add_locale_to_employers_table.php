<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddLocaleToEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function (Blueprint $table) {
            $table->enum('locale', ['ar', 'en'])->default('ar');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('employers', 'locale')) {
            Schema::table('employers', function (Blueprint $table) {
                $table->dropColumn('locale');
            });
        }
    }
}
