<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddClientIdAndVacantIdToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->integer('client_id')
                ->nullable()
                ->index()
                ->after('user_id');
            $table->integer('vacant_id')
                ->nullable()
                ->index()
                ->after('client_id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('candidates', 'client_id')) {
            Schema::table('candidates', function (Blueprint $table) {
                $table->dropColumn('client_id');
            });
        }
        if (Schema::hasColumn('candidates', 'vacant_id')) {
            Schema::table('candidates', function (Blueprint $table) {
                $table->dropColumn('vacant_id');
            });
        }
    }
}
