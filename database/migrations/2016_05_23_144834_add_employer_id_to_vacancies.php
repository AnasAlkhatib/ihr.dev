<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddEmployerIdToVacancies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancies', function (Blueprint $table) {
//            $table->integer('employer_id')
//                ->nullable()
//                ->after('recruiter_id');
            $table->renameColumn('recruiter_id', 'user_id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->dropColumn('employer_id');
            $table->renameColumn('user_id', 'recruiter_id')->unsigned();
        });
    }
}
