<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddCityNameEnglishColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function ($table) {
            $table
                ->string("name_en")
                ->nullable()
                ->after('name');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cities', 'name_en')) {
            Schema::table('cities', function (Blueprint $table) {
                $table->dropColumn('name_en');
            });
        }
    }
}
