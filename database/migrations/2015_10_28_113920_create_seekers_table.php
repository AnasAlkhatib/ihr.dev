<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateSeekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seekers', function (Blueprint $table) {
            $table->increments("ID");
            $table->string("full_name");
            $table->string("user_name")->unique();
            $table->string("password", 60);
            $table->string('activation_code');
            $table->integer('active')->default(0);
            $table->enum("gender", array('male', 'female'));
            $table->string("nationality");
            $table->date("birth_date");
            $table->string("national_num");
            $table->string("marital_status")->nullable();
            $table->integer("dependent_num")->nullable();
            $table->enum("driving_licence", array('yes', 'no'))->nullable();
            $table->enum("owning_vehicle", array('yes', 'no'))->nullable();
            $table->string("email")->unique();
            $table->string("mobile")->nullable();
            $table->integer("city_id");
            $table->string("address")->nullable();
            $table->longtext("skills")->nullable();
            $table->string("desired_job")->nullable();
            $table->string("desired_job_status")->nullable();
            $table->string("desired_salary")->nullable();
            $table->string("notice_period")->nullable();
            $table->string("cv_file_name")->nullable();
            $table->string("pic_file_name")->nullable();
            $table->timestamp('last_login')->nullable();
            $table->integer("education_level_id")->default(0);
          //  $table->integer("country_id")->default(0);
           // $table->integer("residence")->default(0);
            $table->nullableTimestamps();
            $table->rememberToken();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seekers');
    }
}
