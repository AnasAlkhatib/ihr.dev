<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateSeekerStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id')->unsigned();
            $table->integer('seeker_id')->unsigned();
            $table->integer('vacant_id');
            $table->string('status')->nullable();
            $table->string('note')->nullable();
            $table->longText('offer_vacancies')->nullable();
            $table->longText('required_documents')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seeker_statuses');
    }
}
