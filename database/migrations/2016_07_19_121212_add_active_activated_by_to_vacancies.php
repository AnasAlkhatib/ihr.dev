<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveActivatedByToVacancies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->boolean('active')->after('additional_prefered_info')->default(0);
            $table->integer('activated_by')->after('active')->nullable();
            $table->dateTime('activation_date')->after('activated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->dropColumn('active');
            $table->dropColumn('activated_by');
            $table->dropColumn('activation_date');
        });
    }
}