<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCandidatablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('candidatable_id');
            $table->string('candidatable_type');
            $table->integer('client_id');
            $table->integer('vacant_id');
            $table->string('status')->nullable();
            $table->string('notes')->nullable();
            $table->longText('offer_vacancies')->nullable();
            $table->longText('required_documents')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidatables');
    }
}
