<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments("ID");
            $table->integer("seeker_id")->index();
            $table->date("start_date"); 
            $table->date("end_date")->nullable();
            $table->string("company_name");
            $table->string("company_country")->nullable();
            $table->string("company_industry")->nullable();
            $table->string("job_rule")->nullable();
            $table->string("job_title");
            $table->longtext("job_description")->nullable();
            $table->longtext("leave_reason")->nullable();
            $table->string("last_salary")->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('experiences');
    }
}
