<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('educations', function (Blueprint $table) {
            $table->increments("ID");
            $table->integer("seeker_id")->index();
            $table->date("edu_year");
            $table->string("edu_name");
            $table->string("edu_level");
            $table->string("edu_major");
            $table->string("edu_grade")->nullable();
            $table->string("edu_country")->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('educations');
    }

}
