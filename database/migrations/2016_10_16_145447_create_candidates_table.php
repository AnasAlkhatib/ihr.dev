<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("seeker_id")->index();
            $table->integer("user_id")->index();
            $table->string('name');
            $table->string('email');
            $table->string("mobile");
            $table->date("birth_date");
            $table->string("nationality");
            $table->string("national_num");
            $table->integer("city_id");
            $table->string("address")->nullable();
            $table->string('status');
            $table->longText('notes')->nullable();
            $table->longText('offer_vacancies')->nullable();
            $table->longText('required_documents')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
