<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddRefNumToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->string('ref_num')
                ->nullable()
                ->after('id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('candidates', 'ref_num')) {
            Schema::table('candidates', function (Blueprint $table) {
                $table->dropColumn('ref_num');
            });
        }
    }
}
