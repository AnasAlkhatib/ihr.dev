<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddLocaleToSeekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->enum('locale', ['ar', 'en'])->default('ar');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('seekers', 'locale')) {
            Schema::table('seekers', function (Blueprint $table) {
                $table->dropColumn('locale');
            });
        }
    }
}
