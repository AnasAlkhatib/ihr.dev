<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateAppliedVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied_vacancies', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("seeker_id")->index();
            $table->integer('vacant_id')->index();
            $table->date("last_view_date")->nullable();
            $table->integer("view_num")->nullable();
            $table->string("last_view_recriuter")->nullable();
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applied_vacancies');
    }
}
