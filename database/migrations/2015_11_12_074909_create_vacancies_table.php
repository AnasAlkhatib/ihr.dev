<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('ID');
            $table->string("company_industry");
            $table->string("company_location");
            $table->string("job_title");
            $table->string("job_rules");
            $table->string("job_status");
            $table->longtext("job_discription");//@todo fix typo //
            $table->string("education");
            $table->string("certification");
            $table->string("exp_level");
            $table->string("exp_years");
            $table->longtext("skills");
            $table->string("language");
            $table->string("gender");
            $table->string("candidate_age");
            $table->string("candidate_address");
            $table->string("ref_num");
            $table->string("recruiter_id");
            $table->integer('employer_id');
            $table->date("close_date");
            $table->longtext("additional_vacant_info")->nullable();
            $table->longtext("additional_prefered_info")->nullable();//@todo fix typo //
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vacancies');
    }
}
