<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddResidenceToSeekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->integer("residence")
                ->default(0)
                ->after('country_id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('seekers', 'residence')) {
            Schema::table('seekers', function (Blueprint $table) {
                $table->dropColumn('residence');
            });
        }
    }
}

