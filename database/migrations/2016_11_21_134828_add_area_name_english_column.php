<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddAreaNameEnglishColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function ($table) {
            $table
                ->string("name_en")
                ->nullable()
                ->after('name');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('areas', 'name_en')) {
            Schema::table('areas', function (Blueprint $table) {
                $table->dropColumn('name_en');
            });
        }
    }
}
