<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capital')->nullable();
            $table->string('citizenship')->nullable();
            $table->char('country_code')->default('');
            $table->string('currency')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('currency_sub_unit')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('full_name')->nullable();
            $table->char('iso_3166_2')->default('');
            $table->char('iso_3166_3')->default('');
            $table->string('name')->nullable();
            $table->string("name_ar")->nullable();
            $table->char('region_code')->default('');
            $table->char('sub_region_code')->default('');
            $table->boolean('eea')->default(0);
            $table->string('calling_code')->nullable();
            $table->string('flag')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
