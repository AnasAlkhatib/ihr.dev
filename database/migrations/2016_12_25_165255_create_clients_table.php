<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('job_title');
            $table->string('company_name');
            $table->string('company_type');
            $table->string("mobile");
            $table->string('email');
            $table->string('password', 60);
            $table->enum('locale', ['ar', 'en'])->default('ar');
            $table->boolean('active')->default(0);
            $table->timestamp('last_login');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
