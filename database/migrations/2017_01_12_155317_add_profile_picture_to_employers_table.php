<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddProfilePictureToEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function (Blueprint $table) {
            $table->string("pic_file_name")
                ->after('active')
                ->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('employers', 'pic_file_name')) {
            Schema::table('employers', function (Blueprint $table) {
                $table->dropColumn('pic_file_name');
            });
        }
    }
}
