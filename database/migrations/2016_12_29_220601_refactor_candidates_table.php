<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class RefactorCandidatesTable
 */
class RefactorCandidatesTable extends Migration
{
    /**
     * @var string
     */
    protected $table = 'candidates';
    /**
     * @var array
     */
    protected $columns = [
        'user_id',
        'client_id',
        'vacant_id',
        'status',
        'notes',
        'offer_vacancies',
        'required_documents'
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->columns as $column) {
            if (Schema::hasColumn($this->table, $column)) {
                Schema::table($this->table, function (Blueprint $table) use ($column) {
                    $table->dropColumn($column);
                });
            }
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->integer("user_id");
            $table->integer("client_id");
            $table->integer("vacant_id");
            $table->string('status');
            $table->longText('notes');
            $table->longText('offer_vacancies');
            $table->longText('required_documents');
        });
    }
}
