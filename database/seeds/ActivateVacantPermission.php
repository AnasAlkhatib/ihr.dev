<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class ActivateVacantPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = Role::where('name', '=', 'SuperAdmin')->first();
        $activateVacancies = new Permission();
        $activateVacancies->name = 'activate-vacancies';
        $activateVacancies->display_name = 'Activate vacancies';
        $activateVacancies->save();

        $deactivateVacancies = new Permission();
        $deactivateVacancies->name = 'deactivate-vacancies';
        $deactivateVacancies->display_name = 'Deactivate vacancies';
        $deactivateVacancies->save();

        $superAdmin->attachPermission($activateVacancies);
        $superAdmin->attachPermission($deactivateVacancies);
    }
}
