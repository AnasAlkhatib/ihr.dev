<?php

use Illuminate\Database\Seeder;
use App\Employer;
use Faker\Factory as Faker;

class EmployersSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ar_JO');
        Employer::create([
            'name' => 'Employer',
            'email' => 'employer@admin.com',
            'job_title' => $faker->jobTitle,
            'company_name' => $faker->company,
            'mobile' => $faker->phoneNumber,
            'password' => Hash::make('employer'),
            'active' => 1,
        ]);

    }

}
