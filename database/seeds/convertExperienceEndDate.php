<?php

use Illuminate\Database\Seeder;
use App\Experience;

class convertExperienceEndDate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 0;
        $experience = Experience::all();
        foreach ($experience as $exp) {
            if ($exp->end_date == '0000-00-00') {
                $exp->end_date = NULL;
                $exp->save();
                $count++;
            }
        }
        \Log::info('Experience Convert:' . $count);
    }
}
