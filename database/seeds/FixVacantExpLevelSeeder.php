<?php
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Application;
Use App\Vacant;
/**
 * Class FixVacantExpLevelSeeder
 */
class FixVacantExpLevelSeeder extends Seeder
{
    /**
     * @var Application
     */
    protected $app;
    /**
     * FixVacantDataSeeder constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Run the Database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->action();
    }
    /**
     * Main seeder method
     */
    public function action()
    {
        $vacancies = Vacant::all();
        /* loop throw each vacant */
        foreach ($vacancies as $vacant) {
            $vacant->exp_level = $this->matchExpLevel($vacant->exp_level);
            $vacant->save();
        }
    }
    /**
     * Matcher method
     *
     * @param $expLevel
     * @return array|null|string
     */
    public function matchExpLevel($expLevel)
    {
        $this->app->setLocale('en');
        $expNotRequired = \Lang::get('exp_not_required');
        $trainer = \Lang::get('trainer');
        $beginner = \Lang::get('beginner');
        $mid = \Lang::get('mid');
        $management = \Lang::get('management');
        $seniorManagement = \Lang::get('senior_management');
        switch ($expLevel) {
            case $expLevel == 'لا يشترط الخبرة':
                $expLevel = $expNotRequired;
                break;
            case $expLevel == 'متدرب':
                $expLevel = $trainer;
                break;
            case $expLevel == 'مبتدئ':
                $expLevel = $beginner;
                break;
            case $expLevel == 'متوسط الخبرة':
                $expLevel = $mid;
                break;
            case $expLevel == 'إدارة':
                $expLevel = $management;
                break;
            case $expLevel == 'إدارة عليا':
                $expLevel = $seniorManagement;
                break;
            default:
                continue;
        }
        return $expLevel;
    }
}
