<?php

use Illuminate\Database\Seeder;
use App\Vacant;
use Carbon\Carbon;

class ActivateAllVacant extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vacancies = Vacant::all();
        foreach ($vacancies as $vacant) {
            $vacant->active = 1;
            $vacant->activated_by = 9;
            $vacant->activation_date = Carbon::now();
            $vacant->save();
        }
    }
}
