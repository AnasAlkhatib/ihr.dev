<?php

use Illuminate\Database\Seeder;
use App\News;
use Faker\Factory as Faker;

class NewsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ar_JO');
        for ($i = 0; $i < 50; $i++) {
            News::create([
                'news_title' => $faker->title,
                'news_body' => $faker->text,
                'news_posted' => 2
            ]);
        }
    }
}
