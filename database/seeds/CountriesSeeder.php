<?php
use Illuminate\Database\Seeder;
use App\Country;
class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        //Empty the countries table
        \DB::table('countries')->truncate();
//        //Get all of the countries
        $country_json = \File::get(storage_path() . "/country_all.json");
        $countries = collect(json_decode($country_json));
        // remove the id
        for ($i = 0; $i <= count($countries) - 1; $i++) {
            $country = new Country();
            $country->capital = $countries[$i]->capital;
            $country->citizenship = $countries[$i]->citizenship;
            $country->country_code = $countries[$i]->country_code;
            $country->currency = $countries[$i]->currency;
            $country->currency_code = $countries[$i]->currency_code;
            $country->currency_sub_unit = $countries[$i]->currency_sub_unit;
            $country->currency_symbol = $countries[$i]->currency_symbol;
            $country->full_name = $countries[$i]->full_name;
            $country->iso_3166_2 = $countries[$i]->iso_3166_2;
            $country->iso_3166_3 = $countries[$i]->iso_3166_3;
            $country->name = $countries[$i]->name;
            $country->name_ar = $countries[$i]->name_ar;
            $country->region_code = $countries[$i]->region_code;
            $country->sub_region_code = $countries[$i]->sub_region_code;
            $country->eea = $countries[$i]->eea;
            $country->calling_code = $countries[$i]->calling_code;
            $country->flag = $countries[$i]->flag;
            $country->save();
        }
    }
}
