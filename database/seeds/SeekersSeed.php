<?php

use Illuminate\Database\Seeder;
use App\Seeker;

class SeekersSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void city_id
     */
    public function run()
    {
        $ahmad = Seeker::create([
            'full_name' => 'احمد حسن سعيد الطراونة',
            'user_name' => 'ahmad',
            'email' => 'ahmad@sto.com.sa',
            'birth_date' => '1983-01-25',
            'gender' => 'male',
            'password' => Hash::make('123456'),
            'city_id' => 1,
        ]);

        $rana = Seeker::create([
            'full_name' => 'رنا',
            'user_name' => 'rana',
            'email' => 'rana@sto.com.sa',
            'birth_date' => '1983-01-25',
            'gender' => 'female',
            'password' => Hash::make('123456'),
            'city_id' => 2,
        ]);

        $areej = Seeker::create([
            'full_name' => 'اريج',
            'user_name' => 'areej',
            'email' => 'areej@sto.com.sa',
            'birth_date' => '1983-01-25',
            'gender' => 'female',
            'password' => Hash::make('123456'),
            'city_id' => 3,
        ]);
    }

}
