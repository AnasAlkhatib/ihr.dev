<?php
use Illuminate\Database\Seeder;
use App\JobRole;
class JobRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('job_roles')->truncate();
        $jobRolesJson = \File::get(storage_path() . "/job_roles.json");
        $jobRoles = collect(json_decode($jobRolesJson));
        for ($i = 0; $i <= count($jobRoles) - 1; $i++) {
            $jobRole = new JobRole();
            $jobRole->name = $jobRoles[$i]->name;
            $jobRole->name_en = $jobRoles[$i]->name_en;
            $jobRole->save();
        }
    }
}
