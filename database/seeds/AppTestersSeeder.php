<?php
use Illuminate\Database\Seeder;
use App\User;
use App\Seeker;
use App\Employer;
use App\Client;
use Faker\Factory as Faker;
/**
 * Class AppTestersSeeder
 */
class AppTestersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App::environment('local', 'testing')) {
            $this->handle();
        }
    }
    /**
     *
     */
    public function handle()
    {
        $array = $this->testersArray();
        $userUpdateCounter = 0;
        $userCreateCounter = 0;
        $seekerCreateCounter = 0;
        $seekerUpdateCounter = 0;
        $employerCreateCounter = 0;
        $employerUpdateCounter = 0;
        $clientCreateCounter = 0;
        $clientUpdateCounter = 0;
        /* empty tables */
        // $this->emptyTable('users');
        // $this->emptyTable('seekers');
        // $this->emptyTable('employers');
//        $this->emptyTable('clients');
        // $this->emptyTable('role_user');
        for ($i = 0; $i <= count($array) - 1; $i++) {
            // create User
            $userModel = App\User::class;
            if ($this->modelExist($userModel, $array[$i]['email'])) {
                $this->modelResetPassword($userModel, $array[$i]['email'], $array[$i]['password']);
                $userUpdateCounter++;
            } else {
                $this->makeUser($array[$i]['email'], $array[$i]['password']);
                $userCreateCounter++;
            }
            // create Seeker
            $seekerModel = App\Seeker::class;
            if ($this->modelExist($seekerModel, $array[$i]['email'])) {
                $this->modelResetPassword($seekerModel, $array[$i]['email'], $array[$i]['password']);
                $seekerUpdateCounter++;
            } else {
                $this->makeSeeker($array[$i]['email'], $array[$i]['password']);
                $seekerCreateCounter++;
            }
            // create Employer
            $employerModel = App\Employer::class;
            if ($this->modelExist($employerModel, $array[$i]['email'])) {
                $this->modelResetPassword($employerModel, $array[$i]['email'], $array[$i]['password']);
                $employerUpdateCounter++;
            } else {
                $this->makeEmployer($array[$i]['email'], $array[$i]['password']);
                $employerCreateCounter++;
            }
            // create Client
            $clientModel = App\Client::class;
            if ($this->modelExist($clientModel, $array[$i]['email'])) {
                $this->modelResetPassword($clientModel, $array[$i]['email'], $array[$i]['password']);
                $clientUpdateCounter++;
            } else {
                $this->makeClient($array[$i]['email'], $array[$i]['password']);
                $clientCreateCounter++;
            }
        }
        \Log::info('Users Created:' . $userCreateCounter);
        \Log::info('Users Updated:' . $userUpdateCounter);
        \Log::info('Seekers Created:' . $seekerCreateCounter);
        \Log::info('Seekers Updated:' . $seekerUpdateCounter);
        \Log::info('Employers Created:' . $employerCreateCounter);
        \Log::info('Employers Updated:' . $employerUpdateCounter);
        \Log::info('Clients Created:' . $clientCreateCounter);
        \Log::info('Clients Updated:' . $clientUpdateCounter);
    }
    /**
     * @return array
     */
    public function testersArray()
    {
        $array = [
            ['email' => 'ali@sto.com.sa', 'password' => 'ali12345'],
            ['email' => 'fadi@sto.com.sa', 'password' => 'fadi1234'],
            ['email' => 'rasha@sto.com.sa', 'password' => 'rasha123'],
            ['email' => 'nohr@sto.com.sa', 'password' => 'nouf1234'],
            ['email' => 'hr@sto.com.sa', 'password' => 'amal1234'],
            ['email' => 'hrs@sto.com.sa', 'password' => 'nuha1234'],
            ['email' => 'Rayan@sto.com.sa', 'password' => 'rayan123'],
            ['email' => 'ahr@sto.com.sa', 'password' => 'abedalaziz1'],
            ['email' => 'aiman@sto.com.sa', 'password' => 'ayman123'],
            ['email' => 'i.alwi@sto.com.sa', 'password' => 'ibraheem123'],
            ['email' => 'a.alwi@sto.com.sa', 'password' => 'ahmad1234'],
            ['email' => 'wegdan@sto.com.sa', 'password' => 'wegdan123'],
            ['email' => 'alashawi@sto.com.sa', 'password' => 'moh12345'],
            ['email' => 'suhaibani@sto.com.sa', 'password' => 'suhaibani123'],
            ['email' => 'azam@sto.com.sa', 'password' => 'azam1234'],
            ['email' => 'mohammed@sto.com.sa', 'password' => 'mohammed123'],
            ['email' => 'ahoud@sto.com.sa', 'password' => 'ahoud1234'],
            ['email' => 'dalal@sto.com.sa', 'password' => 'dalal123'],
            ['email' => 'johr@sto.com.sa', 'password' => 'johr1234'],
            ['email' => 'job@sto.com.sa', 'password' => 'sara1234'],
            ['email' => 'm.alqarne@sto.com.sa', 'password' => 'alqarne123'],
            ['email' => 'fouad@sto.com.sa', 'password' => 'fouad123'],
        ];
        return $array;
    }
    /**
     * @param $table
     */
    public function emptyTable($table)
    {
        \DB::table($table)->truncate();
    }
    /**
     * @param $email
     * @param $password
     */
    public function makeUser($email, $password)
    {
        $user = new User();
        $user->name = $this->createName(App\User::class, $email, 'name');
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();
        $user->roles()->sync([1]);
    }
    /** create new Seeker
     * @param $email
     * @param $password
     */
    public function makeSeeker($email, $password)
    {
        $faker = Faker::create('ar_SA');
        $seeker = new Seeker();
        $seeker->full_name = $this->createName(App\Seeker::class, $email, 'full_name');
        $seeker->email = $email;
        $seeker->mobile = $faker->phoneNumber;
        $seeker->birth_date = $faker->randomElement(['1991-04-16', '1991-05-22', '1991-09-07', '1991-07-23']);
        $seeker->nationality = 'السعودية';
        $seeker->city_id = $faker->numberBetween(1, 90);
        $seeker->national_num = trim('10' . $faker->randomNumber(8));
        $seeker->driving_licence = $faker->randomElement([1, 0]);
        $seeker->owning_vehicle = $faker->randomElement([1, 0]);
        $seeker->active = 1;
        $seeker->password = bcrypt($password);
        $seeker->save();
    }
    /** create new employer
     * @param $email
     * @param $password
     */
    public function makeEmployer($email, $password)
    {
        $faker = Faker::create('ar_SA');
        $employer = new Employer();
        $employer->name = $this->createName(App\Employer::class, $email, 'name');
        $employer->active = 1;
        $employer->email = $email;
        $employer->job_title = $faker->randomElement(['مدير اداري', 'مسؤول توظيف', 'محاسب', 'مدير تنفيذي']);
        $employer->company_name = $faker->company;
        $employer->company_type = trim($faker->randomElement([
            'private_sector',
            'public_sector',
            'non_profit_organization',
            'recruitment_agency'
        ]));
        $employer->mobile = $faker->phoneNumber;
        $employer->password = bcrypt($password);
        $employer->save();
    }
    /** create new Client
     * @param $email
     * @param $password
     */
    public function makeClient($email, $password)
    {
        $faker = Faker::create('ar_SA');
        $client = new Client();
        $client->name = $this->createName(App\Client::class, $email, 'name');
        $client->active = 1;
        $client->email = $email;
        $client->job_title = $faker->randomElement(['مدير اداري', 'مسؤول توظيف', 'محاسب', 'مدير تنفيذي']);
        $client->company_name = $faker->company;
        $client->company_type = trim($faker->randomElement([
            'private_sector',
            'public_sector',
            'non_profit_organization',
            'recruitment_agency'
        ]));
        $client->mobile = $faker->phoneNumber;
        $client->password = bcrypt($password);
        $client->save();
    }
    /**
     * @param $model
     * @param $email
     * @return mixed
     */
    public function modelExist($model, $email)
    {
        return $model::where('email', '=', $email)->first();
    }
    /**
     * @param $model
     * @param $email
     * @param $password
     * @return mixed
     */
    public function modelResetPassword($model, $email, $password)
    {
        $model::where('email', '=', $email)
            ->update(['password' => bcrypt($password)]);
    }
    /**
     * @param $model
     * @param $email
     * @param $field
     */
    public function createName($model, $email, $field)
    {
        $name_array = explode('@', $email);
        $name = $name_array[0];
        if (!$this->nameIsTaken($model, $field, $name)) {
            return $name;
        } else {
            $faker = Faker::create();
            return $faker->name;
        }
    }
    /**
     * @param $model
     * @param $field
     * @param $name
     * @return mixed
     */
    function nameIsTaken($model, $field, $name)
    {
        $data = $model::where($field, '=', $name);
        return $data->count();
    }
}
