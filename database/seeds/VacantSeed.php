<?php

use Illuminate\Database\Seeder;
use App\Vacant;
use Faker\Factory as Faker;

class VacantSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('ar_JO');
        for ($i = 0; $i < 5; $i++) {
            Vacant::create([
                'company_industry' => $faker->randomElement(['الطبي', 'التسويق', 'التعليم', 'الصناعة', 'البنوك']),
                'company_location' => $faker->randomElement(['الشرقية', 'الغربية', 'الشمالية', 'الجنوبية']),
                'job_title' => $faker->randomElement(['مبرمج', 'خدمة عملاء', 'مبيعات هاتفية', 'محصل']),
                'job_rules' => $faker->randomElement(['مدير تنفيذي', 'مدير', 'مشرف']),
                'job_status' => $faker->randomElement(['دوام كامل', 'دوام جزئي', 'عمل عن بعد']),
                'gender' => $faker->randomElement(['male', 'female', 'both']),
                'job_discription' => $faker->text,
                'education' => $faker->randomElement(['ثانويه عامة', 'بكالوريوس', 'دبلوم', 'ماجستير']),
                'certification' => $faker->randomElement(['شهادة شبكات', 'ICDL شهادة', 'شهادة مدقق مالي ']),
                'exp_level' => $faker->randomElement(['مبتدئ', 'متوسط', 'محترف']),
                'exp_years' => $faker->randomElement(['0', '1', '2', '3', '4', '5']),
                'skills' => $faker->text,
                'user_id' => 1
            ]);

        }

        for ($i = 0; $i < 5; $i++) {
            Vacant::create([
                'company_industry' => $faker->randomElement(['الطبي', 'التسويق', 'التعليم', 'الصناعة', 'البنوك']),
                'company_location' => $faker->randomElement(['الشرقية', 'الغربية', 'الشمالية', 'الجنوبية']),
                'job_title' => $faker->randomElement(['مبرمج', 'خدمة عملاء', 'مبيعات هاتفية', 'محصل']),
                'job_rules' => $faker->randomElement(['مدير تنفيذي', 'مدير', 'مشرف']),
                'job_status' => $faker->randomElement(['دوام كامل', 'دوام جزئي', 'عمل عن بعد']),
                'gender' => $faker->randomElement(['male', 'female', 'both']),
                'job_discription' => $faker->text,
                'education' => $faker->randomElement(['ثانويه عامة', 'بكالوريوس', 'دبلوم', 'ماجستير']),
                'certification' => $faker->randomElement(['شهادة شبكات', 'ICDL شهادة', 'شهادة مدقق مالي ']),
                'exp_level' => $faker->randomElement(['مبتدئ', 'متوسط', 'محترف']),
                'exp_years' => $faker->randomElement(['0', '1', '2', '3', '4', '5']),
                'skills' => $faker->text,
                'employer_id' => 1
            ]);

        }
        for ($i = 0; $i < 5; $i++) {
            Vacant::create([
                'company_industry' => $faker->randomElement(['الطبي', 'التسويق', 'التعليم', 'الصناعة', 'البنوك']),
                'company_location' => $faker->randomElement(['الشرقية', 'الغربية', 'الشمالية', 'الجنوبية']),
                'job_title' => $faker->randomElement(['مبرمج', 'خدمة عملاء', 'مبيعات هاتفية', 'محصل']),
                'job_rules' => $faker->randomElement(['مدير تنفيذي', 'مدير', 'مشرف']),
                'job_status' => $faker->randomElement(['دوام كامل', 'دوام جزئي', 'عمل عن بعد']),
                'gender' => $faker->randomElement(['male', 'female', 'both']),
                'job_discription' => $faker->text,
                'education' => $faker->randomElement(['ثانويه عامة', 'بكالوريوس', 'دبلوم', 'ماجستير']),
                'certification' => $faker->randomElement(['شهادة شبكات', 'ICDL شهادة', 'شهادة مدقق مالي ']),
                'exp_level' => $faker->randomElement(['مبتدئ', 'متوسط', 'محترف']),
                'exp_years' => $faker->randomElement(['0', '1', '2', '3', '4', '5']),
                'skills' => $faker->text,
                'employer_id' => 2
            ]);

        }
    }

}
