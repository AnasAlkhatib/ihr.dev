<?php
use Illuminate\Database\Seeder;
use App\CompanyIndustry;
class CompanyIndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('company_industries')->truncate();
        $companyIndustryJson = \File::get(storage_path() . "/company_industries.json");
        $companyIndustries = collect(json_decode($companyIndustryJson));
        for ($i = 0; $i <= count($companyIndustries) - 1; $i++) {
            $companyIndustry = new CompanyIndustry();
            $companyIndustry->name = $companyIndustries[$i]->name;
            $companyIndustry->name_en = $companyIndustries[$i]->name_en;
            $companyIndustry->save();
        }
    }
}