<?php

use Illuminate\Database\Seeder;
use App\Course;
use Faker\Factory as Faker;

class CoursesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ar_JO');
        for ($i = 0; $i < 50; $i++) {
            Course::create([
                'seeker_id' => $faker->numberBetween(1, 100),
                'course_name' => $faker->company,
                'course_date' => $faker->date()
               
            ]);
        }
    }
}
