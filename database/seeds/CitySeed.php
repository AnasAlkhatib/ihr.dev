<?php

use Illuminate\Database\Seeder;
use App\City;

class CitySeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $cityJson = File::get(storage_path() . "/city.json");
        $city = json_decode($cityJson);
        foreach ($city as $object) {
            City::create([ 'area_id' => $object->area_id, 'name' => $object->city_name]);
        }
    }

}
