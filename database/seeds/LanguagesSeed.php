<?php

use Illuminate\Database\Seeder;
use App\Language;
use Faker\Factory as Faker;

class LanguagesSeed extends Seeder {
    
 
    public function run() {
        $faker = Faker::create('ar_JO');
        for ($i = 0; $i < 50; $i++) {
            Language::create([
                'seeker_id' => $faker->numberBetween(1, 100),
                'name' => $faker->randomElement(['arabic', 'english']),
                'level' => $faker->randomElement(['biggner', 'middle', 'profishinal']),
            ]);
        }
    }

}
