<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->truncate();
        
        $SuperAdmin = User::create([
            'name' => 'SuperAdmin',
            'email' => 'super@admin.com',
            'password' => Hash::make('super'),
        ]);
        $SuperAdmin->attachRole(1);

        $Admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
        ]);
        $Admin->attachRole(2);

        $Recruiter = User::create([
            'name' => 'Recruiter',
            'email' => 'demo@demo.com',
            'password' => Hash::make('demo'),
        ]);
        $Recruiter->attachRole(3);

        $Publisher = User::create([
            'name' => 'Publisher',
            'email' => 'demo2@demo.com',
            'password' => Hash::make('demo2'),
        ]);
        $Publisher->attachRole(4);
    }

}
