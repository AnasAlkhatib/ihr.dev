<?php

use Illuminate\Database\Seeder;
use App\Experience;
use Faker\Factory as Faker;
class ExperiencesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = Faker::create('ar_JO');
        for ($i = 0; $i < 50; $i++) {
            Experience::create([
                'seeker_id' => $faker->numberBetween(1, 100),
                'start_date' => $faker->date(),
                'end_date' => $faker->date(),
                'company_name' => $faker->company,
                'job_title' => $faker->text
            ]);
        }
    }
}
