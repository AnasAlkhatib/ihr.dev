<?php
use Illuminate\Database\Seeder;
use App\Area;
class AreaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('areas')->truncate();
        $areas = array(
            array('name' => 'الشمالية', 'name_en' => 'Northern'),
            array('name' => 'الشرقية', 'name_en' => 'Eastern'),
            array('name' => 'الوسطى', 'name_en' => 'Central'),
            array('name' => 'الغربية', 'name_en' => 'Western'),
            array('name' => 'الجنوبية', 'name_en' => 'Southern'),
            array('name' => 'القصيم', 'name_en' => 'Alqaseem'),
        );
        for ($i = 0; $i <= count($areas) - 1; $i++) {
            $area = new Area();
            $area->name = trim($areas[$i]['name']);
            $area->name_en = trim($areas[$i]['name_en']);
            $area->save();
        }
    }
}
