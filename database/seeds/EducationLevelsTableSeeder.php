<?php

use Illuminate\Database\Seeder;
use App\EducationLevels;


class EducationLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level_no_education=new EducationLevels();
        $level_no_education->code='no_education';
        $level_no_education->level=0;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='high_school';
        $level_no_education->level=1;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='diploma';
        $level_no_education->level=2;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='high_diploma';
        $level_no_education->level=3;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='bachelor_degree';
        $level_no_education->level=4;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='master';
        $level_no_education->level=5;
        $level_no_education->save();

        $level_no_education=new EducationLevels();
        $level_no_education->code='phd';
        $level_no_education->level=6;
        $level_no_education->save();
    }

}
