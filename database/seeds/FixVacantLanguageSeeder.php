<?php
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Application;
Use App\Vacant;
/**
 * Class FixVacantDataSeeder
 */
class FixVacantLanguageSeeder extends Seeder
{
    /**
     * @var Application
     */
    protected $app;
    /**
     * FixVacantDataSeeder constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Run the Database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->action();
    }
    /**
     *
     */
    public function action()
    {
        $vacancies = Vacant::all();
        /* loop throw each vacant */
        foreach ($vacancies as $vacant) {
            $vacantLanguage = $this->getVacantLanguage($vacant);
            $vacantLanguageArray = $this->vacantLanguageArray($vacantLanguage);
            $this->matchArray($vacantLanguageArray);
            $vacant->language = $this->matchArray($vacantLanguageArray);
            $vacant->save();
        }
    }
    /**
     * Get vacant language value
     *
     * @param Vacant $vacant
     * @return string
     */
    public function getVacantLanguage($vacant)
    {
        return (string)$vacant->language;
    }
    /**
     * Convert Vacant Language to array
     *
     * @param $vacantLanguage
     * @return array
     */
    public function vacantLanguageArray($vacantLanguage)
    {
        return explode(',', $vacantLanguage);
    }
    /**
     * Convert Vacant Language to array
     *
     * @param $vacantLanguageArray
     * @return array
     */
    public function matchArray($vacantLanguageArray)
    {
        $this->app->setLocale('en');
        $arabic = \Lang::get('arabic');
        $english = \Lang::get('english');
        $french = \Lang::get('french');
        $array = [];
        foreach ($vacantLanguageArray as $language) {
            switch ($language) {
                case $language == 'العربية':
                    if (!in_array($arabic, $vacantLanguageArray)) {
                        $array[] = $arabic;
                    }
                    break;
                case $language == 'الانجليزية':
                    if (!in_array($english, $vacantLanguageArray)) {
                        $array[] = $english;
                    }
                    break;
                case $language == 'الفرنسية':
                    if (!in_array($french, $vacantLanguageArray)) {
                        $array[] = $french;
                    }
                    break;
                default:
                    $array[] = $language;
            }
        }
        /* Convert Array to String */
        return implode(',', $array);
    }
}
