<?php
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Application;
Use App\Vacant;
/**
 * Class FixVacantDataSeeder
 */
class FixVacantJobStatusSeeder extends Seeder
{
    /**
     * @var Application
     */
    protected $app;
    /**
     * FixVacantDataSeeder constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Run the Database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->action();
    }
    /**
     *
     */
    public function action()
    {
        $vacancies = Vacant::all();
        /* loop throw each vacant */
        foreach ($vacancies as $vacant) {
            $vacant->job_status = $this->matchStatus($vacant->job_status);
            $vacant->save();
        }
    }
    public function matchStatus($status)
    {
        $this->app->setLocale('en');
        $full_time = \Lang::get('full_time');
        $part_time = \Lang::get('part_time');
        $freelance = \Lang::get('freelance');
        switch ($status) {
            case $status == 'دوام كامل':
                $status = $full_time;
                break;
            case $status == 'دوام جزئي':
                $status = $part_time;
                break;
            case $status == 'عمل عن بعد':
                $status = $freelance;
                break;
            default:
                continue;
        }
        return $status;
    }
}
