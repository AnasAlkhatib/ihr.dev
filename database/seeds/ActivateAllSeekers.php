<?php
use Illuminate\Database\Seeder;
use App\Seeker;
class ActivateAllSeekers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getSeekers();
    }
    public function getSeekers()
    {
        $seekers = Seeker::where('active', '=', 0)->get();
        foreach ($seekers as $seeker) {
            $seeker->active = 1;
            $seeker->activation_code = '';
            $seeker->save();
        }
    }
}
