<?php
use Illuminate\Database\Seeder;
use App\Settings;

class DefaultSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* clean DB table before seed */
        \DB::table('settings')->truncate();
        $settings = new Settings();
        $settings->key = 'sdk_license';
        $settings->value = 'LEYZ-YWI6-IVKQ-YITU';
        $settings->save();
        /* ihr_tube_host */
        $settings = new Settings();
        $settings->key = 'ihr_tube_host';
        $settings->value = '34.203.8.148';
        $settings->save();
    }
}
