<?php
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Application;
Use App\Vacant;
/**
 * Class FixVacantEducationSeeder
 */
class FixVacantEducationSeeder extends Seeder
{
    /**
     * @var Application
     */
    protected $app;
    /**
     * FixVacantEducationSeeder constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Run the Database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->action();
    }
    /**
     *
     */
    public function action()
    {
        $vacancies = Vacant::all();
        /* loop throw each vacant */
        foreach ($vacancies as $vacant) {
            $vacant->education = $this->matchEducation($vacant->education);
            $vacant->save();
        }

    }
    public function matchEducation($education)
    {
        /* set app language to english */
        $this->app->setLocale('en');
        /* ini vars */
        $high_school = \Lang::get('high_school');
        $diploma = \Lang::get('diploma');
        $bachelor = \Lang::get('bachelor');
        $high_diploma = \Lang::get('high_diploma');
        $master = \Lang::get('master');
        $doctorate = \Lang::get('doctorate');
        /* switch statement for each case */
        switch ($education) {
            case $education == 'ثانوية عامة':
                $education = $high_school;
                break;
            case $education == 'دبلوم':
                $education = $diploma;
                break;
            case $education == 'بكالوريوس':
                $education = $bachelor;
                break;
            case $education == 'دبلوم عالي':
                $education = $high_diploma;
                break;
            case $education == 'ماجستير':
                $education = $master;
                break;
            case $education == 'دكتوراة':
                $education = $doctorate;
                break;
            default:
                continue;
        }
        /* proper education must updated */
        return $education;
    }
}
