<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Model::unguard();
//        $this->call('SeekersSeed');
//        $this->call('LanguagesSeed');
//        $this->call('ExperiencesSeed');
//        $this->call('CoursesSeed');
//        $this->call('VacantSeed');
//        $this->call('AreaSeed');//should be seeded
//        $this->call('CitySeed');//should be seeded
//        $this->call('NewsSeed');
//        $this->call('RolesPermissionsSeed');//should be seeded
//        $this->call('UsersSeed');//should be seeded
//        $this->call('EmployersSeed');//should be seeded
//        $this->call('ActivateVacantPermission');//should not seeded
//        $this->call('ActivateAllVacant');//should not seeded
//        $this->call('FixVacantLanguageSeeder');//should not seeded
//        $this->call('FixVacantJobStatusSeeder');//should not seeded
//        $this->call('FixVacantEducationSeeder');//should not seeded
//        $this->call('FixVacantExpLevelSeeder');//should not seeded
//        $this->call('ActivateAllSeekers');//should not seeded
//        $this->call('AreaSeed');//should be seeded
//        $this->call('NotificationsSeeder');//should  seeded
//        $this->call('CountriesSeeder');//should  seeded
//        $this->call('JobRolesSeeder');//should  seeded
//        $this->call('CompanyIndustrySeeder');//should  seeded
////      $this->call('AppTestersSeeder');//should  seeded
//        $this->call('VacantLookupFixSeeder');//should  seeded
        //$this->call('PlansSeeder');//should  seeded
        $this->call('EducationLevelsTableSeeder');//should  seeded

    }
}
