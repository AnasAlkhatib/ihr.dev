<?php
use Illuminate\Database\Seeder;
use App\Plans\Models\Plan;
use App\Plans\Models\PlanFeature;
class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->truncate();
        \DB::table('plan_features')->truncate();
//        $plan1 = Plan::create([
//            'name' => 'Demo Account',
//            'description' => 'Demo Account Plan',
//            'price' => 0.00,
//            'interval' => 'week',
//            'interval_count' => 1,
//            'trial_period_days' => 0,
//            'sort_order' => 1,
//        ]);
//        $plan1->features()->saveMany([
//            new PlanFeature(['code' => 'vacants_listings', 'value' => 30, 'sort_order' => 1]),
//            new PlanFeature(['code' => 'candidates_contact_in_system', 'value' => 'Y', 'sort_order' => 2]),
//            new PlanFeature(['code' => 'candidate_resumes_phone_email', 'value' => 'N', 'sort_order' => 3]),
//            new PlanFeature(['code' => 'ads_on_social_media', 'value' => 0, 'sort_order' => 4]),
//            new PlanFeature(['code' => 'ads_on_site', 'value' => 0, 'sort_order' => 5]),
//            new PlanFeature(['code' => '30_sec_video', 'value' => 'N', 'sort_order' => 6]),
//            new PlanFeature(['code' => 'consultation_job_description', 'value' => 'N', 'sort_order' => 7]),
//            new PlanFeature(['code' => 'consultation_employment_plan', 'value' => 'N', 'sort_order' => 8]),
//            new PlanFeature(['code' => 'contact_admins', 'value' => 'N', 'sort_order' => 9]),
//        ]);
        $plan2 = Plan::create([
            'name' => 'Standard Account',
            'description' => 'Standard Account Plan',
            'price' => 7500.0,
            'interval' => 'month',
            'interval_count' => 1,
            'trial_period_days' => 0,
            'sort_order' => 1,
        ]);
        $plan2->features()->saveMany([
            new PlanFeature(['code' => 'vacants_listings', 'value' => 30, 'sort_order' => 1]),
            new PlanFeature(['code' => 'candidates_contact_in_system', 'value' => 'Y', 'sort_order' => 2]),
            new PlanFeature(['code' => 'candidate_resumes_phone_email', 'value' => 'Y', 'sort_order' => 3]),
            new PlanFeature(['code' => 'ads_on_social_media', 'value' => 0, 'sort_order' => 4]),
            new PlanFeature(['code' => 'ads_on_site', 'value' => 0, 'sort_order' => 5]),
            new PlanFeature(['code' => '30_sec_video', 'value' => 'N', 'sort_order' => 6]),
            new PlanFeature(['code' => 'consultation_job_description', 'value' => 'N', 'sort_order' => 7]),
            new PlanFeature(['code' => 'consultation_employment_plan', 'value' => 'N', 'sort_order' => 8]),
            new PlanFeature(['code' => 'contact_admins', 'value' => 'N', 'sort_order' => 9]),
        ]);
        $plan3 = Plan::create([
            'name' => 'Silver Account',
            'description' => 'Silver Account Plan',
            'price' => 15000.0,
            'interval' => 'month',
            'interval_count' => 3,
            'trial_period_days' => 0,
            'sort_order' => 2,
        ]);
        $plan3->features()->saveMany([
            new PlanFeature(['code' => 'vacants_listings', 'value' => 150, 'sort_order' => 1]),
            new PlanFeature(['code' => 'candidates_contact_in_system', 'value' => 'Y', 'sort_order' => 2]),
            new PlanFeature(['code' => 'candidate_resumes_phone_email', 'value' => 'Y', 'sort_order' => 3]),
            new PlanFeature(['code' => 'ads_on_social_media', 'value' => 3, 'sort_order' => 4]),
            new PlanFeature(['code' => 'ads_on_site', 'value' => 0, 'sort_order' => 5]),
            new PlanFeature(['code' => '30_sec_video', 'value' => 'N', 'sort_order' => 6]),
            new PlanFeature(['code' => 'consultation_job_description', 'value' => 'N', 'sort_order' => 7]),
            new PlanFeature(['code' => 'consultation_employment_plan', 'value' => 'N', 'sort_order' => 8]),
            new PlanFeature(['code' => 'contact_admins', 'value' => 'N', 'sort_order' => 9]),
        ]);
        $plan4 = Plan::create([
            'name' => 'Gold Account',
            'description' => 'Gold Account Plan',
            'price' => 25000.0,
            'interval' => 'month',
            'interval_count' => 6,
            'trial_period_days' => 0,
            'sort_order' => 3,
        ]);
        $plan4->features()->saveMany([
            new PlanFeature(['code' => 'vacants_listings', 'value' => 300, 'sort_order' => 1]),
            new PlanFeature(['code' => 'candidates_contact_in_system', 'value' => 'Y', 'sort_order' => 2]),
            new PlanFeature(['code' => 'candidate_resumes_phone_email', 'value' => 'Y', 'sort_order' => 3]),
            new PlanFeature(['code' => 'ads_on_social_media', 'value' => 6, 'sort_order' => 4]),
            new PlanFeature(['code' => 'ads_on_site', 'value' => 7, 'sort_order' => 5]),
            new PlanFeature(['code' => '30_sec_video', 'value' => 'N', 'sort_order' => 6]),
            new PlanFeature(['code' => 'consultation_job_description', 'value' => 'N', 'sort_order' => 7]),
            new PlanFeature(['code' => 'consultation_employment_plan', 'value' => 'Y', 'sort_order' => 8]),
            new PlanFeature(['code' => 'contact_admins', 'value' => 'N', 'sort_order' => 9]),
        ]);
        $plan5 = Plan::create([
            'name' => 'Platinum Account',
            'description' => 'Platinum Account Plan',
            'price' => 36000.0,
            'interval' => 'month',
            'interval_count' => 12,
            'trial_period_days' => 0,
            'sort_order' => 4,
        ]);
        $plan5->features()->saveMany([
            new PlanFeature(['code' => 'vacants_listings', 'value' => 'UNLIMITED', 'sort_order' => 1]),
            new PlanFeature(['code' => 'candidates_contact_in_system', 'value' => 'Y', 'sort_order' => 2]),
            new PlanFeature(['code' => 'candidate_resumes_phone_email', 'value' => 'Y', 'sort_order' => 3]),
            new PlanFeature(['code' => 'ads_on_social_media', 'value' => 12, 'sort_order' => 4]),
            new PlanFeature(['code' => 'ads_on_site', 'value' => 30, 'sort_order' => 5]),
            new PlanFeature(['code' => '30_sec_video', 'value' => 'Y', 'sort_order' => 6]),
            new PlanFeature(['code' => 'consultation_job_description', 'value' => 'Y', 'sort_order' => 7]),
            new PlanFeature(['code' => 'consultation_employment_plan', 'value' => 'Y', 'sort_order' => 8]),
            new PlanFeature(['code' => 'contact_admins', 'value' => 'Y', 'sort_order' => 9]),
        ]);
    }
}
