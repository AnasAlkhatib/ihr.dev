<?php

use Illuminate\Database\Seeder;

use App\Permission;
use App\Role;

class RolesPermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        /**
         * Create Super Admin Role .
         */
        $superAdmin = new Role();
        $superAdmin->name = 'SuperAdmin';
        $superAdmin->display_name = 'SuperAdmin';
        $superAdmin->description = 'المشرف العام للنظام';
        $superAdmin->save();
        /**
         * Create Admin Role .
         */

        $admin = new Role();
        $admin->name = 'Admin';
        $admin->display_name = 'Administrator';
        $admin->description = 'مشرف لوحة التحكم';
        $admin->save();
        /**
         * Create recruiter Role .
         */
        $recruiter = new Role();
        $recruiter->name = 'Recruiter';
        $recruiter->display_name = 'Recruiter';
        $recruiter->description = 'مسؤول توظيف';
        $recruiter->save();

        /**
         * Create recruiter Role .
         */

        $publisher = new Role();
        $publisher->name = 'Publisher';
        $publisher->display_name = 'Publisher';
        $publisher->description = 'محرر محتوى';
        $publisher->save();
        /**
         * Create all  Permissions .
         */
        $this->createPermissions();
        /**
         * Final task attach Permissions to Role  .
         */
        $this->superAdminPermissionsAttach();
        $this->adminPermissionsAttach();
        $this->recruiterPermissionsAttach();
        $this->publisherPermissionsAttach();
    }


    public function createPermissions()
    {
        $this->userPermissions();
        $this->employerPermissions();
        $this->jobPermissions();
        $this->newsPermissions();
    }

    /**
     * Create Users  Permissions .
     */
    public function userPermissions()
    {
        $addUsers = new Permission();
        $addUsers->name = 'add-users';
        $addUsers->display_name = 'Add Users';
        $addUsers->save();

        $listUsers = new Permission();
        $listUsers->name = 'list-users';
        $listUsers->display_name = 'List Users';
        $listUsers->save();

        $updateUsers = new Permission();
        $updateUsers->name = 'update-users';
        $updateUsers->display_name = 'Update Users';
        $updateUsers->save();

        $deleteUsers = new Permission();
        $deleteUsers->name = 'delete-users';
        $deleteUsers->display_name = 'Delete Users';
        $deleteUsers->save();
    }

    /**
     * Create Employers  Permissions .
     */
    public function employerPermissions()
    {
        $addEmployers = new Permission();
        $addEmployers->name = 'add-employers';
        $addEmployers->display_name = 'Add Employers';
        $addEmployers->save();

        $listEmployers = new Permission();
        $listEmployers->name = 'list-employers';
        $listEmployers->display_name = 'List Employers';
        $listEmployers->save();


        $updateEmployers = new Permission();
        $updateEmployers->name = 'update-employers';
        $updateEmployers->display_name = 'Update Employers';
        $updateEmployers->save();

        $deleteEmployers = new Permission();
        $deleteEmployers->name = 'delete-employers';
        $deleteEmployers->display_name = 'Delete Employers';
        $deleteEmployers->save();
    }

    /**
     * Create Jobs  Permissions .
     */
    public function jobPermissions()
    {
        $addJobs = new Permission();
        $addJobs->name = 'add-jobs';
        $addJobs->display_name = 'Add Jobs';
        $addJobs->save();

        $listJobs = new Permission();
        $listJobs->name = 'list-jobs';
        $listJobs->display_name = 'List Jobs';
        $listJobs->save();

        $updateJobs = new Permission();
        $updateJobs->name = 'update-jobs';
        $updateJobs->display_name = 'Update Jobs';
        $updateJobs->save();

        $deleteJobs = new Permission();
        $deleteJobs->name = 'delete-jobs';
        $deleteJobs->display_name = 'Delete jobs';
        $deleteJobs->save();
    }

    /**
     * Create News  Permissions.
     */
    public function newsPermissions()
    {
        $addNews = new Permission();
        $addNews->name = 'add-news';
        $addNews->display_name = 'Add News';
        $addNews->save();

        $listNews = new Permission();
        $listNews->name = 'list-news';
        $listNews->display_name = 'List News';
        $listNews->save();

        $updateNews = new Permission();
        $updateNews->name = 'update-news';
        $updateNews->display_name = 'Update News';
        $updateNews->save();

        $deleteNews = new Permission();
        $deleteNews->name = 'delete-news';
        $deleteNews->display_name = 'Delete News';
        $deleteNews->save();
    }

    public function assignPermissionRole($role, $permissionName)
    {
        $role = Role::where('name', $role)->first();
        $permission = Permission::where('name', $permissionName)->first();
        $role->attachPermission($permission);
    }

    public function superAdminPermissionsAttach()
    {
        /* Users */
        $this->assignPermissionRole('SuperAdmin', 'add-users');
        $this->assignPermissionRole('SuperAdmin', 'list-users');
        $this->assignPermissionRole('SuperAdmin', 'update-users');
        $this->assignPermissionRole('SuperAdmin', 'delete-users');

        /* employers */
        $this->assignPermissionRole('SuperAdmin', 'add-employers');
        $this->assignPermissionRole('SuperAdmin', 'list-employers');
        $this->assignPermissionRole('SuperAdmin', 'update-employers');
        $this->assignPermissionRole('SuperAdmin', 'delete-employers');

        /* Jobs */
        $this->assignPermissionRole('SuperAdmin', 'add-jobs');
        $this->assignPermissionRole('SuperAdmin', 'list-jobs');
        $this->assignPermissionRole('SuperAdmin', 'update-jobs');
        $this->assignPermissionRole('SuperAdmin', 'delete-jobs');

        /* News */
        $this->assignPermissionRole('SuperAdmin', 'add-news');
        $this->assignPermissionRole('SuperAdmin', 'list-news');
        $this->assignPermissionRole('SuperAdmin', 'update-news');
        $this->assignPermissionRole('SuperAdmin', 'delete-news');
    }

    public function adminPermissionsAttach()
    {
        /* Jobs */
        $this->assignPermissionRole('Admin', 'add-jobs');
        $this->assignPermissionRole('Admin', 'list-jobs');
        $this->assignPermissionRole('Admin', 'update-jobs');
        $this->assignPermissionRole('Admin', 'delete-jobs');

        /* News */
        $this->assignPermissionRole('Admin', 'add-news');
        $this->assignPermissionRole('Admin', 'list-news');
        $this->assignPermissionRole('Admin', 'update-news');
        $this->assignPermissionRole('Admin', 'delete-news');
    }

    public function recruiterPermissionsAttach()
    {
        /* Jobs */
        $this->assignPermissionRole('Recruiter', 'add-jobs');
        $this->assignPermissionRole('Recruiter', 'list-jobs');
        $this->assignPermissionRole('Recruiter', 'update-jobs');
        $this->assignPermissionRole('Recruiter', 'delete-jobs');

        /* News */
        $this->assignPermissionRole('Recruiter', 'add-news');
        $this->assignPermissionRole('Recruiter', 'list-news');
        $this->assignPermissionRole('Recruiter', 'update-news');
        $this->assignPermissionRole('Recruiter', 'delete-news');
    }

    public function publisherPermissionsAttach()
    {
        /* News */
        $this->assignPermissionRole('Publisher', 'add-news');
        $this->assignPermissionRole('Publisher', 'list-news');
        $this->assignPermissionRole('Publisher', 'update-news');
        $this->assignPermissionRole('Publisher', 'delete-news');
    }


}
