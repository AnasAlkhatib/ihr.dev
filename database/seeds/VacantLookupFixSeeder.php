<?php
use Illuminate\Database\Seeder;
use App\CompanyIndustry;
use App\JobRole;
use App\Vacant;
/**
 * Class VacantLookupFixSeeder
 */
class VacantLookupFixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->action();
    }
    /**
     * Main Company Industry Job replace name with id
     *
     */
    public function action()
    {
        $data = Vacant::all();
        $this->modelSaver($data, App\CompanyIndustry::class, 'name', 'company_industry');
        $this->modelSaver($data, App\JobRole::class, 'name', 'job_rules');
        $this->fixArea($data, App\Area::class, 'name', 'company_location');
    }
    /**
     * @param $data
     * @param $model
     * @param $keyFinder
     * @param $keySaver
     */
    public function fixArea($data, $model, $keyFinder, $keySaver)
    {
        foreach ($data as $object) {
            $areaArray = $this->commaToArray($object->{$keySaver});
            $value = $this->modelKeyArray($areaArray, $model, $keyFinder, false);
            if ($value) {
                $object->{$keySaver} = $value;
                $object->save();
            }
        }
    }
    /**
     * save new key in Vacant
     * @param $data
     * @param $model
     * @param $keyFinder
     * @param $keySaver
     */
    public function modelSaver($data, $model, $keyFinder, $keySaver)
    {
        foreach ($data as $object) {
            $key = $this->modelFinder($model, $keyFinder, $object->{$keySaver});
            if ($key) {
                $object->{$keySaver} = $key;
                $object->save();
            }
        }
    }
    /**
     * @param $model
     * @param $keyFinder
     * @param $value
     * @return null
     */
    public function modelFinder($model, $keyFinder, $value)
    {
        $modelFound = $model::where($keyFinder, '=', $value)->first();
        if ($modelFound) {
            return $modelFound->getKey();
        }
        return false;
    }
    /**
     * Convert string comma to array
     * @param $string
     * @param string $separator
     * @return array
     */
    function commaToArray($string, $separator = ',')
    {
        $values = explode($separator, $string);
        foreach ($values as $key => $val) {
            $values[$key] = trim($val);
        }
        return array_diff($values, array(""));
    }
    /**
     * @param $data
     * @param $model
     * @param $key
     * @param bool $serialize
     * @return string
     */
    function modelKeyArray($data, $model, $key, $serialize = false)
    {
        $array = array();
        foreach ($data as $name) {
            $id = trim($this->modelFinder($model, $key, $name));
            if ($id) {
                $array [] = $id;
            }
        }
        if (!empty($array)) {
            array_unique($array);
            if ($serialize) {
                return serialize($array);
            }
            return implode(",", $array);
        }
        return false;
    }
}
