<?php

/**
 * Class MainTestCase
 */
abstract class MainTestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://ihr.com.jo';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';
        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();
        return $app;
    }

    /**
     * Default preparation for each test
     */
    protected function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
        Artisan::call('migrate');
    }

    /**
     *
     */
    public function tearDown()
    {
        DB::rollBack();
        parent::tearDown();
    }
}
