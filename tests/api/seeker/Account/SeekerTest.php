<?php
//use App\Seeker;
//use Faker\Factory as Faker;
//use App\Country;
//
///**
// * Class SeekerTest
// */
//class SeekerTest extends MainTestCase
//{
//    /**
//     * @var string baseUrl
//     */
//    protected $baseUrl = 'http://api.ihr.com.jo';
//
//    public function setUp()
//    {
//        parent::setUp();
//        $this->seedCountry();
//    }
//
//    /**
//     * test seeker registration
//     */
//    public function test_seeker_auth()
//    {
//        $seeker = factory(App\Seeker::class)->create(['residence' => 114])->getAttributes();
//        $credentials = ['email' => $seeker['email'], 'password' => 123456, 'locale' => 'en'];
//        $login = $this->call('POST', '/login', $credentials);
//        $token = $this->parseJson($login)->token;
//        $auth = $this->call('POST', '/auth', ['token' => $token]);
//        $data = $this->parseJson($auth);
//        $this->assertEquals($data->data->residence_name, 'Jordan');
//    }
//
//    public function test_seeker_registration()
//    {
//        $seeker = factory(App\Seeker::class)->make(['locale' => 'en'])->getAttributes();
//        $this->call('POST', 'seeker', $seeker);
//        $this->seeInDatabase('seekers', ['full_name' => $seeker['full_name'], 'email' => $seeker['email']]);
//    }
//
//    /**
//     * test if the seeker can update his residence location
//     */
//    public function test_seeker_residence()
//    {
//        $seeker = factory(Seeker::class)->create(['residence' => 12]);
//        $getSeeker = Seeker::find($seeker->ID);
//        $this->assertEquals($getSeeker->residence, 12);
//    }
//
//    public function test_seeker_registration_mobile_between()
//    {
//        /* 9 - 14 */
//        $mobile = mt_rand(100000000, 99999999999999);
//        /* 10 - 14 */
//        $national_num = mt_rand(1000000000, 99999999999999);
//        $seeker = factory(App\Seeker::class)->make([
//            'locale' => 'en',
//            'mobile' => $mobile,
//            'national_num' => $national_num
//        ])->getAttributes();
//        $this->call('POST', 'seeker', $seeker);
//        $this->seeInDatabase('seekers', ['full_name' => $seeker['full_name'], 'email' => $seeker['email']]);
//    }
//
//    /**
//     * @param \Illuminate\Http\JsonResponse $response
//     * @return mixed
//     */
//    protected function parseJson($response)
//    {
//        return json_decode($response->getContent());
//    }
//
//    protected function seedCountry()
//    {
////        //Get all of the countries
//        $country_json = \File::get(storage_path() . "/country_all.json");
//        $countries = collect(json_decode($country_json));
//        // remove the id
//        for ($i = 0; $i <= count($countries) - 1; $i++) {
//            $country = new Country();
//            $country->capital = $countries[$i]->capital;
//            $country->citizenship = $countries[$i]->citizenship;
//            $country->country_code = $countries[$i]->country_code;
//            $country->currency = $countries[$i]->currency;
//            $country->currency_code = $countries[$i]->currency_code;
//            $country->currency_sub_unit = $countries[$i]->currency_sub_unit;
//            $country->currency_symbol = $countries[$i]->currency_symbol;
//            $country->full_name = $countries[$i]->full_name;
//            $country->iso_3166_2 = $countries[$i]->iso_3166_2;
//            $country->iso_3166_3 = $countries[$i]->iso_3166_3;
//            $country->name = $countries[$i]->name;
//            $country->name_ar = $countries[$i]->name_ar;
//            $country->region_code = $countries[$i]->region_code;
//            $country->sub_region_code = $countries[$i]->sub_region_code;
//            $country->eea = $countries[$i]->eea;
//            $country->calling_code = $countries[$i]->calling_code;
//            $country->flag = $countries[$i]->flag;
//            $country->save();
//        }
//    }
//}
