<?php
use App\Vacant;
use App\Seeker;
use App\Employer;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

/**
 * Class EmployerVacantAppliedTest
 */
class EmployerVacantAppliedTest extends MainTestCase
{
    protected $baseUrl = 'http://api.ihr.com.jo';

    /**
     * this test make sure the the employer create new vacant and the seekers
     * applied to the same vacant
     */
//    public function test_employer_create_vacant_applied_seeker_count()
//    {
//        $seeker1 = factory(Seeker::class)->create();
//        $seeker2 = factory(Seeker::class)->create();
//        $seeker3 = factory(Seeker::class)->create();
//        $employer = factory(App\Employer::class)->create();
//        $vacant = factory(App\Vacant::class)->create(['user_id' => 0, 'employer_id' => $employer->id]);
//        $seeker1->appliedVacant()->create(['seeker_id' => $seeker1->ID, 'vacant_id' => $vacant->ID]);
//        $seeker2->appliedVacant()->create(['seeker_id' => $seeker2->ID, 'vacant_id' => $vacant->ID]);
//        $seeker3->appliedVacant()->create(['seeker_id' => $seeker3->ID, 'vacant_id' => $vacant->ID]);
//        $Applied_seeker = $vacant->Applied_seeker->all();
//        $this->assertEquals(count($Applied_seeker), 3);
//    }

    public function test_employer_applicants()
    {
        /* create employer then login */
        $employer = factory(App\Employer::class)->create(['active' => 1])->getAttributes();
        $credentials = ['email' => $employer['email'], 'password' => 123456, 'locale' => 'en'];
        $response = $this->call('POST', 'employer/login', $credentials);
        $token = $this->parseJson($response)->token;
        /* create seekers then set them online */
        $seeker1 = factory(Seeker::class)->create();
        $seeker2 = factory(Seeker::class)->create();
        $seeker3 = factory(Seeker::class)->create();

        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('seeker-is-online-' . $seeker1->ID, true, $expiresAt);
        /* create vacant  */
        $vacant = factory(App\Vacant::class)->create(['user_id' => 0, 'employer_id' => $employer['id']]);
        /* seekers  applied to the vacant  */
        $seeker1->appliedVacant()->create(['seeker_id' => $seeker1->ID, 'vacant_id' => $vacant->ID]);
        $seeker2->appliedVacant()->create(['seeker_id' => $seeker2->ID, 'vacant_id' => $vacant->ID]);
        $seeker3->appliedVacant()->create(['seeker_id' => $seeker3->ID, 'vacant_id' => $vacant->ID]);

        $response = $this->call('GET', 'employer/vacant/applicants?token=' . $token);
        $data = $this->parseJson($response);
        $this->assertTrue($data->applicants[0]->is_online);

    }

    /**
     * @param \Illuminate\Http\JsonResponse $response
     * @return mixed
     */
    protected function parseJson(Illuminate\Http\JsonResponse $response)
    {
        return json_decode($response->getContent());
    }
}
