<?php
//use App\Employer;
///**
// * Class EmployerVacantAppliedTest
// */
//class EmployerApiAuthTest extends MainTestCase
//{
//    /**
//     * @var string
//     */
//    protected $baseUrl = 'http://api.ihr.com.jo';
//    /**
//     * test not active employer login
//     * return error message
//     *
//     */
//    public function test_employer_not_active_should_not_receive_token()
//    {
//        $employer = factory(App\Employer::class)->create(['active' => 0])->getAttributes();
//        $credentials = ['email' => $employer['email'], 'password' => 123456, 'locale' => 'en'];
//        $response = $this->call('POST', 'employer/login', $credentials);
//        $data = $this->parseJson($response);
//        $this->assertEquals($data->error, 'This Account Not Active');
//        $this->assertEquals($response->getStatusCode(), 400);
//    }
//    /**
//     * test active employer login
//     * return token
//     */
//    public function test_employer_active_should_receive_token()
//    {
//        $employer = factory(App\Employer::class)->create(['active' => 1])->getAttributes();
//        $credentials = ['email' => $employer['email'], 'password' => 123456, 'locale' => 'en'];
//        $response = $this->call('POST', 'employer/login', $credentials);
//        $data = $this->parseJson($response);
//        $this->assertNotEmpty($data->token);
//        $this->assertEquals($response->getStatusCode(), 200);
//    }
//    /**
//     * @param \Illuminate\Http\JsonResponse $response
//     * @return mixed
//     */
//    protected function parseJson(Illuminate\Http\JsonResponse $response)
//    {
//        return json_decode($response->getContent());
//    }
//}
