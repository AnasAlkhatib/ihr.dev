<?php
//use App\Services\Employment\EmploymentBuilder;
//use App\User;
//use App\Client;
//use App\Candidate;
//use App\Jobs\CandidateClientSuggested;
//use Illuminate\Contracts\Mail\Mailer;
///**
// * Class EmploymentBuilderTest
// */
//class EmploymentBuilderTest extends MainTestCase
//{
//    public function test_should_update_exists_candidate_owned_by_same_user_suggested_same_client_same_vacant()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 1];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->run();
////        $this->assertTrue($employmentBuilder->saved);
//    }
//    public function test_should_create_candidate_owned_by_user_suggested_same_client_another_vacant()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 2];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->run();
////        $attributes2 = ['client_id' => $client->id, 'vacant_id' => 3];
////        $employmentBuilder->attributes = $attributes2;
////        $employmentBuilder->run();
////        $this->assertEquals(2, $employmentBuilder->action);
//    }
//    /**
//     *
//     */
//    public function test_should_create_candidate_exists_owned_by_user_suggested_another_client()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $client2 = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 2];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->client = $client2;
////        $attributes2 = ['client_id' => $client2->id, 'vacant_id' => 2];
////        $employmentBuilder->attributes = $attributes2;
////        $employmentBuilder->run();
////        $this->assertEquals(3, $employmentBuilder->action);
//    }
//    /**
//     *
//     */
//    public function test_duplicate_feedback_candidate_same_client_same_vacant()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $user2 = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 2];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->user = $user2;
////        $employmentBuilder->client = $client;
////        $employmentBuilder->attributes = ['client_id' => $client->id, 'vacant_id' => 2];
////        $employmentBuilder->run();
////        $this->assertEquals(4, $employmentBuilder->action);
////        $this->assertEquals('warning', $employmentBuilder->alertClass);
//    }
//    public function test_candidate_exists_not_owned_by_user_same_client_another_vacant()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $user2 = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 1];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->user = $user2;
////        $employmentBuilder->attributes = ['client_id' => $client->id, 'vacant_id' => 2];
////        $employmentBuilder->run();
////        $this->assertEquals(5, $employmentBuilder->action);
//    }
////case ($this->exists($this->candidate) && !$this->getOwnerUser()):
//    public function test_candidate_exists_not_owned_by_user()
//    {
////        $candidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $user2 = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $candidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 1];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $candidate, $values, $attributes);
////        $employmentBuilder->createCandidate();
////        $employmentBuilder->suggest();
////        $employmentBuilder->run();
////        $employmentBuilder->user = $user2;
////        $employmentBuilder->attributes = ['client_id' => $client->id, 'vacant_id' => 1];
////        $employmentBuilder->run();
////        $this->assertEquals(4, $employmentBuilder->action);
//    }
//    public function test_candidate_exists_owned_by_user_update()
//    {
////        $newCandidate = factory(App\Candidate::class)->make();
////        $user = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $values = $newCandidate->getAttributes();
////        $attributes = ['client_id' => $client->id, 'vacant_id' => 1];
////        $employmentBuilder = new EmploymentBuilder($user, $client, $newCandidate, $values, $attributes);
////        $employmentBuilder->run();
////        $sameCandidate = New Candidate;
////        $sameCandidate->fill($values);
////        $employmentBuilder2 = new EmploymentBuilder($user, $client, $sameCandidate, $values, $attributes);
////        $employmentBuilder2->run();
////        dd($employmentBuilder2->action);
//    }
//    public function test_new_candidate_created_job()
//    {
////        $user = factory(App\User::class)->create();
////        $client = factory(App\Client::class)->create();
////        $area = factory(App\Area::class)->create(['name' => 'الوسطى', 'name_en' => 'Central']);
////        $city = factory(App\City::class)->create([
////            'area_id' => $area->ID,
////            'name' => 'الرياض',
////            'name_en' => 'Riyadh'
////        ]);
////        $seeker = factory(App\Seeker::class)->create(['city_id' => $city->ID]);
////        $candidate = factory(App\Candidate::class)->create(['seeker_id' => $seeker->ID]);
////        $job = new CandidateClientSuggested($user, $client, $candidate);
////        $mailer = app('Illuminate\Contracts\Mail\Mailer');
////        $job->handle($mailer);
//    }
//}