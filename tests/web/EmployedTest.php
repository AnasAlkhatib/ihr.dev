<?php
//use App\Util\Traits\EmployedTrait;
//use App\Seeker;
//use Carbon\Carbon;
//
///**
// * Class EmployedTest
// */
//class EmployedTest extends MainTestCase
//{
//    use EmployedTrait;
//    /**
//     *
//     */
//
//    public function test_eager_loading()
//    {
//        $start_date = array('year' => 2016, 'month' => 4, 'day' => 13);
//        $end_date = array('year' => 2017, 'month' => 1, 'day' => 1);
//        /* seeker list */
//        $seeker1 = factory(Seeker::class)->create(['residence' => 12]);
//        $seeker2 = factory(Seeker::class)->create(['residence' => 13]);
//        $seeker3 = factory(Seeker::class)->create(['residence' => 14]);
//        $seeker4 = factory(Seeker::class)->create(['residence' => 15]);
//        $seeker5 = factory(Seeker::class)->create(['residence' => 16]);
//        /* exp list */
//        $seeker1->Experience()->create($this->expArray($start_date, $end_date));//not working
//        $seeker2->Experience()->create($this->expArray($start_date, $end_date));//not working
//        $seeker3->Experience()->create($this->expArray($start_date, $end_date));// not working
//        $seeker3->Experience()->create($this->expArray($start_date));//working
//        /* Seekers  Query */
//        $seekers = Seeker::query();
//        $working = $this->querySeekers($seekers, 'working');
//        $this->assertEquals(1, $working->count());
//        $seekers = Seeker::query();
//        $not_working = $this->querySeekers($seekers, 'not_working');
//        $this->assertEquals(5, $not_working->count());
//    }
//
//
//    /**
//     * @param $startDate
//     * @param $endDate
//     * @return array
//     */
//    private function expArray($startDate, $endDate = NULL)
//    {
//        $faker = Faker\Factory::create();
//        $start_date = Carbon::create($startDate['year'], $startDate['month'], $startDate['day'], 0, 0, 0);
//        if ($endDate != NULL) {
//            $end_date = Carbon::create($endDate['year'], $endDate['month'], $endDate['day'], 0, 0, 0);
//        } else {
//            $end_date = NULL;
//        }
//        return [
//            'job_title' => $faker->jobTitle,
//            'company_name' => $faker->company,
//            'start_date' => $start_date,
//            'end_date' => $end_date,
//        ];
//    }
//
//    private function querySeekers($seekers, $employmentStatus)
//    {
//        $data = $seekers->whereHas('experience', function ($query) use ($employmentStatus) {
//            if ($employmentStatus == 'working') {
//                $query->whereNull('end_date');
//            } elseif ($employmentStatus == 'not_working') {
//                $query->whereNotNull('end_date')
//                    ->leftJoinWhere('seekers', 'experiences.seeker_id', '!=', 'seekers.ID');
//            }
//
//        });
//
//        return $data;
//    }
//}