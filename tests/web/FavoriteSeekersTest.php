<?php
///**
// * Class FavoriteSeekersTest
// */
//class FavoriteSeekersTest extends MainTestCase
//{
//    /*
//       |--------------------------------------------------------------------------
//       | User Favorite Seekers test
//       |--------------------------------------------------------------------------
//      */
//    public function test_add_seeker_to_favorite_by_user()
//    {
//        $seeker = factory(App\Seeker::class)->create();
//        $user = factory(App\User::class)->create();
//        $user->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
//        $this->assertEquals($user->favoriteSeekers->count(), 1);
//    }
//    /**
//     *  Determine whether a seeker has been marked as favorite by a user.
//     *
//     */
//    public function test_isFavorited_by_user()
//    {
//        $seeker = factory(App\Seeker::class)->create();
//        $user = factory(App\User::class)->create();
//        $user->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
//        $isFavoritedForUser = $seeker->isFavorited($user);
//        $this->assertTrue($isFavoritedForUser);
//    }
//    /**
//     * Delete Seeker from user favorite seekers list
//     *
//     */
//    public function test_delete_seeker_from_favorite_by_user()
//    {
//        $user = factory(App\User::class)->create();
//        $user->favoriteSeekers()->create(['seeker_id' => 1]);
//        $user->favoriteSeekers()->create(['seeker_id' => 2]);
//        $user->favoriteSeekers()->create(['seeker_id' => 3]);
//        $user->favoriteSeekers()->where('seeker_id', '=', 1)->delete();
//        $this->assertEquals($user->favoriteSeekers->count(), 2);
//    }
//    /*
//       |--------------------------------------------------------------------------
//       | Employer Favorite Seekers test
//       |--------------------------------------------------------------------------
//      */
//    public function test_add_seeker_to_favorite_by_employer()
//    {
//        $seeker = factory(App\Seeker::class)->create();
//        $employer = factory(App\Employer::class)->create();
//        $employer->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
//        $this->assertEquals($employer->favoriteSeekers->count(), 1);
//    }
//    /**
//     *  Determine whether a seeker has been marked as favorite by a user.
//     *
//     */
//    public function test_isFavorited_by_employer()
//    {
//        $seeker = factory(App\Seeker::class)->create();
//        $employer = factory(App\Employer::class)->create();
//        $employer->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
//        $isFavoritedForUser = $seeker->isFavorited($employer);
//        $this->assertTrue($isFavoritedForUser);
//    }
//    /**
//     * Delete Seeker from employer favorite seekers list
//     *
//     */
//    public function test_delete_seeker_from_favorite_by_employer()
//    {
//        $employer = factory(App\Employer::class)->create();
//        $employer->favoriteSeekers()->create(['seeker_id' => 1]);
//        $employer->favoriteSeekers()->create(['seeker_id' => 2]);
//        $employer->favoriteSeekers()->create(['seeker_id' => 3]);
//        $employer->favoriteSeekers()->where('seeker_id', '=', 1)->delete();
//        $this->assertEquals($employer->favoriteSeekers->count(), 2);
//    }
//}
