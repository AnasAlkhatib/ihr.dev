<?php
//use Illuminate\Database\Eloquent\Model;
///**
// * Class EmployerSeekerStatusTest
// */
//class EmployerSeekerStatusTest extends MainTestCase
//{
//    /**
//     * add new status for seeker
//     */
//    public function test_add_seeker_status()
//    {
//        $employer = factory(App\Employer::class)->create();
//        $seeker = factory(App\Seeker::class)->create();
//        $employer->seekers()->attach($seeker->ID, ['vacant_id' => 1, 'status' => 'Ok']);
//        $this->assertEquals($employer->seekers[0]->pivot->status, 'Ok');
//        $this->assertEquals(1, $employer->seekers->count());
//    }
//    /**
//     * update  status for seeker
//     */
//    public function test_update_seeker_status()
//    {
//        $employer = factory(App\Employer::class)->create();
//        $seeker = factory(App\Seeker::class)->create();
//        $seeker2 = factory(App\Seeker::class)->create();
//        $employer->seekers()->attach($seeker->ID, ['vacant_id' => 1, 'status' => 'Ok']);
//        $employer->seekers()->attach($seeker2->ID, ['vacant_id' => 2, 'status' => 'NO']);
//        $employer->seekers()->syncWithoutDetaching([$seeker->ID => ['status' => 'Updated 1']]);
//        $this->assertEquals($employer->seekers[0]->pivot->status, 'Updated 1');
//    }
//    /**
//     * delete seeker status
//     */
//    public function test_delete_seeker_status()
//    {
//        $employer = factory(App\Employer::class)->create();
//        $seeker = factory(App\Seeker::class)->create();
//        $employer->seekers()->attach($seeker->ID, ['vacant_id' => 1, 'status' => 'Ok']);
//        $employer->seekers()->attach($seeker->ID, ['vacant_id' => 2, 'status' => 'NO']);
//        $employer->seekers()->newPivotStatement()->where('id', 1)->delete();
//        $this->assertEquals(1, $employer->seekers->count());
//    }
//}
