<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class Client
 * @package App
 */
class Client extends Authenticatable
{
    /**
     * @var string
     */
    protected $table = 'clients';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'job_title',
        'company_name',
        'company_type',
        'mobile',
        'password',
        'active',
        'locale'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes for  pivot candidatables
     *
     * @var array
     */
    protected $candidateColumns = [
        'id',
        'candidate_id',
        'candidatable_id',
        'candidatable_type',
        'client_id',
        'vacant_id',
        'notes',
        'status',
        'offer_vacancies',
        'required_documents'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vacancies()
    {
        return $this->hasMany(Vacant::class);
    }
    /**
     * Get all of the candidates for the client.
     */
    public function candidates()
    {
        return $this->morphToMany('App\Candidate', 'candidatable')
            ->withPivot($this->candidateColumns)->withTimestamps();
    }
}

