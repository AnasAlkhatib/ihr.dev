<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteVacant extends Model {

    protected $table = "favorite_vacancies";
    protected $primaryKey = 'ID';
    protected $fillable = ['seeker_id', 'vacant_id'];

}
