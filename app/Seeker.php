<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;


/**
 * Class Seeker
 * @package App
 */
class Seeker extends Authenticatable implements JWTSubject
{
    /**
     * @var string
     */
    protected $table = 'seekers';
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $fillable = [
        'full_name',
        'password',
        'activation_code',
        'mobile',
        'email',
        'birth_date',
        'nationality',
        'national_num',
        'gender',
        'country_id',
        'residence',
        'city_id',
        'address',
        'skills',
        'marital_status',
        'dependent_num',
        'driving_licence',
        'owning_vehicle',
        'desired_job',
        'desired_job_status',
        'desired_salary',
        'notice_period',
        'locale',
        'edu_level_id'

    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'country_id' => 'int',
        'residence' => 'int',
        'city_id' => 'int',
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'activation_code', 'remember_token'];

    /**
     * Get the country record associated with the seeker.
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     *  Seeker Relations
     */
    public function language()
    {
        return $this->hasMany('App\Language');
    }

    /**
     * Experience Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function experience()
    {
        return $this->hasMany('App\Experience');
    }

    /**
     * Course Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course()
    {
        return $this->hasMany('App\Course');
    }

    /**
     * Education Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function education()
    {
        return $this->hasMany('App\Education');
    }

    /**
     * Favorite Vacant Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoriteVacant()
    {
        return $this->hasMany('App\FavoriteVacant');
    }

    /**
     * Vacant Favorite Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vacantFavorite()
    {
        return $this->belongsToMany('App\Vacant', 'favorite_vacancies')->withPivot('ID')->withTimestamps();
    }

    /**
     * Applied Vacant Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appliedVacant()
    {
        return $this->hasMany('App\AppliedVacant');
    }

    /**
     * Vacant Applied Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vacantApplied()
    {
        return $this->belongsToMany('App\Vacant', 'applied_vacancies')->withPivot(
            'ID',
            'last_view_date',
            'view_num',
            'last_view_recriuter'
        )->withTimestamps();
    }

    /**
     * Notification device id  Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notification()
    {
        return $this->hasMany('App\Notification');
    }

    /**
     * Get the vacantTag record associated with the seeker.
     */
    public function vacantTag()
    {
        return $this->hasOne('App\SeekerVacantTag');
    }

    /**
     * activate seeker account by given Activation Code
     * @param $code
     * @return bool
     */
    public function accountIsActive($code)
    {
        $seeker = Seeker::where('activation_code', '=', $code)->first();
        if ($seeker) {
            $seeker->active = 1;
            $seeker->activation_code = '';
            $seeker->save();
            return true;
        }
        return false;
    }

    /**
     *
     * Check if the seeker has been activated his/here account before
     * @param  string $code
     * @return bool
     */
    public function Activated($code)
    {
        $seeker = Seeker::where('activation_code', '=', $code)->first();
        if (!$seeker) {
            return true;
        }
        return false;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     *  Determine whether a seeker has been marked as favorite by a user or employer .
     *
     * @param $model
     * @return bool
     */
    public function isFavorited($model)
    {
        return (bool)$model->favoriteSeekers()
            ->where('seeker_id', $this->ID)
            ->first();
    }

    /**
     * employer seekers status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employers()
    {
        return $this->belongsToMany(Employer::class);
    }

    /**
     * check if the seeker on line
     * @return bool
     */
    public function isOnline()
    {
        return \Cache::has('seeker-is-online-' . $this->ID);
    }
}