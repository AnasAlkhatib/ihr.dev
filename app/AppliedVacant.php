<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class AppliedVacant
 * @package App
 */
class AppliedVacant extends Model
{
    /**
     * @var string
     */
    protected $table = "applied_vacancies";
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $fillable = ['seeker_id', 'vacant_id', 'last_view_date', 'view_num', 'last_view_recriuter'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vacant()
    {
        return $this->belongsTo('App\Vacant');
    }
}
