<?php
namespace App;

use App\Base\Model\Abstracts\VacantAbstract;
use App\Util\Traits\VacantTranslatable;
/**
 * Class VacantApi
 * @package App
 */
class VacantApi extends VacantAbstract
{
    use VacantTranslatable;
    /**
     * The primary key for the model.
     *
     * @var string
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_industry',
        'company_location',
        'job_title',
        'job_rules',
        'job_status',
        'job_discription',
        'education',
        'certification',
        'exp_level',
        'exp_years',
        'skills',
        'language',
        'gender',
        'candidate_age',
        'candidate_address',
        'ref_num',
        'employer_id',
        'close_date',
        'additional_vacant_info',
        'additional_prefered_info',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Applied_seeker()
    {
        return $this->belongsToMany('App\Seeker', 'applied_vacancies', 'vacant_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}