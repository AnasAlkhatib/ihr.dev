<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class JobRole extends Model
{
    protected $table = 'job_roles';
    protected $primaryKey = 'ID';
    protected $hidden = [ 'created_at', 'updated_at'];
    protected $fillable = ['name', 'name_en'];
}