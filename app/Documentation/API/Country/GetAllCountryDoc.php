<?php
/**
 * @SWG\Get(
 *      path="/country",
 *      summary="Get all countries",
 *      tags={"Country"},
 *      description="Get all countries",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */