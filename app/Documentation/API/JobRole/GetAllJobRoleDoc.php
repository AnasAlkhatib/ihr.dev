<?php
/**
 * @SWG\Get(
 *      path="/job_role",
 *      summary="Get all Jobs Roles",
 *      tags={"Job Role"},
 *      description="Get all Jobs Roles",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */