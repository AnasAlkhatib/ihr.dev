<?php
/**
 * @SWG\Get(
 *      path="/area/{area_id}/city",
 *      summary="Get all city in this Area",
 *      tags={"Area"},
 *      description="Get all city in this Area",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="area_id",
 *         name="area_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */