<?php
/**
 * @SWG\Get(
 *      path="/area",
 *      summary="Get all Areas",
 *      tags={"Area"},
 *      description="Get all Areas",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */