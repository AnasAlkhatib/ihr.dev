<?php
/**
 * @SWG\Delete(
 *      path="/employer/candidate/{candidate_id}",
 *      summary="Employer Delete candidate",
 *      tags={"Employer Candidate"},
 *      description="Delete Candidate",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="candidate_id",
 *         name="candidate_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error or Unauthorized",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */