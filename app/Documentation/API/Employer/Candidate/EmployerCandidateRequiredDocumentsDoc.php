<?php
/**
 * @SWG\Get(
 *      path="/employer/candidate/requiredDocuments",
 *      summary="Get Candidate Required Documents",
 *      tags={"Employer Candidate"},
 *      description="Get Candidate Required Documents",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */