<?php
/**
 * @SWG\Put(
 *      path="/employer/candidate/{candidate_id}",
 *      summary="Update Candidate",
 *      tags={"Employer Candidate"},
 *      description="Update Candidate",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="candidate_id",
 *         name="candidate_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Candidate Information",
 *          required=true,
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="seeker_id",
 *                  description="seeker_id",
 *                  default="1",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="vacant_id",
 *                  description="vacant_id",
 *                  default="2",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="status",
 *                  description="status",
 *                  default="accept_candidate",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="note",
 *                  description="note",
 *                  default="This Candidate is good",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="offer_vacancies",
 *                  description="offer_vacancies",
 *                  default="1,2,3",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="required_documents",
 *                  description="required_documents",
 *                  default="job_guarantee,criminal_evidence",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */