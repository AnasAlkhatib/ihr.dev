<?php
/**
 * @SWG\Get(
 *      path="/employer/candidate/{candidate_id}",
 *      summary="Get Candidate  By candidate_id",
 *      tags={"Employer Candidate"},
 *      description="Get Candidate  By candidate_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="candidate_id",
 *         name="candidate_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */