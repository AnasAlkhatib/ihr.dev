<?php
/**
 * @SWG\Get(
 *      path="/employer/candidate",
 *      summary="Get All Employer Candidates",
 *      tags={"Employer Candidate"},
 *      description="Get All Employer Candidates",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */