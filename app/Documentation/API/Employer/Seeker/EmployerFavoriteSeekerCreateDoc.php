<?php
/**
 * @SWG\Post(
 *      path="/employer/favoriteSeeker/{seeker_id}",
 *      summary="Add Seeker to Favorite List",
 *      tags={"Employer Favorite Seeker"},
 *      description="Add Seeker to Favorite",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=201,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error or Unauthorized",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */
