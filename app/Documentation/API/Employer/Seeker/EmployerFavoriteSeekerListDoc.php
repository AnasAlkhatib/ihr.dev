<?php
/**
 * @SWG\GET(
 *      path="/employer/favoriteSeeker",
 *      summary="Get Employer Favorite Seekers List",
 *      tags={"Employer Favorite Seeker"},
 *      description="Employer Favorite Seekers List",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error or Unauthorized",
 *     )
 * )
 */
