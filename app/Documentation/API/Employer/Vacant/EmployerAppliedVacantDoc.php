<?php
/**
 * @SWG\Post(
 *      path="/employer/vacant/{vacant_id}/appliedVacant",
 *      summary="Get Vacancy Applicants",
 *      tags={"Employer Vacant"},
 *      description="Get Vacancy Applicants",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="vacant_id",
 *         name="vacant_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Search Prameters",
 *          required=true,
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="gender",
 *                  description="gender",
 *                  default="male",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="area",
 *                  description="area",
 *                  default="1",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="city",
 *                  description="city",
 *                  default="1",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="nationality",
 *                  description="nationality",
 *                  default="1",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error or Unauthorized",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */
