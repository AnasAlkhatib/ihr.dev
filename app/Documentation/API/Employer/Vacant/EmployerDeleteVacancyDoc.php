<?php
/**
 * @SWG\Delete(
 *      path="/employer/vacant/{vacant_id}",
 *      summary="Employer Delete Vacancy",
 *      tags={"Employer Vacant"},
 *      description="Delete Vacancy",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="vacant_id",
 *         name="vacant_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error or Unauthorized",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */