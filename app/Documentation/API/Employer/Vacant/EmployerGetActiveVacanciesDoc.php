<?php
/**
 * @SWG\Get(
 *      path="/employer/vacant/activeVacancies",
 *      summary="Get Employer Active Vacancies",
 *      tags={"Employer Vacant"},
 *      description="Get employer Active Vacancies",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */