<?php
/**
 * @SWG\Get(
 *      path="/employer/vacant/{vacant_id}",
 *      summary="Get All Employer Vacancies",
 *      tags={"Employer Vacant"},
 *      description="Get Employer Vacancy By vacant_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="vacant_id",
 *         name="vacant_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */