<?php
/**
 * @SWG\Get(
 *      path="/employer/plans/{plan_id}/unsubscribe",
 *      summary="Unsubscribe From a Plan",
 *      tags={"Employer Plan"},
 *      description="Unsubscribe From a Plan",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *         description="plan_id",
 *         name="plan_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     )
 * )
 */