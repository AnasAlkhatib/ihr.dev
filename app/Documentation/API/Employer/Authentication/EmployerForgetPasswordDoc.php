<?php
/**
 * @SWG\Post(
 *      path="/employer/password/email",
 *      summary="Forget Password",
 *      tags={"Employer Authentication"},
 *      description="send email with reset password instructions",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Employer Email",
 *          required=true,
 *          default="{""email"":""employer@admin.com""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="email",
 *                  description="email",
 *                  type="string"
 *              )
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Send Reset Password link.",
 *      ),
 *    @SWG\Response(
 *         response="404",
 *         description="Account not Found.",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */