<?php
/**
 * @SWG\Post(
 *      path="/employer/auth",
 *      summary="Get Employer Information",
 *      tags={"Employer Authentication"},
 *      description="Get Employer information from given token",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="Get Employer Information",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */