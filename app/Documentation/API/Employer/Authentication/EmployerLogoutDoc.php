<?php
/**
 * @SWG\Post(
 *      path="/employer/logout",
 *      summary="Logout Employer",
 *      tags={"Employer Authentication"},
 *      description="Logout Employer and invalidate token",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="invalidate the token",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */