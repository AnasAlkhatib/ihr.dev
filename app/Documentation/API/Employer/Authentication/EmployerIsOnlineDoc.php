<?php
/**
 * @SWG\Post(
 *      path="/employer/online",
 *      summary="Set employer online",
 *      tags={"Employer Authentication"},
 *      description="Set Employer online",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Employer that should be stored",
 *          required=true,
 *          default="{""token"":""""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="token",
 *                  description="token",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Set Employer online",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */