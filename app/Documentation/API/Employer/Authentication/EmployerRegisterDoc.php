<?php
/**
 * @SWG\Post(
 *      path="/employer/register",
 *      summary="Employer Registeration",
 *      tags={"Employer Authentication"},
 *      description="Register New Employer",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Employer Information",
 *          required=true,
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="name",
 *                  description="name",
 *                  default="employer",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="email",
 *                  description="email",
 *                  default="example@example.com",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="password",
 *                  description="password",
 *                  default="123456",
 *                  type="string"
 *              ),
 *
 *              @SWG\Property(
 *                  property="job_title",
 *                  description="job_title",
 *                  default="Programmer",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="company_name",
 *                  description="company_name",
 *                  default="Acme",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="company_type",
 *                  description="company_type",
 *                  default="private_sector",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="mobile",
 *                  description="mobile",
 *                  description="mobile",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Get Token.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Invalid Credentials.",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */
return [
    'name' => 'required|max:255',
    'email' => 'required|email|unique:employers,email',
    'job_title' => 'required|string',
    'company_name' => 'required|string',
    'company_type' => 'required|in:private_sector,public_sector,non_profit_organization,recruitment_agency',
    'mobile' => 'required|unique:employers,mobile',
    'password' => 'required|min:6',
    'locale' => 'in:en,ar'
];