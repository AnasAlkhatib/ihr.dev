<?php
/**
 * @SWG\Post(
 *      path="/employer/refresh_token",
 *      summary="Refresh Given Token",
 *      tags={"Employer Authentication"},
 *      description="Refresh Given Token",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="New token.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */