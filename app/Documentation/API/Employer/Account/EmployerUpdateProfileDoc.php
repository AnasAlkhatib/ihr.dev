<?php
/**
 * @SWG\Put(
 *      path="/employer/updateProfile",
 *      summary="Update Employer Profile",
 *      tags={"Employer Account"},
 *      description="Update Employer Profile",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Update Employer Information",
 *          required=true,
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="name",
 *                  description="name",
 *                  default="employer",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="email",
 *                  description="email",
 *                  default="example@example.com",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="mobile",
 *                  description="mobile",
 *                  default="0501111112",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="job_title",
 *                  description="job_title",
 *                  default="Swift Programmer",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="company_name",
 *                  description="company_name",
 *                  default="Acme2",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="company_type",
 *                  description="company_type",
 *                  default="public_sector",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="locale",
 *                  description="locale",
 *                  default="en",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */