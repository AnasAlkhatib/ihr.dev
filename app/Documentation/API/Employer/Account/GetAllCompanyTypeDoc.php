<?php
/**
 * @SWG\Get(
 *      path="/employer/companyType",
 *      summary="Get all Companies Type",
 *      tags={"Employer Account"},
 *      description="Get all Companies Type",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */