<?php
/**
 * @SWG\Post(
 *      path="/employer/changePassword",
 *      summary="Employer Change Password",
 *      tags={"Employer Account"},
 *      description="Change Password",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Employer Change Password",
 *          required=true,
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="current_password",
 *                  description="current_password",
 *                  default="123456",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="new_password",
 *                  description="new_password",
 *                  default="12345678",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="new_password_confirmation",
 *                  description="new_password_confirmation",
 *                  default="12345678",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Password will be Changed and Get Token.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Token Error",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */