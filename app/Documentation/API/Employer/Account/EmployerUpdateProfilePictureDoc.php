<?php
/**
 * @SWG\Post(
 *   path="/employer/updateProfilePicture",
 *   tags={"Employer Account"},
 *   summary="Update Employer Profile Picture",
 *   description="Update Employer Profile Picture",
 *   produces={"application/json"},
 *      @SWG\Parameter(
 *         description="token",
 *         name="token",
 *         required=true,
 *         in="query",
 *         type="string"
 *     ),
 *   @SWG\Parameter(
 *     name="file",
 *     in="formData",
 *     description="File to Upload",
 *     required=true,
 *     type="file",
 *   ),
 *      @SWG\Response(
 *          response=201,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */