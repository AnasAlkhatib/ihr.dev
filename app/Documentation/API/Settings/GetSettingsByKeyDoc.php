<?php
/**
 * @SWG\Get(
 *     path="/settings/{key}",
 *     summary="Get settings By key",
 *     tags={"Settings"},
 *     description="Get settings By keys",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *     name="key",
 *     description="key",
 *     in="path",
 *     required=true,
 *     type="string",
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */