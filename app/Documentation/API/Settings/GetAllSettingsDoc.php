<?php
/**
 * @SWG\Get(
 *      path="/settings",
 *      summary="Get all settings",
 *      tags={"Settings"},
 *      description="Get all settings",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */