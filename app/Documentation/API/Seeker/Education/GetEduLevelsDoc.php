<?php

/**
 * @SWG\Get(
 *      path="/educationlevels",
 *      summary="Get List of all   Education Levels",
 *      tags={"Seeker Education"},
 *      description="Get  Education Levels",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="bad credentials.",
 *     )
 * )
 */
