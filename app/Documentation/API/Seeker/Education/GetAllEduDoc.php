<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/education",
 *      summary="Get Seeker Education",
 *      tags={"Seeker Education"},
 *      description="Get all Seeker Education  By seeker_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */