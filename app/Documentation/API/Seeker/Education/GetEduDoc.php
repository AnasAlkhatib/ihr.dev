<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/education/{education_id}",
 *      summary="Get Seeker Education by education_id",
 *      tags={"Seeker Education"},
 *      description="Get Seeker Education by education_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="education_id",
 *         name="education_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */