<?php
/**
 * @SWG\Put(
 *      path="/seeker/{seeker_id}/education/{education_id}",
 *      summary="Update Seeker Education",
 *      tags={"Seeker Education"},
 *      description="Update Seeker Education",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="education_id",
 *         name="education_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="",
 *          default="{""edu_name"":""Acme university"",""edu_level"":""Master"",""edu_level_id"":""5"",""edu_major"":""Software Engineering"",""edu_year"":""2007-01-01""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="edu_name",
 *                description="edu_name",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="edu_level",
 *                description="edu_level",
 *                type="string"
 *             ),
 *               @SWG\Property(
 *                property="edu_level_id",
 *                description="edu_level_id",
 *                type="integer"
 *             ),
 *              @SWG\Property(
 *                property="edu_major",
 *                description="edu_major",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="edu_year",
 *                description="edu_year",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="edu_grade",
 *                description="edu_grade",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="edu_country",
 *                description="edu_country",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=201,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */
