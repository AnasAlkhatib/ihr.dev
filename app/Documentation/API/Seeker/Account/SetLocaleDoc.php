<?php
/**
 * @SWG\Post(
 *      path="/seeker/{seeker_id}/setLocale",
 *      summary="Change Seeker Preferred language",
 *      tags={"Seeker Account"},
 *      description="Change Seeker Preferred language",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="update seeker locale",
 *          default="{""locale"":""en""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="locale",
 *                description="locale",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */