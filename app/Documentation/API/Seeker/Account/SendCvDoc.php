<?php
/**
 * @SWG\Post(
 *      path="/seeker/{seeker_id}/sendCv",
 *      summary="Send seeker Cv",
 *      tags={"Seeker Account"},
 *      description="Send seeker Cv as PDF attachment to the given email",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="email To",
 *          default="{""email"":""example@example.com""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="email",
 *                description="email",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */