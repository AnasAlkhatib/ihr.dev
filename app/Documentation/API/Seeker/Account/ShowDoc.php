<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}",
 *      summary="Get Seeker",
 *      tags={"Seeker Account"},
 *      description="Get Seeker By Id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */