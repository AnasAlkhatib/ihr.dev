<?php
/**
 * @SWG\Put(
 *      path="/seeker/{seeker_id}",
 *      summary="Update Seeker Basic Info",
 *      tags={"Seeker Account"},
 *      description="Update seeker basic information",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Seeker that should be Updated",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="full_name",
 *                description="full_name",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                  property="locale",
 *                  description="Preferred language",
 *                  enum={"ar","en"},
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="gender",
 *                  description="Gender",
 *                  enum={"male","female"},
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="birth_date",
 *                  description="Birth Date",
 *                  default="1990-03-27",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="nationality",
 *                  description="seeker nationality",
 *                  default="Saudi",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="national_num",
 *                  description="National num",
 *                  default="1029654732",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="country_id",
 *                  description="country_id",
 *                  default=3,
 *                  type="integer"
 *              ),
 *              @SWG\Property(
 *                  property="city_id",
 *                  description="city_id",
 *                  default=3,
 *                  type="integer"
 *              ),
 *              @SWG\Property(
 *                  property="residence",
 *                  description="residence",
 *                  default=3,
 *                  type="integer"
 *              ),
 *              @SWG\Property(
 *                  property="address",
 *                  description="seeker address",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="skills",
 *                  description="Skills",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="marital_status",
 *                  description="Marital Status",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="dependent_num",
 *                  description="Dependent num",
 *                  default=1,
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="driving_licence",
 *                  description="Driving licence",
 *                  enum={"yes","no",""},
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="owning_vehicle",
 *                  description="Owning vehicle",
 *                  enum={"yes","no",""},
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="desired_job",
 *                  description="Desired job",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="desired_job_status",
 *                  description="Desired Job Status",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="desired_salary",
 *                  description="Desired Salary",
 *                  default="3500 SAR",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="notice_period",
 *                  description="Notice Period",
 *                  type="string"
 *              ),
 *               @SWG\Property(
 *                  property="education_level_id",
 *                  description="Education Level Id 0,1,2,3,4,5,6",
 *                  type="integer"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *      ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error."
 *     )
 * )
 */