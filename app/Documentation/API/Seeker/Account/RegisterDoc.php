<?php
/**
 * @SWG\Post(
 *      path="/seeker",
 *      summary="Register New seeker",
 *      tags={"Seeker Account"},
 *      description="Register a seeker and send activation code by email && sms",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Register New seeker",
 *          required=true,
 *          default="{""full_name"":""John Doe"",""email"":""seeker@ihr.dev"",""password"":""123456"",""mobile"":""0501111111"",""gender"":""male"",""birth_date"":""1990-10-10"",""nationality"":""Saudi Arabia""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="full_name",
 *                  description="full_name",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="email",
 *                  description="email",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="password",
 *                  description="password",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="mobile",
 *                  description="mobile",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="nationality",
 *                  description="nationality",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="gender",
 *                  description="gender",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="birth_date",
 *                  description="birth_date",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *    @SWG\Response(
 *         response="201",
 *         description="Seeker Registered successfully",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error",
 *     )
 * )
 */