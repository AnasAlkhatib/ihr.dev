<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/language/{language_id}",
 *      summary="Get Seeker Language by language_id",
 *      tags={"Seeker Language"},
 *      description="Get Seeker Language by language_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="language_id",
 *         name="language_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */