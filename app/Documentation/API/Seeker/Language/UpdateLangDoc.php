<?php
/**
 * @SWG\Put(
 *      path="/seeker/{seeker_id}/language/{language_id}",
 *      summary="Update Seeker Language",
 *      tags={"Seeker Language"},
 *      description="Update Seeker Language",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="language_id",
 *         name="language_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="name ,level",
 *          default="{""language_name"":""English"",""language_level"":""Good""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="language_name",
 *                description="language_name",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="language_level",
 *                description="language_level",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */