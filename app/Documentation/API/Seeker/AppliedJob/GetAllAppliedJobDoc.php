<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/appliedJob",
 *      summary="Get Seeker Applied Jobs",
 *      tags={"Seeker Applied Job"},
 *      description="Get all Seeker Applied Jobs  By seeker_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */