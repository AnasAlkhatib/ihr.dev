<?php
/**
 * @SWG\Post(
 *      path="/seeker/{seeker_id}/appliedJob",
 *      summary="Create Seeker Applied Job",
 *      tags={"Seeker Applied Job"},
 *      description="Create Seeker Applied Job",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="",
 *          default="{""vacant_id"":""1""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="vacant_id",
 *                description="vacant_id",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=201,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */
