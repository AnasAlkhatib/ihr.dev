<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/appliedJob/{vacant_id}",
 *      summary="Get Seeker Applied Job by vacant_id",
 *      tags={"Seeker Applied Job"},
 *      description="Get Seeker Applied Job by vacant_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="vacant_id",
 *         name="vacant_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */