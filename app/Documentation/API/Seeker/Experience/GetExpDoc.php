<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/experience/{experience_id}",
 *      summary="Get Seeker Experience by experience_id",
 *      tags={"Seeker Experience"},
 *      description="Get Seeker Experience by experience_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="experience_id",
 *         name="experience_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */