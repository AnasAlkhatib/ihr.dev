<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/experience",
 *      summary="Get Seeker Experience",
 *      tags={"Seeker Experience"},
 *      description="Get all Seeker Experience  By seeker_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */