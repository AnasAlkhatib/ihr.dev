<?php
/**
 * @SWG\Delete(
 *      path="/seeker/{seeker_id}/favoriteJob/{vacant_id}",
 *      summary="Delete Seeker Favorite Job",
 *      tags={"Seeker Favorite Job"},
 *      description="Delete Seeker Favorite Job",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="vacant_id",
 *         name="vacant_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */