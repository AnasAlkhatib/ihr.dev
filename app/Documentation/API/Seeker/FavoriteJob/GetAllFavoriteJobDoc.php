<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/favoriteJob",
 *      summary="Get Seeker Favorite Jobs",
 *      tags={"Seeker Favorite Job"},
 *      description="Get all Seeker Favorite Jobs  By seeker_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */