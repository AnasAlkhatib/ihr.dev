<?php
/**
 * @SWG\Get(
 *      path="/vacantTags",
 *      summary="Get All Available Vacant Tags",
 *      tags={"Seeker Vacant"},
 *      description="Get All Available Vacant Tags",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */
