<?php
/**
 * @SWG\Get(
 *      path="/vacant",
 *      summary="Get all vacancies",
 *      tags={"Seeker Vacant"},
 *      description="Get all vacancies",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */