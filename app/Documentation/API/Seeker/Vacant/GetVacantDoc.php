<?php
/**
 * @SWG\Get(
 *     path="/vacant/{vacant_id}",
 *     summary="Get Vacancy By vacant_id",
 *     tags={"Seeker Vacant"},
 *     description="Get Vacancy By vacant_id",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *     name="vacant_id",
 *     description="vacant_id",
 *     in="path",
 *     required=true,
 *     type="integer",
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */