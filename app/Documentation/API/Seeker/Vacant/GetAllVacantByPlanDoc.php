<?php
/**
 * @SWG\Get(
 *      path="/vacants/byplan/",
 *      summary="Get all vacancies by plan",
 *      tags={"Seeker Vacant"},
 *      description="Get all vacancies by plan",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *        name="plan_id",
 *        description="plan_id",
 *        in="query",
 *        required=true,
 *        type="integer",
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */