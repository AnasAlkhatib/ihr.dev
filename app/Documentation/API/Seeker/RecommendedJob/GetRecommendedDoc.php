<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/recommendedJob",
 *      summary="Get Recommended Jobs for Seeker",
 *      tags={"Seeker Recommended Job"},
 *      description="Get Recommended Jobs for Seeker By seeker_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     ),
 *     deprecated=true
 * )
 */