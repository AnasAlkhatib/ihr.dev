<?php
/**
 * @SWG\Get(
 *      path="/resendcode/{seeker_id}",
 *      summary="Regenerate Activation code",
 *      tags={"Seeker Activation"},
 *      description="Regenerate Activation Code and Send to the seeker via SMS",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */