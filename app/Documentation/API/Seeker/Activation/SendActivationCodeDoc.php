<?php
/**
 * @SWG\Get(
 *      path="/activatesms/{seeker_id}/{code}",
 *      summary="Account Activation by Code",
 *      tags={"Seeker Activation"},
 *      description="Activate seeker account by given code",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="activation code",
 *         name="code",
 *         required=true,
 *         in="path",
 *         type="string"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */