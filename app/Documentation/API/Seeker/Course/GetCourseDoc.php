<?php
/**
 * @SWG\Get(
 *      path="/seeker/{seeker_id}/course/{course_id}",
 *      summary="Get Seeker Course by course_id",
 *      tags={"Seeker Course"},
 *      description="Get Seeker Course by course_id",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="course_id",
 *         name="course_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */