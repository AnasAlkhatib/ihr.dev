<?php
/**
 * @SWG\Post(
 *      path="/seeker/{seeker_id}/course",
 *      summary="Create Seeker Course",
 *      tags={"Seeker Course"},
 *      description="Create Seeker Course",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="",
 *          default="{""course_name"":""Python"",""center_name"":""Acme"",""course_hours"":""120"",""course_date"":""2013-01-01""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="course_name",
 *                description="course_name",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="center_name",
 *                description="center_name",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="company_country",
 *                description="company_country",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="course_hours",
 *                description="course_hours",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="course_date",
 *                description="course_date",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=201,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */
