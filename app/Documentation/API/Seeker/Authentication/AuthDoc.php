<?php
/**
 * @SWG\Post(
 *      path="/auth",
 *      summary="Get seeker information from given token",
 *      tags={"Seeker Authentication"},
 *      description="Get seeker information from given token",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="token",
 *          required=true,
 *          default="{""token"":""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmloci5jb20uam8vbG9naW4iLCJpYXQiOjE0ODQ4NjI4NjMsImV4cCI6MTQ4NzQ5MDg2MywibmJmIjoxNDg0ODYyODYzLCJqdGkiOiJVSXkyUGV5dFFwSEVTTWk2Iiwic3ViIjoxMzE1NH0.GCC-ymjsCZzA_v6ojx2LJ5INtT3skRzraDwUpxiwxaw""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="token",
 *                  description="token",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation and get Seeker Data",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */