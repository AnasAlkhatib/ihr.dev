<?php
/**
 * @SWG\Post(
 *      path="/online",
 *      summary="Set Seeker online",
 *      tags={"Seeker Authentication"},
 *      description="Set Seeker online",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Seeker that should be stored",
 *          required=true,
 *          default="{""token"":""""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="token",
 *                  description="token",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Set Seeker online",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */