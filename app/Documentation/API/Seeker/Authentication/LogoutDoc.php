<?php
/**
 * @SWG\Post(
 *      path="/logout",
 *      summary="Logout Seeker",
 *      tags={"Seeker Authentication"},
 *      description="Logout Seeker and invalidate token",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Seeker that should be stored",
 *          required=true,
 *          default="{""token"":""""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="token",
 *                  description="token",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="invalidate the token",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="The token could not be parsed from the request.",
 *     )
 * )
 */