<?php
/**
 * @SWG\Post(
 *      path="/login",
 *      summary="Seeker Login",
 *      tags={"Seeker Authentication"},
 *      description="Generate authentication token based on email and password",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="Seeker Credentials",
 *          required=true,
 *          default="{""email"":""seeker@ihr.dev"",""password"":""123456""}",
 *        @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                  property="email",
 *                  description="email",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="password",
 *                  description="password",
 *                  type="string"
 *              ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Get Token.",
 *      ),
 *     @SWG\Response(
 *         response="401",
 *         description="Invalid Credentials.",
 *     ),
 *    @SWG\Response(
 *         response="400",
 *         description="Account Not Activate.",
 *     ),
 *    @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     )
 * )
 */