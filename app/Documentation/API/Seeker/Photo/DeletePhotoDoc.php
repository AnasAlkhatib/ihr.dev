<?php
/**
 * @SWG\Delete(
 *   path="/seeker/{seeker_id}/photo",
 *   tags={"Seeker Photo"},
 *   summary="Delete Seeker Photo",
 *   description="Delete Seeker Photo",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *     name="seeker_id",
 *     description="seeker_id",
 *     in="path",
 *     required=true,
 *     type="integer",
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */