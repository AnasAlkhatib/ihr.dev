<?php
/**
 * @SWG\Post(
 *   path="/seeker/{seeker_id}/photo",
 *   tags={"Seeker Photo"},
 *   summary="Create Seeker Photo",
 *   description="Create Seeker Photo",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *     name="seeker_id",
 *     description="seeker_id",
 *     in="path",
 *     required=true,
 *     type="integer",
 *   ),
 *   @SWG\Parameter(
 *     name="file",
 *     in="formData",
 *     description="File to Upload",
 *     required=true,
 *     type="file",
 *   ),
 *      @SWG\Response(
 *          response=201,
 *          description="Ok.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */