<?php
/**
 * @SWG\Delete(
 *      path="/seeker/{seeker_id}/notification/{notification_id}",
 *      summary="Delete Seeker Notification",
 *      tags={"Seeker Notification"},
 *      description="Delete Seeker Notification",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *         description="notification_id",
 *         name="notification_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */