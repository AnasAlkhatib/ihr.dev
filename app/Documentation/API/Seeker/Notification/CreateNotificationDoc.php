<?php
/**
 * @SWG\Post(
 *      path="/seeker/{seeker_id}/notification",
 *      summary="Update an exist Notification or create new one",
 *      tags={"Seeker Notification"},
 *      description="Update an exist Notification or create new one",
 *      produces={"application/json"},
 *      @SWG\Parameter(
 *         description="seeker_id",
 *         name="seeker_id",
 *         required=true,
 *         in="path",
 *         type="integer"
 *     ),
 *      @SWG\Parameter(
 *          name="body",
 *          in="body",
 *          description="",
 *          default="{""device_token"":""string"",""device_model"":""ONEPLUS A3003"",""device_type"":""2"",""device_version"":""23"",""notification_active"":""1""}",
 *          required=true,
 *          @SWG\Schema(
 *              type="object",
 *              @SWG\Property(
 *                property="device_token",
 *                description="device_token",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="device_model",
 *                description="device_model",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="device_type",
 *                description="device_type",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="device_version",
 *                description="device_version",
 *                type="string"
 *             ),
 *              @SWG\Property(
 *                property="notification_active",
 *                description="notification_active",
 *                type="string"
 *             ),
 *          )
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="Updated.",
 *      ),
 *      @SWG\Response(
 *          response=201,
 *          description="Created.",
 *      ),
 *     @SWG\Response(
 *         response="422",
 *         description="Validation Error.",
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */
