<?php
/**
 * @SWG\Get(
 *      path="/company_industry",
 *      summary="Get all Companies Industries",
 *      tags={"Company Industry"},
 *      description="Get all Companies Industries",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */