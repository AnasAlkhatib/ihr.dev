<?php
/**
 * @SWG\Get(
 *     path="/news/{news_id}",
 *     summary="Get News By news_id",
 *     tags={"News"},
 *     description="Get News By news_id",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *     name="news_id",
 *     description="news_id",
 *     in="path",
 *     required=true,
 *     type="integer",
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      ),
 *     @SWG\Response(
 *         response="404",
 *         description="Not Found.",
 *     )
 * )
 */