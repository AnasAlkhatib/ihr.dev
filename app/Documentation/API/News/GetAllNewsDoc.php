<?php
/**
 * @SWG\Get(
 *      path="/news",
 *      summary="Get all News",
 *      tags={"News"},
 *      description="Get all News",
 *      produces={"application/json"},
 *      @SWG\Response(
 *          response=200,
 *          description="OK.",
 *      )
 * )
 */