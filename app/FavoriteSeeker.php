<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class FavoriteSeeker
 * @package App
 */
class FavoriteSeeker extends Model
{
    /**
     * @var string
     */
    protected $table = "favorite_seekers";
    /**
     * @var array
     */
    protected $fillable = ['seeker_id'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'seeker_id' => 'int',
    ];
    /**
     * Get all of the owning favoritable models.
     */
    public function favoritable()
    {
        $this->morphTo();
    }
}
