<?php
namespace App;

use App\Base\Model\Abstracts\VacantAbstract;
/**
 * Class Vacant
 * @package App
 */
class Vacant extends VacantAbstract
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'company_industry',
        'company_location',
        'job_title',
        'job_rules',
        'job_status',
        'job_discription',
        'education',
        'certification',
        'exp_level',
        'exp_years',
        'skills',
        'language',
        'gender',
        'candidate_age',
        'candidate_address',
        'ref_num',
        'user_id',
        'employer_id',
        'client_id',
        'close_date',
        'additional_vacant_info',
        'additional_prefered_info',
        'active',
        'push_notification',
        'activated_by',
        'activation_date'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Applied_seeker()
    {
        return $this->belongsToMany('App\Seeker', 'applied_vacancies', 'vacant_id')->withPivot('ID',
            'last_view_date', 'view_num', 'last_view_recriuter')->withTimestamps();
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}