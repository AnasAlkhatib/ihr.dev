<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";
    protected $primaryKey = 'ID';
    protected $hidden = [ 'updated_at'];
    protected $fillable = ["news_title","news_body" ,"news_img",'news_posted','news_img','news_thumbnail'];
}
