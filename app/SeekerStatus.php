<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class SeekerStatus
 * @package App
 */
class SeekerStatus extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'seeker_statuses';
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'employer_id',
        'status',
        'note',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'seeker_id' => 'int',
        'employer_id' => 'int'
    ];
}
