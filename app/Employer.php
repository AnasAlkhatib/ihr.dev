<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Plans\Contracts\PlanSubscriberInterface;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use App\Plans\Traits\PlanSubscriber;

/**
 * Class Employer
 * @package App
 */
class Employer extends Authenticatable implements JWTSubject, PlanSubscriberInterface
{
    use PlanSubscriber;
    protected $table = 'employers';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'job_title',
        'company_name',
        'company_type',
        'mobile',
        'password',
        'active',
        'locale',
        'pic_file_name',
        'website'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    public function vacancies()
    {
        return $this->hasMany(Vacant::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * employer favorite seekers Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function favoriteSeekers()
    {
        return $this->morphMany(FavoriteSeeker::class, 'favoritable');
    }

    /**
     * employer seekers status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function seekers()
    {
        return $this->belongsToMany(Seeker::class, 'seeker_statuses', 'employer_id', 'seeker_id')
            ->withPivot('id', 'status', 'note', 'vacant_id', 'offer_vacancies', 'required_documents')
            ->withTimestamps();
    }

    /**
     * check if the seeker on line
     * @return bool
     */
    public function isOnline()
    {
        return \Cache::has('employer-is-online-' . $this->id);
    }
}
