<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Language extends Model
{
    protected $table = 'languages';
    protected $primaryKey = 'ID';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['seeker_id', 'language_name', 'language_level'];
    public function seeker()
    {
        return $this->belongsTo('App\Seeker');
    }
}
