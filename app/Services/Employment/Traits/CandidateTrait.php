<?php
namespace App\Services\Employment\Traits;

use App\Candidate;
use App\Client;
use App\User;
/**
 * Class CandidateTrait
 * @package App\Services\Employment\Traits
 */
trait CandidateTrait
{
    /** check if the candidate exists in the database
     * @param  $candidate
     * @return bool
     */
    public function exists($candidate)
    {
        $exists = Candidate::where('seeker_id', $candidate->seeker_id)->get();
        if (count($exists) > 0) {
            return true;
        }
        return false;
    }
    /** attach candidate to the model (user, client)
     * @param User | Client $model
     * @param Candidate $candidate
     * @param $attributes
     */
    public function attachCandidate($model, $candidate, $attributes)
    {
        $model->candidates()->attach($candidate->getKey(), $attributes);
    }
    /** update candidate to the model (user, client)
     * @param User | Client $model
     * @param Candidate $candidate
     * @param $attributes
     */
    public function updateExistingCandidate($model, $candidate, $attributes)
    {
        $model->candidates()->updateExistingPivot($candidate->getKey(), $attributes);
    }
    /** check if the candidate owned buy this user
     * @param $model
     * @param $key
     * @param $value
     * @return mixed
     */
    public function ownedBy($model, $key, $value)
    {
        $collection = $model->candidates->map(function ($query) {
            return $query->pivot;
        })->contains($key, $value);
        return $collection;
    }
}