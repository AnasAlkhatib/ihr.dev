<?php
namespace App\Services\Employment;

use App\User;
use App\Client;
use App\Candidate;
use App\Services\Employment\Traits\CandidateTrait;
use App\Util\Traits\ReferenceIDTrait;
use App\Jobs\CandidateClientSuggested;
use App\Services\Employment\Contracts\EmploymentInterface;
/**
 * Class EmploymentBuilder
 * @package App\Services\Employment
 */
class EmploymentBuilder implements EmploymentInterface
{
    use CandidateTrait;
    use ReferenceIDTrait;
    /**
     * @var User
     */
    public $user;
    /**
     * @var Client
     */
    public $client;
    /**
     * @var
     */
    protected $candidate;
    /**
     * @var array
     */
    protected $values = [];
    /**
     * @var array
     */
    public $attributes = [];
    /**
     * @var
     */
    public $alertClass;
    /**
     * @var
     */
    public $message;
    /**
     * @var
     */
    public $action;
    /**
     * EmploymentBuilder constructor.
     * @param User $user
     * @param Client $client
     * @param $candidate
     * @param $values
     * @param $attributes
     */
    public function __construct(User $user, Client $client, $candidate, $values, $attributes)
    {
        $this->user = $user;
        $this->client = $client;
        $this->candidate = $candidate;
        $this->values = $values;
        $this->attributes = $attributes;
    }
    /**
     * main class function
     * @return $this
     */
    public function run()
    {
        $this->globalCheck();
        /*
         * action 1 means update the record
         * action 2, 3, 5 means create the record
         * action 4  duplicate
         */
        switch ($this->action) {
            case in_array($this->action, array(2, 3, 5)):
                $this->createCandidate();
                $this->suggest();
                $this->notifyClient();
                break;
            case $this->action === 4:
                $this->duplicateFeedback();
                break;
            case $this->action === 1:
                $this->updateSuggested();
                $this->saveCandidate();
                break;
        }
        return $this;
    }
    /**
     * create && suggest candidate to client
     */
    public function suggest()
    {
        $this->attachCandidate($this->user, $this->candidate, $this->attributes);
        $clientAttributes = array_only($this->attributes, ['client_id', 'vacant_id']);
        $this->attachCandidate($this->client, $this->candidate, $clientAttributes);
        return $this;
    }
    /**
     * update && suggest candidate to client
     */
    protected function updateSuggested()
    {
        $this->updateExistingCandidate($this->user, $this->candidate, $this->attributes);
        $clientAttributes = array_only($this->attributes, ['client_id', 'vacant_id']);
        $this->updateExistingCandidate($this->client, $this->candidate, $clientAttributes);
        return $this;
    }
    /**
     *  $this
     */
    protected function saveCandidate()
    {
        $values = $this->values;
        $candidate = Candidate::query();
        $candidate->where('seeker_id', $this->candidate->seeker_id);
        foreach ($values as $field => $value) {
            $candidate->{$field} = $value;
        }
        if ($candidate->update($values)) {
            $this->candidate = $candidate;
            $this->message = trans('app.seeker_candidate_updated',
                ['candidate_name' => $this->candidate->name, 'client_name' => $this->client->name]);
            $this->alertClass = 'success';
        }
        return $this;
    }
    /**
     * Create  New Candidate
     */
    public function createCandidate()
    {
        $candidate = Candidate::create($this->values);
        if ($candidate) {
            $this->candidate = $candidate;
            $this->message = trans('app.seeker_candidate_created',
                ['candidate_name' => $this->candidate['name'], 'client_name' => $this->client->name]);
            $this->alertClass = 'success';
        }
        return $this;
    }
    /**
     * check the ownership of the candidate
     *
     */
    protected function globalCheck()
    {
        switch ($this) {
            case ($this->exists($this->candidate) && $this->getOwnerUser() && $this->getOwnerClient() && $this->getOwnerVacant()):
                $this->action = 1;
                break;
            case ($this->exists($this->candidate) && $this->getOwnerUser() && $this->getOwnerClient() && !$this->getOwnerVacant()):
                $this->action = 2;
                break;
            case ($this->exists($this->candidate) && $this->getOwnerUser() && !$this->getOwnerClient()):
                $this->action = 3;
                break;
            case ($this->exists($this->candidate) && !$this->getOwnerUser() && $this->getOwnerClient() && $this->getOwnerVacant()):
                $this->action = 4;
                break;
            case ($this->exists($this->candidate) && !$this->getOwnerUser() && $this->getOwnerClient() && !$this->getOwnerVacant()):
                $this->action = 5;
        }
    }
    /** if this user is owned this candidate
     * @return mixed
     */
    protected function getOwnerUser()
    {
        return $this->ownedBy($this->user, 'candidatable_id', $this->user->id);
    }
    /** if this client is owned this candidate
     * @return mixed
     */
    protected function getOwnerClient()
    {
        return $this->ownedBy($this->client, 'candidatable_id', $this->client->id);
    }
    /**
     * if this candidate suggested before to this vacant
     * @return mixed
     */
    protected function getOwnerVacant()
    {
        return $this->ownedBy($this->client, 'vacant_id', $this->attributes['vacant_id']);
    }
    /**
     * feedback message for avoid duplicate
     */
    protected function duplicateFeedback()
    {
        $this->alertClass = 'warning';
        $this->message = trans('app.seeker_candidate_created_before',
            ['candidate_name' => $this->candidate['name'], 'client_name' => $this->client->name]);
        return $this;
    }
    /**
     * Notify the client new Candidate Created
     * @return $this
     */
    protected function notifyClient()
    {
        dispatch(new CandidateClientSuggested($this->user, $this->client, $this->candidate));
        return $this;
    }
    /**
     * Re Generate Reference ID
     */
    protected function reGenerateReferenceID()
    {
        $this->values['ref_num'] = $this->generateReferenceID('App\Candidate', 'ref_num');
        return $this;
    }
}