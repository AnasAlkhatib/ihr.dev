<?php
namespace App\Services\Employment\Contracts;

/**
 * Interface EmploymentInterface
 * @package App\Services\Employment\Contracts
 */
interface EmploymentInterface
{
    /**
     * @param $candidate
     * @return mixed
     */
    public function exists($candidate);
    /**
     * @param $model
     * @param $candidate
     * @param $attributes
     * @return mixed
     */
    public function attachCandidate($model, $candidate, $attributes);
    /**
     * @param $model
     * @param $candidate
     * @param $attributes
     * @return mixed
     */
    public function updateExistingCandidate($model, $candidate, $attributes);
    /**
     * @param $model
     * @param $key
     * @param $value
     * @return mixed
     */
    public function ownedBy($model, $key, $value);
}