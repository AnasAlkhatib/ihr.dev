<?php
/**
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="IHR  APIs",
 *     version="3.8.3",
 *   )
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="jwt",
 *   type="apiKey",
 *   in="header",
 *   name="token"
 * )
 */