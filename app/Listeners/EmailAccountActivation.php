<?php
namespace App\Listeners;

use App\Events\SeekerWasRegistered;
use Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
class EmailAccountActivation implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Handle the event.
     *
     * @param  SeekerWasRegistered $event
     * @return void
     */
    public function handle(SeekerWasRegistered $event)
    {
        $email = $event->data["email"];
        $full_name = $event->data["full_name"];
        $activation_code = $event->data["activation_code"];
        Mail::send('auth.front.emails.verify', ['activation_code' => $activation_code, 'url' => env('API_URL')],
            function ($message) use ($email, $full_name) {
                $message->from('admin@sto.com.sa', trans('activate.sto'));
                $message->to($email, $full_name)->subject(trans('activate.title'));
            });
    }
}
