<?php
namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
class LastLoginListener implements ShouldQueue
{
    public function __construct()
    {
    }
    public function handle(Login $event)
    {
        $event->user->last_login = Carbon::now();
        $event->user->save();
    }
}
