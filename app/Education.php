<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Education
 * @package App
 */
class Education extends Model
{
    /**
     * @var string
     */
    protected $table = "educations";
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * @var array
     */
    protected $fillable = ['seeker_id', 'edu_year', 'edu_name', 'edu_level', 'edu_major', 'edu_grade', 'edu_country','edu_level_id'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seeker()
    {
        return $this->belongsTo('App\Seeker');
    }
}
