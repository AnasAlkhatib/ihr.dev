<?php

namespace App\Providers;

use Illuminate\Validation\Validator;

class Validation extends Validator {

    public function validatePeriod($attribute, $value, $parameters, $validator) {
        $data = $validator->getData();
        if (!array_key_exists('end_date', $data)) {
            $data["end_date"] = NULL;
        }
        if ($data["end_date"] === NULL || empty($data["end_date"]) ){
            return $this;
        }
        if (($data["start_date"] >= $data["end_date"])){
            return false;
        }else{
          return $this;  
        }
      
    }

}
