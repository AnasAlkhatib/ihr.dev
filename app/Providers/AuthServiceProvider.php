<?php
namespace App\Providers;

Use App\Vacant;
Use App\User;
Use App\Client;
Use App\Employer;
Use App\Candidate;
use App\Policies\VacantPolicy;
use App\Policies\ClientSettingsPolicy;
use App\Policies\UserProfilePolicy;
use App\Policies\EmployerSettingsPolicy;
Use App\Policies\CandidatePolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Vacant::class => VacantPolicy::class,
        User::class => UserProfilePolicy::class,
        Client::class => ClientSettingsPolicy::class,
        Employer::class => EmployerSettingsPolicy::class,
        Candidate::class => CandidatePolicy::class
    ];
    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
    }
}
