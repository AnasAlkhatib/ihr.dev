<?php
namespace App\Repositories;

use App\User;
use App\Employer;
class VacantRepository
{
    /**
     * Get all of the vacancies for a given User.
     *
     * @param  User $User
     * @return Collection
     */
    public function forUser(User $User)
    {
        return $User->vacancies()
            ->orderBy('created_at', 'asc')
            ->get();
    }
    /**
     * Get all of the vacancies for a given employer.
     *
     * @param  Employer $employer
     * @return Collection
     */
    public function forEmployer(Employer $employer)
    {
        return $employer->vacancies()
            ->orderBy('created_at', 'asc')
            ->get();
    }
}