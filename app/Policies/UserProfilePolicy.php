<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserProfilePolicy
{
    use HandlesAuthorization;

    /**
     * show currant user profile
     *
     * @param User $user
     * @param $auth_user
     * @return bool
     */
    public function editProfile(User $user, $auth_user)
    {
        return $user->id === $auth_user->id;
    }

    /**
     * Update currant user profile
     *
     * @param User $user
     * @param $auth_user
     * @return bool
     */
    public function updateProfile(User $user, $auth_user)
    {
        return $user->id === $auth_user->id;
    }

    /**
     * show currant user password change form
     *
     * @param User $user
     * @param $auth_user
     * @return bool
     */
    public function editPassword(User $user, $auth_user)
    {
        return $user->id === $auth_user->id;
    }

    /**
     * Update currant user password
     *
     * @param User $user
     * @param $auth_user
     * @return bool
     */
    public function updatePassword(User $user, $auth_user)
    {
        return $user->id === $auth_user->id;
    }
}
