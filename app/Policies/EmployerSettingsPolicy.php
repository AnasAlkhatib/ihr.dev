<?php
namespace App\Policies;

use App\Employer;
use Illuminate\Auth\Access\HandlesAuthorization;
class EmployerSettingsPolicy
{
    use HandlesAuthorization;
    /**
     * show  employer profile
     * @param Employer $employer
     * @param $auth_user
     * @return bool
     */
    public function editProfile(Employer $employer, $auth_user)
    {
        return $employer->id === $auth_user->id;
    }
    /**
     * Update  Employer profile
     * @param Employer $employer
     * @param $auth_user
     * @return bool
     */
    public function updateProfile(Employer $employer, $auth_user)
    {
        return $employer->id === $auth_user->id;
    }
    /**
     * show  employer password change form
     * @param Employer $employer
     * @param $auth_user
     * @return bool
     */
    public function editPassword(Employer $employer, $auth_user)
    {
        return $employer->id === $auth_user->id;
    }
    /**
     * update employer password change form
     * @param Employer $employer
     * @param $auth_user
     * @return bool
     */
    public function updatePassword(Employer $employer, $auth_user)
    {
        return $employer->id === $auth_user->id;
    }
}
