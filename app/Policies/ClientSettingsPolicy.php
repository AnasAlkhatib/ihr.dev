<?php
namespace App\Policies;

use App\Client;
use Illuminate\Auth\Access\HandlesAuthorization;
class ClientSettingsPolicy
{
    use HandlesAuthorization;
    /**
     * show  client profile
     * @param Client $client
     * @param $auth_user
     * @return bool
     */
    public function editProfile(Client $client, $auth_user)
    {
        return $client->id === $auth_user->id;
    }
    /**
     * Update  Client profile
     * @param Client $client
     * @param $auth_user
     * @return bool
     */
    public function updateProfile(Client $client, $auth_user)
    {
        return $client->id === $auth_user->id;
    }
    /**
     * show  client password change form
     * @param Client $client
     * @param $auth_user
     * @return bool
     */
    public function editPassword(Client $client, $auth_user)
    {
        return $client->id === $auth_user->id;
    }
    /**
     * update client password change form
     * @param Client $client
     * @param $auth_user
     * @return bool
     */
    public function updatePassword(Client $client, $auth_user)
    {
        return $client->id === $auth_user->id;
    }
}
