<?php
namespace App\Policies;

use App\Candidate;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
/**
 * Class CandidatePolicy
 * @package App\Policies
 */
class CandidatePolicy
{
    use HandlesAuthorization;
    /**
     * Update candidate only for this user
     * @param Candidate $candidate
     * @param $user
     * @return bool
     */
    public function updateCandidate($user, Candidate $candidate)
    {
        $data = $this->getCandidatables($user, $candidate->id);
        if (!empty($data)) {
            return $user->id === $data[0]['pivot']['candidatable_id'] && $candidate->id === $data[0]['pivot']['candidate_id'];
        }
    }
    /**
     * @param $user
     * @param $candidateId
     * @return mixed
     */
    public function getCandidatables($user, $candidateId)
    {
        $data = $user->candidates()->wherePivot('candidate_id',
            $candidateId)->get()->toArray();
        return $data;
    }
}