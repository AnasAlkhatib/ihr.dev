<?php
namespace App\Policies;

use App\Client;
use App\Employer;
use App\Vacant;
use Illuminate\Auth\Access\HandlesAuthorization;
/**
 * Class VacantPolicy
 * @package App\Policies
 */
class VacantPolicy
{
    use HandlesAuthorization;
    /**
     * Determine if the given  can View the given Vacant.
     *
     * @param  $user
     * @param  Vacant $vacant
     * @return bool
     */
    public function show($user, Vacant $vacant)
    {
        if ($user instanceof Client) {
            return $user->id === $vacant->client_id;
        } elseif ($user instanceof Employer) {
            return $user->id === $vacant->employer_id;
        }
    }
    /**
     * Determine if the given  can edit the given Vacant.
     *
     * @param   $user
     * @param  Vacant $vacant
     * @return bool
     */
    public function edit($user, Vacant $vacant)
    {
        if ($user instanceof Client) {
            return $user->id === $vacant->client_id;
        } elseif ($user instanceof Employer) {
            return $user->id === $vacant->employer_id;
        }
    }
    /**
     * Determine if the given  can update the given Vacant.
     *
     * @param   $user
     * @param  Vacant $vacant
     * @return bool
     */
    public function update($user, Vacant $vacant)
    {
        if ($user instanceof Client) {
            return $user->id === $vacant->client_id;
        } elseif ($user instanceof Employer) {
            return $user->id === $vacant->employer_id;
        }
    }
    /**
     * Determine if the given  can delete the given Vacant.
     *
     * @param   $user
     * @param  Vacant $vacant
     * @return bool
     */
    public function delete($user, Vacant $vacant)
    {
        if ($user instanceof Client) {
            return $user->id === $vacant->client_id;
        } elseif ($user instanceof Employer) {
            return $user->id === $vacant->employer_id;
        }
    }
}
