<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
class User extends Authenticatable
{
    use EntrustUserTrait;
    protected $table = 'users';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'candidate_id' => 'int',
        'candidatable_id' => 'int',
        'client_id' => 'int',
        'vacant_id' => 'int',
    ];
    /**
     * The attributes for  pivot candidatables
     *
     * @var array
     */
    protected $pivotCandidate = [
        'id',
        'candidate_id',
        'candidatable_id',
        'candidatable_type',
        'client_id',
        'vacant_id',
        'notes',
        'status',
        'offer_vacancies',
        'required_documents'
    ];
    /**
     * Get all of the candidates for the user.
     */
    public function candidates()
    {
        return $this->morphToMany('App\Candidate', 'candidatable')
            ->withPivot($this->pivotCandidate)->withTimestamps();
    }
    /**
     * user favorite seekers Relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function favoriteSeekers()
    {
        return $this->morphMany(FavoriteSeeker::class, 'favoritable');
    }
}
