<?php
namespace App\Base\Model\Abstracts;

/**
 * Class VacantAbstract
 * @package App\Base\Model\Abstracts
 */
use Conner\Tagging\Taggable;
abstract class VacantAbstract extends Model
{
    use Taggable;
    protected $morphClass = \App\Vacant::class;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "vacancies";
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'active',
        'activated_by',
        'activation_date',
        'employer_id',
        'user_id',
        'client_id',
        'push_notification'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'employer_id' => 'int',
        'user_id' => 'int',
        'client_id' => 'int',
    ];
    /**
     * Define relationship.
     * @return mixed
     */
    abstract function Applied_seeker();
    /**
     * Define relationship.
     * @return mixed
     */
    abstract function employer();
    /**
     * Define relationship.
     * @return mixed
     */
    abstract function user();
}