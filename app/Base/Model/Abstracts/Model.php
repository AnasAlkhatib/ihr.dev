<?php
namespace App\Base\Model\Abstracts;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;
/**
 * Class Model
 * @package App\Base\Model\Abstracts
 */
abstract class Model extends LaravelEloquentModel
{
}