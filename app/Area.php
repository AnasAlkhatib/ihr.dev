<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model {
    protected $primaryKey = 'ID';
    protected $hidden = [ 'created_at', 'updated_at'];
    protected $fillable = ['name', 'name_en'];

    public function city() {
        return $this->hasMany('App\City');
    }

}
