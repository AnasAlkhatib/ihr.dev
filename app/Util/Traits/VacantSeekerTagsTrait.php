<?php
namespace App\Util\Traits;

use App\Vacant;
use App\SeekerVacantTag;
use App\Notification;
/**
 * Class VacantSeekerTagsTrait
 * @package App\Util\Traits
 */
trait VacantSeekerTagsTrait
{
    /**
     *g et the seekers id for push notification
     * @param Vacant $vacant
     * @return mixed
     */
    public function handlePushTask(Vacant $vacant)
    {
        $vacantTags = $this->getAllTags($vacant);
        $seekerIds = $this->getSeekerVacantTag($vacantTags);
        return $this->getSeekerNotificationData($seekerIds);
    }
    /**
     * get all this vacant tags
     * @param $vacant
     * @return mixed
     */
    private function getAllTags($vacant)
    {
        return $vacant->tags->pluck('name')->toArray();
    }
    /**
     * get seekers who have those tags
     * @param array $vacantTags
     * @return array
     */
    private function getSeekerVacantTag(array $vacantTags)
    {
        $seekersVacantTag = SeekerVacantTag::query()->get();
        $data = $seekersVacantTag->map(function ($item) use ($vacantTags) {
            foreach (unserialize($item->tags) as $tag) {
                if (in_array($tag, $vacantTags)) {
                    return $item->seeker_id;
                }
            }
        })->all();
        return array_filter($data);
    }
    /**
     * get device tokens to seekers
     * @param array $seekerIds
     * @return mixed
     */
    private function getSeekerNotificationData(array $seekerIds)
    {
        $seekerNotificationData = Notification::whereIn('seeker_id', $seekerIds)
            ->where('notification_active', 1)
            ->get()
            ->toArray();
        return $seekerNotificationData;
    }
}