<?php
namespace App\Util\Traits;

/**
 * Class SeekerSearchableTrait
 * @package App\Util\Traits
 */
trait SeekerSearchableTrait
{
    /**
     * get seeker by age
     * @param $seekers
     * @param $age
     */
    public function seekerByAge($seekers, $age)
    {
        $ageFrom = substr($age, 0, strrpos($age, '_'));
        $ageTo = substr($age, strpos($age, "_") + 1);
        $seekers->whereBetween(\DB::raw('TIMESTAMPDIFF(YEAR,seekers.birth_date,CURDATE())'),
            array($ageFrom, $ageTo));
        return $seekers;
    }

    /**
     * get seeker by Education level
     * @param $seekers
     * @param $level
     * @return mixed
     */
    public function seekerByEduLevel($seekers, $level)
    {
        $eduLevel = trans('app.' . $level, [], '', 'ar');
        $seekers->whereHas('education', function ($query) use ($eduLevel) {
            $query->where('edu_level', 'LIKE', '%' . $eduLevel . '%');
        });
        return $seekers;
    }

    /**
     * get seeker by ByExpYears
     * @param $seekers
     * @param $expYears
     * @return mixed
     */
    public function seekerByExpYears($seekers, $expYears)
    {
        $expYearsFrom = substr($expYears, 0, strrpos($expYears, '_')) * 365;
        $expYearsTo = substr($expYears, strpos($expYears, "_") + 1) * 365;
        $seekers->whereHas('experience', function ($query) use ($expYearsFrom, $expYearsTo) {
            $query->select(\DB::raw('sum( datediff(ifnull(experiences.end_date, now()), experiences.start_date)+1) AS total',
                [$expYearsFrom, $expYearsTo]))
                ->having('total', '>=', $expYearsFrom)
                ->having('total', '<=', $expYearsTo);
        });
        return $seekers;
    }

    /**
     * get Seeker By Company Industry
     * @param $seekers
     * @param $companyIndustry
     * @return mixed
     */
    public function seekerByCompanyIndustry($seekers, $companyIndustry)
    {
        $companyIndustryText = \App\CompanyIndustry::find($companyIndustry)->name;
        $seekers->whereHas('experience', function ($query) use ($companyIndustryText) {
            $query->where('company_industry', 'LIKE', '%' . $companyIndustryText . '%');
        });
        return $seekers;
    }

    /**
     * get Seeker By Employment Status
     * @param $seekers
     * @param $employmentStatus
     * @return mixed
     */
    public function seekerByEmploymentStatus($seekers, $employmentStatus)
    {
        if ($employmentStatus == 'working') {
            $data = $seekers->whereHas('experience', function ($query) {
                $query->whereNull('end_date');
            });
            return $data;
        } elseif ($employmentStatus == 'not_working') {
            $data = $seekers->whereDoesntHave("experience", function ($query) {
                $query->join('experiences as exp', function ($join) {
                    $join->on('experiences.seeker_id', '=', 'experiences.seeker_id')
                        ->whereNull('experiences.end_date');
                });
            });
            return $data;
        }
    }

}

