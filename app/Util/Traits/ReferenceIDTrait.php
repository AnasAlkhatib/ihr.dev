<?php
namespace App\Util\Traits;

/**
 * Class ReferenceIDTrait
 * @package App\Util\Traits
 */
trait ReferenceIDTrait
{
    public function generateReferenceID($model, $field, $length = 9)
    {
        $referenceID = '';
        $referenceIdFound = false;
        $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $numbers = "0123456789";
        $s_letters = str_shuffle($letters);
        $s_numbers = str_shuffle($numbers);
        while (!$referenceIdFound) {
            $i = 0;
            while ($i < $length) {
                $char = substr($s_letters, 24) . substr($s_numbers, 3);
                $referenceID = substr($char, 0, $length);
                $i++;
            }
            $query = $model::where($field, $referenceID)->count();
            if ($query === 0) {
                $referenceIdFound = true;
            }
        }
        return $referenceID;
    }
}

