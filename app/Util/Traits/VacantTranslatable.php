<?php
namespace App\Util\Traits;

/**
 * Class VacantTranslatable
 * @package App\Util\Traits
 */
trait VacantTranslatable
{
    /**
     * Translate company Industry
     * @param $value
     * @return mixed
     */
    public function getCompanyIndustryAttribute($value)
    {
        $companyIndustry = \App\CompanyIndustry::find($value);
        if ($companyIndustry) {
            if (\App::getLocale() == 'en') {
                return $companyIndustry->name_en;
            }
            return $companyIndustry->name;
        }
        return $value;
    }
    /**
     * Translate Company locations
     * @param $value
     * @return mixed
     */
    public function getCompanyLocationAttribute($value)
    {
        $areas = explode(',', $value);
        $appLocale = \App::getLocale();
        $data = array();
        foreach ($areas as $area) {
            $modelByID = \App\Area::find($area);
            $modelByName = \App\Area::where('name', '=', $area)->first();
            switch ($area) {
                case $modelByID && $appLocale == 'en':
                    $data [] = $modelByID->name_en;
                    break;
                case $modelByID && $appLocale == 'ar':
                    $data [] = $modelByID->name;
                    break;
                case $modelByName && $appLocale == 'en':
                    $data [] = $modelByName->name_en;
                    break;
                case $modelByName && $appLocale == 'ar':
                    $data [] = $modelByName->name;
            }
        }
        return implode(',', $data);
    }
    /**
     * Translate Job Rules
     * @param $value
     * @return mixed
     */
    public function getJobRulesAttribute($value)
    {
        $jobRole = \App\JobRole::find($value);
        if ($jobRole) {
            if (\App::getLocale() == 'en') {
                return $jobRole->name_en;
            }
            return $jobRole->name;
        }
        return $value;
    }
    /**
     * Translate Job status
     * @param $value
     * @return mixed
     */
    public function getJobStatusAttribute($value)
    {
        if ($value != '') {
            return trans('app.' . $value);
        }
        return $value;
    }
//    /**
//     * Translate Education
//     * @param $value
//     * @return mixed
//     */
//    public function getEducationAttribute($value)
//    {
//        if ($value != '') {
//            return trans('app.' . $value);
//        }
//        return $value;
//    }
    /**
     * Translate Experience Level
     * @param $value
     * @return mixed
     */
    public function getExpLevelAttribute($value)
    {
        if ($value != '') {
            return trans('app.' . $value);
        }
        return $value;
    }
    /**
     * Translate Language
     * @param $value
     * @return mixed
     */
    public function getLanguageAttribute($value)
    {
        $data = array();
        if ($value != '') {
            $languages = explode(',', $value);
            foreach ($languages as $language) {
                $data [] = trim(trans('app.' . $language));
            }
            return implode(',', $data);
        }
        return $value;
    }
    /**
     * Translate Gender
     * @param $value
     * @return mixed
     */
    public function getGenderAttribute($value)
    {
        if ($value != '') {
            return trans('app.' . $value);
        }
        return $value;
    }
    /**
     * exp_level Setter
     */
    public function setExpLevelAttribute()
    {
        $this->attributes['exp_level'] = trim(\Request::input('exp_level'));
    }
}