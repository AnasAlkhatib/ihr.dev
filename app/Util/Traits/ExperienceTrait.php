<?php
namespace App\Util\Traits;

use DB;
/**
 * Class ExperienceTrait
 * @package App\Util\Traits
 */
trait ExperienceTrait
{
    /**
     * @param $model
     * @param $where
     * @return string
     */
    public function getExperience($model, $where)
    {
        $totalExperience = DB::table('experiences')
            ->select(DB::raw('sum( datediff( ifnull(experiences.end_date, now()), experiences.start_date)+1) AS total'))
            ->where($where, '=', $model->getKey())
            ->groupBy($where)
            ->get();
        if (count($totalExperience) > 0) {
            $days = (int)$totalExperience[0]->total;
            return $this->convertExperienceDays($days);
        }
        return " " . trans('app.not_found');
    }
    /**
     * @param $days
     * @return string
     */
    public function convertExperienceDays($days)
    {
        $years = ($days / 365);
        $years = floor($years);
        $month = ($days % 365) / 30.5;
        $months = floor($month);
        ($years > 0) ? $returned_years = $years . " " . trans('app.year') : $returned_years = '';
        ($months > 0) ? $returned_months = $months . " " . trans('app.month') : $returned_months = '';
        return $returned_years . ' ' . $returned_months;
    }
}
