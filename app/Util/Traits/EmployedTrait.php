<?php
namespace App\Util\Traits;

/**
 * Class EmployedTrait
 * @package App\Util\Traits
 */
trait EmployedTrait
{
    /**
     * @param $model
     * @return bool
     */
    public function isEmployed($model)
    {
        $array = array();
        $is_working = false;
        $experience = $model->experience->all();
        foreach ($experience as $exp) {
            if (!$exp->end_date || strtotime($exp->end_date) < 0) {
                $array [] = $exp->start_date;
            }
        }
        if (!empty($array)) {
            $is_working = true;
        }
        return $is_working;
    }
}
