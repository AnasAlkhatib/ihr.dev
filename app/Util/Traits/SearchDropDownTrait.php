<?php
namespace App\Util\Traits;

/**
 * Class SearchDropDownTrait
 * @package App\Util\Traits
 */
trait SearchDropDownTrait
{
    public function buildSearchDropDown()
    {
        /*
         * genders for search filter
         */
        $genders = collect(
            [
                ['name' => trans('app.male'), 'value' => 'male'],
                ['name' => trans('app.female'), 'value' => 'female']
            ]
        );
        /*
        * nationalities for search filter
        */
        $nationalities = collect(
            [
                ['name' => trans('app.saudi'), 'value' => 1],
                ['name' => trans('app.non_saudi'), 'value' => 2]
            ]
        );
        /*
         * AgePeriods for search filter
         */
        $agePeriods = collect([
            ['name' => trans('advanced_search.50_older'), 'value' => '50_150'],
            ['name' => trans('advanced_search.45_50'), 'value' => '45_50'],
            ['name' => trans('advanced_search.40_45'), 'value' => '40_45'],
            ['name' => trans('advanced_search.35_40'), 'value' => '35_40'],
            ['name' => trans('advanced_search.30_35'), 'value' => '30_35'],
            ['name' => trans('advanced_search.25_30'), 'value' => '25_30'],
            ['name' => trans('advanced_search.20_25'), 'value' => '20_25'],
            ['name' => trans('advanced_search.20_younger'), 'value' => '0_20'],
        ]);
        /*
         * Educations for search filter
         */
        $educationLevel = collect([
            ['name' => trans('app.doctorate'), 'value' => 'doctorate'],
            ['name' => trans('app.master'), 'value' => 'master'],
            ['name' => trans('app.high_diploma'), 'value' => 'high_diploma'],
            ['name' => trans('app.bachelor'), 'value' => 'bachelor'],
            ['name' => trans('app.diploma'), 'value' => 'diploma'],
            ['name' => trans('app.high_school'), 'value' => 'high_school'],
        ]);
        /*
         * experience years  for search filter
         */
        $expYears = collect([
            ['name' => trans('advanced_search.experience_less_one'), 'value' => '0_1'],
            ['name' => trans('advanced_search.experience_1_2'), 'value' => '1_2'],
            ['name' => trans('advanced_search.experience_2_5'), 'value' => '2_5'],
            ['name' => trans('advanced_search.experience_5_10'), 'value' => '5_10'],
            ['name' => trans('advanced_search.experience_more_10'), 'value' => '10_100'],
        ]);
        $companyIndustry = \App\CompanyIndustry::all();
        $companyIndustryData = [];
        foreach ($companyIndustry as $object) {
            if (\App::getLocale() == 'en') {
                $companyIndustryData [] = ['value' => $object->ID, 'name' => $object->name_en];
            } else {
                $companyIndustryData [] = ['value' => $object->ID, 'name' => $object->name];
            }
        }
        /*
         *    build master array
         */
        $dataArray['genders'] = $genders;
        $dataArray['nationalities'] = $nationalities;
        $dataArray['agePeriods'] = $agePeriods;
        $dataArray['educationLevel'] = $educationLevel;
        $dataArray['expYears'] = $expYears;
        $dataArray['companyIndustry'] = $companyIndustryData;
        return $dataArray;
    }
}

