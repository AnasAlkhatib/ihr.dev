<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = "notifications";
    protected $primaryKey = 'ID';
    //protected $hidden = [ 'created_at', 'updated_at'];
    protected $fillable = ['seeker_id', 'device_token','device_type',
        'device_model', 'device_version', 'notification_active'
    ];
    
    public function seeker() {
        return $this->belongsToMany('App\Seeker');
    }
}
