<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Candidate
 * @package App
 */
class Candidate extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * @var array
     */
    protected $fillable = [
        /* System information */
        'seeker_id',
        'ref_num',
        /* Personal information */
        'name',
        'email',
        'mobile',
        'birth_date',
        'nationality',
        'national_num',
        'city_id',
        'address',
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * The attributes for  pivot candidatables
     *
     * @var array
     */
    protected $pivotCandidate = [
        'id',
        'candidate_id',
        'candidatable_id',
        'candidatable_type',
        'client_id',
        'vacant_id',
        'notes',
        'status',
        'offer_vacancies',
        'required_documents'
    ];
    /**
     * Get all of the users that are assigned this candidate.
     */
    public function users()
    {
        return $this->morphedByMany('App\User', 'candidatable')
            ->withPivot($this->pivotCandidate)
            ->withTimestamps();
    }
    /**
     * Get all of the clients that are assigned this candidate.
     */
    public function clients()
    {
        return $this->morphedByMany('App\Client', 'candidatable')
            ->withPivot($this->pivotCandidate)
            ->withTimestamps();
    }
}
