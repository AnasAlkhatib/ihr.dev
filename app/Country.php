<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Country extends Model
{
    protected $table = 'countries';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'capital',
        'citizenship',
        'country_code',
        'currency',
        'currency_code',
        'currency_sub_unit',
        'currency_symbol',
        'full_name',
        'iso_3166_2',
        'iso_3166_3',
        'name',
        'name_ar',
        'region_code',
        'sub_region_code',
        'eea',
        'calling_code',
        'flag'
    ];
    /**
     * Get the user that owns the phone.
     */
    public function seeker()
    {
        return $this->hasMany('App\Seeker');
    }
}
