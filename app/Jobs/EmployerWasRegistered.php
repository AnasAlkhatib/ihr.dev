<?php
namespace App\Jobs;

use App\User;
use App\Employer;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
class EmployerWasRegistered extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user;
    protected $employer;
    protected $employerPlan;
    /**
     * Create a new job instance.
     *
     * @param  User $user
     * @param  Employer $employer
     * @param  Employer $employerPlan
     * EmployerWasRegistered constructor.
     */
    public function __construct(User $user, Employer $employer, $employerPlan)
    {
        $this->user = $user;
        $this->employer = $employer;
        $this->employerPlan = $employerPlan;
    }
    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('auth.admin.emails.employer_was_registered',
            [
                'user' => $this->user,
                'employer' => $this->employer,
                'employerPlan' => $this->employerPlan,
                'link' => \Config::get('app.url')
            ],
            function ($message) {
                $message->from('no-reply@sto.com.sa', trans('app.sto'));
                $message->to($this->user->email,
                    $this->user->name)->subject(trans('system_message.employer_registered.subject'));
            });
    }
}
