<?php
namespace App\Jobs;

use App\Jobs\Job;
use App\Vacant;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
/**
 * Class PermissionPublishClientVacant
 * @package App\Jobs
 */
class PermissionPublishUserVacant extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var User
     */
    protected $user;
    /**
     * @var Vacant
     */
    protected $vacant;
    /**
     * PermissionPublishClientVacant constructor.
     * @param User $user
     * @param Vacant $vacant
     */
    public function __construct(User $user, Vacant $vacant)
    {
        $this->user = $user;
        $this->vacant = $vacant;
    }
    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $recipientsList = \Config::get('vacant-post.recipients_list');
        $mailer->send('auth.admin.emails.user_vacant_was_created',
            ['vacant' => $this->vacant, 'user' => $this->user, 'link' => \Config::get('app.url')],
            function ($message) use ($recipientsList) {
                $message->from('no-reply@sto.com.sa', trans('vacant_event.sto'));
                $message->to($recipientsList)->subject(trans('vacant_event.title'));
            });
    }
}
