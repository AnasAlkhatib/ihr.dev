<?php
namespace App\Jobs;

use File;
use App\Jobs\Job;
use App\Candidate;
use App\User;
use App\Seeker;
use App\City;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use  \Elibyy\TCPDF\TCPDF as PDF;
/**
 * Class GenerateSeekerCv
 * @package App\Jobs
 */
class GenerateCandidateProfile extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var array
     */
    protected $recipient;
    /**
     * @var Candidate
     */
    protected $candidate;
    /**
     * @var User
     */
    protected $user;
    /**
     * GenerateCandidateProfile constructor.
     * @param User $user
     * @param Candidate $candidate
     * @param array $recipient
     */
    public function __construct(User $user, Candidate $candidate, $recipient)
    {
        $this->user = $user;
        $this->candidate = $candidate;
        $this->recipient = $recipient;
    }
    /**
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $this->checkDir();
        $pdf_settings = \Config::get('tcpdf');
        $pdf = new PDF($pdf_settings['page_orientation'], $pdf_settings['page_units'], $pdf_settings['page_format'],
            true, 'UTF-8', true);
        $pdf->SetFont('AlMohanad', '', '15');
        $pdf->SetMargins(3, 1, 3, true);
        $pdf->AddPage();
        /* footer call back */
        $pdf->setFooterCallback(function ($pdf) {
            $image = public_path('img/powered_by_red.png');
            if ($pdf->PageNo() === $pdf->getNumPages()) {
                $pdf->SetY(-18.10);
                $pdf->setImageScale(1.53);
                $pdf->Image($image, '', '', '', '', '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
            }
        });
        $address = $this->getCandidateFullAddress($this->candidate->city_id);
        $seeker = $this->getCandidateSeekerProfile($this->candidate->seeker_id);
        if (empty($seeker)) {
            $view = view('web.dashboard.candidate.email.no_seeker_profile',
                ['candidate' => $this->candidate, 'seeker' => $seeker, 'address' => $address])->render();
        } else {
            $view = view('web.dashboard.candidate.email.has_seeker_profile',
                ['candidate' => $this->candidate, 'seeker' => $seeker, 'address' => $address])->render();
        }
        $pdf->writeHTML($view, true, false, true, false, '');
        $cv_data_name = 'Profile_' . $this->candidate->ref_num . '_' . date("Y-m-d");
        $file_name = public_path('temp/') . $cv_data_name . '.pdf';
        $pdf->output($file_name, 'F');
        if ($pdf) {
            $this->sendEmail($mailer, $this->recipient, $file_name);
        }
    }
    /**
     * @param $mailer
     * @param $recipient
     * @param $file_name
     */
    public function sendEmail($mailer, $recipient, $file_name)
    {
        $mailer->send('web.dashboard.candidate.email.generated_profile_email',
            ['candidate' => $this->candidate, 'user' => $this->user],
            function ($message) use ($recipient, $file_name) {
                $message->from($this->user->email, trans('app.sto'));
                $message->to($recipient)->subject(trans('system_message.candidate_profile_pdf.subject'));
                $message->attach($file_name);
            });
        if (count($mailer->failures()) == 0) {
            File::cleanDirectory(public_path('/temp'));
        }
    }
    /**
     *
     */
    public function checkDir()
    {
        $temp_path = public_path('/temp');
        if (!File::exists($temp_path)) {
            File::makeDirectory($temp_path, $mode = 0777, true, true);
        }
    }
    /**
     * @param $seeker_id
     * @return array
     */
    public function getCandidateSeekerProfile($seeker_id)
    {
        $seeker = array();
        if ($seeker_id !== 0) {
            $seeker = Seeker::find($seeker_id);
        }
        return $seeker;
    }
    /**
     * @param $city_id
     * @return array
     */
    public function getCandidateFullAddress($city_id)
    {
        $address = array();
        $city = City::find($city_id);
        if ($city) {
            $address ['city'] = $city->name;
            $address ['area'] = $city->area()->first()->name;
        }
        return $address;
    }
}
