<?php
namespace App\Jobs;

use File;
use App\Jobs\Job;
use App\Seeker;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use  \Elibyy\TCPDF\TCPDF as PDF;
/**
 * Class GenerateSeekerCv
 * @package App\Jobs
 */
class GenerateSeekerCv extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $recipient;
    protected $seeker;
    protected $data;
    /**
     * GenerateSeekerCv constructor.
     * @param Seeker $seeker
     * @param array $recipient
     */
    public function __construct(Seeker $seeker, $recipient)
    {
        $this->seeker = $seeker;
        $this->recipient = $recipient;
    }
    public function handle(Mailer $mailer)
    {
        $this->checkDir();
        $pdf_settings = \Config::get('tcpdf');
        $pdf = new PDF($pdf_settings['page_orientation'], $pdf_settings['page_units'], $pdf_settings['page_format'],
            true, 'UTF-8', true);
        $pdf->SetFont('AlMohanad', '', '15');
        $pdf->SetMargins(3, 1, 3, true);
        $pdf->AddPage();
        /* footer call back */
        $pdf->setFooterCallback(function ($pdf) {
            $image = public_path('img/powered_by_red.png');
            if ($pdf->PageNo() === $pdf->getNumPages()) {
                $pdf->SetY(-18.10);
                $pdf->setImageScale(1.53);
                $pdf->Image($image, '', '', '', '', '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
            }
        });
        $view = view('api.seeker.seeker_cv_pdf', ['seeker' => $this->seeker])->render();
        $pdf->writeHTML($view, true, false, true, false, '');
        $cv_data_name = 'cv_' . $this->seeker->full_name . '_' . date("Y-m-d");
        $file_name = public_path('temp/') . $cv_data_name . '.pdf';
        $pdf->output($file_name, 'F');
        if ($pdf) {
            $this->sendEmail($mailer, $this->recipient, $file_name);
        }
    }
    public function sendEmail($mailer, $recipient, $file_name)
    {
        $mailer->send('api.seeker.generated_cv_email', ['seeker' => $this->seeker],
            function ($message) use ($recipient, $file_name) {
                $message->from('no-reply@sto.com.sa', trans('app.sto'));
                $message->to($recipient)->subject(trans('system_message.seeker_cv_pdf.subject'));
                $message->attach($file_name);
            });
        if (count($mailer->failures()) == 0) {
            File::cleanDirectory(public_path('/temp'));
        }
    }
    public function checkDir()
    {
        $temp_path = public_path('/temp');
        if (!File::exists($temp_path)) {
            File::makeDirectory($temp_path, $mode = 0777, true, true);
        }
    }
}
