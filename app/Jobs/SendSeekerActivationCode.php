<?php
namespace App\Jobs;

use App\Jobs\Job;
use App\Seeker;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
/**
 * Class SendSeekerActivationCode
 * @package App\Jobs
 */
class SendSeekerActivationCode extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var Seeker
     */
    protected $seeker;
    /**
     * SendSeekerActivationCode constructor.
     * @param Seeker $seeker
     */
    public function __construct(Seeker $seeker)
    {
        $this->seeker = $seeker;
    }
    /**
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('auth.front.emails.verify',
            ['activation_code' => $this->seeker->activation_code, 'url' => \Config::get('app.api_url')],
            function ($message) {
                $message->from('no-reply@sto.com.sa', trans('activate.sto'));
                $message->to($this->seeker->email, $this->seeker->full_name)->subject(trans('activate.title'));
            });
    }
}
