<?php
namespace App\Jobs;

use App\Jobs\Job;
use App\Vacant;
use App\Employer;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
class PermissionPublishEmployerVacant extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $employer;
    protected $vacant;
    public function __construct(Employer $employer, Vacant $vacant)
    {
        $this->employer = $employer;
        $this->vacant = $vacant;
    }
    /**
     * Execute the job.
     *
     * @param  Mailer $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $recipientsList = \Config::get('vacant-post.recipients_list');
        $mailer->send('auth.admin.emails.employer_vacant_was_created',
            ['vacant' => $this->vacant, 'employer' => $this->employer, 'link' => \Config::get('app.url')],
            function ($message) use ($recipientsList) {
                $message->from('no-reply@sto.com.sa', trans('vacant_event.sto'));
                $message->to($recipientsList)->subject(trans('vacant_event.title'));
            });
    }
}
