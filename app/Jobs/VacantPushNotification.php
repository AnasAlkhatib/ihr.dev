<?php
namespace App\Jobs;

use App\Jobs\Job;
use App\Vacant;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Edujugon\PushNotification\PushNotification;

/**
 * Class VacantPushNotification
 * @package App\Jobs
 */
class VacantPushNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var
     */
    protected $vacant;

    /**
     * VacantPushNotification constructor.
     * @param $data
     * @param Vacant $vacant
     */
    public function __construct($data, Vacant $vacant)
    {
        $this->data = $data;
        $this->vacant = $vacant;
    }

    /**
     * job handel method
     */
    public function handle()
    {
        $this->processJob();
        $this->vacantPushed($this->vacant);
    }

    /**
     * main push notification job
     *
     */
    protected function processJob()
    {
        /*
         * get the message title and body
         */
        $messageTitle = $this->messageTitle();
        $messageBody = $this->messageBody();
        /*
         * loop throw data array ,seeker device token
         */
        for ($i = 0; $i <= count($this->data) - 1; $i++) {
            if ($this->data[$i]['device_type'] == 1) {
                $this->pushAPNS($this->data[$i]['device_token'], $messageTitle, $messageBody, 'job_id',
                    $this->vacant->ID);
            } elseif ($this->data[$i]['device_type'] == 2) {
                $this->pushFCM($this->data[$i]['device_token'], $messageTitle, $messageBody, 'job_id',
                    $this->vacant->ID);
            }
        }
    }

    /**
     * Push notification using FCM Provider
     * @param $deviceToken
     * @param $messageTitle
     * @param $messageBody
     * @param $dataKey
     * @param $dataValue
     */
    protected function pushFCM($deviceToken, $messageTitle, $messageBody, $dataKey, $dataValue)
    {
        $push = new PushNotification('fcm');
        $push->setMessage([
            'notification' => [
                'title' => $messageTitle,
                'body' => $messageBody,
                'click_action' => 'com.example.areejzu.ihrapp.activities.JobDetailsTestAct',
                'sound' => 'default'
            ],
            'data' => [$dataKey => $dataValue]
        ])
            ->setDevicesToken($deviceToken)
            ->send()
            ->getFeedback();
        /* get the feed back */
        if (isset($push->feedback->error)) {
            \Log::info('notification Not Pushed to fcm:' . $deviceToken);
        } else {
            \Log::info('notification  Pushed successfully to fcm:' . $deviceToken);
        }
    }

    /**
     * Push notification using APN Provider
     * @param $deviceToken
     * @param $messageTitle
     * @param $messageBody
     * @param $dataKey
     * @param $dataValue
     */
    protected function pushAPNS($deviceToken, $messageTitle, $messageBody, $dataKey, $dataValue)
    {
        $push = new PushNotification('apn');
        $push->setMessage([
            'aps' => [
                'alert' => [
                    'title' => $messageTitle,
                    'body' => $messageTitle,
                ],
                'sound' => 'default'
            ],
            'data' => [$dataKey => $dataValue]

        ])->setDevicesToken($deviceToken)
            ->send()
            ->getFeedback();
        /* get the feed back */
        if (isset($push->feedback->error)) {
            \Log::info('notification Not Pushed to apn:' . $deviceToken);
        } else {
            \Log::info('notification  Pushed successfully to apn:' . $deviceToken);
        }
    }

    /**
     * Push Notification Message Title
     * @return string
     */
    protected function messageTitle()
    {
        /*
         * @todo
         * we need to check the seeker locale not the app locale
         * for now the default is arabic
         */
        //$locale = \App::getLocale();
        $locale = 'ar';
        $message = trans('app.added_new_job', [], '', $locale);
        $message .= " ( " . $this->vacant->job_title . " ) ";
        $message .= trans('app.suitable_your_cv', [], '', $locale);
        return $message;
    }

    /**
     * Push Notification Message Body
     * @return string
     * @todo this method not implemented yet , arrangement with the mobile developers
     */
    protected function messageBody()
    {
        // return 'This is the message body';
    }

    /**
     * flag the vacant as pushed
     * @param Vacant $vacant
     */
    protected function vacantPushed(Vacant $vacant)
    {
        $vacant->push_notification = true;
        $vacant->save();
    }
}
