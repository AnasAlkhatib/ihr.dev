<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class CompanyIndustry
 * @package App
 */
class CompanyIndustry extends Model
{
    /**
     * @var string
     */
    protected $table = 'company_industries';
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * @var array
     */
    protected $fillable = ['name', 'name_en'];
}