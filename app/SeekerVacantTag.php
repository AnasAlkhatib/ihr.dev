<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class SeekerVacantTag
 * @package App
 */
class SeekerVacantTag extends Model
{
    /**
     * @var string
     */
    protected $table = "seeker_vacant_tag";
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = ['seeker_id', 'tags'];
    /**
     * Get the seeker that owns the  Vacant Tag.
     *
     */
    public function seeker()
    {
        return $this->belongsTo('App\Seeker');
    }
}
