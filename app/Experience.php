<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Experience
 * @package App
 */
class Experience extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'seeker_id',
        'company_industry',
        'job_rule',
        'job_title',
        'job_description',
        'leave_reason',
        'last_salary',
        'company_country',
        'company_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seeker()
    {
        return $this->belongsTo('App\Seeker');
    }
}
