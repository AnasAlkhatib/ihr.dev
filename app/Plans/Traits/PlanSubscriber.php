<?php

namespace App\Plans\Traits;

use App;
use Carbon\Carbon;
use App\Plans\SubscriptionBuilder;
use App\Plans\SubscriptionUsageManager;
use App\Plans\Contracts\PlanInterface;
use App\Plans\Contracts\PlanSubscriptionInterface;

trait PlanSubscriber
{
    /**
     * Get a subscription by name.
     *
     * @param  string $name
     * @return \App\Plans\Models\Subscription|null
     */
    public function subscription($name = 'default')
    {
        $subscriptions = $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        });

        foreach ($subscriptions as $key => $subscription) {
            if ($subscription->name === $name) {
                return $subscription;
            }
        }
    }

    /**
     * Get user plan subscription.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscriptions()
    {
        return $this->hasMany(config('ihrplans.models.plan_subscription'));
    }

    /**
     * Check if the user has a given subscription.
     *
     * @param  string $subscription
     * @param  int $planId
     * @return bool
     */
    public function subscribed($subscription = 'default', $planId = null)
    {
        $subscription = $this->subscription($subscription);

        if (is_null($subscription))
            return false;

        if (is_null($planId))
            return $subscription->active();

        if ($planId == $subscription->plan_id AND $subscription->active())
            return true;

        return false;
    }

    /**
     * @param string $subscription
     * @param $plan
     * @return SubscriptionBuilder
     */
    public function newSubscription($subscription = 'default', $plan)
    {
        return new SubscriptionBuilder($this, $subscription, $plan);
    }

    /**
     * @param string $subscription
     * @return SubscriptionUsageManager
     */
    public function subscriptionUsage($subscription = 'default')
    {
        return new SubscriptionUsageManager($this->subscription($subscription));
    }
}
