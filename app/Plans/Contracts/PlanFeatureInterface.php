<?php

namespace App\Plans\Contracts;

interface PlanFeatureInterface
{
    public function plan();
    public function usage();
}