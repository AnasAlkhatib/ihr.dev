<?php
namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\XSSMiddleware::class,
            \App\Http\Middleware\LocalizationMiddleware::class,
        ],
        'admin' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\XSSMiddleware::class,
            \App\Http\Middleware\LocalizationMiddleware::class,
        ],
        'client' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\XSSMiddleware::class,
            \App\Http\Middleware\LocalizationMiddleware::class,
            \App\Http\Middleware\LogLastEmployerActivity::class,
        ],
        'employer' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\XSSMiddleware::class,
            \App\Http\Middleware\LocalizationMiddleware::class,
          //  \App\Http\Middleware\LogLastEmployerActivity::class,
        ],
        'front' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\XSSMiddleware::class,
            \App\Http\Middleware\LocalizationMiddleware::class,
        ],
        'api' => [
            'throttle:60,1',
        ],
    ];
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        /* default auth */
        'auth' => \App\Http\Middleware\Authenticate::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        /* admin auth */
        'admin.auth' => \App\Http\Middleware\AdminAuthenticate::class,
        'admin.guest' => \App\Http\Middleware\RedirectAdminIfAuthenticated::class,
        /* employer auth */
        'client.auth' => \App\Http\Middleware\ClientAuthenticate::class,
        'client.guest' => \App\Http\Middleware\RedirectClientIfAuthenticated::class,
        /* employer auth */
        'employer.auth' => \App\Http\Middleware\EmployerAuthenticate::class,
        'employer.guest' => \App\Http\Middleware\RedirectEmployerIfAuthenticated::class,
        /* public auth */
        'front.auth' => \App\Http\Middleware\FrontAuthenticate::class,
        'front.guest' => \App\Http\Middleware\RedirectFrontIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'jwt.auth' => \Tymon\JWTAuth\Http\Middleware\Authenticate::class,
        'jwt.refresh' => \Tymon\JWTAuth\Http\Middleware\RefreshToken::class,
        'xss' => \App\Http\Middleware\XSSMiddleware::class,
        'locale' => \App\Http\Middleware\LocalizationMiddleware::class,
        'api.locale' => \App\Http\Middleware\APILocalizationMiddleware::class,
    ];
}
