<?php
/*
  |--------------------------------------------------------------------------
  | Web application  Main  Routes  www.ihr.com.sa
  |--------------------------------------------------------------------------
 */
Route::group(['domain' => env('APP_URL', 'testing.ihr.com.sa')], function () {
    Route::group(['middleware' => ['front']], function () {
        Route::group(['namespace' => 'Web\Front'], function () {
            /*  Home Page  */
            Route::get('/', 'HomeController@index');
            /*  Authentication routes... */
            Route::get('login', 'Auth\AuthController@getLogin');
            Route::post('login', 'Auth\AuthController@postLogin');
            Route::get('logout', 'Auth\AuthController@logout');
            /* Password reset link request routes... */
            Route::get('password/email', 'Auth\PasswordController@getEmail');
            Route::post('password/email', 'Auth\PasswordController@postEmail');
            /* Password reset routes... */
            Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
            Route::post('password/reset', 'Auth\PasswordController@postReset');
        });
    });
    /* Dashboard routes */
    Route::group(['middleware' => ['admin']], function () {
        /* language  switcher */
        Route::post('lang', ['as' => 'lang', 'uses' => 'LanguageSwitcherController@index']);
        /* Dashboard  routes */
        Route::group(['prefix' => 'dashboard/', 'namespace' => 'Web\Dashboard'], function () {
            Route::get('login', 'Auth\AuthController@getLogin');
            Route::post('login', 'Auth\AuthController@postLogin');
            Route::get('logout', 'Auth\AuthController@logout');
            /* Password reset link request routes... */
            Route::get('password/email', 'Auth\PasswordController@getEmail');
            Route::post('password/email', 'Auth\PasswordController@postEmail');
            /* Password reset routes... */
            Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
            Route::post('password/reset', 'Auth\PasswordController@postReset');
            /* Dashboard routes */
            Route::resource('/', 'DashboardController', ['only' => ['index']]);
            /* user routes */
            Route::get('user/list', 'UserController@user_list');
            /* edit profile */
            Route::get('user/{id}/editProfile', 'UserController@editProfile');
            Route::post('user/{id}/updateProfile', 'UserController@updateProfile');
            /* User change password  */
            Route::get('user/{id}/editPassword', 'UserController@editPassword');
            Route::post('user/{id}/updatePassword', 'UserController@updatePassword');
            Route::resource('user', 'UserController');
            /* employer routes */
            Route::get('employer/list', 'EmployerController@employer_list');
            Route::resource('employer', 'EmployerController');
            /* client routes */
            Route::get('client/getAllClients', 'ClientController@getAllClients');
            Route::get('client/getClientVacancies/{id}', 'ClientController@getClientVacancies');
            Route::get('client/list', 'ClientController@clientList');
            Route::resource('client', 'ClientController');
            /* Vacant routes */
            Route::get('vacant/list', 'VacantController@vacant_list');
            Route::get('vacant/{id}/activate', 'VacantController@activate');
            Route::get('vacant/{id}/deactivate', 'VacantController@deactivate');
            Route::resource('vacant', 'VacantController');
            /* News routes */
            Route::get('news/list', 'NewsController@news_list');
            Route::resource('news', 'NewsController');
            /* AppliedVacant */
            Route::any('vacant/{id}/applied-vacant', ['uses' => 'AppliedVacantController@index']);
            /* Area  for drop down */
            Route::resource('area', 'AreaController');
            /* cities  for drop down */
            Route::resource('city', 'CityController');
            /* relation area city */
            Route::resource('area.city', 'CityController@getCity');
            /* list all seekers */
            Route::get('seeker', 'SeekerController@index');
            /* git  seeker */
            Route::get('seeker/{id}', 'SeekerController@get');
            /* Manually activate seeker Account */
            Route::get('seeker/{id}/activateAccount', 'SeekerController@activateAccount');
            /* Manually deactivate seeker Account */
            Route::get('seeker/{id}/deactivateAccount', 'SeekerController@deactivateAccount');
            /* Delete seeker account */
            Route::delete('seeker/{id}', 'SeekerController@destroy');
            /* Candidate Routes */
            Route::get('candidate/selectCandidate/{id}', 'CandidateController@selectCandidate');
            Route::get('candidate/getAllVacancies', 'CandidateController@getAllVacancies');
            Route::get('candidate/requiredDocuments', 'CandidateController@requiredDocuments');
            Route::get('candidate/list', 'CandidateController@candidate_list');
            Route::post('candidate/shareProfile/{id}', 'CandidateController@shareProfile');
            Route::resource('candidate', 'CandidateController');
            /**
             * translation routes
             */
            Route::get('translations', 'TranslationController@index');
            Route::post('translations/area/{id}/edit', 'TranslationController@editArea');
            Route::post('translations/city/{id}/edit', 'TranslationController@editCity');
            Route::post('translations/ci/{id}/edit', 'TranslationController@editCI');
            Route::post('translations/ci/create', 'TranslationController@addCI');
            Route::post('translations/jr/{id}/edit', 'TranslationController@editJR');
            Route::post('translations/jr/create', 'TranslationController@addJR');
            /**
             * Plans routes
             */
            Route::get('plan', 'Plan\PlanController@index');
            Route::get('plan/list', 'Plan\PlanController@plan_list');
            Route::get('plan/{id}/edit', 'Plan\PlanController@edit');
            Route::put('plan/{id}', 'Plan\PlanController@update');
            /* favorite Seeker */
            Route::post('favoriteSeeker/{seeker_id}', 'SeekerController@favoriteSeeker');
            Route::post('unFavoriteSeeker/{seeker_id}', 'SeekerController@unFavoriteSeeker');
            Route::get('favoriteSeeker', 'SeekerController@getFavoriteSeekers');
        });
    });
    /* Client routes */
    Route::group(['middleware' => 'client'], function () {
        Route::group(['prefix' => 'client', 'namespace' => 'Web\Client'], function () {
            /* Register Client routes ... */
            Route::post('register', 'Auth\AuthController@register');
            Route::get('register', 'Auth\AuthController@showRegistrationForm');
            /* Login Client routes ... */
            Route::get('login', 'Auth\AuthController@getLogin');
            Route::post('login', 'Auth\AuthController@postLogin');
            Route::get('logout', 'Auth\AuthController@logout');
            /* Password reset link request routes... */
            Route::get('password/email', 'Auth\PasswordController@getEmail');
            Route::post('password/email', 'Auth\PasswordController@postEmail');
            /* Password reset routes... */
            Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
            Route::post('password/reset', 'Auth\PasswordController@postReset');
            /* Edit Client Profile */
            Route::get('settings/profile', 'Account\SettingsController@editProfile');
            Route::put('settings/profile/{id}', 'Account\SettingsController@updateProfile');
            /* Change Password */
            Route::get('settings/password', 'Account\SettingsController@editPassword');
            Route::put('settings/password/{id}', 'Account\SettingsController@updatePassword');
//            /* Dashboard routes */
            Route::get('/', 'Dashboard\ClientDashboardController@index');
//            /* Vacant routes */
            Route::get('vacant/list', 'Vacant\VacantController@vacant_list');
            Route::resource('vacant', 'Vacant\VacantController');
//            /* Candidates */
            Route::any('vacant/{id}/candidates', ['uses' => 'Candidate\CandidateController@index']);
            Route::get('candidate/list', 'Candidate\CandidateController@candidate_list');
            Route::get('candidate/{id}/edit', 'Candidate\CandidateController@edit');
            Route::put('candidate/{id}', 'Candidate\CandidateController@update');
        });
    });
    /* Employer routes */
    Route::group(['middleware' => 'employer'], function () {
        Route::group(['prefix' => 'employer', 'namespace' => 'Web\Employer'], function () {
            /* Register Employer routes ... */
            Route::post('register', 'Auth\AuthController@register');
            Route::get('register', 'Auth\AuthController@showRegistrationForm');
            /* Login Employer routes ... */
            Route::get('login', 'Auth\AuthController@getLogin');
            Route::post('login', 'Auth\AuthController@postLogin');
            Route::get('logout', 'Auth\AuthController@logout');
            /* Password reset link request routes... */
            Route::get('password/email', 'Auth\PasswordController@getEmail');
            Route::post('password/email', 'Auth\PasswordController@postEmail');
            /* Password reset routes... */
            Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
            Route::post('password/reset', 'Auth\PasswordController@postReset');
            /* Edit Employer Profile */
            Route::post('settings/profile/{id}/UpdatePicture', 'Account\SettingsController@updateProfilePicture');
            Route::get('settings/profile', 'Account\SettingsController@editProfile');
            Route::put('settings/profile/{id}', 'Account\SettingsController@updateProfile');
            /* Change Password */
            Route::get('settings/password', 'Account\SettingsController@editPassword');
            Route::put('settings/password/{id}', 'Account\SettingsController@updatePassword');
            /* Dashboard routes */
            Route::get('/', 'EmployerDashboardController@index');
            /* Vacant routes */
            Route::get('vacant/list', 'VacantController@vacant_list');
            Route::resource('vacant', 'VacantController');
            /* AppliedVacant */
            Route::any('vacant/{id}/applied-vacant', ['uses' => 'AppliedVacantController@index']);
            /* git  seeker */
            Route::get('seeker/{id}', 'SeekerController@get');
            /* favorite Seeker */
            Route::post('favoriteSeeker/{seeker}', 'SeekerController@favoriteSeeker');
            Route::post('unFavoriteSeeker/{seeker}', 'SeekerController@unFavoriteSeeker');
            Route::get('favoriteSeeker', 'SeekerController@getFavoriteSeekers');
            /* Area  for drop down */
            Route::resource('area', 'AreaController');
            /* cities  for drop down */
            Route::resource('city', 'CityController');
            /* relation area city */
            Route::resource('area.city', 'CityController@getCity');
            /* employer plans*/
            Route::get('plans', 'PlanController@getPricingTable');
            /* Candidate Routes */
            Route::get('candidate/select/{id}/vacant/{vacant_id}', 'CandidateController@selectCandidate');
            Route::get('candidate/list', 'CandidateController@candidate_list');
            Route::get('candidate/getAllVacancies', 'CandidateController@getAllVacancies');
            Route::get('candidate/requiredDocuments', 'CandidateController@requiredDocuments');
            Route::resource('candidate', 'CandidateController');
            Route::get('candidate/generatecv/{id}/', 'CandidateController@generatepdfcv');




        });
    });
});
/*
  |--------------------------------------------------------------------------
  | API  Main  Routes  www.api.ihr.com.sa
  |--------------------------------------------------------------------------
 */
Route::group(['domain' => env('API_URL', 'api.testing.ihr.com.sa'), 'middleware' => ['api.locale', 'cors']],
//Route::group(['domain' =>'api.ihr.master', 'middleware' => ['api.locale', 'cors']],
    function () {
        /* test push notification */
        if (\App::environment('local')) {
            Route::post('testPush', 'API\TestPushController@index');
        }
        /* end test push notification */
        /* Seeker Route */
        Route::resource('seeker', 'API\SeekerController', ['except' => ['create', 'edit', 'destroy']]);
        Route::resource('seeker.language', 'API\LanguageController', ['except' => ['edit', 'create']]);
        Route::resource('seeker.experience', 'API\ExperienceController', ['except' => ['edit', 'create']]);
        Route::resource('seeker.course', 'API\CourseController', ['except' => ['edit', 'create']]);
        Route::resource('seeker.education', 'API\EducationController', ['except' => ['edit', 'create']]);
        Route::resource("seeker.favoriteJob", 'API\FavoriteVacantController', ['except' => ['edit', 'create']]);
        Route::resource("seeker.appliedJob", 'API\AppliedVacantController', ['except' => ['edit', 'create']]);
        Route::resource('seeker.recommendedJob', 'API\RecommendedJobController', ['only' => ['index']]);
        Route::resource("seeker.notification", 'API\NotificationController', ['only' => ['index', 'store', 'destroy']]);
        Route::resource("seeker.SeekerVacantTag", 'API\SeekerVacantTagController',
            ['only' => ['index', 'store', 'destroy']]);
        /*Education List*/
        Route::get('educationlevels', ['uses' => 'API\EducationLevelsController@index']);
        /* Seeker.photo routes */
        Route::post('seeker/{id}/photo', ['uses' => 'API\PhotoController@store']);
        Route::get('seeker/{id}/photo', ['uses' => 'API\PhotoController@show']);
        Route::put('seeker/{id}/photo', ['uses' => 'API\PhotoController@update']);
        Route::delete('seeker/{id}/photo', ['uses' => 'API\PhotoController@destroy']);
        /* Seeker.resume routes */
        Route::get('seeker/{id}/resume', ['uses' => 'API\ResumeController@show']);
        Route::post('seeker/{id}/resume', ['uses' => 'API\ResumeController@store']);
        Route::put('seeker/{id}/resume', ['uses' => 'API\ResumeController@update']);
        Route::delete('seeker/{id}/resume', ['uses' => 'API\ResumeController@destroy']);
        /* Area && city routes  */
        Route::resource("area", 'API\AreaController', ['only' => ['index']]);
        Route::resource("area.city", 'API\CityController', ['except' => ['create', 'edit']]);
        /* Vacant && news  routes */
        Route::resource("vacant", 'API\VacantController', ['only' => ['index', 'show']]);

        Route::get("vacants/byplan/", 'API\VacantController@getbyplan');

        Route::post("vacant/search", 'API\VacantController@search');
        Route::resource("news", 'API\NewsController', ['except' => ['edit', 'create']]);
        /* get all available vacant tags */
        Route::get("vacantTags", 'API\SeekerController@vacantTags');
        /* activate route */
        Route::get('activate/{code}', 'API\SeekerController@activateAccount');
        /* activate and resend activation code sms*/
        Route::get('activatesms/{id}/{code}', 'API\SeekerController@activateAccountmobile');
        Route::get('resendcode/{id}', 'API\SeekerController@resendActivationCode');
        /* Generate and send Seeker CV */
        Route::post('seeker/{id}/sendCv', 'API\SeekerController@sendCv');
        Route::post('seeker/{id}/setLocale', 'API\SeekerController@setSeekerLocale');
        /* Reset password routes */
        Route::post('password/email', 'API\Auth\PasswordController@postEmail');
        /* Set online routes */
        Route::post('online', 'API\Auth\AuthController@online');
        /*  Access token routes */
        Route::post('login', 'API\Auth\AuthController@login');
        Route::post('auth', 'API\Auth\AuthController@getAuthenticatedUser');
        Route::post('logout', 'API\Auth\AuthController@logout');
        Route::post('refresh_token', [
            'middleware' => 'jwt.refresh',
            function () {
            }
        ]);
        /* Data for drop down list routes */
        Route::get('country', function () {
            (\App::getLocale() == 'en') ? $data = \App\Country::orderBy('name')->get() : $data = \App\Country::orderBy('name_ar')->get();
            return response()->json(["data" => $data, "code" => "200"], 200);
        });
        Route::get('company_industry', function () {
            $company_industry = \App\CompanyIndustry::all();
            $data = [];
            foreach ($company_industry as $object) {
                $data [] = ['ID' => $object->ID, 'title' => $object->name, 'title_en' => $object->name_en];
            }
            return response()->json(["data" => $data, "code" => "200"], 200);
        });
        Route::get('job_role', function () {
            $job_roles = \App\JobRole::all();
            $data = array();
            foreach ($job_roles as $object) {
                $data [] = ['ID' => $object->ID, 'title' => $object->name, 'title_en' => $object->name_en];
            }
            return response()->json(["data" => $data, "code" => "200"], 200);
        });
        /* system settings */
        Route::get('settings', 'API\SettingsController@getAll');
        /* system settings get by key */
        Route::get('settings/{key}', 'API\SettingsController@getByKey');
        /* Employer API */
        Route::group(['prefix' => 'employer', 'namespace' => 'API\Employer'], function () {
            /* Set online routes */
            Route::post('online', 'Auth\EmployerAuthController@online');
            /* Employer Login */
            Route::post('login', 'Auth\EmployerAuthController@login');
            Route::post('auth', 'Auth\EmployerAuthController@getAuthenticatedUser');
            Route::post('refresh_token', 'Auth\EmployerAuthController@refresh');
            Route::post('logout', 'Auth\EmployerAuthController@logout');
            /* Reset password routes */
            Route::post('password/email', 'Auth\EmployerPasswordController@postEmail');
            /* Employer registration */
            Route::post('register', 'Auth\EmployerAuthController@register');
            /* Company Types */
            Route::get('companyType', 'Account\EmployerAPIController@companyType');
            /* Update Profile */
            Route::put('updateProfile', 'Account\EmployerAPIController@updateProfile');
            Route::post('updateProfilePicture', 'Account\EmployerAPIController@updateProfilePicture');
            /* Change Password */
            Route::post('changePassword', 'Account\EmployerAPIController@changePassword');
            /* Vacant crud */
            Route::get('vacant', 'Vacant\EmployerVacantController@index');
            Route::get('vacant/applicants', 'Vacant\EmployerVacantController@applicants');
            Route::get('vacant/activeVacancies', 'Vacant\EmployerVacantController@getActiveVacancies');
            Route::get('vacant/{id}', 'Vacant\EmployerVacantController@show');
            Route::post('vacant', 'Vacant\EmployerVacantController@store');
            Route::put('vacant/{id}', 'Vacant\EmployerVacantController@update');
            Route::delete('vacant/{id}', 'Vacant\EmployerVacantController@destroy');
            Route::post('vacant/{id}/appliedVacant', 'Applied\EmployerAppliedVacantController@index');
            /* Plans endpoints */
            Route::get('plans', 'Plan\PlanController@index');
            Route::get('plans/{plan}', 'Plan\PlanController@show');
            Route::get('plans/{plan}/subscribe', 'Plan\PlanController@subscribe');
            Route::get('plans/{plan}/check', 'Plan\PlanController@check');
            Route::get('plans/{plan}/unsubscribe', 'Plan\PlanController@unsubscribe');
            /* favorite Seeker */
            Route::post('favoriteSeeker/{seeker_id}', 'Seeker\EmployerSeekerController@favoriteSeeker');
            Route::post('unFavoriteSeeker/{seeker_id}', 'Seeker\EmployerSeekerController@unFavoriteSeeker');
            Route::get('favoriteSeeker', 'Seeker\EmployerSeekerController@getFavoriteSeekers');
            /* candidate */
            Route::get('candidate/requiredDocuments', 'Candidate\EmployerCandidateController@requiredDocuments');
            Route::resource('candidate', 'Candidate\EmployerCandidateController');


            /* applied applicants */
            Route::get('allapplicants', 'Applied\GetAllAppliedApplicantsController@handle');

        });
    });
/* End routes file */
