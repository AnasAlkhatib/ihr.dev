<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
class ExperienceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title' => 'required|string',
            'company_name' => 'required|string',
            'start_date' => 'required|date|date_format:"Y-m-d"',
            'end_date' => 'date|after:start_date|date_format:"Y-m-d"',
            'company_country' => 'string',
            'company_industry' => 'string',
            'job_rule' => 'string',
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
