<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEducationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edu_name' => 'required|string',
            'edu_level' => 'required|string',
            'edu_major' => 'required|string',
            'edu_year' => 'required|date|date_format:"Y-m-d"',
            'edu_grade' => 'string',
            'edu_country' => 'string',
            'edu_level_id' => 'filled|exists:education_levels,level',
        ];
    }

    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
