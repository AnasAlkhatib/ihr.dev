<?php

namespace App\Http\Requests\Web;

use App\Http\Requests\Request;

class NewsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news_title' => 'required|string',
            'news_body' => 'required|string',
            'news_posted' => 'integer',
            'news_img' => 'image|max:20000'
        ];
    }
    
  
    
}
