<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
use App\Employer;
class EmployerUpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $employer = Employer::find($this->route()->id);
        return [
            'current_password' => 'required|hash:' . $employer->password,
            'password' => 'required|confirmed|min:6',
        ];
    }
}
