<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class CandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'mobile' => 'required',
            'birth_date' => 'required|date|date_format:"Y-m-d"',
            'nationality' => 'required',
            'national_num' => 'required|digits:10',
            'city_id' => 'required',
            'status' => 'required',
            'client_id' => 'required',
            'vacant_id' => 'required',
        ];
    }
}
