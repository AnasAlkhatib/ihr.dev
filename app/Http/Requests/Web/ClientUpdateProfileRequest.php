<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class ClientUpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->id;
        return [
            'name' => 'required|max:255',
            'email' => 'required|unique:clients,email,' . $id,
            'job_title' => 'required|string',
            'company_name' => 'required|string',
            'company_type' => 'required|string',
            'mobile' => 'required|unique:clients,mobile,' . $id,
        ];
    }
}
