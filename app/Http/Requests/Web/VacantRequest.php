<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class VacantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_industry' => 'required',
            'company_location' => 'required',
            'job_title' => 'required',
            'job_rules' => 'required',
            'job_status' => 'required',
            'job_discription' => 'required',
            'exp_level' => 'required',
            'close_date' => 'date|date_format:"Y-m-d"',
            'tags' => 'required',
        ];
    }
}
