<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
use App\User;
class UpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->route()->id);
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|unique:users,name,' . $user->id,
                    'email' => 'required|unique:users,email,' . $user->id,
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|unique:users,email,' . $user->id,
                ];
            }
            default:
                break;
        }
    }
}
