<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class ClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int)$this->client;
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:clients,email',
                    'job_title' => 'required|string',
                    'company_name' => 'required|string',
                    'company_type' => 'required|string',
                    'mobile' => 'required|unique:clients,mobile',
                    'password' => 'required|min:6|confirmed',
                    'active' => 'integer'
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|unique:clients,email,' . $id,
                    'job_title' => 'required|string',
                    'company_name' => 'required|string',
                    'company_type' => 'required|string',
                    'mobile' => 'required|unique:clients,mobile,' . $id,
                    'password' => 'min:6|confirmed',
                    'active' => 'integer'
                ];
            }
            default:
                break;
        }
    }
}
