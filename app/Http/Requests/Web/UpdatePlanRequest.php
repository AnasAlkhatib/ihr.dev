<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class UpdatePlanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->id;
        return [
            'name' => 'filled|unique:plans,name,' . $id,
            'description' => 'filled',
            'price' => 'filled|numeric|regex:/^\d*(\.\d{2})?$/',
            'interval' => 'filled|in:day,week,month,year',
            'interval_count' => 'filled|numeric',
            'trial_period_days' => 'filled|numeric',
            'sort_order' => 'filled|numeric',
        ];
    }
}
