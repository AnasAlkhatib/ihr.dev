<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
class EmployerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int)$this->employer;
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:employers,email',
                    'job_title' => 'required|string',
                    'company_name' => 'required|string',
                    'company_type' => 'required|string',
                    'mobile' => 'required|unique:employers,mobile',
                    'password' => 'required|min:6',
                    'website' => '',
                    'plan' => 'filled|integer',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|unique:employers,email,' . $id,
                    'job_title' => 'required|string',
                    'company_name' => 'required|string',
                    'company_type' => 'required|string',
                    'mobile' => 'required|unique:employers,mobile,' . $id,
                    'password' => 'min:6',
                    'active' => 'integer',
                    'website' => '',
                    'plan' => 'filled|integer',
                ];
            }
            default:
                break;
        }
    }
}
