<?php
namespace App\Http\Requests\Web;

use App\Http\Requests\Request;
use App\User;
class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->route()->id);
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'current_password' => 'required|hash:' . $user->password,
                    'password' => 'required|different:current_password|confirmed|min:6',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'current_password' => 'required|hash:' . $user->password,
                    'password' => 'required|different:current_password|confirmed|min:6',
                ];
            }
            default:
                break;
        }
    }
}
