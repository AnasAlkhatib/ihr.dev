<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
/**
 * Class LanguageRequest
 * @package App\Http\Requests
 */
class LanguageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     * @todo refactor this validation rules some cases make duplicate
     * @return array
     */
    public function rules()
    {
        $seeker_id = (int)$this->seeker;
        $langId = (int)$this->route('language');
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return
                    [
                        'language_name' => 'required|unique:languages,language_name,NULL,ID,seeker_id,' . $seeker_id,
                        'language_level' => 'required|string'
                    ];
            }
            case 'PUT':
            case 'PATCH': {
                return
                    [
                        'language_name' => 'filled|unique:languages,language_name,NULL,ID,seeker_id,' . $langId,
                        'language_level' => 'filled|string'
                    ];
            }
        }
    }
    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
