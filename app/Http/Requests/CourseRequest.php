<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_name' => 'required|string',
            'center_name' => 'required|string',
            'course_date' => 'required|date|date_format:"Y-m-d"',
            'course_hours' => 'string',
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
