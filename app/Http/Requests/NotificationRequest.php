<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Notification;
class NotificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seeker_id' => 'exists:seekers,ID',
            'device_token' => 'required|string',
            'device_model' => 'string',
            'device_type' => 'required|string|in:1,2',
            'device_version' => 'string',
            'notification_active' => 'in:0,1'
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
