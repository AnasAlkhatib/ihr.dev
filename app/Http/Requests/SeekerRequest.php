<?php
namespace App\Http\Controllers;
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Seeker;
/**
 * Class SeekerRequest
 * @package App\Http\Requests
 */
class SeekerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET': {
                return [];
            }
            case 'POST': {
                return
                    [
                        'full_name' => 'required',
                        'password' => 'required|min:6',
                        'mobile' => 'required|digits_between:9,14|unique:seekers,mobile',
                        'national_num' => 'digits_between:10,14|unique:seekers,national_num',
                        'email' => 'required|email|unique:seekers,email',
                        'nationality' => 'required',
                        'gender' => 'filled|in:male,female',
                        'birth_date' => 'filled|date|date_format:"Y-m-d"',
                        'city_id' => 'filled',
                        'country_id' => 'filled|integer|exists:countries,id',
                        'residence' => 'filled|integer',
                        'locale' => 'in:en,ar'
                    ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = (int)$this->seeker;
                return
                    [
                        'full_name' => 'filled|unique:seekers,full_name,' . $id . ',ID',
                        'nationality' => 'filled',
                        'gender' => 'filled|in:male,female',
                        'national_num' => 'filled|digits_between:10,14|unique:seekers,national_num,' . $id . ',ID',
                        'email' => 'filled|email|unique:seekers,email,' . $id . ',ID',
                        'mobile' => 'filled|digits_between:9,14|unique:seekers,mobile,' . $id . ',ID',
                        'birth_date' => 'filled|date|date_format:"Y-m-d"',
                        'city_id' => 'filled',
                        'country_id' => 'filled',
                        'residence' => 'filled',
                        'locale' => 'filled|in:en,ar',
                        'education_level_id' => 'filled',
                    ];
            }
            default:
                break;
        }
    }
    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
