<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ResumeRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    return [
                        'file' => 'required|mimes:pdf,doc,docx|max:20000'
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return
                            [ 'file' => 'required|mimes:pdf,docx,doc|max:20000'];
                }
        }
    }

    public function response(array $errors) {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }

}
