<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Seeker;

class AppliedVacantRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $seeker = Seeker::find($this->seeker);
        if ($seeker["ID"] === null) {
            return [];
        }
        return [
            'seeker_id' => 'exists:seekers,id',
            'vacant_id' => 'required|exists:vacancies,id||unique:applied_vacancies,vacant_id,NULL,id,seeker_id,' . $seeker->ID
        ];
    }

    public function response(array $errors) {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }

}
