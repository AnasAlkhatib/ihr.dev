<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
class EmployerCreateVacantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title' => 'required',
            'company_location' => 'required',
            'company_industry' => 'required',
            'job_rules' => 'required',
            'job_status' => 'required',
            'job_discription' => 'required',
            'exp_level' => 'required',
            'close_date' => 'date|date_format:"Y-m-d"',
            'tags' => 'required',
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
