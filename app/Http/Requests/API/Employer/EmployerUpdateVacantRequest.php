<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
class EmployerUpdateVacantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title',
            'company_location',
            'company_industry',
            'job_rules',
            'job_status',
            'job_discription',
            'exp_level',
            'tags',
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
