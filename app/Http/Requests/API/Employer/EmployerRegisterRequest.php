<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
class EmployerRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:employers,email',
            'job_title' => 'required|string',
            'company_name' => 'required|string',
            'company_type' => 'required|in:private_sector,public_sector,non_profit_organization,recruitment_agency',
            'mobile' => 'required|unique:employers,mobile',
            'password' => 'required|min:6',
            'locale' => 'in:en,ar'
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
