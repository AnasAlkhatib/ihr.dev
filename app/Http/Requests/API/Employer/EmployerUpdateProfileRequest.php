<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
class EmployerUpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::guard("api-employer")->user()->id;
        return [
            'name' => 'filled|max:255',
            'email' => 'filled|unique:employers,email,' . $id,
            'job_title' => 'filled|string',
            'company_name' => 'filled|string',
            'company_type' => 'filled|in:private_sector,public_sector,non_profit_organization,recruitment_agency',
            'mobile' => 'filled|unique:employers,mobile,' . $id,
            'locale' => 'filled|in:ar,en'
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
