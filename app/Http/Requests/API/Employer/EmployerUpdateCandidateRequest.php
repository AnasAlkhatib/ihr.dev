<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
/**
 * Class EmployerUpdateCandidateRequest
 * @package App\Http\Requests\API\Employer
 */
class EmployerUpdateCandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seeker_id' => 'required|exists:seekers,ID',
            'vacant_id' => 'required|exists:vacancies,ID',
            'status' => 'filled|in:accept_candidate,reject_candidate,telephone_interview,preselection,offer_vacancies,final_interview',
            'note' => '',
            'offer_vacancies' => '',
            'required_documents' => '',
        ];
    }
    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
