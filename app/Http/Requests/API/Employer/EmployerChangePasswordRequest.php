<?php
namespace App\Http\Requests\API\Employer;

use App\Http\Requests\Request;
class EmployerChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $employer = \Auth::guard("api-employer")->user();
        return [
            'current_password' => 'required|hash:' . $employer->password,
            'new_password' => 'required|confirmed|min:6',
        ];
    }
    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'code' => "422"], 422);
    }
}
