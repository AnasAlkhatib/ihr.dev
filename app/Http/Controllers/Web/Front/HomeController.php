<?php
namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Controller;
/**
 * Class HomeController
 * @package App\Http\Controllers\Web\Front
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('web.front.index');
    }
}
