<?php
namespace App\Http\Controllers\Web\Client\Dashboard;

use App\Client;
use App\AppliedVacant;
use App\Http\Controllers\Controller;
/**
 * Class ClientDashboardController
 * @package App\Http\Controllers\Web\Employer
 */
class ClientDashboardController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $client;
    /**
     * ClientDashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('client.auth');
        $this->client = \Auth::guard('client')->user();
    }
    /**
     * Client Dashboard page
     *
     */
    public function index()
    {
        $vacancies = $this->last_vacancies($this->client);
        $vacant_count = $this->last_vacancies($this->client)->count();
//        $applied_vacant_count = $this->vacancies_applied($this->client)->count();
        $candidates = $this->getCandidates($this->client)->count();
        return view('web.dashboard.client.index')
            ->with('vacancies', $vacancies)
            ->with('vacant_count', $vacant_count)
//            ->with('applied_vacant_count', $applied_vacant_count)
            ->with('candidates', $candidates);
    }
    /**
     * get all client Vacancies
     * @param Client $client
     * @return mixed
     */
    public function all_vacancies(Client $client)
    {
        return $client->vacancies()
            ->orderBy('created_at', 'asc')
            ->get();
    }
    /**
     * get last 7 client Vacancies
     * @param Client $client
     * @return mixed
     */
    public function last_vacancies(Client $client)
    {
        return $client->vacancies()
            ->orderBy('created_at', 'asc')
            ->take(7)
            ->get();
    }
    /**
     * Display a listing  of applied seekers  for this Client vacancies .
     *
     * @param Client $client
     * @return mixed
     */
    public function vacancies_applied(Client $client)
    {
        $vacancies = $client->vacancies()->get(['ID']);
        $vacancies_ids = [];
        if ($vacancies) {
            foreach ($vacancies as $vacant) {
                $vacancies_ids [] = $vacant->ID;
            }
            return AppliedVacant::orderBy('ID', 'desc')
                ->whereIn('applied_vacancies.vacant_id', $vacancies_ids)
                ->get(['ID']);
        }
    }
    public function getCandidates(Client $client)
    {
        return $client->candidates()
            ->orderBy('created_at', 'asc')
            ->get();
    }
}
