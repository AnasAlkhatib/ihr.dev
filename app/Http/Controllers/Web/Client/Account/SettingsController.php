<?php
namespace App\Http\Controllers\Web\Client\Account;

use App\Http\Requests\Web\ClientUpdateProfileRequest as updateProfile;
use App\Http\Requests\Web\ClientUpdatePasswordRequest as updatePassword;
use App\Http\Controllers\Controller;
/**
 * Class SettingsController
 * @package App\Http\Controllers\Web\Client\Account
 */
class SettingsController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $client;
    /**
     * SettingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('client.auth');
        $this->client = \Auth::guard('client')->user();
    }
    /**
     * Show edit profile form
     */
    public function editProfile()
    {
        $client = \App\Client::find($this->client->id);
        $this->authorizeForUser($client, 'editProfile', $this->client);
        $companyTypes = $this->getCompanyTypes();
        return view('web.dashboard.client.account.edit_profile')
            ->with(['companyTypes' => $companyTypes])
            ->with(['client' => $client]);
    }
    /**
     * update client profile
     * @param updateProfile $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(updateProfile $request, $id)
    {
        $client = \App\Client::find($id);
        $this->authorizeForUser($client, 'updateProfile', $this->client);
        $values = array_map('trim', $request->except(['_token', '_method']));
        foreach ($values as $field => $value) {
            $client->$field = $value;
        }
        if ($client->save()) {
            return redirect('client/settings/profile')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return redirect('/')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * edit Client Password
     *
     */
    public function editPassword()
    {
        $client = \App\Client::find($this->client->id);
        $this->authorizeForUser($client, 'editPassword', $this->client);
        return view('web.dashboard.client.account.edit_password')
            ->with(['client' => $client]);
    }
    /**
     * update Client Password
     * @param updatePassword $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(updatePassword $request, $id)
    {
        $client = \App\Client::find($id);
        $this->authorizeForUser($client, 'updatePassword', $this->client);
        $new_password = trim($request->input('password'));
        $client->password = bcrypt($new_password);
        if ($client->save()) {
            \Auth()->logout($client, true);
            return redirect('/client/settings/password')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
    /**
     * array for supported Company Types
     * @return \Illuminate\Support\Collection
     */
    public function getCompanyTypes()
    {
        $company_types = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return $company_types;
    }
}
