<?php
namespace App\Http\Controllers\Web\Client\Candidate;

use App\Seeker;
use App\Candidate;
use Datatables;
use Illuminate\Http\Request;
use App\Http\Requests\Web\ClientUpdateCandidateRequest as updateCandidate;
use App\Http\Controllers\Controller;
use App\Util\Traits\ExperienceTrait;
use App\Services\Employment\EmploymentBuilder;
/**
 * Class CandidateController
 * @package App\Http\Controllers\Web\Client\Candidate
 */
class CandidateController extends Controller
{
    use ExperienceTrait;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $client;
    /**
     * CandidateController constructor.
     */
    public function __construct()
    {
        $this->middleware('client.auth');
        $this->client = \Auth::guard('client')->user();
    }
    /**
     * @param $id
     */
    public function index($id)
    {
        $vacant = \App\Vacant::find($id);
        return view('web.dashboard.client.candidate.candidates_list')->with(['vacant' => $vacant]);
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function candidate_list(Request $request)
    {
        $query = Candidate::with('clients', 'users')->select('candidates.*');
        if ($request->ajax()) {
            $query->whereHas('clients', function ($q) {
                $q->where('client_id', $this->client->id);
            });
            return Datatables::eloquent($query)
                ->editColumn(
                    'name',
                    function ($candidate) {
                        $response = '<a  href="' . url('client/candidate/' . $candidate->id) . '" >'
                            . $candidate->name . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'recruiter_status',
                    function ($candidate) {
                        $response = '---';
                        $status = $candidate->users()
                            ->wherePivot('candidate_id', $candidate->id)
                            ->wherePivot('client_id', $this->client->id)
                            ->pluck('status')->implode('<br>');
                        if ($status) {
                            switch ($status) {
                                case $status == 'accept_candidate':
                                    $response = "<span class='text-success'>" . trans('app.accept_candidate') . "</span>";
                                    break;
                                case $status == 'reject_candidate':
                                    $response = "<span class='text-danger'>" . trans('app.reject_candidate') . "</span>";
                                    break;
                                case $status == 'offer_vacancies':
                                    $response = "<span class='text-warning'>" . trans('app.offer_vacancies') . "</span>";
                                    break;
                                case $status == 'final_interview':
                                    $response = "<span class='text-info'>" . trans('app.final_interview') . "</span>";
                                    break;
                                case $status == 'rehabilitation_candidate':
                                    $response = "<span class='text-info'>" . trans('app.rehabilitation_candidate') . "</span>";
                                    break;
                                case $status == 'telephone_interview':
                                    $response = "<span class='text-info'>" . trans('app.telephone_interview') . "</span>";
                                    break;
                                case $status == 'preselection':
                                    $response = "<span class='text-info'>" . trans('app.preselection') . "</span>";
                            }
                        }
                        return $response;
                    }
                )
                ->editColumn(
                    'client_status',
                    function ($candidate) {
                        $response = '---';
                        $status = $candidate->clients()->wherePivot('candidate_id',
                            $candidate->id)->pluck('status')->implode('<br>');
                        if ($status) {
                            switch ($status) {
                                case $status == 'accept_candidate':
                                    $response = "<span class='text-success'>" . trans('app.accept_candidate') . "</span>";
                                    break;
                                case $status == 'reject_candidate':
                                    $response = "<span class='text-danger'>" . trans('app.reject_candidate') . "</span>";
                                    break;
                                case $status == 'offer_vacancies':
                                    $response = "<span class='text-warning'>" . trans('app.offer_vacancies') . "</span>";
                                    break;
                                case $status == 'final_interview':
                                    $response = "<span class='text-info'>" . trans('app.final_interview') . "</span>";
                            }
                        }
                        return $response;
                    }
                )
                ->addColumn(
                    'posted_by',
                    function ($candidate) {
                        $userName = $candidate->users->last();
                        $response = $userName['name'];
                        return $response;
                    }
                )
                ->editColumn(
                    'created_at',
                    function ($candidate) {
                        return $candidate->created_at->format('Y-m-d H:i');
                    }
                )
                ->addColumn(
                    'action',
                    function ($candidate) {
                        $html = '<a href="/client/candidate/' . $candidate->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.action') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('id', '{{$id}}')
                ->make(true);
        }
    }
    /**
     * @param $id
     */
    public function edit($id)
    {
        $seekerAccount = false;
        $seeker = new Seeker();
        $seekerExperience = array();
        $seekerVacantApplied = array();
        $candidateOfferVacancies = array();
        $candidateRequiredDocuments = array();
        $candidate = Candidate::find($id);
        /* if not found */
        if (!$candidate) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        if ($candidate->seeker_id) {
            $seeker = Seeker::find($candidate->seeker_id);
            $seekerExperience = $this->getExperience($seeker, 'seeker_id');
            $seekerVacantApplied = $seeker->vacantApplied()->paginate(3);
        }
        $city = \App\City::find($candidate->city_id);
        $location = $this->getCandidateLocation($city);
        $candidatables = $this->getCandidatables($this->client, $candidate);
        return view('web.dashboard.client.candidate.edit_candidate')
            ->with([
                'candidate' => $candidate,
                'candidateArea' => $location['area'],
                'candidateCity' => $location['city'],
                'candidatables' => $candidatables,
                'seekerExperience' => $seekerExperience,
                'seekerVacantApplied' => $seekerVacantApplied,
                'candidateOfferVacancies' => $candidateOfferVacancies,
                'candidateRequiredDocuments' => $candidateRequiredDocuments,
            ]);
    }
    public function update(updateCandidate $request, $id)
    {
        $candidate = Candidate::find($id);
        /* if not found */
        if (!$candidate) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $values = $request->except(['_token', '_method']);
        $collection = collect($values)->only(['notes', 'status']);
        if ($request->has('required_documents')) {
            $collection = $collection->merge(['required_documents' => serialize($request->input('required_documents'))]);
        }
        if ($request->has('offer_vacancies')) {
            $collection = $collection->merge(['offer_vacancies' => serialize($request->input('offer_vacancies'))]);
        }
        $attributes = array_filter($collection->all());
        $data = $candidate->users()->where('candidate_id', $id)->get()->toArray();
        $vacantId = $data[0]['pivot']['vacant_id'];
        $attributes['vacant_id'] = $vacantId;
        $update = $candidate->clients()->updateExistingPivot($this->client->id, $attributes);
        if ($update) {
            return redirect('client/vacant/' . $vacantId . '/candidates');
        }
    }
    public function getCandidatables($client, $candidate)
    {
        $data = $client->candidates()->wherePivot('candidate_id', $candidate->id)->get()->toArray();
        return $data[0]['pivot'];
    }
    /** get candidate area and city based on application locale
     * @param $city
     * @return mixed
     */
    public function getCandidateLocation($city)
    {
        $area = $city->area()->first();
        $appLocale = \App::getLocale();
        switch ($area) {
            case ($appLocale == 'en' && $city->name_en && $area->name_en):
                $data = ['area' => $area->name_en, 'city' => $city->name_en];
                break;
            case ($appLocale == 'en' && $area->name_en && $city->name):
                $data = ['area' => $area->name_en, 'city' => $city->name];
                break;
            default:
                $data = ['area' => $area->name, 'city' => $city->name];
        }
        return $data;
    }
}
