<?php
namespace App\Http\Controllers\Web\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\Web\ClientRequest;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Jobs\EmployerWasRegistered;
class AuthController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */
    use AuthenticatesAndRegistersUsers,
        ThrottlesLogins;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $guard = 'client';
    protected $redirectTo = '/client';
    protected $loginPath = '/client/login';
    protected $redirectAfterLogout = '/client/login';
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('client.guest', ['except' => 'logout']);
    }
    /**
     * Create a new Employer instance after a valid registration.
     *
     * @param  array $data
     * @return Employer
     */
    protected function create(array $data)
    {
        $client = \App\Client::create([
            'job_title' => $data['job_title'],
            'company_name' => $data['company_name'],
            'company_type' => $data['company_type'],
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $user = User::Where('email', '=', env('EMPLOYER_ACTIVATE_EMAIL', 'ahmad@sto.com.sa'))->first();
        //  $this->dispatch(new EmployerWasRegistered($user, $client));
    }
    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.client.login');
    }
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $company_types = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return view('auth.client.register')->with(['company_types' => $company_types]);
    }
    /**
     * Handle a registration request for the application
     *
     * @param ClientRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(ClientRequest $request)
    {
        $values = array_map('trim', $request->except(['_token', 'password_confirmation']));
        $this->create($values);
        return redirect()->back()->with('message', trans('app.registration_successful'));
    }
    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validateLogin($request);
        if (!Auth::guard($this->getGuard())->attempt($request->only('email', 'password'))) {
            return redirect()->back()
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => $this->getFailedLoginMessage(),
                ]);
        }
        if (Auth::guard($this->getGuard())->once(Input::only('email', 'password'))) {
            if (!Auth::guard($this->getGuard())->user()->active) {
                Auth::guard($this->getGuard())->logout();
                return redirect()->back()
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getNotActivatedMessage(),
                    ]);
            }
        }
        return redirect()->intended($this->redirectPath());
    }
    /**
     * Get the failed login message if user not activated .
     *
     * @return string
     */
    protected function getNotActivatedMessage()
    {
        return Lang::has('app.not_activated')
            ? Lang::get('app.not_activated')
            : 'Account not activated.';
    }
}
