<?php
namespace App\Http\Controllers\Web\Employer;

use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vacant;
use App\Area;
use App\City;
use File;
use App\Util\Traits\ExperienceTrait;
use App\Util\Traits\EmployedTrait;
/**
 * Class AppliedVacantController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class AppliedVacantController extends Controller
{
    use ExperienceTrait;
    use EmployedTrait;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * AppliedVacantController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request, $id)
    {
        $vacant = Vacant::find($id);
        try {
            if ($this->authorizeForUser($this->employer, 'show', $vacant)) {
                $genders = collect(
                    [
                        ['name' => trans('app.male'), 'value' => 'male'],
                        ['name' => trans('app.female'), 'value' => 'female']
                    ]
                );
                $nationalities = collect(
                    [
                        ['name' => trans('app.saudi'), 'value' => 1],
                        ['name' => trans('app.non_saudi'), 'value' => 2]
                    ]
                );
                /* find all Applied seekers */
                $seekers = $vacant->Applied_seeker();
                /* if no seekers applied yet */
                if (!$seekers) {
                    return response()->json(['message' => 'No Seeker Applied for this vacant', 'code' => "404"], 404);
                }
                if ($request->input('gender')) {
                    $seekers = $seekers->where('gender', '=', $request->input('gender'));
                }
                if ($request->input('area')) {
                    $cities = Area::find($request->input('area'))->city;
                    foreach ($cities as $city) {
                        $array[] = $city->ID;
                    }
                    $seekers = $seekers->wherein('city_id', $array);
                }
                if ($request->input('city')) {
                    $seekers = $seekers->where('city_id', '=', $request->input('city'));
                }
                if ($request->input('nationality')) {
                    if ($request->input('nationality') === '1') {
                        $seekers = $seekers->wherein('nationality', ['السعودية', 'سعودي']);
                    } elseif ($request->input('nationality') === '2') {
                        $seekers = $seekers->whereNotIn('nationality', ['السعودية', 'سعودي']);
                    }
                }
                /* paginate $seekers object */
                $seekers = $seekers->paginate(6);
                /* loop throw each object and modify some properties */
                $seekers->each(
                    function ($seeker) use ($id) {
                        $seeker->gender = $this->getSeekerGender($seeker->gender);
                        if ($seeker->city_id) {
                            $seeker->setAttribute('city', $this->get_city_name($seeker->city_id));
                            $seeker->setAttribute('area', $this->get_area_name($seeker->city_id));
                        }
                        $seeker->setAttribute('age', $this->get_seeker_age($seeker->birth_date));
                        $seeker->setAttribute('edu', $this->get_seeker_education($seeker));
                        $seeker->setAttribute('exp', $this->getExperience($seeker, 'seeker_id'));
                        $seeker->setAttribute('candidate_id',$this->getCandidateData($this->employer, $seeker->ID, $id));
                        $seeker->setAttribute('isCandidate', $this->checkCandidate($this->employer, $seeker->ID, $id));
                        $seeker->is_employed = $this->isEmployed($seeker);
                        $pic = public_path() . '/uploads/seeker/photo/' . $seeker->pic_file_name;
                        if (File::exists($pic) && $seeker->pic_file_name) {
                            $seeker->setAttribute('pic_url', url("/uploads/seeker/photo/" . $seeker->pic_file_name));
                        } else {
                            $seeker->setAttribute('pic_url', url("assets/img/user.jpg"));
                        }
                    }
                );
                if ($request->ajax()) {
                    return response()->json(
                        view('web.dashboard.employer.applied-vacant.ajax-seekers')->with([
                            'seekers' => $seekers,
                            'vacant' => $vacant
                        ])->render()
                    );
                }
                return view('web.dashboard.employer.applied-vacant.index')
                    ->with('nationalities', $nationalities)
                    ->with('genders', $genders)
                    ->with('seekers', $seekers)
                    ->with('vacant', $vacant);
            }
        } catch (AuthorizationException $e) {
            return redirect('/employer/vacant')->with([
                'message' => trans('app.Unauthorized'),
                'alert-class' => 'alert-danger'
            ]);
        }
    }
    /**
     * calculate seeker age
     *
     * @param $birthday
     * @return int
     */
    public function get_seeker_age($birthday)
    {
        $age = date_create($birthday)->diff(date_create('today'))->y;
        return $age;
    }
    /**
     * get seeker last education
     *
     * @param $seeker
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function get_seeker_education($seeker)
    {
        $education = $seeker->education->all();
        if ($education) {
            return $this->transEducation($seeker->education->last()->edu_level);
        } else {
            return trans('app.not_found');
        }
    }
    /**
     * get City name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_city_name($city_id)
    {
        if (\App::getLocale() == 'en') {
            return City::find($city_id)->name_en;
        } else {
            return City::find($city_id)->name;
        }
    }
    /**
     * get Area name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_area_name($city_id)
    {
        $city = City::find($city_id);
        if (\App::getLocale() == 'en') {
            return $city->area()->first()->name_en;
        } else {
            return $city->area()->first()->name;
        }
    }
    /**
     * @param $education
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function transEducation($education)
    {
        switch ($education) {
            case $education == 'ثانوية عامة':
                $education = trans('app.high_school');
                break;
            case $education == 'دبلوم':
                $education = trans('app.diploma');
                break;
            case $education == 'بكالوريوس':
                $education = trans('app.bachelor');
                break;
            case $education == 'دبلوم عالي':
                $education = trans('app.high_diploma');
                break;
            case $education == 'ماجستير':
                $education = trans('app.master');
                break;
            case $education == 'دكتوراة':
                $education = trans('app.doctorate');
                break;
            default:
                $education = '';
        }
        return $education;
    }
    /**
     * @param $gender
     * @return string
     */
    private function getSeekerGender($gender)
    {
        if (\App::getLocale() == 'ar') {
            if ($gender == 'male') {
                $gender = 'ذكر';
            } else {
                $gender = 'أنثى';
            }
        }
        return ucwords($gender);
    }
    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function checkCandidate($employer, $seekerId, $vacantId)
    {
        $isCandidate = false;
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            $isCandidate = true;
        }
        return $isCandidate;
    }
    /**
     * @param $employer
     * @param $candidateId
     * @param $vacantId
     * @return mixed
     */
    private function getCandidateData($employer, $candidateId, $vacantId)
    {
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $candidateId)
            ->wherePivot('vacant_id', $vacantId)
            ->get()
            ->toArray();
        if (!empty($data)) {
            return $data[0]['pivot']['id'];
        } else {
            return '123';
        }
    }
}
