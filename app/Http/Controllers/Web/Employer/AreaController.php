<?php

namespace App\Http\Controllers\Web\Employer;

use App\Http\Controllers\Controller;
use App\Area;


/**
 * Class AreaController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class AreaController extends Controller
{


    /**
     * AreaController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $Areas = Area::all();
        foreach ($Areas as $area) {
            $data [] = array("text" => $area->name, "id" => $area->ID);
        }

        return response()->json($data, 200);
    }
}
