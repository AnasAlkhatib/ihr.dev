<?php
namespace App\Http\Controllers\Web\Employer;

use App\Seeker;
use App\City;
use File;
use App\Http\Controllers\Controller;
use App\Util\Traits\ExperienceTrait;
use App\Util\Traits\EmployedTrait;
/**
 * Class SeekerController
 * @package App\Http\Controllers\Web\Employer
 */
class SeekerController extends Controller
{
    use ExperienceTrait,EmployedTrait;
    /**
     * @var $employer
     */
    protected $employer;
    /**
     * SeekerController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * get seeker by id
     * @param $id
     */
    public function get($id)
    {
        $city = '';
        $area = '';
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return redirect('employer/seeker')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $seekerExperience = $this->getExperience($seeker, 'seeker_id');
        if ($seeker->city_id) {
            $cityObject = City::find($seeker->city_id);
            $areaObject = $cityObject->area()->first();
            if (\App::getLocale() == 'en') {
                $area = $areaObject->name_en;
                $city = $cityObject->name_en;
            } else {
                $area = $areaObject->name;
                $city = $cityObject->name;
            }
        }
        return view('web.dashboard.employer.seeker.show_seeker')->with([
            'seeker' => $seeker,
            'seekerArea' => $area,
            'seekerCity' => $city,
            'seekerExperience' => $seekerExperience,
        ]);
    }
    /**
     * calculate seeker age
     *
     * @param $birthday
     * @return int
     */
    public function get_seeker_age($birthday)
    {
        $age = date_create($birthday)->diff(date_create('today'))->y;
        return $age;
    }
    /**
     * get seeker last education
     *
     * @param $seeker
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function get_seeker_education($seeker)
    {
        $education = $seeker->education->all();
        if ($education) {
            return $this->transEducation($seeker->education->last()->edu_level);
        } else {
            return trans('app.not_found');
        }
    }
    /**
     * get City name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_city_name($city_id)
    {
        if (\App::getLocale() == 'en') {
            return City::find($city_id)->name_en;
        } else {
            return City::find($city_id)->name;
        }
    }
    /**
     * get Area name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_area_name($city_id)
    {
        $city = City::find($city_id);
        if (\App::getLocale() == 'en') {
            return $city->area()->first()->name_en;
        } else {
            return $city->area()->first()->name;
        }
    }
    /**
     * @param $education
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function transEducation($education)
    {
        switch ($education) {
            case $education == 'ثانوية عامة':
                $education = trans('app.high_school');
                break;
            case $education == 'دبلوم':
                $education = trans('app.diploma');
                break;
            case $education == 'بكالوريوس':
                $education = trans('app.bachelor');
                break;
            case $education == 'دبلوم عالي':
                $education = trans('app.high_diploma');
                break;
            case $education == 'ماجستير':
                $education = trans('app.master');
                break;
            case $education == 'دكتوراة':
                $education = trans('app.doctorate');
                break;
            default:
                $education = '';
        }
        return $education;
    }
    /**
     * add seeker to favorite
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function favoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        $employer = $this->employer;
        $employer->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
        return back();
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unFavoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        $employer = $this->employer;
        $employer->favoriteSeekers()->where('seeker_id', '=', $seeker->ID)->delete();
        return back();
    }
    /**
     *
     */
    public function getFavoriteSeekers()
    {
        $employer = $this->employer;
        $seekerIds = $employer->favoriteSeekers->pluck('seeker_id');
        $seekers = Seeker::whereIn('ID', $seekerIds)
            ->paginate(6);
        $seekers->each(
            function ($seeker) {
                $seeker->gender = $this->getSeekerGender($seeker->gender);
                if ($seeker->city_id) {
                    $seeker->setAttribute('city', $this->get_city_name($seeker->city_id));
                    $seeker->setAttribute('area', $this->get_area_name($seeker->city_id));
                }
                $seeker->setAttribute('age', $this->get_seeker_age($seeker->birth_date));
                $seeker->setAttribute('edu', $this->get_seeker_education($seeker));
                $seeker->setAttribute('exp', $this->getExperience($seeker, 'seeker_id'));
                $seeker->is_employed = $this->isEmployed($seeker);
                $pic = public_path() . '/uploads/seeker/photo/' . $seeker->pic_file_name;
                if (File::exists($pic) && $seeker->pic_file_name) {
                    $seeker->setAttribute('pic_url', url("/uploads/seeker/photo/" . $seeker->pic_file_name));
                } else {
                    $seeker->setAttribute('pic_url', url("assets/img/user.jpg"));
                }
            }
        );
        return view('web.dashboard.employer.seeker.favorite_seekers')->with(['seekers' => $seekers]);
    }
    /**
     * @param $gender
     * @return string
     */
    private function getSeekerGender($gender)
    {
        if (\App::getLocale() == 'ar') {
            if ($gender == 'male') {
                $gender = 'ذكر';
            } else {
                $gender = 'أنثى';
            }
        }
        return ucwords($gender);
    }
}
