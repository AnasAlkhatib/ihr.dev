<?php
namespace App\Http\Controllers\Web\Employer\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Web\EmployerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Jobs\EmployerWasRegistered;
use Illuminate\Http\Request;
use App\Employer;
use App\User;
use Input;
class AuthController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */
    use AuthenticatesAndRegistersUsers,
        ThrottlesLogins;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $guard = 'employer';
    protected $redirectTo = '/employer';
    protected $loginPath = '/employer/login';
    protected $redirectAfterLogout = '/employer/login';
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.guest', ['except' => 'logout']);
    }
    /**
     *
     * Create a new Employer instance after a valid registration.
     * @param  array $data
     * @return Employer
     */
    protected function create(array $data)
    {
        $employer = Employer::create([
            'job_title' => $data['job_title'],
            'company_name' => $data['company_name'],
            'company_type' => $data['company_type'],
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        if ($employer) {
            $plan = \App\Plans\Models\Plan::find($data['plan']);
            $employerPlan = \Lang::get('app.plans.' . $plan->id . '.name');
            $user = User::Where('email', '=', env('EMPLOYER_ACTIVATE_EMAIL', 'ahmad@sto.com.sa'))->first();
            $this->dispatch(new EmployerWasRegistered($user, $employer, $employerPlan));
        }
    }
    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.employer.login');
    }
    /**
     * Show the application registration form.
     * @param $request
     */
    public function showRegistrationForm(Request $request)
    {
        /* make sure provided valid plan id */
        if (!$request->has('plan')) {
            return redirect('employer/register?plan=1');
        };
        $planId = (int)$request->get('plan');
        if ($planId) {
            $allPlans = \App\Plans\Models\Plan::find($planId);
            if (!$allPlans) {
                return redirect('employer/plans');
            }
        }
        /* collect some look up data */
        $plans = $this->getPlansCollection();
        $company_types = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return view('auth.employer.register')->with([
            'company_types' => $company_types,
            'plans' => $plans,
            'planId' => $planId
        ]);
    }
    /**
     * Handle a registration request for the application
     *
     * @param EmployerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(EmployerRequest $request)
    {
        $values = array_map('trim', $request->except(['_token']));
        $this->create($values);
        return redirect()->back()->with('message', trans('employer_register_message.content'));
    }
    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     */
    public function postLogin(Request $request)
    {
        $this->validateLogin($request);
        if (!Auth::guard($this->getGuard())->attempt($request->only('email', 'password'))) {
            return redirect()->back()
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => $this->getFailedLoginMessage(),
                ]);
        }
        if (Auth::guard($this->getGuard())->once(Input::only('email', 'password'))) {
            if (!Auth::guard($this->getGuard())->user()->active) {
                Auth::guard($this->getGuard())->logout();
                return redirect()->back()
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getNotActivatedMessage(),
                    ]);
            }
        }
        return redirect()->intended($this->redirectPath());
    }
    /**
     * Get the failed login message if user not activated .
     *
     * @return string
     */
    protected function getNotActivatedMessage()
    {
        return Lang::has('app.not_activated')
            ? Lang::get('app.not_activated')
            : 'Account not activated.';
    }
    /**
     * @return array|\Illuminate\Support\Collection
     */
    protected function getPlansCollection()
    {
        $array = array();
        $currency = trans('app.SAR');
        $plans = \App\Plans\Models\Plan::all();
        foreach ($plans as $plan) {
            $array [] = [
                'name' => \Lang::get('app.plans.' . $plan->id . '.name') . " ( " . $plan->price . " $currency )",
                'value' => $plan->id
            ];
        }
        return $array;
    }
}
