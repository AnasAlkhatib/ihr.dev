<?php
namespace App\Http\Controllers\Web\Employer;

use Gate;
use App\Employer;
use App\Vacant;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\VacantRequest;
use Illuminate\Http\Request;
use App\AppliedVacant;
use Datatables;
use App\Area;
use File;
use App\Jobs\PermissionPublishEmployerVacant;
use App\CompanyIndustry;
use App\JobRole;
class VacantController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * VacantController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * Index function
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        return view('web.dashboard.employer.vacant.vacant_list');
    }
    /**
     * vacant_list function
     * @param Request $request
     * @return mixed
     */
    public function vacant_list(Request $request)
    {
        $vacant = Vacant::select('*')->where('employer_id', '=', $this->employer->id);
        if ($request->ajax()) {
            return Datatables::of($vacant)
                ->editColumn(
                    'job_title',
                    function ($vacant) {
                        $response = '<a  href="' . url(
                                'employer/vacant/' . $vacant->ID
                            ) . '" >' . $vacant->job_title . '</a>';
                        return $response;
                    }
                )
                ->addColumn(
                    'AppliedVacant',
                    function ($vacant) {
                        $AppliedVacant = AppliedVacant::with('vacant')->where('vacant_id', $vacant->ID)->get();
                        if (count($AppliedVacant) > 0) {
                            $response = '<a  href="' . url(
                                    'employer/vacant/' . $vacant->ID . '/applied-vacant'
                                ) . '" >' . count($AppliedVacant) . '</a>';
                        } else {
                            $response = '<a  href="' . url('#') . '" >' . count($AppliedVacant) . '</a>';
                        }
                        return $response;
                    }
                )
                ->editColumn(
                    'created_at',
                    function ($vacant) {
                        return $vacant->created_at->format('Y-m-d');
                    }
                )
                ->editColumn(
                    'close_date',
                    function ($vacant) {
                        return ($vacant->close_date === '0000-00-00' ? '---' : $vacant->close_date);
                    }
                )
                ->addColumn(
                    'action',
                    function ($vacant) {
                        $html = '<a href="vacant/' . $vacant->ID . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $vacant->ID . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('ID', '{{$ID}}')
                ->make(true);
        }
    }
    /**
     * Create function
     *
     *
     */
    public function create()
    {
        /* company_industry collection */
        $company_industry = $this->company_industry();
        /* job_rule array */
        $job_rule = $this->job_rule();
        /* Area collection */
        $area = $this->company_location();
        /* Data collection */
        $preloadData = $this->preloadData();
        /*  Education collection */
        $education = $preloadData['education'];
        /*  Language collection */
        $language = $preloadData['language'];
        /*  gender collection */
        $gender = $preloadData['gender'];
        /*  exp_level collection */
        $exp_level = $preloadData['exp_level'];
        /*  exp_years collection */
        $exp_years = $preloadData['exp_years'];
        /*  job_status collection */
        $job_status = $preloadData['job_status'];
        $tags = Vacant::existingTags()->pluck('name');
        return view('web.dashboard.employer.vacant.add_vacant')
            ->with(['company_industry' => $company_industry])
            ->with(['job_rule' => $job_rule])
            ->with(['area' => $area])
            ->with(['job_status' => $job_status])
            ->with(['education' => $education])
            ->with(['exp_level' => $exp_level])
            ->with(['Genders' => $gender])
            ->with(['language' => $language])
            ->with(['tags' => $tags])
            ->with(['exp_years' => $exp_years]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param VacantRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VacantRequest $request)
    {
        $languages = $request->input("language");
        $company_location = $request->input("company_location");
        /* Get all values except language */
        $values = array_map('trim', $request->except(['language', 'company_location', 'tags']));
        /* Get all values from language  if not empty array */
        if (!empty($languages)) {
            /* trim  all values */
            $language_array = array_map('trim', $languages);
            /* Convert all values to one string */
            $values['language'] = implode(",", $language_array);
        }
        if (!empty($company_location)) {
            /* trim  all values */
            $company_array = array_map('trim', $company_location);
            /* Convert all values to one string */
            $values['company_location'] = implode(",", $company_array);
        }
        /* Create Reference Number for the vacant   */
        $values['ref_num'] = $this->referenceNumber(9);
        /* Create the vacant and redirect to list */
        $vacant = Vacant::create($values);
        $vacant->tag(array_map('trim', $request->input('tags')));
        /**
         * Todo
         * if vacant created we get the values and sent this vacant to matched seeker
         */
        // $vacant_id = $vacant->ID;
        $this->dispatch(new PermissionPublishEmployerVacant($this->employer, $vacant));
        return redirect('employer/vacant')->with([
            'message' => trans('app.created_successfully'),
            'alert-class' => 'alert-success'
        ]);
    }
    /**
     * @param $id
     */
    public function show($id)
    {
        $vacant = Vacant::find($id);
        $this->authorizeForUser($this->employer, 'show', $vacant);
        if ($vacant) {
            $companyIndustry = $this->companyIndustryTransValue($vacant->company_industry);
            $companyLocation = $this->companyLocationTransValue($vacant->company_location);
            $jobRules = $this->jobRuleTransValue($vacant->job_rules);
            return view('web.dashboard.employer.vacant.show_vacant')
                ->with(['companyIndustry' => $companyIndustry])
                ->with(['companyLocation' => $companyLocation])
                ->with(['jobRules' => $jobRules])
                ->with(['vacant' => $vacant]);
        }
        return redirect('employer/vacant');
    }
    /**
     * @param $id
     */
    public function edit($id)
    {
        $vacant = Vacant::find($id);
        $this->authorizeForUser($this->employer, 'edit', $vacant);
        /* company_industry collection */
        $company_industry = $this->company_industry();
        /* job_rule array */
        $job_rule = $this->job_rule();
        /* Area collection */
        $area = $this->company_location();
        /* Data collection */
        $preloadData = $this->preloadData();
        /*  Education collection */
        $education = $preloadData['education'];
        /*  Language collection */
        $language = $preloadData['language'];
        /*  gender collection */
        $gender = $preloadData['gender'];
        /*  exp_level collection */
        $exp_level = $preloadData['exp_level'];
        /*  exp_years collection */
        $exp_years = $preloadData['exp_years'];
        /*  job_status collection */
        $job_status = $preloadData['job_status'];
        /* get all tags */
        $tags = Vacant::existingTags()->pluck('name');
        /* get vacant  */
        $vacant = Vacant::find($id);
        /* get related tags  */
        $vacantTags = $vacant->tagNames();
        return view('web.dashboard.employer.vacant.edit_vacant')
            ->with(['company_industry' => $company_industry])
            ->with(['job_rule' => $job_rule])
            ->with(['area' => $area])
            ->with(['job_status' => $job_status])
            ->with(['education' => $education])
            ->with(['exp_level' => $exp_level])
            ->with(['Genders' => $gender])
            ->with(['language' => $language])
            ->with(['exp_years' => $exp_years])
            ->with(['tags' => $tags])
            ->with(['vacantTags' => $vacantTags])
            ->with(['vacant' => $vacant]);
    }
    /**
     * Update the specified resource in storage
     *
     * @param VacantRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VacantRequest $request, $id)
    {
        $vacant = Vacant::find($id);
        $this->authorizeForUser($this->employer, 'update', $vacant);
        $languages = $request->input("language");
        $company_location = $request->input("company_location");
        /* Get all $values except  */
        $values = array_map('trim',
            $request->except(['language', 'company_location', '_method', '_token', 'tags']));
        /* Get all $values from language  if not empty array */
        if (!empty($languages)) {
            /* trim  all $values */
            $language_array = array_map('trim', $languages);
            /* Convert all $values to one string */
            $values['language'] = implode(",", $language_array);
        }
        if (!empty($company_location)) {
            /* trim  all $values */
            $company_location_array = array_map('trim', $company_location);
            /* Convert all $values to one string */
            $values['company_location'] = implode(",", $company_location_array);
        }
        foreach ($values as $field => $value) {
            $vacant->$field = $value;
        }
        $vacant->untag();
        $vacant->tag(array_map('trim', $request->input('tags')));
        if ($vacant->save()) {
            return redirect('employer/vacant')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return redirect('/employer/vacant')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $vacant = Vacant::find($id);
        $this->authorizeForUser($this->employer, 'delete', $vacant);
        if ($vacant->delete()) {
            return redirect('employer/vacant')->with(['message' => trans('app.deleted_successfully')]);
        }
        return redirect()->back()->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * get all Company Locations
     * @return array
     */
    public function company_location()
    {
        $areas = Area::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($areas as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($areas as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * git all company industry
     *
     * @return mixed
     */
    public function company_industry()
    {
        $company_industry_json = CompanyIndustry::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * git all job rule
     *
     * @return mixed
     */
    public function job_rule()
    {
        $company_industry_json = JobRole::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * Generate reference number for newly created vacant
     *
     * @param $length
     * @return string
     */
    private function referenceNumber($length)
    {
        $unique_ref_found = false;
        $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $numbers = "0123456789";
        $s_letters = str_shuffle($letters);
        $s_numbers = str_shuffle($numbers);
        while (!$unique_ref_found) {
            $i = 0;
            while ($i < $length) {
                $char = substr($s_letters, 24) . substr($s_numbers, 3);
                $unique_ref = substr($char, 0);
                $i++;
            }
            $query = Vacant::where('ref_num', $unique_ref)->count();
            if ($query === 0) {
                $unique_ref_found = true;
            }
        }
        return $unique_ref;
    }
    /**
     * @return array
     */
    public function preloadData()
    {
        $preloadDataArray = array();
        /* $exp_level collection  */
        $expLevel = collect(
            [
                ['name' => trans('app.exp_not_required'), 'value' => 'exp_not_required'],
                ['name' => trans('app.trainer'), 'value' => 'trainer'],
                ['name' => trans('app.beginner'), 'value' => 'beginner'],
                ['name' => trans('app.mid'), 'value' => 'mid'],
                ['name' => trans('app.management'), 'value' => 'management'],
                ['name' => trans('app.senior_management'), 'value' => 'senior_management']
            ]
        );
        /* education collection  */
        $education = collect(
            [
                ['name' => trans('app.high_school'), 'value' => 'high_school'],
                ['name' => trans('app.diploma'), 'value' => 'diploma'],
                ['name' => trans('app.bachelor'), 'value' => 'bachelor'],
                ['name' => trans('app.high_diploma'), 'value' => 'high_diploma'],
                ['name' => trans('app.master'), 'value' => 'master'],
                ['name' => trans('app.doctorate'), 'value' => 'doctorate']
            ]
        );
        /* language collection */
        $language = collect(
            [
                ['name' => trans('app.arabic'), 'value' => 'arabic'],
                ['name' => trans('app.english'), 'value' => 'english'],
                ['name' => trans('app.french'), 'value' => 'french']
            ]
        );
        /* gender collection  */
        $gender = collect(
            [
                ['name' => trans('app.male'), 'value' => 'male'],
                ['name' => trans('app.female'), 'value' => 'female'],
                ['name' => trans('app.both'), 'value' => 'both']
            ]
        );
        /* experience years array  $exp_level*/
        $expYears = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        /* job_status collection  */
        $jobStatus = collect(
            [
                ['name' => trans('app.full_time'), 'value' => 'full_time'],
                ['name' => trans('app.part_time'), 'value' => 'part_time'],
                ['name' => trans('app.freelance'), 'value' => 'freelance']
            ]
        );
        /* build master array */
        $preloadDataArray['education'] = $education;
        $preloadDataArray['language'] = $language;
        $preloadDataArray['gender'] = $gender;
        $preloadDataArray['exp_level'] = $expLevel;
        $preloadDataArray['exp_years'] = $expYears;
        $preloadDataArray['job_status'] = $jobStatus;
        return $preloadDataArray;
    }
    /**
     * translate companyIndustryTransValue based on locale
     * @param $id
     * @return mixed
     */
    public function companyIndustryTransValue($id)
    {
        $companyIndustry = \App\CompanyIndustry::find($id);
        if (\App::getLocale() === 'en' && $companyIndustry) {
            return $companyIndustry->name_en;
        } elseif (\App::getLocale() === 'ar' && $companyIndustry) {
            return $companyIndustry->name;
        }
        return $companyIndustry->name;
    }
    /**
     * translate companyLocationTransValue based on locale
     * @param $companyLocation
     * @return string
     */
    public function companyLocationTransValue($companyLocation)
    {
        $areas = explode(',', $companyLocation);
        $appLocale = \App::getLocale();
        $data = array();
        foreach ($areas as $area) {
            $modelByID = \App\Area::find($area);
            switch ($area) {
                case $modelByID && $appLocale == 'en':
                    $data [] = $modelByID->name_en;
                    break;
                case $modelByID && $appLocale == 'ar':
                    $data [] = $modelByID->name;
            }
        }
        return implode(' , ', $data);
    }
    /**
     *  translate jobRuleTransValue based on locale
     * @param $value
     * @return mixed
     */
    public function jobRuleTransValue($value)
    {
        $jobRole = \App\JobRole::find($value);
        if ($jobRole) {
            if (\App::getLocale() == 'en') {
                return $jobRole->name_en;
            }
            return $jobRole->name;
        }
        return $value;
    }
}
