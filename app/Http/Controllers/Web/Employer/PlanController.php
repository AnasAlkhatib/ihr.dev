<?php
namespace App\Http\Controllers\Web\Employer;

use App\Http\Controllers\Controller;
use App\Plans\Models\Plan;
use App\Plans\Models\PlanFeature;
/**
 * Class PlanController
 * @package App\Http\Controllers\Web\Employer
 */
class PlanController extends Controller
{
    /**
     *
     */
    public function getPricingTable()
    {
        $plans = Plan::with('features')->get();
        $plans->each(function ($plan) {
            $this->setPricingTableStyle($plan);
        });
        $features = PlanFeature::orderBy('sort_order', 'ASC')
            ->pluck('code')->unique();
        return view('web.dashboard.employer.plan.pricing_table')
            ->with(['plans' => $plans])
            ->with(['features' => $features]);
    }
    /**
     * set Pricing Table Style
     * @param $plan
     */
    protected function setPricingTableStyle($plan)
    {
        switch ($plan->id) {
            case 1:
                $plan->background = '#FFF';
                $plan->color = '#888';
                $plan->text_shadow = '0 1px 0 #FFF';
                break;
            case 2:
                $plan->background = '#666666';
                $plan->color = '#FFF';
                $plan->text_shadow = '0 1px 0 #333';
                break;
            case 3:
                $plan->background = '#DAA520';
                $plan->color = '#FFF';
                $plan->text_shadow = '0 1px 0 #999';
                break;
            case 4:
                $plan->background = '#C0C0C0';
                $plan->color = '#FFF';
                $plan->text_shadow = '0 1px 0 #999';
        }
        return $plan;
    }
}
