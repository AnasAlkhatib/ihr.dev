<?php
namespace App\Http\Controllers\Web\Employer;

use App\Employer;
use App\AppliedVacant;
use App\Http\Controllers\Controller;
use App\Vacant;
/**
 * Class EmployerDashboardController
 * @package App\Http\Controllers\Web\Employer
 */
class EmployerDashboardController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * EmployerDashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * Employer Home page
     */
    public function index()
    {
        $vacancies = $this->last_vacancies($this->employer);
        $vacant_count = $this->last_vacancies($this->employer)->count();
        $applied_vacant_count = $this->vacancies_applied($this->employer)->count();
        $employerFileName = $this->getEmployerFileName($this->employer);
        return view('web.dashboard.employer.index')
            ->with('vacancies', $vacancies)
            ->with('vacant_count', $vacant_count)
            ->with('applied_vacant_count', $applied_vacant_count)
            ->with(['employerFileName' => $employerFileName]);
    }
    /**
     * Display a listing vacancies for this Employer.
     *
     * @param Employer $employer
     * @return mixed
     */
    public function all_vacancies(Employer $employer)
    {
        return $employer->vacancies()
            ->orderBy('created_at', 'asc')
            ->get();
    }
    /**
     * Display a listing vacancies for this Employer.
     *
     * @param Employer $employer
     * @return mixed
     */
    public function last_vacancies(Employer $employer)
    {
        return $employer->vacancies()
            ->orderBy('created_at', 'desc')
            ->take(7)
            ->get();
    }
    /**
     * Display a listing  of applied seekers  for this Employer vacancies .
     *
     * @param Employer $employer
     * @return mixed
     */
    public function vacancies_applied(Employer $employer)
    {
        $vacancies = $employer->vacancies()->get(['ID']);
        $vacancies_ids = [];
        if ($vacancies) {
            foreach ($vacancies as $vacant) {
                $vacancies_ids [] = $vacant->ID;
            }
            return AppliedVacant::orderBy('ID', 'desc')
                ->whereIn('applied_vacancies.vacant_id', $vacancies_ids)
                ->get(['ID']);
        }
    }
    /**
     * Get Employer Profile image
     *
     * @param $employer
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getEmployerFileName($employer)
    {
        $employerPhotoUrl = asset('assets/img/user.jpg');
        $photoUrl = public_path() . '/uploads/employer/photo/' . $employer->pic_file_name;
        if (\File::exists($photoUrl) && $employer->pic_file_name) {
            $employerPhotoUrl = url('/uploads/employer/photo/' . $employer->pic_file_name);
        }
        return $employerPhotoUrl;
    }
}
