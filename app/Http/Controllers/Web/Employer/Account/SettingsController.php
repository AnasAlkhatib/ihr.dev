<?php
namespace App\Http\Controllers\Web\Employer\Account;

use App\Http\Requests\Web\EmployerUpdateProfileRequest as updateProfile;
use App\Http\Requests\Web\EmployerUpdatePasswordRequest as updatePassword;
use App\Http\Requests\API\Employer\EmployerUpdateProfilePictureRequest as UpdateProfilePicture;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Plans\Models\Plan;
use Carbon\Carbon;
use App\Employer;
use Config;
/**
 * Class SettingsController
 * @package App\Http\Controllers\Web\Employer\Account
 */
class SettingsController extends Controller
{
    /**
     * @var string
     */
    protected $path = '/uploads/employer/photo/';
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * SettingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * Show edit profile form
     */
    public function editProfile()
    {
        $employer = Employer::find($this->employer->id);
        $this->authorizeForUser($employer, 'editProfile', $this->employer);
        $companyTypes = $this->getCompanyTypes();
        $plans = $this->getPlans();
        $employerPlan = $this->getEmployerSubscriptions($employer);
        $employerFileName = $this->getEmployerFileName($employer);
        return view('web.dashboard.employer.account.edit_profile')
            ->with(['employerPlan' => $employerPlan])
            ->with(['plans' => $plans])
            ->with(['companyTypes' => $companyTypes])
            ->with(['employer' => $employer])
            ->with(['employerFileName' => $employerFileName]);
    }
    /**
     * update employer profile
     * @param updateProfile $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(updateProfile $request, $id)
    {
        $employer = \App\Employer::find($id);
        $this->authorizeForUser($employer, 'updateProfile', $this->employer);
        $values = array_map('trim', $request->except(['_token', '_method', 'plan']));
        foreach ($values as $field => $value) {
            $employer->$field = $value;
        }
        $currentPlan = $this->getEmployerSubscriptions($employer);
        if ($employer->save()) {
            if ($request->has('plan')) {
                $planId = (int)$request->input('plan');
                if ($currentPlan->plan_id != $planId) {
                    $this->updateEmployerSubscriptions($employer, $planId);
                }
            }
            return redirect('employer/settings/profile')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return redirect('/')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * edit Employer Password
     *
     */
    public function editPassword()
    {
        $employer = \App\Employer::find($this->employer->id);
        $this->authorizeForUser($employer, 'editPassword', $this->employer);
        return view('web.dashboard.employer.account.edit_password')
            ->with(['employer' => $employer]);
    }
    /**
     * update Employer Password
     * @param updatePassword $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(updatePassword $request, $id)
    {
        $employer = \App\Employer::find($id);
        $this->authorizeForUser($employer, 'updatePassword', $this->employer);
        $new_password = trim($request->input('password'));
        $employer->password = bcrypt($new_password);
        if ($employer->save()) {
            \Auth()->logout($employer, true);
            return redirect('/employer/settings/password')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
    /**
     * array for supported Company Types
     * @return \Illuminate\Support\Collection
     */
    public function getCompanyTypes()
    {
        $company_types = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return $company_types;
    }
    /** get all available plans
     * @return mixed
     */
    protected function getPlans()
    {
        $plans = Plan::all();
        $data = $plans->each(function ($plan) {
            return $plan->name = \Lang::get('app.plans.' . $plan->id . '.name');
        })->all();
        return $data;
    }
    /**
     * get current employer plan
     * @param Employer $employer
     * @return mixed | null
     */
    protected function getEmployerSubscriptions(Employer $employer)
    {
        $employerPlan = $employer->subscriptions()
            ->where('employer_id', $employer->id)
            ->get()->last();
        if ($employerPlan) {
            return $employerPlan;
        }
        return null;
    }
    /**
     * @param Employer $employer
     * @param $newPlanId
     */
    protected function updateEmployerSubscriptions(Employer $employer, $newPlanId)
    {
        /*
         * check if the employer current plan
         */
        $currentPlan = $this->getEmployerSubscriptions($employer);
        /*
         * check if this user want to change employer plan and not the current plan
         * no need for new subscription
         * if he is not subscribed
         * make new subscription and un subscribe him from old one
         */
        if ($employer->subscribed('sto', $currentPlan['plan_id'])) {
            $oldPlan = Plan::find($currentPlan['plan_id']);
            $employer->subscription('sto', $oldPlan)->cancel();
        }
        $newPlan = Plan::find($newPlanId);
        $employer->newSubscription('sto', $newPlan)->create();
        $this->deactivateEmployer($employer);
//        if ($newPlan->id != 1) {
//            $this->deactivateEmployer($employer);
//        }
    }
    /**
     * if he changed his plan to another except the free plan
     * @param $employer
     */
    protected function deactivateEmployer($employer)
    {
        $employer->active = 0;
        $employer->save();
        \Auth::guard('employer')->logout();
    }
    /**
     * @param UpdateProfilePicture $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfilePicture(UpdateProfilePicture $request, $id)
    {
        $employer = Employer::find($id);
        if ($request->hasFile('file')) {
            if ($employer->pic_file_name) {
                \File::delete(public_path() . $this->path . $employer->pic_file_name);
            }
            $file = $request->file('file');
            $name = $this->uploadPhoto($file);
            $employer->pic_file_name = $name;
            $employer->save();
            return response()->json([
                'message' => "Profile Picture correctly added",
                "pic_url" => url($this->path . $employer->pic_file_name),
                "code" => "201"
            ], 201);
        }
    }
    /**
     * upload and resize img
     *
     * @param  int $file
     * @return array
     */
    protected function uploadPhoto($file)
    {
        $config = config::get('profile-picture');
        $name = sha1(Carbon::now()) . '.' . $file->guessExtension();
        Image::make($file->getRealPath())
            ->fit($config['width'], $config['height'], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save('uploads/employer/photo/' . $name);
        return $name;
    }
    /**
     * @param $employer
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getEmployerFileName($employer)
    {
        $employerPhotoUrl = asset('assets/img/user.jpg');
        $photoUrl = public_path() . $this->path . $employer->pic_file_name;
        if (\File::exists($photoUrl) && $employer->pic_file_name) {
            $employerPhotoUrl = url($this->path . $employer->pic_file_name);
        }
        return $employerPhotoUrl;
    }
}
