<?php

namespace App\Http\Controllers\Web\Employer;


use App\Http\Controllers\Controller;
use App\City;
use App\Area;


/**
 * Class CityController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class CityController extends Controller
{


    /**
     * CityController constructor.
     */
    public function __construct()
    {
        $this->middleware('employer.auth');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = [];
        $Cities = City::all();
        foreach ($Cities as $city) {
            $data [] = array("text" => $city->name, "id" => $city->ID);
        }

        return response()->json($data, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCity($id)
    {
        $data = [];
        $Cities = Area::find($id)->city;
        foreach ($Cities as $city) {
            $data [] = array("text" => $city->name, "id" => $city->ID);
        }

        return response()->json($data, 200);
    }
}
