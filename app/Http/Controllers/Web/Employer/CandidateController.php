<?php
namespace App\Http\Controllers\Web\Employer;

use Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Web\EmployerCandidateRequest;
use App\Util\Traits\ExperienceTrait;
use  \Elibyy\TCPDF\TCPDF as PDF;
use File;
use App\Seeker;
/**
 * Class CandidateController
 * @package App\Http\Controllers\Web\Employer
 */
class CandidateController extends Controller
{
    use ExperienceTrait;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * CandidateController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->middleware('employer.auth');
        $this->employer = \Auth::guard('employer')->user();
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('web.dashboard.employer.candidate.candidates_list');
    }
    /**
     * @param $id
     * @param $vacantId
     * @return mixed
     */
    public function selectCandidate($id, $vacantId)
    {
        $seeker = \App\Seeker::find($id);
        /* if not found */
        if (!$seeker) {
            return redirect('employer/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-error'
            ]);
        }
        $vacant = $this->employer->vacancies->contains('ID', $vacantId);
        if (!$vacant) {
            return redirect()->back()->with(['message' => trans('app.Unauthorized'), 'alert-class' => 'alert-danger']);
        }
        $isCandidate = $this->checkCandidate($this->employer, $id, $vacantId);
        if ($isCandidate) {
            return redirect()->back()->with(['message' => trans('app.nominated'), 'alert-class' => 'alert-warning']);
        }
        return $this->showCandidateForm($seeker, $vacantId);
    }
    /**
     * @param $request
     */
    public function candidate_list(Request $request)
    {
        $query = $this->employer->seekers()->select('seeker_statuses.*');
        if ($request->ajax()) {
            return Datatables::eloquent($query)
                ->editColumn(
                    'name',
                    function ($candidate) {
                        $name = \App\Seeker::find($candidate->seeker_id)->full_name;
                        $response = '<a  href="' . url('employer/seeker/' . $candidate->seeker_id) . '" >' . $name . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'vacant_id',
                    function ($candidate) {
                        $jobTitle = \App\Vacant::find($candidate->vacant_id)->job_title;
                        $response = '<a  href="' . url('employer/vacant/' . $candidate->vacant_id) . '" >' . $jobTitle . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'status',
                    function ($candidate) {
                        $response = '---';
                        $status = $candidate->status;
                        if ($status) {
                            switch ($status) {
                                case $status == 'accept_candidate':
                                    $response = "<span class='text-success'>" . trans('app.accept_candidate') . "</span>";
                                    break;
                                case $status == 'reject_candidate':
                                    $response = "<span class='text-danger'>" . trans('app.reject_candidate') . "</span>";
                                    break;
                                case $status == 'offer_vacancies':
                                    $response = "<span class='text-warning'>" . trans('app.offer_vacancies') . "</span>";
                                    break;
                                case $status == 'final_interview':
                                    $response = "<span class='text-info'>" . trans('app.final_interview') . "</span>";
                                    break;
                                case $status == 'rehabilitation_candidate':
                                    $response = "<span class='text-info'>" . trans('app.rehabilitation_candidate') . "</span>";
                                    break;
                                case $status == 'telephone_interview':
                                    $response = "<span class='text-info'>" . trans('app.telephone_interview') . "</span>";
                                    break;
                                case $status == 'preselection':
                                    $response = "<span class='text-info'>" . trans('app.preselection') . "</span>";
                            }
                        }
                        return $response;
                    }
                )
                ->editColumn(
                    'created_at',
                    function ($candidate) {
                        return $candidate->created_at->format('Y-m-d H:i');
                    }
                )
                ->addColumn(
                    'action',
                    function ($candidate) {
                        $html = '<a href="/employer/candidate/' . $candidate->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $candidate->id . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('id', '{{$id}}')
                ->make(true);
        }
    }
    /**
     * edit seeker status
     * @param $id
     */
    public function edit($id)
    {
        $candidate = $this->employer->seekers()->wherePivot('id', $id)->first();
        $seekerExperience = $this->getExperience($candidate, 'seeker_id');
        /* if not found */
        if ($candidate->count() === 0) {
            return redirect('employer/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $address = $this->getCandidateFullAddress($candidate->city_id);
        $candidatables = $this->getCandidatables($this->employer, $candidate->ID, $candidate->pivot->vacant_id);
        return view('web.dashboard.employer.candidate.edit_candidate')
            ->with([
                'candidate' => $candidate,
                'candidatables' => $candidatables,
                'address' => $address,
                'seekerExperience' => $seekerExperience
            ]);
    }
    public function store(EmployerCandidateRequest $request)
    {
        $seekerId = (int)$request->input('seeker_id');
        /*
         * Todo check if the seeker id passed already applied for one of the employer vacancies
         */
        $collection = collect($request->except(['_token', 'seeker_id', '_method']));
        if ($request->has('required_documents')) {
            $collection = $collection->merge(['required_documents' => serialize($request->input('required_documents'))]);
        }
        if ($request->has('offer_vacancies')) {
            $collection = $collection->merge(['offer_vacancies' => serialize($request->input('offer_vacancies'))]);
        }
        $attributes = array_filter($collection->all());
        $this->employer->seekers()->attach($seekerId, $attributes);
        return redirect('employer/candidate')->with([
            'message' => trans('app.created_successfully'),
            'alert-class' => 'alert-success'
        ]);
    }
    /**
     * @param EmployerCandidateRequest $request
     * @param $id
     */
    public function update(EmployerCandidateRequest $request, $id)
    {
        $vacantId = (int)$request->input('vacant_id');
        $seekerId = (int)$request->input('seeker_id');
        $collection = collect($request->except(['_token', '_method']));
        if ($request->has('required_documents')) {
            $collection = $collection->merge(['required_documents' => serialize($request->input('required_documents'))]);
        }
        if ($request->has('offer_vacancies')) {
            $collection = $collection->merge(['offer_vacancies' => serialize($request->input('offer_vacancies'))]);
        }
        $attributes = array_filter($collection->all());
        $update = \DB::table('seeker_statuses')
            ->where('id', $id)
            ->where('seeker_id', $seekerId)
            ->where('vacant_id', $vacantId)
            ->update($attributes);
        if ($update) {
            return redirect('employer/candidate')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
    /**
     * delete seeker status
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->employer->seekers()->newPivotStatement()->where('id', $id)->delete();
        return redirect('employer/candidate')->with([
            'message' => trans('app.deleted_successfully'),
            'alert-class' => 'alert-success'
        ]);
    }

    public function checkDir()
    {
        $temp_path = public_path('/temp');
        if (!File::exists($temp_path)) {
            File::makeDirectory($temp_path, $mode = 0777, true, true);
        }
    }

    public function generatepdf($seeker)
    {
        $this->checkDir();
        $pdf_settings = \Config::get('tcpdf');
        $pdf = new PDF($pdf_settings['page_orientation'], $pdf_settings['page_units'], $pdf_settings['page_format'],
            true, 'UTF-8', true);
        $pdf->SetFont('AlMohanad', '', '15');
        $pdf->SetMargins(3, 1, 3, true);
        $pdf->AddPage();
        /* footer call back */
        $pdf->setFooterCallback(function ($pdf) {
            $image = public_path('img/powered_by_red.png');
            if ($pdf->PageNo() === $pdf->getNumPages()) {
                $pdf->SetY(-18.10);
                $pdf->setImageScale(1.53);
                $pdf->Image($image, '', '', '', '', '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
            }
        });


        $view = view('web.dashboard.employer.candidate.seeker_cv_pdf', ['seeker' => $seeker])->render();
        $pdf->writeHTML($view, true, false, true, false, '');
        $cv_data_name = 'cv_' . $seeker->full_name . '_' . date("Y-m-d");
        $file_name = public_path('temp/') . $cv_data_name . '.pdf';
        $pdf->output($file_name, 'D');



    }

    public function generatepdfcv($seekerId)
    {
        $seeker = Seeker::find($seekerId);
        if($seeker)
         $this->generatepdf($seeker);
        return;
    }

    /**
     * @param $seeker
     * @param $vacantId
     */
    private function showCandidateForm($seeker, $vacantId)
    {
        $address = $this->getCandidateFullAddress($seeker->city_id);
        $seekerExperience = $this->getExperience($seeker, 'seeker_id');
        return view('web.dashboard.employer.candidate.show_candidate_form')->with([
            'candidate' => $seeker,
            'address' => $address,
            'vacantId' => $vacantId,
            'seekerExperience' => $seekerExperience
        ]);
    }
    /**
     * @param $employer
     * @param $candidateId
     * @param $vacantId
     * @return mixed
     */
    private function getCandidatables($employer, $candidateId, $vacantId)
    {
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $candidateId)
            ->wherePivot('vacant_id', $vacantId)
            ->get()->toArray();
        return $data[0]['pivot'];
    }
    /**
     * @param $cityId
     * @return array
     */
    private function getCandidateFullAddress($cityId)
    {
        $address = array();
        $city = \App\City::find($cityId);
        if (\App::getLocale() == 'en' && $city) {
            $address ['city'] = $city->name_en;
            $address ['area'] = $city->area()->first()->name_en;
        } elseif (\App::getLocale() == 'ar' && $city) {
            $address ['city'] = $city->name;
            $address ['area'] = $city->area()->first()->name;
        }
        return $address;
    }
    /**
     * Get All Vacancies for this employer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllVacancies(Request $request)
    {
        if ($request->ajax()) {
            $data = array();
            $vacants = \App\Vacant::orderBy('ID', 'desc')
                ->where('active', 1)
                ->where('employer_id', $this->employer->id)
                ->get();
            foreach ($vacants as $vacant) {
                $data [] = array("text" => $vacant->job_title, "id" => $vacant->ID);
            }
            return response()->json($data, 200);
        }
    }
    /**
     * Get All Required Documents
     * @return \Illuminate\Http\JsonResponse
     */
    public function requiredDocuments()
    {
        $documents = collect([
            ['text' => trans('app.job_guarantee'), 'id' => 'job_guarantee'],
            ['text' => trans('app.criminal_evidence'), 'id' => 'criminal_evidence'],
            ['text' => trans('app.proof_of_residence'), 'id' => 'proof_of_residence'],
            ['text' => trans('app.medical_examination'), 'id' => 'medical_examination'],
        ]);
        return response()->json($documents, 200);
    }
    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function checkCandidate($employer, $seekerId, $vacantId)
    {
        $isCandidate = false;
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            $isCandidate = true;
        }
        return $isCandidate;
    }
}
