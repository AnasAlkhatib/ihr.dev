<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\Web\ClientRequest;
use App\Client;
use Datatables;
/**
 * Class ClientController
 * @package App\Http\Controllers\Web\Dashboard
 */
class ClientController extends Controller
{
    /**
     * @var $user
     */
    protected $user;
    /**
     * @var $auth
     */
    protected $auth;
    /**
     * ClientController constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->middleware('admin.auth');
        $this->user = $auth::user();
        $this->auth = $auth::guard("admin");
    }
    /**
     * Display a listing of the clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->user->can('list-users')) {
            return view('web.dashboard.client.client_list');
        }
        return redirect('dashboard')->with(['message' => trans('web.Unauthorized')]);
    }
    /** Ajax client Datatables
     * @param Request $request
     * @return bool
     */
    public function clientList(Request $request)
    {
        $client = Client::select('*');
        if ($request->ajax()) {
            return Datatables::of($client)
                ->editColumn(
                    'name',
                    function ($client) {
                        $response = '<a  href="' . url(
                                'dashboard/client/' . $client->id
                            ) . '" >' . $client->name . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'company_type',
                    function ($client) {
                        $response = trans('app.' . $client->company_type);
                        return $response;
                    }
                )
                ->editColumn(
                    'active',
                    function ($client) {
                        if ($client->active) {
                            return "<span class='text-success'>" . trans('app.active') . "</span>";
                        }
                        return "<span class='text-danger'>" . trans('app.inactive') . "</span>";
                    }
                )
                ->addColumn(
                    'action',
                    function ($client) {
                        $html = '<a href="/dashboard/client/' . $client->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $client->id . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->make(true);
        }
        return false;
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if ($this->user->can('add-employers')) {
            $companyTypes = self::companyTypes();
            return view('web.dashboard.client.add_client')->with(['companyTypes' => $companyTypes]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * Store a newly created resource in storage.
     * @param ClientRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ClientRequest $request)
    {
        $values = array_map('trim', $request->except(['_token', 'password_confirmation']));
        $values['password'] = bcrypt($request['password']);
        $values['active'] = (int)$request['active'];
        $client = Client::create($values);
        if ($client) {
            return redirect('dashboard/client')->with([
                'message' => trans('created_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     */
    public function show($id)
    {
        $client = Client::find($id);
        return view('web.dashboard.client.show_client')
            ->with(['client' => $client]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     */
    public function edit($id)
    {
        if ($this->user->can('update-employers')) {
            $companyTypes = self::companyTypes();
            $client = Client::find($id);
            return view('web.dashboard.client.edit_client')
                ->with(['client' => $client])
                ->with(['companyTypes' => $companyTypes]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * Update the specified resource in storage.
     * @param ClientRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClientRequest $request, $id)
    {
        if ($this->user->can('update-employers')) {
            $client = Client::find($id);
            $values = array_map('trim', $request->except(['_token', '_method', 'password_confirmation']));
            $values['active'] = (int)$request['active'];
            if (empty($request['password'])) {
                unset($values['password']);
            } else {
                $values['password'] = bcrypt($request['password']);
            }
            foreach ($values as $field => $value) {
                $client->$field = $value;
            }
            if ($client->save()) {
                return redirect('dashboard/client')->with([
                    'message' => trans('app.updated_successfully'),
                    'alert-class' => 'alert-success'
                ]);
            }
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-employers')) {
            $client = Client::find($id);
            $client->delete();
            return redirect('dashboard/client')->with(['message' => trans('app.deleted_successfully')]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /** collection of supported companyTypes
     * @return \Illuminate\Support\Collection
     */
    public static function companyTypes()
    {
        $companyTypes = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return $companyTypes;
    }
    /** get clients for ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllClients()
    {
        $data = [];
        $clients = Client::all();
        foreach ($clients as $client) {
            $data [] = array("text" => $client->name, "id" => $client->id);
        }
        return response()->json($data, 200);
    }
    /** get Client Vacancies
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClientVacancies($clientId)
    {
        $data [] = array();
        $vacants = \App\Vacant::orderBy('ID', 'desc')
            ->where('active', '=', 1)
            ->where('client_id', '=', $clientId)
            ->get();
        if (count($vacants) > 0) {
            foreach ($vacants as $vacant) {
                $data [] = array("text" => $vacant->job_title, "id" => $vacant->ID);
            }
        }
        return response()->json($data, 200);
    }
}

