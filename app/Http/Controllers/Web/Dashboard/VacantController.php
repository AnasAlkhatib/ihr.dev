<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\CompanyIndustry;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\VacantRequest;
use App\JobRole;
use Illuminate\Http\Request;
use App\AppliedVacant;
use Datatables;
use App\User;
use Carbon\Carbon;
use App\Vacant;
use App\Area;
use App\Util\Traits\VacantSeekerTagsTrait;
use App\Jobs\VacantPushNotification;
use App\Util\Traits\ReferenceIDTrait;
use App\Jobs\PermissionPublishUserVacant;
/**
 * Class VacantController
 * @package App\Http\Controllers\Web\Dashboard
 */
class VacantController extends Controller
{
    use VacantSeekerTagsTrait, ReferenceIDTrait;
    /**  authenticated user
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $user;
    /**
     * VacantController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
        $this->user = \Auth::guard('admin')->user();
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if ($this->user->can('list-jobs')) {
            return view('web.dashboard.vacant.vacant_list');
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function vacant_list(Request $request)
    {
        $vacant = Vacant::select('*');
        if ($request->ajax()) {
            return Datatables::of($vacant)
                ->editColumn(
                    'job_title',
                    function ($vacant) {
                        $response = '<a  href="' . url(
                                'dashboard/vacant/' . $vacant->ID
                            ) . '" >' . $vacant->job_title . '</a>';
                        return $response;
                    }
                )
                ->addColumn(
                    'AppliedVacant',
                    function ($vacant) {
                        $AppliedVacant = AppliedVacant::with('vacant')->where('vacant_id', $vacant->ID)->get();
                        if (count($AppliedVacant) > 0) {
                            $response = '<a  href="' . url(
                                    'dashboard/vacant/' . $vacant->ID . '/applied-vacant'
                                ) . '" >' . count($AppliedVacant) . '</a>';
                        } else {
                            $response = '<a  href="' . url('#') . '" >' . count($AppliedVacant) . '</a>';
                        }
                        return $response;
                    }
                )
                ->addColumn(
                    'posted_by',
                    function ($vacant) {
                        switch ($vacant) {
                            case $vacant->user_id != 0:
                                return self::postedBy('\App\User', $vacant->user_id, 'dashboard/user/');
                                break;
                            case $vacant->employer_id != 0:
                                return self::postedBy('\App\Employer', $vacant->employer_id, 'dashboard/employer/');
                                break;
                            case $vacant->client_id != 0:
                                return self::postedBy('\App\Client', $vacant->client_id, 'dashboard/client/');
                        }
                    }
                )
                ->editColumn(
                    'created_at',
                    function ($vacant) {
                        return $vacant->created_at->format('Y-m-d');
                    }
                )
                ->editColumn(
                    'close_date',
                    function ($vacant) {
                        return ($vacant->close_date === '0000-00-00' ? '---' : $vacant->close_date);
                    }
                )
                ->editColumn(
                    'status',
                    function ($vacant) {
                        if ($vacant->active == 1) {
                            return "<span class='text-success'>" . trans('app.active') . "</span>";
                        } else {
                            return "<span class='text-danger'>" . trans('app.inactive') . "</span>";
                        }
                    }
                )
                ->editColumn(
                    'activated_by',
                    function ($vacant) {
                        if ($vacant->active) {
                            $user = User::find($vacant->activated_by);
                            $response = '<a  href="' . url('dashboard/user/' . $vacant->activated_by) . '" >'
                                . $user->name . '</a>';
                            return $response;
                        } else {
                            return "<span class='text-danger'>" . '---' . "</span>";
                        }
                    }
                )
                ->addColumn(
                    'action',
                    function ($vacant) {
                        if ($vacant->active == 1) {
                            $html = '<a href="vacant/' . $vacant->ID . '/deactivate"> <button class="btn btn-warning btn-xs">
                        <i class="fa fa-ban margin-right-5"></i>' . trans('app.disable') . '</button></a>';
                        } else {
                            $html = '<a href="vacant/' . $vacant->ID . '/activate" class="btn-activate"> <button class="btn btn-success btn-xs">
                        <i class="fa fa-check-square-o margin-right-5"></i>' . trans('app.enable') . '</button></a>';
                        }
                        if (!$this->user->can('activate-vacancies') || !$this->user->can('deactivate-vacancies')) {
                            $html = "";
                        }
                        $html .= '<a href="vacant/' . $vacant->ID . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $vacant->ID . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('ID', '{{$ID}}')
                ->make(true);
        }
    }
    /**
     *  Store new vacant
     *
     */
    public function create()
    {
        if ($this->user->can('add-jobs')) {
            /* company_industry collection */
            $company_industry = $this->company_industry();
            /* job_rule array */
            $job_rule = $this->job_rule();
            /* Area collection */
            $area = $this->company_location();
            /* Data collection */
            $preloadData = $this->preloadData();
            /*  Education collection */
            $education = $preloadData['education'];
            /*  Language collection */
            $language = $preloadData['language'];
            /*  gender collection */
            $gender = $preloadData['gender'];
            /*  exp_level collection */
            $exp_level = $preloadData['exp_level'];
            /*  exp_years collection */
            $exp_years = $preloadData['exp_years'];
            /*  job_status collection */
            $job_status = $preloadData['job_status'];
            $tags = Vacant::existingTags()->pluck('name');
            return view('web.dashboard.vacant.add_vacant')
                ->with(['company_industry' => $company_industry])
                ->with(['job_rule' => $job_rule])
                ->with(['area' => $area])
                ->with(['job_status' => $job_status])
                ->with(['education' => $education])
                ->with(['exp_level' => $exp_level])
                ->with(['Genders' => $gender])
                ->with(['language' => $language])
                ->with(['tags' => $tags])
                ->with(['exp_years' => $exp_years]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * @param VacantRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VacantRequest $request)
    {
        if ($this->user->can('add-jobs')) {
            $languages = $request->input("language");
            $company_location = $request->input("company_location");
            /* Get all values except language */
            $values = array_map('trim', $request->except(['language', 'company_location', 'tags']));
            /* Get all values from language  if not empty array */
            if (!empty($languages)) {
                /* trim  all values */
                $language_array = array_map('trim', $languages);
                /* Convert all values to one string */
                $values['language'] = implode(",", $language_array);
            }
            if (!empty($company_location)) {
                /* trim  all values */
                $company_array = array_map('trim', $company_location);
                /* Convert all values to one string */
                $values['company_location'] = implode(",", $company_array);
            }
            /* Create Reference Number for the vacant   */
            $values['ref_num'] = $this->generateReferenceID(\App\Vacant::class, 'ref_num', $length = 9);
            /* auto activation if the vacant add by the User Super admin */
            if ($this->user->can('activate-vacancies')) {
                $values['active'] = true;
                $values['activated_by'] = $this->user->id;
                $values['activation_date'] = Carbon::now();
            }
            $vacant = Vacant::create($values);
            $vacant->tag(array_map('trim', $request->input('tags')));
            /* fire email */
            $this->dispatch(new PermissionPublishUserVacant($this->user, $vacant));
            /*  the push notification job */
            $this->pushNotificationJob($vacant->ID);
            return redirect('dashboard/vacant')->with([
                'message' => trans('app.created_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return redirect('dashboard/vacant')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * show vacant
     *
     */
    public function show($id)
    {
        if ($this->user->can('list-jobs')) {
            $vacant = Vacant::find($id);
            if ($vacant) {
                $companyIndustry = $this->companyIndustryTransValue($vacant->company_industry);
                $companyLocation = $this->companyLocationTransValue($vacant->company_location);
                $jobRules = $this->jobRuleTransValue($vacant->job_rules);
                return view('web.dashboard.vacant.show_vacant')
                    ->with(['companyIndustry' => $companyIndustry])
                    ->with(['companyLocation' => $companyLocation])
                    ->with(['jobRules' => $jobRules])
                    ->with(['vacant' => $vacant]);
            }
            return redirect('dashboard/vacant');
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     *
     * Edit vacant
     *
     */
    public function edit($id)
    {
        if ($this->user->can('update-jobs')) {
            /* company_industry collection */
            $company_industry = $this->company_industry();
            /* job_rule array */
            $job_rule = $this->job_rule();
            /* Area collection */
            $area = $this->company_location();
            /* Data collection */
            $preloadData = $this->preloadData();
            /*  Education collection */
            $education = $preloadData['education'];
            /*  Language collection */
            $language = $preloadData['language'];
            /*  gender collection */
            $gender = $preloadData['gender'];
            /*  exp_level collection */
            $exp_level = $preloadData['exp_level'];
            /*  exp_years collection */
            $exp_years = $preloadData['exp_years'];
            /*  job_status collection */
            $job_status = $preloadData['job_status'];
            /* get all tags */
            $tags = Vacant::existingTags()->pluck('name');
            /* get vacant  */
            $vacant = Vacant::find($id);
            /* get related tags  */
            $vacantTags = $vacant->tagNames();
            return view('web.dashboard.vacant.edit_vacant')
                ->with(['company_industry' => $company_industry])
                ->with(['job_rule' => $job_rule])
                ->with(['area' => $area])
                ->with(['job_status' => $job_status])
                ->with(['education' => $education])
                ->with(['exp_level' => $exp_level])
                ->with(['Genders' => $gender])
                ->with(['language' => $language])
                ->with(['exp_years' => $exp_years])
                ->with(['tags' => $tags])
                ->with(['vacantTags' => $vacantTags])
                ->with(['vacant' => $vacant]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * @param VacantRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VacantRequest $request, $id)
    {
        if ($this->user->can('update-jobs')) {
            $languages = $request->input("language");
            $company_location = $request->input("company_location");
            $vacant = Vacant::find($id);
            /* Get all $values except  */
            $values = array_map('trim',
                $request->except(['language', 'company_location', '_method', '_token', 'tags']));
            /* Get all $values from language  if not empty array */
            if (!empty($languages)) {
                /* trim  all $values */
                $language_array = array_map('trim', $languages);
                /* Convert all $values to one string */
                $values['language'] = implode(",", $language_array);
            }
            if (!empty($company_location)) {
                /* trim  all $values */
                $company_location_array = array_map('trim', $company_location);
                /* Convert all $values to one string */
                $values['company_location'] = implode(",", $company_location_array);
            }
            foreach ($values as $field => $value) {
                $vacant->$field = $value;
            }
            $vacant->untag();
            $vacant->tag(array_map('trim', $request->input('tags')));
            if ($vacant->save()) {
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.updated_successfully'),
                    'alert-class' => 'alert-success'
                ]);
            }
        }
        return redirect('/dashboard/vacant')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-jobs')) {
            $vacant = Vacant::find($id);
            $vacant->delete();
            return redirect('dashboard/vacant')->with(['message' => trans('app.deleted_successfully')]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * get all Company Locations
     * @return array
     */
    public function company_location()
    {
        $areas = Area::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($areas as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($areas as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * git all company industry
     *
     * @return mixed
     */
    public function company_industry()
    {
        $company_industry_json = CompanyIndustry::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * git all job rule
     *
     * @return mixed
     */
    public function job_rule()
    {
        $company_industry_json = JobRole::all();
        $response = [];
        if (\App::getLocale() === 'en') {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name_en);
            }
            return $response;
        } else {
            foreach ($company_industry_json as $key => $value) {
                $response [] = array('id' => $value->ID, 'name' => $value->name);
            }
            return $response;
        }
    }
    /**
     * activate vacant
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id)
    {
        if ($this->user->can('activate-vacancies')) {
            $vacant = Vacant::find($id);
            if (!$vacant) {
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.vacant_not_found'),
                    'alert-class' => 'alert-danger'
                ]);
            }
            if ($vacant->active == 1) {
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.already_activated'),
                    'alert-class' => 'alert-info'
                ]);
            }
            $vacant->active = 1;
            $vacant->activated_by = $this->user->id;
            $vacant->activation_date = Carbon::now();
            if ($vacant->save()) {
                /*  the push notification job */
                if ($vacant->push_notification == 0) {
                    $this->pushNotificationJob($vacant->ID);
                }
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.activated_successfully'),
                    'alert-class' => 'alert-success'
                ]);
            }
        }
        return redirect('/dashboard/vacant')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * deactivate vacant
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate($id)
    {
        if ($this->user->can('deactivate-vacancies')) {
            $vacant = Vacant::find($id);
            if (!$vacant) {
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.vacant_not_found'),
                    'alert-class' => 'alert-danger'
                ]);
            }
            $vacant->active = 0;
            if ($vacant->save()) {
                return redirect('dashboard/vacant')->with([
                    'message' => trans('app.deactivated_successfully'),
                    'alert-class' => 'alert-warning'
                ]);
            }
        }
        return redirect('/dashboard/vacant')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /**
     * @return array
     */
    public function preloadData()
    {
        $preloadDataArray = array();
        /* $exp_level collection  */
        $expLevel = collect(
            [
                ['name' => trans('app.exp_not_required'), 'value' => 'exp_not_required'],
                ['name' => trans('app.trainer'), 'value' => 'trainer'],
                ['name' => trans('app.beginner'), 'value' => 'beginner'],
                ['name' => trans('app.mid'), 'value' => 'mid'],
                ['name' => trans('app.management'), 'value' => 'management'],
                ['name' => trans('app.senior_management'), 'value' => 'senior_management']
            ]
        );
        /* education collection  */
        $education = collect(
            [
                ['name' => trans('app.high_school'), 'value' => 'high_school'],
                ['name' => trans('app.diploma'), 'value' => 'diploma'],
                ['name' => trans('app.bachelor'), 'value' => 'bachelor'],
                ['name' => trans('app.high_diploma'), 'value' => 'high_diploma'],
                ['name' => trans('app.master'), 'value' => 'master'],
                ['name' => trans('app.doctorate'), 'value' => 'doctorate']
            ]
        );
        /* language collection */
        $language = collect(
            [
                ['name' => trans('app.arabic'), 'value' => 'arabic'],
                ['name' => trans('app.english'), 'value' => 'english'],
                ['name' => trans('app.french'), 'value' => 'french']
            ]
        );
        /* gender collection  */
        $gender = collect(
            [
                ['name' => trans('app.male'), 'value' => 'male'],
                ['name' => trans('app.female'), 'value' => 'female'],
                ['name' => trans('app.both'), 'value' => 'both']
            ]
        );
        /* experience years array  $exp_level*/
        $expYears = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        /* job_status collection  */
        $jobStatus = collect(
            [
                ['name' => trans('app.full_time'), 'value' => 'full_time'],
                ['name' => trans('app.part_time'), 'value' => 'part_time'],
                ['name' => trans('app.freelance'), 'value' => 'freelance']
            ]
        );
        /* build master array */
        $preloadDataArray['education'] = $education;
        $preloadDataArray['language'] = $language;
        $preloadDataArray['gender'] = $gender;
        $preloadDataArray['exp_level'] = $expLevel;
        $preloadDataArray['exp_years'] = $expYears;
        $preloadDataArray['job_status'] = $jobStatus;
        return $preloadDataArray;
    }
    /** pushNotification Main Job
     * @param $vacant_id
     */
    protected function pushNotificationJob($vacant_id)
    {
        $vacant = Vacant::find($vacant_id);
        /* if the vacant not pushed yet  and the push notification settings in the env file is true */
        if (($vacant->push_notification == 0) && (config('app.seeker_vacant_push_notification') == true)) {
            $seekersData = $this->handlePushTask($vacant);
            if (count($seekersData) > 0) {
                dispatch(new VacantPushNotification($seekersData, $vacant));
            } else {
                \Log::info('No seeker has this tag to push notification');
            }
        }
    }
    public static function postedBy($model, $id, $url)
    {
        $postedBy = $model::find($id);
        $response = '<a  href="' . url($url . $id) . '" >' . $postedBy->name . '</a>';
        return $response;
    }
    /**
     * translate companyIndustryTransValue based on locale
     * @param $id
     * @return mixed
     */
    public function companyIndustryTransValue($id)
    {
        $companyIndustry = \App\CompanyIndustry::find($id);
        if (\App::getLocale() === 'en' && $companyIndustry) {
            return $companyIndustry->name_en;
        } elseif (\App::getLocale() === 'ar' && $companyIndustry) {
            return $companyIndustry->name;
        }
        return $companyIndustry->name;
    }
    /**
     * translate companyLocationTransValue based on locale
     * @param $companyLocation
     * @return string
     */
    public function companyLocationTransValue($companyLocation)
    {
        $areas = explode(',', $companyLocation);
        $appLocale = \App::getLocale();
        $data = array();
        foreach ($areas as $area) {
            $modelByID = \App\Area::find($area);
            switch ($area) {
                case $modelByID && $appLocale == 'en':
                    $data [] = $modelByID->name_en;
                    break;
                case $modelByID && $appLocale == 'ar':
                    $data [] = $modelByID->name;
            }
        }
        return implode(' , ', $data);
    }
    /**
     *  translate jobRuleTransValue based on locale
     * @param $value
     * @return mixed
     */
    public function jobRuleTransValue($value)
    {
        $jobRole = \App\JobRole::find($value);
        if ($jobRole) {
            if (\App::getLocale() == 'en') {
                return $jobRole->name_en;
            }
            return $jobRole->name;
        }
        return $value;
    }
}
