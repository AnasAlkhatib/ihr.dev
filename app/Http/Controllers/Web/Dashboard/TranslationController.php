<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Area;
use App\City;
use App\CompanyIndustry;
use App\Http\Controllers\Controller;
use App\JobRole;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TranslationController extends Controller
{
    public function index()
    {
        $areas = Area::all();
        $cities = City::all();
        $companyIndustries = CompanyIndustry::all();
        $jobRoles = JobRole::all();

        return view('web.dashboard.translation.trans_list')
            ->with(['areas' => $areas])
            ->with(['cities' => $cities])
            ->with(['companyIndustries' => $companyIndustries])
            ->with(['jobRoles' => $jobRoles]);
    }

    public function editArea($id)
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $area = Area::find($id);
            $area->name = Input::get('name');
            $area->name_en = Input::get('name_en');
            $area->save();

            return Redirect::to('dashboard/translations');
        }
    }

    public function editCity($id)
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $city = City::find($id);
            $city->name = Input::get('name');
            $city->name_en = Input::get('name_en');
            $city->save();

            return Redirect::to('dashboard/translations');
        }
    }

    public function editCI($id)
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $ci = CompanyIndustry::find($id);
            $ci->name = Input::get('name');
            $ci->name_en = Input::get('name_en');
            $ci->save();

            return Redirect::to('dashboard/translations');
        }
    }

    public function addCI()
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $ci = new CompanyIndustry;
            $ci->name = Input::get('name');
            $ci->name_en = Input::get('name_en');
            $ci->save();

            return Redirect::to('dashboard/translations');
        }
    }

    public function editJR($id)
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $jr = JobRole::find($id);
            $jr->name = Input::get('name');
            $jr->name_en = Input::get('name_en');
            $jr->save();

            return Redirect::to('dashboard/translations');
        }
    }

    public function addJR()
    {
        $rules = array(
            'name' => 'required',
            'name_en' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('dashboard/translations')
                ->withErrors($validator);
        } else {
            // store
            $jr = new JobRole;
            $jr->name = Input::get('name');
            $jr->name_en = Input::get('name_en');
            $jr->save();

            return Redirect::to('dashboard/translations');
        }
    }

}