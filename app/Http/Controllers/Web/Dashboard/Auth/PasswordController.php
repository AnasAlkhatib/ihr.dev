<?php
namespace App\Http\Controllers\Web\Dashboard\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
class PasswordController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */
    use ResetsPasswords;
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    protected $guard = 'admin';
    protected $broker = 'admin';
    protected $redirectTo = '/dashboard';
    public function __construct()
    {
        $this->middleware('admin.guest');
    }
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.admin.passwords.email');
    }
    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string|null $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }
        $email = $request->input('email');
        return view('auth.admin.passwords.reset')->with(compact('token', 'email'));
    }
    public function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : trans('system_message.reset_password.subject');
    }
}
