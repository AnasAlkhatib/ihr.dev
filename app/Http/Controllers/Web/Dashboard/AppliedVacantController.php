<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vacant;
use App\Area;
use App\City;
use File;
use App\Util\Traits\ExperienceTrait;
use App\Util\Traits\SearchDropDownTrait;
use App\Util\Traits\SeekerSearchableTrait;
use App\Util\Traits\EmployedTrait;
/**
 * Class AppliedVacantController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class AppliedVacantController extends Controller
{
    use ExperienceTrait;
    use SearchDropDownTrait;
    use SeekerSearchableTrait;
    use EmployedTrait;
    /**
     * AppliedVacantController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }
    /**
     * @param Request $request
     * @param $id
     */
    public function index(Request $request, $id)
    {
        // find the vacant
        $vacant = Vacant::find($id);
        /* find all Applied seekers */
        $seekers = $vacant->Applied_seeker();
        /* if no seekers applied yet */
        if (!$seekers) {
            return response()->json(['message' => 'No Seeker Applied for this vacant', 'code' => "404"], 404);
        }
        if ($request->input('gender')) {
            $seekers = $seekers->where('gender', '=', $request->input('gender'));
        }
        if ($request->input('area')) {
            $cities = Area::find($request->input('area'))->city;
            foreach ($cities as $city) {
                $array[] = $city->ID;
            }
            $seekers = $seekers->wherein('city_id', $array);
        }
        if ($request->input('city')) {
            $seekers = $seekers->where('city_id', '=', $request->input('city'));
        }
        if ($request->input('nationality')) {
            if ($request->input('nationality') === '1') {
                $seekers = $seekers->wherein('nationality', ['السعودية', 'سعودي']);
            } elseif ($request->input('nationality') === '2') {
                $seekers = $seekers->whereNotIn('nationality', ['السعودية', 'سعودي']);
            }
        }
        /*
        * form here
        */
        if ($request->input('age')) {
            $seekers = $this->seekerByAge($seekers, $request->input('age'));
        }
        if ($request->input('edu_level')) {
            $seekers = $this->seekerByEduLevel($seekers, $request->input('edu_level'));
        }
        if ($request->input('exp_years')) {
            $seekers = $this->seekerByExpYears($seekers, $request->input('exp_years'));
        }
        if ($request->input('company_industry')) {
            $seekers = $this->seekerByCompanyIndustry($seekers, $request->input('company_industry'));
        }
        /*
         * to here
         */
        /* paginate $seekers object */
        $seekers = $seekers->paginate(6);
        /* loop throw each object and modify some properties */
        $seekers->each(
            function ($seeker) {
                $seeker->gender = ($seeker->gender === 'male' ? 'ذكر' : 'أنثى ');
                if ($seeker->city_id) {
                    $seeker->setAttribute('city', $this->get_city_name($seeker->city_id));
                    $seeker->setAttribute('area', $this->get_area_name($seeker->city_id));
                }
                $seeker->setAttribute('age', $this->get_seeker_age($seeker->birth_date));
                $seeker->setAttribute('edu', $this->get_seeker_education($seeker));
                $seeker->setAttribute('exp', $this->getExperience($seeker, 'seeker_id'));
                $seeker->is_employed = $this->isEmployed($seeker);
                $pic = public_path() . '/uploads/seeker/photo/' . $seeker->pic_file_name;
                if (File::exists($pic) && $seeker->pic_file_name) {
                    $seeker->setAttribute('pic_url', url("/uploads/seeker/photo/" . $seeker->pic_file_name));
                } else {
                    $seeker->setAttribute('pic_url', url("assets/img/user.jpg"));
                }
            }
        );
        if ($request->ajax()) {
            return response()->json(
                view('web.dashboard.applied-vacant.ajax-seekers')->with(compact('seekers'))->render()
            );
        }
        $dropDownData = $this->buildSearchDropDown();
        return view('web.dashboard.applied-vacant.index')
            ->with('dropDownData', $dropDownData)
            ->with('seekers', $seekers)
            ->with('vacant', $vacant);
    }
    /**
     * calculate seeker age
     *
     * @param $birthday
     * @return int
     */
    public function get_seeker_age($birthday)
    {
        $age = date_create($birthday)->diff(date_create('today'))->y;
        return $age;
    }
    /**
     * get seeker last education
     *
     * @param $seeker
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function get_seeker_education($seeker)
    {
        $education = $seeker->education->all();
        if ($education) {
            // return trans_choice('app.search', $seeker->education->last()->edu_level);
            return $this->transEducation($seeker->education->last()->edu_level);
        } else {
            return trans('app.not_found');
        }
    }
    /**
     * get City name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_city_name($city_id)
    {
        return City::find($city_id)->name;
    }
    /**
     * get Area name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_area_name($city_id)
    {
        $city = City::find($city_id);
        return $city->area()->first()->name;
    }
    public function transEducation($education)
    {
        switch ($education) {
            case $education == 'ثانوية عامة':
                $education = trans('app.high_school');
                break;
            case $education == 'دبلوم':
                $education = trans('app.diploma');
                break;
            case $education == 'بكالوريوس':
                $education = trans('app.bachelor');
                break;
            case $education == 'دبلوم عالي':
                $education = trans('app.high_diploma');
                break;
            case $education == 'ماجستير':
                $education = trans('app.master');
                break;
            case $education == 'دكتوراة':
                $education = trans('app.doctorate');
                break;
            default:
                $education = '';
        }
        return $education;
    }
}
