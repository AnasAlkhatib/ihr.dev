<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\AppliedVacant;
use App\Vacant;
use App\Seeker;
use Auth;
use Carbon\Carbon;
use DB;
/**
 * Class DashboardController
 * @package App\Http\Controllers\Web\Dashboard
 */
class DashboardController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $user;
    /**
     * DashboardController constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth::guard('admin');
        $this->user = $this->auth->user();
        $this->middleware('admin.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = $this->last_vacancies();
        $news = $this->last_news();
        $vacant_count = Vacant::all()->count();
        $seekers_count = Seeker::all()->count();
        $applied_vacant_count = AppliedVacant::all()->count();
        return view('web.dashboard.index')
            ->with('user', $this->user)
            ->with('vacancies', $vacancies)
            ->with('news', $news)
            ->with('vacant_count', $vacant_count)
            ->with('seekers_count', $seekers_count)
            ->with('applied_vacant_count', $applied_vacant_count);
    }
    /**
     * Display a listing vacancies.
     *
     * @return array|static[]
     */
    public function last_vacancies()
    {
        $vacant = DB::table('vacancies')
            ->take(7)
            ->orderBy('ID', 'desc')
            ->get();
        return $vacant;
    }
    /**
     * Display a listing news.
     *
     * @return array|static[]
     */
    public function last_news()
    {
        $news = DB::table('news')
            ->take(7)
            ->orderBy('ID', 'desc')
            ->get();
        return $news;
    }
}
