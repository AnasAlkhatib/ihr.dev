<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\City;
use App\Area;
/**
 * Class CityController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class CityController extends Controller
{
    /**
     * CityController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = [];
        $cities = City::all();
        $appLocale = \App::getLocale();
        foreach ($cities as $city) {
            switch ($city) {
                case ($appLocale == 'en' && $city->name_en):
                    $data [] = array("text" => $city->name_en, "id" => $city->ID);
                    break;
                default:
                    $data [] = array("text" => $city->name, "id" => $city->ID);
            }
        }
        return response()->json($data, 200);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCity($id)
    {
        $data = [];
        $appLocale = \App::getLocale();
        $cities = Area::find($id)->city;
        foreach ($cities as $city) {
            switch ($city) {
                case ($appLocale == 'en' && $city->name_en):
                    $data [] = array("text" => $city->name_en, "id" => $city->ID);
                    break;
                default:
                    $data [] = array("text" => $city->name, "id" => $city->ID);
            }
        }
        return response()->json($data, 200);
    }
}
