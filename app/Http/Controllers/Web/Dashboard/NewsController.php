<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\NewsRequest;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Datatables;
use App\News;
use App\User;
use File;
//use Illuminate\Contracts\Filesystem\FileNotFoundException;
class NewsController extends Controller
{
    protected $path = '/uploads/news';
    private $user;
    public function __construct(\Auth $auth)
    {
        $this->middleware('admin.auth');
        $this->user = $auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->user->can('list-news')) {
            $breadcrumb = collect(
                [
                    ['title' => trans('web.news_list'), 'link' => '#', 'class' => 'active']
                ]
            );
            return view('web.dashboard.news.news_list')
                ->with(['breadcrumb' => $breadcrumb]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    public function news_list(Request $request)
    {
        $news = News::select('*');
        /* @To do */
        // eager loading $vacants = Vacant::with('user');
        if ($request->ajax()) {
            return Datatables::of($news)
                ->editColumn(
                    'news_title',
                    function ($news) {
                        $response = '<a  href="' . url(
                                'dashboard/news/' . $news->ID
                            ) . '" >' . $news->news_title . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'news_posted_by',
                    function ($news) {
                        return User::find($news->news_posted)->name;
                    }
                )
                ->editColumn(
                    'news_created_at',
                    function ($news) {
                        return $news->created_at->format('Y-m-d');
                    }
                )
                ->addColumn(
                    'action',
                    function ($news) {
                        $html = '<a href="news/' . $news->ID . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $news->ID . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('ID', '{{$ID}}')
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->user->can('add-news')) {
            $breadcrumb = collect(
                [
                    ['title' => trans('web.news_list'), 'link' => url('dashboard/news'), 'class' => ''],
                    ['title' => trans('web.add_news'), 'link' => '#', 'class' => 'active']
                ]
            );
            return view('web.dashboard.news.add_news')
                ->with(['breadcrumb' => $breadcrumb]);
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NewsRequest $request)
    {
        if ($this->user->can('add-news')) {
            $upload = array();
            if ($request->hasFile('news_img')) {
                $img = $request->file('news_img');
                $upload = $this->upload_img($img);
            }
            $values = array_map('trim', $request->except(['_token', 'news_img']));
            $values['news_posted'] = \Auth::user()->id;
            if (!empty($upload)) {
                $values['news_img'] = $upload['original'];
                $values['news_thumbnail'] = $upload['thumbnail'];
            }
            $news = News::create($values);
            if ($news) {
                return redirect('dashboard/news');
            }
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->user->can('list-news')) {
            $breadcrumb = collect(
                [
                    ['title' => trans('web.news_list'), 'link' => url('dashboard/news'), 'class' => ''],
                    ['title' => trans('web.news_show'), 'link' => '#', 'class' => 'active']
                ]
            );
            $news = News::find($id);
            if ($news) {
                return view('web.dashboard.news.show_news')
                    ->with(['news' => $news])
                    ->with(['breadcrumb' => $breadcrumb]);
            }
            return redirect('dashboard/news');
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->user->can('update-news')) {
            $breadcrumb = collect(
                [
                    ['title' => trans('web.news_list'), 'link' => url('dashboard/news'), 'class' => ''],
                    ['title' => trans('web.news_edit'), 'link' => '#', 'class' => 'active']
                ]
            );
            $news = News::find($id);
            if ($news) {
                return view('web.dashboard.news.edit_news')
                    ->with(['news' => $news])
                    ->with(['breadcrumb' => $breadcrumb]);
            }
            return redirect('dashboard/news');
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * Update the specified resource in storage
     *
     * @param NewsRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsRequest $request, $id)
    {
        if ($this->user->can('update-news')) {
            $upload = array();
            if ($request->hasFile('news_img')) {
                $img = $request->file('news_img');
                $upload = $this->upload_img($img);
                $news = News::find($id);
                $img_path = public_path() . $this->path . '/img/' . $news->news_img;
                $thumbnail_path = public_path() . $this->path . '/thumbnail/' . $news->news_thumbnail;
                if (File::exists($img_path)) {
                    File::delete($img_path);
                }
                if (File::exists($thumbnail_path)) {
                    File::delete($thumbnail_path);
                }
            }
            $news = News::find($id);
            $values = array_map('trim', $request->except(['_token', '_method', 'news_img']));
            $values['news_posted'] = \Auth::user()->id;
            if (!empty($upload)) {
                $values['news_img'] = $upload['original'];
                $values['news_thumbnail'] = $upload['thumbnail'];
            }
            foreach ($values as $field => $value) {
                $news->$field = $value;
            }
            if ($news->save()) {
                return redirect('dashboard/news')->with(['message' => 'updated !!!']);
            }
            return redirect('/')->with(['message' => trans('web.Unauthorized')]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-news')) {
            $news = News::find($id);
            $img_path = public_path() . $this->path . '/img/' . $news->news_img;
            $thumbnail_path = public_path() . $this->path . '/thumbnail/' . $news->news_thumbnail;
            if (File::exists($img_path)) {
                File::delete($img_path);
            }
            if (File::exists($thumbnail_path)) {
                File::delete($thumbnail_path);
            }
            $news->delete();
            return redirect('dashboard/news')->with(['message' => 'Deleted!!']);
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * upload and resize img
     *
     * @param  int $img
     * @return array
     */
    private function upload_img($img)
    {
        $name = sha1(Carbon::now()) . '.' . $img->guessExtension();
        Image::make($img->getRealPath())->save('uploads/news/img/' . $name);
        Image::make($img->getRealPath())->fit(350, 170)->save('uploads/news/thumbnail/' . $name);
        $response = collect(['original' => $name, 'thumbnail' => $name]);
        return $response;
    }
}
