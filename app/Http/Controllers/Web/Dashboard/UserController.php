<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\UserRequest;
use App\Http\Requests\Web\UpdateProfileRequest;
use App\Http\Requests\Web\UpdatePasswordRequest;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
/**
 * Class UserController
 * @package App\Http\Controllers\Web\Dashboard
 */
class UserController extends Controller
{
    protected $user;
    protected $auth;
    /**
     * UserController constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->middleware('admin.auth');
        $this->user = $auth::user();
        $this->auth = $auth::guard("admin");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->user->can('list-users')) {
            return view('web.dashboard.user.user_list');
        }
        return redirect('dashboard')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function user_list(Request $request)
    {
        $user = User::select('*');
        if ($request->ajax()) {
            return Datatables::of($user)
                ->editColumn(
                    'name',
                    function ($user) {
                        $response = '<a  href="' . url(
                                '/dashboard/user/' . $user->id
                            ) . '" >' . $user->name . '</a>';
                        return $response;
                    }
                )
                ->addColumn(
                    'role',
                    function ($user) {
                        $role = $user->roles()->first()->name;
                        return trans('app.' . $role);
                    }
                )
                ->addColumn(
                    'action',
                    function ($user) {
                        $html = '<a href="/dashboard/user/' . $user->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $user->id . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        if ($this->user->can('add-users')) {
            return view('web.dashboard.user.add_user')
                ->with(['roles' => $roles]);
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * @param UserRequest $request
     * @return string
     */
    public function store(UserRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        $user->attachRole($request['role']);
        return redirect('/dashboard/user')
            ->with('message', trans('web.user_created'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->back()->with(['message' => trans('app.not_found')]);
        }
        return view('web.dashboard.user.show_user')
            ->with(['user' => $user]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->user->can('update-users')) {
            $user = User::find($id);
            $roles = Role::all();
            return view('web.dashboard.user.edit_user')
                ->with(['roles' => $roles])
                ->with(['user' => $user]);
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * @param UserRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $id)
    {
        if ($this->user->can('update-users')) {
            $user = User::find($id);
            $values = array_map('trim', $request->except(['_token', 'role', '_method', 'password_confirmation']));
            if (empty($request['password'])) {
                unset($values['password']);
            } else {
                $values['password'] = bcrypt($request['password']);
            }
            foreach ($values as $field => $value) {
                $user->$field = $value;
            }
            if ($user->save()) {
                $user->roles()->sync([$request['role']]);
                return redirect('/dashboard/user')->with(['message' => 'updated !!!']);
            }
        }
        return redirect('/')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * edit User Profile form .
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->back()->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-danger'
            ]);
        }
        $this->authorizeForUser($user, 'editProfile', $this->user);
        return view('web.dashboard.user.edit_profile')
            ->with(['user' => $user]);
    }
    /**
     * Update User Profile method
     *
     * @param UpdateProfileRequest $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateProfile(UpdateProfileRequest $request, $id)
    {
        $user = User::find($id);
        $this->authorizeForUser($user, 'updateProfile', $this->user);
        if (!$user) {
            return redirect()->back()->with([
                'message' => trans('web.not_found'),
                'alert-class' => 'alert-danger'
            ]);
        }
        $values = array_map('trim', $request->only(['email', 'name']));
        foreach ($values as $field => $value) {
            $user->$field = $value;
        }
        if ($user->save()) {
            return redirect()->back()->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return view('web.dashboard.user.edit_profile')
            ->with(['user' => $user]);
    }
    /**
     * edit User Profile form .
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editPassword($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->back()->with([
                'message' => trans('web.not_found'),
                'alert-class' => 'alert-danger'
            ]);
        }
        $this->authorizeForUser($user, 'editPassword', $this->user);
        return view('web.dashboard.user.edit_password')
            ->with(['user' => $user]);
    }
    /**
     * Update User password method .
     *
     * @param UpdatePasswordRequest $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updatePassword(UpdatePasswordRequest $request, $id)
    {
        $user = User::find($id);
        $this->authorizeForUser($user, 'updatePassword', $this->user);
        if (!$user) {
            return redirect()->back()->with([
                'message' => trans('web.not_found'),
                'alert-class' => 'alert-danger'
            ]);
        }
        $new_password = trim($request->input('password'));
        $user->password = bcrypt($new_password);
        if ($user->save()) {
            $this->auth->login($user, true);
            return redirect('/dashboard/user/' . $user->id . '/editPassword')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
        return view('web.dashboard.user.edit_profile')
            ->with(['user' => $user]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-users')) {
            $user = User::find($id);
            $user->delete();
            return redirect('/dashboard/user')->with(['message' => 'deleted!!']);
        }
        return redirect()->back()->with(['message' => trans('web.Unauthorized')]);
    }
}
