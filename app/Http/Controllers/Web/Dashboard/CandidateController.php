<?php
namespace App\Http\Controllers\Web\Dashboard;

use DB;
use App\City;
use App\Seeker;
use App\Vacant;
use Datatables;
use App\Candidate;
use Illuminate\Http\Request;
use App\Http\Requests\Web\CandidateRequest;
use App\Http\Requests\Web\SendCandidateProfileRequest;
use App\Http\Controllers\Controller;
use App\Jobs\GenerateCandidateProfile;
use App\Util\Traits\ExperienceTrait;
use App\Util\Traits\ReferenceIDTrait;
use App\Services\Employment\EmploymentBuilder;
/**
 * Class CandidateController
 * @package App\Http\Controllers\Web\Dashboard
 */
class CandidateController extends Controller
{
    use ExperienceTrait;
    use ReferenceIDTrait;
    /**
     * @var $auth
     */
    protected $auth;
    /**
     * @var $user
     */
    protected $user;
    /**
     * SeekerController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard('admin');
        $this->user = $this->auth->user();
        $this->middleware('admin.auth');
    }
    /**
     * @param $id
     * @param $request
     */
    public function selectCandidate($id, Request $request)
    {
        $seeker = Seeker::find($id);
        /* if not found */
        if (!$seeker) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-error'
            ]);
        }
        return $this->showCandidateForm($seeker, $request);
    }
    /**
     * form for Transfer Seeker to Candidate
     * @param $seeker
     * @param $request
     */
    public function showCandidateForm($seeker, Request $request)
    {
        $city = City::find($seeker->city_id);
        $seekerArea = $city->area()->first()->ID;
        $seekerExperience = $this->getExperience($seeker, 'seeker_id');
        $seekerVacantApplied = $seeker->vacantApplied()->paginate(3);
        if ($request->ajax()) {
            return response()->json(
                view('web.dashboard.candidate.candidate_vacant_applied')->with(compact('seekerVacantApplied'))->render()
            );
        }
        return view('web.dashboard.candidate.show_candidate_form')->with([
            'seeker' => $seeker,
            'seekerArea' => $seekerArea,
            'seekerExperience' => $seekerExperience,
            'seekerVacantApplied' => $seekerVacantApplied,
            'clients' => \App\Client::all()
        ]);
    }
    /**
     * Get Seeker Experience
     * @param $seeker
     * @return string
     */
    public function getSeekerExperience($seeker)
    {
        $totalExperience = DB::table('experiences')
            ->select(DB::raw('sum( datediff( ifnull(experiences.end_date, now()), experiences.start_date)+1) AS total'))
            ->where('seeker_id', '=', $seeker->ID)
            ->groupBy('seeker_id')
            ->get();
        if (count($totalExperience) > 0) {
            $days = (int)$totalExperience[0]->total;
            return $this->convertExperienceDays($days);
        }
    }
    /**
     * Convert Experience Days
     * @param $days
     * @return string
     */
    public function convertExperienceDays($days)
    {
        $years = ($days / 365); // days / 365 days
        $years = floor($years); // Remove all decimals
        $month = ($days % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
        $months = floor($month); // Remove all decimals
        ($years > 0) ? $returned_years = $years . " " . trans('app.year') : $returned_years = '';
        ($months > 0) ? $returned_months = $months . " " . trans('app.month') : $returned_months = '';
        return $returned_years . ' ' . $returned_months;
    }
    /**
     * Get All Required Documents
     * @return \Illuminate\Http\JsonResponse
     */
    public function requiredDocuments()
    {
        $documents = collect([
            ['text' => trans('app.job_guarantee'), 'id' => 'job_guarantee'],
            ['text' => trans('app.criminal_evidence'), 'id' => 'criminal_evidence'],
            ['text' => trans('app.proof_of_residence'), 'id' => 'proof_of_residence'],
            ['text' => trans('app.medical_examination'), 'id' => 'medical_examination'],
        ]);
        return response()->json($documents, 200);
    }
    /**
     * Candidates list
     */
    public function index()
    {
        return view('web.dashboard.candidate.candidates_list');
    }
    /**
     * Candidates ajax list
     * @param Request $request ;
     */
    public function candidate_list(Request $request)
    {
        $query = Candidate::with('users', 'clients')->select('candidates.*');
        if ($request->ajax()) {
            return Datatables::eloquent($query)
                ->editColumn(
                    'name',
                    function ($candidate) {
                        $response = '<a  href="' . url('dashboard/candidate/' . $candidate->id) . '" >'
                            . $candidate->name . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'client_name',
                    function ($candidate) {
                        $clientName = $candidate->clients->last();
                        $response = '<a  href="' . url('dashboard/client/' . $clientName['id']) . '" >'
                            . $clientName['name'] . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'recruiter_status',
                    function ($candidate) {
                        $response = '---';
                        $status = $candidate->users()->wherePivot('candidate_id',
                            $candidate->id)->pluck('status')->implode('<br>');
                        if ($status) {
                            switch ($status) {
                                case $status == 'accept_candidate':
                                    $response = "<span class='text-success'>" . trans('app.accept_candidate') . "</span>";
                                    break;
                                case $status == 'reject_candidate':
                                    $response = "<span class='text-danger'>" . trans('app.reject_candidate') . "</span>";
                                    break;
                                case $status == 'offer_vacancies':
                                    $response = "<span class='text-warning'>" . trans('app.offer_vacancies') . "</span>";
                                    break;
                                case $status == 'final_interview':
                                    $response = "<span class='text-info'>" . trans('app.final_interview') . "</span>";
                                    break;
                                case $status == 'rehabilitation_candidate':
                                    $response = "<span class='text-info'>" . trans('app.rehabilitation_candidate') . "</span>";
                                    break;
                                case $status == 'telephone_interview':
                                    $response = "<span class='text-info'>" . trans('app.telephone_interview') . "</span>";
                                    break;
                                case $status == 'preselection':
                                    $response = "<span class='text-info'>" . trans('app.preselection') . "</span>";
                            }
                        }
                        return $response;
                    }
                )
                ->editColumn(
                    'client_status',
                    function ($candidate) {
                        $response = '---';
                        $status = $candidate->clients()->wherePivot('candidate_id',
                            $candidate->id)->pluck('status')->implode('<br>');
                        if ($status) {
                            switch ($status) {
                                case $status == 'accept_candidate':
                                    $response = "<span class='text-success'>" . trans('app.accept_candidate') . "</span>";
                                    break;
                                case $status == 'reject_candidate':
                                    $response = "<span class='text-danger'>" . trans('app.reject_candidate') . "</span>";
                                    break;
                                case $status == 'offer_vacancies':
                                    $response = "<span class='text-warning'>" . trans('app.offer_vacancies') . "</span>";
                                    break;
                                case $status == 'final_interview':
                                    $response = "<span class='text-info'>" . trans('app.final_interview') . "</span>";
                            }
                        }
                        return $response;
                    }
                )
                ->addColumn(
                    'posted_by',
                    function ($candidate) {
                        $userName = $candidate->users->last();
                        $response = '<a  href="' . url('dashboard/user/' . $userName['id']) . '" >'
                            . $userName['name'] . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'created_at',
                    function ($candidate) {
                        return $candidate->created_at->format('Y-m-d H:i');
                    }
                )
                ->addColumn(
                    'action',
                    function ($candidate) {
                        $html = '<a href="#' . $candidate->id . '" class="btn-share"> <button class="btn btn-default btn-xs">
                                                <i class="fa fa-envelope-o  margin-right-5"></i>' . trans('app.share') . '</button></a>';
                        $html .= '<a href="/dashboard/candidate/' . $candidate->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $candidate->id . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('id', '{{$id}}')
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     *
     */
    public function create()
    {
        return view('web.dashboard.candidate.add_candidate')->with(['user' => $this->user]);
    }
    /**
     * Store new Candidate
     * @param CandidateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CandidateRequest $request)
    {
        $values = $request->except(['_token', 'area']);
        $collection = collect($values)->only(['client_id', 'vacant_id', 'notes', 'status']);
        if ($request->has('required_documents')) {
            $collection = $collection->merge(['required_documents' => serialize($request->input('required_documents'))]);
        }
        if ($request->has('offer_vacancies')) {
            $collection = $collection->merge(['offer_vacancies' => serialize($request->input('offer_vacancies'))]);
        }
        $attributes = array_filter($collection->all());
        $values = $this->getValuesFromRequest($request);
        $values['ref_num'] = $this->generateReferenceID('App\Candidate', 'ref_num');
        $client = \App\Client::find($attributes['client_id']);
        $candidate = New Candidate();
        $candidate->fill($values);
        $employmentBuilder = new EmploymentBuilder($this->user, $client, $candidate, $values, $attributes);
        if ($employmentBuilder->run()) {
            return redirect('dashboard/candidate')->with([
                'message' => $employmentBuilder->message,
                'alert-class' => "alert-" . $employmentBuilder->alertClass
            ]);
        }
        return redirect()->back()->withErrors([
            'message' => $employmentBuilder->message,
            'alert-class' => "alert-" . $employmentBuilder->alertClass
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     */
    public function show($id)
    {
        $seekerAccount = false;
        $seeker = new Seeker();
        $seekerExperience = array();
        $seekerVacantApplied = array();
        $candidateOfferVacancies = array();
        $candidateRequiredDocuments = array();
        $candidate = Candidate::find($id);
        /* if not found */
        if (!$candidate) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        if ($candidate->seeker_id) {
            $seeker = Seeker::find($candidate->seeker_id);
            $seekerExperience = $this->getExperience($seeker, 'seeker_id');
            $seekerVacantApplied = $seeker->vacantApplied()->paginate(3);
            $seekerAccount = true;
        }
        $city = City::find($candidate->city_id);
        $area = $city->area()->first();
        if ($candidate->offer_vacancies) {
            $candidateOfferVacancies = Vacant::find(unserialize($candidate->offer_vacancies));
        }
        if ($candidate->required_documents) {
            $candidateRequiredDocuments = unserialize($candidate->required_documents);
        }
        return view('web.dashboard.candidate.show_candidate')->with([
            'user' => $this->user,
            'seeker' => $seeker,
            'seekerAccount' => $seekerAccount,
            'candidate' => $candidate,
            'candidateArea' => $area->name,
            'candidateCity' => $city->name,
            'seekerExperience' => $seekerExperience,
            'seekerVacantApplied' => $seekerVacantApplied,
            'candidateOfferVacancies' => $candidateOfferVacancies,
            'candidateRequiredDocuments' => $candidateRequiredDocuments,
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     */
    public function edit($id)
    {
        $candidate = Candidate::find($id);
        /* if not found */
        if (!$candidate) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $this->authorizeForUser($this->user, 'updateCandidate', $candidate);
        $city = City::find($candidate->city_id);
        $area = $city->area()->first();
        $candidatables = $this->getCandidatables($this->user, $candidate->id);
        return view('web.dashboard.candidate.edit_candidate')
            ->with([
                'candidate' => $candidate,
                'candidateArea' => $area->ID,
                'clients' => \App\Client::all(),
                'candidatables' => $candidatables
            ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  CandidateRequest $request
     * @param  int $id
     */
    public function update(CandidateRequest $request, $id)
    {
        $candidate = Candidate::find($id);
        /* if not found */
        if (!$candidate) {
            return redirect('dashboard/candidate')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $values = $request->except(['_token', 'area']);
        $collection = collect($values)->only(['client_id', 'vacant_id', 'notes', 'status']);
        if ($request->has('required_documents')) {
            $collection = $collection->merge(['required_documents' => serialize($request->input('required_documents'))]);
        }
        if ($request->has('offer_vacancies')) {
            $collection = $collection->merge(['offer_vacancies' => serialize($request->input('offer_vacancies'))]);
        }
        $attributes = array_filter($collection->all());
        $values = $this->getValuesFromRequest($request);
        //$values['ref_num'] = $this->generateReferenceID('App\Candidate', 'ref_num');
        $client = \App\Client::find($attributes['client_id']);
        $employmentBuilder = new EmploymentBuilder($this->user, $client, $candidate, $values, $attributes);
        if ($employmentBuilder->run()) {
            return redirect('dashboard/candidate')->with([
                'message' => $employmentBuilder->message,
                'alert-class' => "alert-" . $employmentBuilder->alertClass
            ]);
        }
        return redirect()->back()->withErrors([
            'message' => $employmentBuilder->message,
            'alert-class' => "alert-" . $employmentBuilder->alertClass
        ]);
    }
    /**
     * Share Candidate Profile to given email
     *
     * @param  Candidate $id
     * @param  SendCandidateProfileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function shareProfile($id, SendCandidateProfileRequest $request)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return response()->json(['message' => 'This Candidate does not exist', 'code' => "404"], 404);
        }
        $recipient = $request->input("email");
        $this->dispatch(new GenerateCandidateProfile($this->user, $candidate, $recipient));
        return response()->json(['message' => trans('app.candidate_profile_sent_successfully'), 'code' => "200"], 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::find($id);
        $candidate->delete();
        return redirect('dashboard/candidate')->with([
            'message' => trans('app.deleted_successfully'),
            'alert-class' => 'alert-success'
        ]);
    }
    /**
     * @param $request
     * @return mixed
     */
    protected function getValuesFromRequest($request)
    {
        $values = $request->except([
            '_token',
            '_method',
            'vacant_id',
            'client_id',
            'area',
            'notes',
            'status',
            'required_documents',
            'offer_vacancies'
        ]);
        return array_filter(array_map('trim', $values));
    }
    /**
     * @param $city_id
     * @return array
     */
    public function getCandidateFullAddress($city_id)
    {
        $address = array();
        $city = City::find($city_id);
        if ($city) {
            $address ['city'] = $city->name;
            $address ['area'] = $city->area()->first()->name;
        }
        return $address;
    }
    /**
     * Get All Vacancies
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllVacancies(Request $request)
    {
        if ($request->ajax()) {
            $data = array();
            $vacants = Vacant::orderBy('ID', 'desc')
                ->where('active', '=', 1)
                ->where('employer_id', '=', 0)
                ->get();
            foreach ($vacants as $vacant) {
                $data [] = array("text" => $vacant->job_title, "id" => $vacant->ID);
            }
            return response()->json($data, 200);
        }
    }
    /** Get Candidatables Data
     * @param $user
     * @param $candidateId
     * @return mixed
     */
    public function getCandidatables($user, $candidateId)
    {
        $data = $user->candidates()->wherePivot('candidate_id',
            $candidateId)->get()->toArray();
        return $data[0]['pivot'];
    }
}