<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\EmployerRequest;
use Illuminate\Http\Request;
use Datatables;
use App\Employer;
use App\User;
use App\Plans\Models\Plan;
/**
 * Class EmployerController
 * @package App\Http\Controllers\Web\Dashboard
 */
class EmployerController extends Controller
{
    /**
     *  Authenticated user
     *
     * @var User|null
     */
    protected $user;
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * EmployerController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->middleware('admin.auth');
        $this->user = $auth::user();
        $this->auth = $auth::guard("admin");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->user->can('list-employers')) {
            return view('web.dashboard.employer.employer_list');
        }
        return redirect('dashboard')->with(['message' => trans('web.Unauthorized')]);
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function employer_list(Request $request)
    {
        $employer = Employer::select('*')->with('subscriptions');
        if ($request->ajax()) {
            return Datatables::of($employer)
                ->editColumn(
                    'company_name',
                    function ($employer) {
                        if ($employer->company_name) {
                            return $employer->company_name;
                        }
                        return "---";
                    }
                )
                ->editColumn(
                    'name',
                    function ($employer) {
                        $response = '<a  href="' . url(
                                'dashboard/employer/' . $employer->id
                            ) . '" >' . $employer->name . '</a>';
                        return $response;
                    }
                )
                ->editColumn(
                    'active',
                    function ($employer) {
                        if ($employer->active) {
                            return "<span class=\"text-success\">" . trans('app.active') . "</span>";
                        }
                        return "<span class=\"text-danger\">" . trans('app.inactive') . "</span>";
                    }
                )
                ->addColumn('plan', function ($employer) {
                    $response = '---';
                    $employerPlan = $employer->subscriptions->last();
                    if ($employerPlan['plan_id']) {
                        $response = \Lang::get('app.plans.' . $employerPlan['plan_id'] . '.name');
                    }
                    return $response;
                })
                ->addColumn(
                    'action',
                    function ($employer) {
                        $html = '<a href="/dashboard/employer/' . $employer->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        $html .= '<a href="#' . $employer->id . '" class="btn-delete"> <button class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o margin-right-5"></i>' . trans('app.delete') . '</button></a>';
                        return $html;
                    }
                )
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        if ($this->user->can('add-employers')) {
            $plans = $this->getPlans();
            $company_types = collect(
                [
                    ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                    ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                    ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                    ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
                ]);
            return view('web.dashboard.employer.add_employer')->with([
                'company_types' => $company_types,
                'plans' => $plans
            ]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * Create new Employer
     *
     * @param EmployerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmployerRequest $request)
    {
        $values = array_map('trim', $request->except(['_token', 'password_confirmation']));
        $values['password'] = bcrypt($request['password']);
        $values['active'] = (int)$request['active'];
        $planId = $request->input('plan');
        $plan = Plan::find($planId);
        $employer = Employer::create($values);
        if ($employer) {
            if ($plan) {
                $employer->newSubscription('sto', $plan)->create();
            }
            return redirect('dashboard/employer')
                ->with('message', trans('app.created_successfully'));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     */
    public function show($id)
    {
        $employer = Employer::find($id);
        return view('web.dashboard.employer.show_employer')
            ->with(['employer' => $employer]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     */
    public function edit($id)
    {
        if ($this->user->can('update-employers')) {
            $employer = Employer::find($id);
            if (!$employer) {
                return redirect('dashboard/employer')->with(['message' => trans('app.not_found')]);
            }
            /*
             * get all plans
             */
            $plans = $this->getPlans();
            /*
             * get current Employer Plan
             */
            $employerPlan = $this->getEmployerSubscriptions($employer);
            /*
            * get collection of companies types
            */
            $company_types = collect(
                [
                    ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                    ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                    ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                    ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
                ]);
            return view('web.dashboard.employer.edit_employer')
                ->with(['employer' => $employer])
                ->with(['plans' => $plans])
                ->with(['employerPlan' => $employerPlan])
                ->with(['company_types' => $company_types]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * @param EmployerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EmployerRequest $request, $id)
    {
        if ($this->user->can('update-employers')) {
            $employer = Employer::find($id);
            $values = array_map('trim', $request->except(['_token', '_method', 'plan']));
            $values['active'] = (int)$request['active'];
            if (empty($request['password'])) {
                unset($values['password']);
            } else {
                $values['password'] = bcrypt($request['password']);
            }
            foreach ($values as $field => $value) {
                $employer->$field = $value;
            }
            if ($employer->save()) {
                /*
                 *check the plan subscriptions
                */
                if ($request->has('plan')) {
                    $this->updateEmployerSubscriptions($employer, $request->input('plan'));
                }
                return redirect('dashboard/employer')->with(['message' => 'Updated !!!']);
            }
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-employers')) {
            $employer = Employer::find($id);
            $employer->delete();
            return redirect('dashboard/employer')->with(['message' => trans('app.deleted_successfully')]);
        }
        return redirect('/')->with(['message' => trans('app.Unauthorized')]);
    }
    /** get all available plans
     * @return mixed
     */
    protected function getPlans()
    {
        $plans = Plan::all();
        $data = $plans->each(function ($plan) {
            return $plan->name = \Lang::get('app.plans.' . $plan->id . '.name');
        })->all();
        return $data;
    }
    /**
     * get current employer plan
     * @param Employer $employer
     * @return mixed | null
     */
    protected function getEmployerSubscriptions(Employer $employer)
    {
        $employerPlan = $employer->subscriptions()
            ->where('employer_id', $employer->id)
            ->get()->last();
        if ($employerPlan) {
            return $employerPlan;
        }
        return null;
    }
    /**
     * @param Employer $employer
     * @param $newPlanId
     */
    protected function updateEmployerSubscriptions(Employer $employer, $newPlanId)
    {
        /*
         * check if the employer current plan
         */
        $currentPlan = $this->getEmployerSubscriptions($employer);
        /*
         * check if this user want to change employer plan and not the current plan
         * no need for new subscription
         * if he is not subscribed
         * make new subscription and un subscribe him from old one
         */
        if ($employer->subscribed('sto', $currentPlan['plan_id'])) {
            $oldPlan = Plan::find($currentPlan['plan_id']);
            $employer->subscription('sto', $oldPlan)->cancel();
        }
        $newPlan = Plan::find($newPlanId);
        $employer->newSubscription('sto', $newPlan)->create();
    }
}
