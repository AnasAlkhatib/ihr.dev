<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Seeker;
use App\Area;
use App\City;
use Illuminate\Http\Request;
use App\Util\Traits\ExperienceTrait;
use App\Http\Controllers\Controller;
use App\Util\Traits\SearchDropDownTrait;
use App\Util\Traits\SeekerSearchableTrait;
use App\Util\Traits\EmployedTrait;

/**
 * Class SeekerController
 * @package App\Http\Controllers\Web\Dashboard
 */
class SeekerController extends Controller
{
    use ExperienceTrait;
    use SearchDropDownTrait;
    use SeekerSearchableTrait;
    use EmployedTrait;
    /**
     * @var $auth
     */
    protected $auth;
    /**
     * @var $user
     */
    protected $user;

    /**
     * SeekerController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard('admin');
        $this->user = $this->auth->user();
        $this->middleware('admin.auth');
    }

    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        $employmentStatus = '';
        $seekers = Seeker::orderBy('seekers.ID', 'DESC');
        /* if search has gender */
        if ($request->input('gender')) {
            $seekers->where('gender', '=', $request->input('gender'));
        }
        /* if search has area */
        if ($request->input('area')) {
            $cities = Area::find($request->input('area'))->city;
            foreach ($cities as $city) {
                $array[] = $city->ID;
            }
            $seekers = $seekers->wherein('city_id', $array);
        }
        /* if search has city */
        if ($request->input('city')) {
            $seekers = $seekers->where('city_id', '=', $request->input('city'));
        }
        /* if search has nationality */
        if ($request->input('nationality')) {
            $saudiOptions = $this->getSaudiOptions();
            if ($request->input('nationality') == '1') {
                $seekers = $seekers->wherein('nationality', $saudiOptions);
            } elseif ($request->input('nationality') == '2') {
                $seekers = $seekers->whereNotIn('nationality', $saudiOptions);
            }
        }
        /* if search has age */
        if ($request->input('age')) {
            $seekers = $this->seekerByAge($seekers, $request->input('age'));
        }
        /* if search has edu_level */
        if ($request->input('edu_level')) {
            $seekers = $this->seekerByEduLevel($seekers, $request->input('edu_level'));
        }
        /* if search has exp_years */
        if ($request->input('exp_years')) {
            $seekers = $this->seekerByExpYears($seekers, $request->input('exp_years'));
        }
        /* if search has company_industry */
        if ($request->input('company_industry')) {
            $seekers = $this->seekerByCompanyIndustry($seekers, $request->input('company_industry'));
        }
        /* if search has employment_status */
        if ($request->input('employment_status')) {
            $seekers = $this->seekerByEmploymentStatus($seekers, $request->input('employment_status'));
        }
        $seekers = $seekers->paginate(6);
        /* loop throw each object and modify some properties */
        $seekers->each(function ($seeker) use ($seekers, $employmentStatus) {
            $seeker->gender = $this->getSeekerGender($seeker->gender);
            if ($seeker->city_id) {
                $seeker->city = $this->get_city_name($seeker->city_id);
                $seeker->area = $this->get_area_name($seeker->city_id);
            }
            $seeker->age = $this->get_seeker_age($seeker->birth_date);
            $seeker->edu = $this->get_seeker_education($seeker);
            $seeker->exp = $this->getExperience($seeker, 'seeker_id');
            $seeker->is_employed = $this->isEmployed($seeker);
            /* seeker picture  */
            $pic = public_path() . '/uploads/seeker/photo/' . $seeker->pic_file_name;
            if (\File::exists($pic) && $seeker->pic_file_name) {
                $seeker->pic_url = url("/uploads/seeker/photo/" . $seeker->pic_file_name);
            } else {
                $seeker->pic_url = url("assets/img/user.jpg");
            }
        });
        if ($request->ajax()) {
            return response()->json(
                view('web.dashboard.seeker.ajax-seekers')->with(compact('seekers'))->render()
            );
        }
        /*
         * Drop Down Data For Search Filter
         */
        $dropDownData = $this->buildSearchDropDown();
        return view('web.dashboard.seeker.index')
            ->with('dropDownData', $dropDownData)
            ->with('seekers', $seekers);
    }

    /**
     * calculate seeker age
     *
     * @param $birthday
     * @return int
     */
    public function get_seeker_age($birthday)
    {
        return date_create($birthday)->diff(date_create('today'))->y;
    }

    /**
     * get seeker last education
     * @todo display seeker education by order if has many
     * @param $seeker
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function get_seeker_education($seeker)
    {
        $education = $seeker->education->all();
        if ($education) {
            /* here shown the last record based on user input */
            return $this->transEducation($seeker->education->last()->edu_level);
        } else {
            return trans('app.not_found');
        }
    }

    /**
     * get City name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_city_name($city_id)
    {
        $city = City::find($city_id);
        if (\App::getLocale() == 'en') {
            return $city->name_en;
        }
        return $city->name;
    }

    /**
     * get Area name
     *
     * @param $city_id
     * @return mixed
     */
    public function get_area_name($city_id)
    {
        $city = City::find($city_id);
        if (\App::getLocale() == 'en') {
            return $city->area()->first()->name_en;
        }
        return $city->area()->first()->name;
    }

    /**
     * @param $education
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function transEducation($education)
    {
        switch ($education) {
            case $education == 'ثانوية عامة':
                $education = trans('app.high_school');
                break;
            case $education == 'دبلوم':
                $education = trans('app.diploma');
                break;
            case $education == 'بكالوريوس':
                $education = trans('app.bachelor');
                break;
            case $education == 'دبلوم عالي':
                $education = trans('app.high_diploma');
                break;
            case $education == 'ماجستير':
                $education = trans('app.master');
                break;
            case $education == 'دكتوراه':
                $education = trans('app.doctorate');
                break;
            default:
                $education = '';
        }
        return $education;
    }

    /**
     * get seeker by id
     * @param $id
     * @param request $request
     */
    public function get($id, Request $request)
    {
        $city = '';
        $area = '';
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return redirect('dashboard/seeker')->with([
                'message' => trans('app.not_found'),
                'alert-class' => 'alert-warning'
            ]);
        }
        $seekerVacantApplied = $seeker->vacantApplied()->paginate(3);
        $seekerExperience = $this->getExperience($seeker, 'seeker_id');
        if ($request->ajax()) {
            return response()->json(
                view('web.dashboard.candidate.candidate_vacant_applied')->with(compact('seekerVacantApplied'))->render()
            );
        }
        if ($seeker->city_id) {
            $cityObject = City::find($seeker->city_id);
            $areaObject = $cityObject->area()->first();
            if (\App::getLocale() == 'en') {
                $area = $areaObject->name_en;
                $city = $cityObject->name_en;
            } else {
                $area = $areaObject->name;
                $city = $cityObject->name;
            }
        }
        return view('web.dashboard.seeker.show_seeker')->with([
            'user' => $this->user,
            'seeker' => $seeker,
            'seekerArea' => $area,
            'seekerCity' => $city,
            'seekerVacantApplied' => $seekerVacantApplied,
            'seekerExperience' => $seekerExperience,
        ]);
    }

    /** Activate Seeker Account
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activateAccount($id)
    {
        /* only Super Admin can activate  */
        if ($this->user->hasRole('SuperAdmin')) {
            $seeker = Seeker::find($id);
            /* if seeker not found */
            if (!$seeker) {
                return redirect()->back()->with([
                    'message' => trans('app.not_found'),
                    'alert-class' => 'alert-warning'
                ]);
            }
            /* if seeker account already active */
            if ($seeker->active == 1) {
                return redirect()->back()->with([
                    'message' => trans('app.account_already_active'),
                    'alert-class' => 'alert-info'
                ]);
            }
            $seeker->active = 1;
            $seeker->activation_code = '';
            $seeker->save();
            return redirect()->back()->with([
                'message' => trans('app.activated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }

    /**
     * Deactivate Seeker Account
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivateAccount($id)
    {
        /* only Super Admin can deactivate  */
        if ($this->user->hasRole('SuperAdmin')) {
            $seeker = Seeker::find($id);
            /* if seeker not found */
            if (!$seeker) {
                return redirect()->back()->with([
                    'message' => trans('app.not_found'),
                    'alert-class' => 'alert-warning'
                ]);
            }
            $seeker->active = 0;
            $seeker->save();
            return redirect()->back()->with([
                'message' => trans('app.deactivated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }

    /**
     * add seeker to favorite
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function favoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        $user = $this->user;
        $user->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unFavoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        $user = $this->user;
        $user->favoriteSeekers()->where('seeker_id', '=', $seeker->ID)->delete();
        return back();
    }

    /**
     *
     */
    public function getFavoriteSeekers()
    {
        $user = $this->user;
        $seekerIds = $user->favoriteSeekers->pluck('seeker_id');
        $seekers = Seeker::whereIn('ID', $seekerIds)
            ->paginate(6);
        $seekers->each(
            function ($seeker) {
                $seeker->gender = $this->getSeekerGender($seeker->gender);
                if ($seeker->city_id) {
                    $seeker->setAttribute('city', $this->get_city_name($seeker->city_id));
                    $seeker->setAttribute('area', $this->get_area_name($seeker->city_id));
                }
                $seeker->setAttribute('age', $this->get_seeker_age($seeker->birth_date));
                $seeker->setAttribute('edu', $this->get_seeker_education($seeker));
                $seeker->setAttribute('exp', $this->getExperience($seeker, 'seeker_id'));
                $seeker->is_employed = $this->isEmployed($seeker);
                $pic = public_path() . '/uploads/seeker/photo/' . $seeker->pic_file_name;
                if (\File::exists($pic) && $seeker->pic_file_name) {
                    $seeker->setAttribute('pic_url', url("/uploads/seeker/photo/" . $seeker->pic_file_name));
                } else {
                    $seeker->setAttribute('pic_url', url("assets/img/user.jpg"));
                }
            }
        );
        return view('web.dashboard.seeker.favorite_seekers')->with(['seekers' => $seekers]);
    }

    /**
     * @param $gender
     * @return string
     */
    private function getSeekerGender($gender)
    {
        if (\App::getLocale() == 'ar') {
            if ($gender == 'male') {
                $gender = 'ذكر';
            } else {
                $gender = 'أنثى';
            }
        }
        return ucwords($gender);
    }

    /**
     * get all available saudi options
     * @return array
     */
    private function getSaudiOptions()
    {
        return ['سعودي', 'السعودية', 'المملكة العربية السعودية', 'Saudi Arabian', 'Saudi Arabia'];
    }

    /** Delete seeker account
     * @toDo delete all relation
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        /* only Super Admin can Delete Seeker Account */
        if ($this->user->hasRole('SuperAdmin')) {
            $seeker = Seeker::find($id);
            /* if seeker not found */
            if (!$seeker) {
                return redirect()->back()->with([
                    'message' => trans('app.not_found'),
                    'alert-class' => 'alert-warning'
                ]);
            }
            $seeker->delete();
            return redirect()->back()->with([
                'message' => trans('app.deleted_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
}
