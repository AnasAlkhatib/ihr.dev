<?php
namespace App\Http\Controllers\Web\Dashboard\Plan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Web\UpdatePlanRequest;
use App\Plans\Models\Plan;
use Datatables;
/**
 * Class PlanController
 * @package App\Http\Controllers\Web\Dashboard\Plan
 */
class PlanController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $user;
    /**
     * PlanController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard('admin');
        $this->user = $this->auth->user();
        $this->middleware('admin.auth');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('web.dashboard.plan.plan_list');
    }
    /**
     * @param Request $request
     */
    public function plan_list(Request $request)
    {
        $plan = Plan::query()->orderBy('sort_order', 'ASC');
        if ($request->ajax()) {
            return Datatables::of($plan)
                ->editColumn('name', function ($plan) {
                    $response = trans('app.plans.' . $plan->id . '.name');
                    return $response;
                })
                ->editColumn('interval', function ($plan) {
                    $response = trans_choice('app.interval_description.' . $plan->interval, $plan->interval_count);
                    return $response;
                })
                ->addColumn(
                    'action',
                    function ($plan) {
                        $html = '<a href="plan/' . $plan->id . '/edit"> <button class="btn btn-default btn-xs">
                        <i class="fa fa-edit margin-right-5"></i>' . trans('app.edit') . '</button></a>';
                        return $html;
                    }
                )
                ->editColumn('id', '{{$id}}')
                ->make(true);
        }
    }
    /** Edit plan
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        if ($this->user->hasRole('SuperAdmin')) {
            $plan = Plan::find($id);
            $intervals = $this->intervals();
            $plansCount = Plan::all()->count();
            return view('web.dashboard.plan.edit_plan')
                ->with(['plansCount' => $plansCount])
                ->with(['intervals' => $intervals])
                ->with(['plan' => $plan]);
        }
        return redirect('/dashboard/plan')->with([
            'message' => trans('app.Unauthorized'),
            'alert-class' => 'alert-danger'
        ]);
    }
    /** update plan
     * @param UpdatePlanRequest $request
     * @param $id
     */
    public function update(UpdatePlanRequest $request, $id)
    {
        $plan = Plan::find($id);
        $values = array_map('trim', $request->except(['_token', '_method']));
        foreach ($values as $field => $value) {
            $plan->{$field} = $value;
        }
        if ($plan->save()) {
            return redirect('dashboard/plan')->with([
                'message' => trans('app.updated_successfully'),
                'alert-class' => 'alert-success'
            ]);
        }
    }
    /**
     * Intervals array for dropdown menu in the edit page
     * @return \Illuminate\Support\Collection
     */
    public function intervals()
    {
        $interval = collect(
            [
                ['name' => trans('app.daily'), 'value' => 'day'],
                ['name' => trans('app.weekly'), 'value' => 'week'],
                ['name' => trans('app.monthly'), 'value' => 'month'],
                ['name' => trans('app.yearly'), 'value' => 'year'],
            ]
        );
        return $interval;
    }
}
