<?php
namespace App\Http\Controllers\Web\Dashboard;

use App\Http\Controllers\Controller;
use App\Area;
/**
 * Class AreaController
 *
 * @package App\Http\Controllers\Web\Dashboard
 */
class AreaController extends Controller
{
    /**
     * AreaController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = [];
        $Areas = Area::all();
        foreach ($Areas as $area) {
            if (\App::getLocale() == 'en') {
                $data [] = array("text" => $area->name_en, "id" => $area->ID);
            } else {
                $data [] = array("text" => $area->name, "id" => $area->ID);
            }
        }
        return response()->json($data, 200);
    }
}
