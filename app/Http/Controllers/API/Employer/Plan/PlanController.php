<?php
namespace App\Http\Controllers\API\Employer\Plan;

use App\Http\Controllers\Controller;
use App\Plans\Models\Plan;
use Illuminate\Support\Facades\Lang;

class PlanController extends Controller
{
    protected $auth;
    protected $employer;

    public function __construct()
    {
        $this->auth = \Auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        $data = \App\Plans\Models\Plan::all();
        $items = ['data' => []];
        foreach ($data as $key) {
            $response = $this->_transform($key);
            array_push($items['data'], $response);
            unset($response);
        }
        return response()
            ->json($items);
    }

    public function subscribe(Plan $plan)
    {
        try{
            $this->employer->newSubscription('sto', $plan)->create();
            return response()->json(
                ['data' =>
                    [
                        'user' => $this->employer ,
                        'plan' => $plan->name,
                    ]
                ]
            );
        }catch (\Exception $e){
            return response()->json(
               ['error' => $e->getMessage()]
            );
        }

    }

    public function check(Plan $plan)
    {
        if($this->employer->subscribed('sto', $plan->id)){
            return response()->json(
                [
                    'data' =>
                        [
                            'plan' => $this->_transform($plan),
                            'status' => $plan->subscriptions()->orderBy('created_at', 'desc')->first()->status,
                        ]
                ]
            );
        }else{
            return response()->json(
                [
                    'data' =>
                        [
                            'plan' => $this->_transform($plan),
                            'status' => $plan->subscriptions()->orderBy('created_at', 'desc')->first()->status,
                        ]
                ]
            );
        }
    }

    public function unsubscribe(Plan $plan)
    {
        try{
            $this->employer->subscription('sto')->cancel();
        }catch (\Exception $e){
            return response()->json(
                ['error' => $e->getMessage()]
            );
        }
    }

    public function show(Plan $plan)
    {
        return response()->json(['data' => $this->_transform($plan)]);
    }

    private function _transform(Plan $plan)
    {
        return [
            'plan_id' => $plan->id,
            'name' => Lang::get('app.plans.' . $plan->id . '.name'),
            'description' => Lang::get('app.plans.' . $plan->id . '.description'),
            'price' => $plan->price,
            'interval' => $plan->interval_count . ' ' . Lang::get('app.' . $plan->interval),
            'features' => $plan
                ->features()
                ->orderBy('sort_order')
                ->get(['code', 'value']),
            'is_subscribed' => ($this->employer->subscribed('sto', $plan->id)) ? 'true' : 'false',
            'status' => ($this->employer->subscribed('sto', $plan->id)) ?
                $plan->subscriptions()->orderBy('created_at', 'desc')->first()->status :
                'false'
        ];
    }
}