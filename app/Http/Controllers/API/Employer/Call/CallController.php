<?php
namespace App\Http\Controllers\API\Employer\Call;

use App\Http\Controllers\Controller;


/**
 * Class CallController
 * @package App\Http\Controllers\API\Employer\Call
 */
class CallController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;

    /**
     * CallController constructor.
     * //        $callSettings = [];
     * //        $employer = $this->employer;
     */
    public function __construct()
    {
        $this->auth = \Auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }

    public function run()
    {
        // set call Settings + data
        //$seeker is on line
        // get device_token
    }
}