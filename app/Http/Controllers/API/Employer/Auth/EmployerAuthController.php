<?php
namespace App\Http\Controllers\API\Employer\Auth;

use App\Employer;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\API\Employer\EmployerLoginRequest;
use App\Http\Requests\API\Employer\EmployerRegisterRequest;
use App\Jobs\EmployerWasRegistered;

/**
 * Class EmployerAuthController
 * @package App\Http\Controllers\API\Employer\Auth
 */
class EmployerAuthController extends Controller
{
    /**
     * @var $auth  Auth facade
     */
    protected $auth;

    /**
     * EmployerAuthController constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth::guard("api-employer");
        $this->middleware('jwt.auth', ['except' => ['login', 'register']]);
    }

    /**
     * @param EmployerLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(EmployerLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = $this->auth->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if ($this->auth->user()->active) {
            return response()->json(compact('token'));
        }
        return response()->json(['error' => trans('app.not_activated'), 'code' => 400], 400);
    }

    /**
     *
     * get auth user from provided token
     */
    public function getAuthenticatedUser()
    {
        $employer = $this->auth->user();
        $employer->company_type = trans('app.' . $employer->company_type);
        $employer->pic_file_name = $this->getEmployerPicUrl($employer);
        $employer->plan = $this->getEmployerPlan($employer);
        $employer->is_online = $employer->isOnline();
        return response()->json(['data' => $employer, "code" => "200"], 200);
    }

    /**
     * @param EmployerRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(EmployerRegisterRequest $request)
    {
        $values = $request->all();
        $values['password'] = trim(bcrypt($request['password']));
        $values['email'] = strtolower($request['email']);
        $result = array_map('trim', $values);
        $employer = Employer::create($result);
        if ($employer) {
            $plan = \App\Plans\Models\Plan::first();
            $employerPlan = \Lang::get('app.plans.' . $plan->id . '.name');
            // fire the event
            $user = \App\User::Where('email', '=', env('EMPLOYER_ACTIVATE_EMAIL', 'ahmad@sto.com.sa'))->first();
            $this->dispatch(new EmployerWasRegistered($user, $employer, $employerPlan));
        }
        // try to login
        $credentials = [
            'email' => strtolower($request['email']),
            'password' => $request['password'],
        ];
        $token = $this->auth->attempt($credentials, true);
        return response()->json(['token' => $token, "code" => "200"], 200);
    }

    /**
     * refresh token
     * Invalidate the token, so user cannot use it anymore
     * They have to get a new token
     */
    public function refresh()
    {
        $newToken = $this->auth->refresh();
        return response()->json(['newToken' => $newToken, "code" => "200"], 200);
    }

    /**
     * set employer online
     * @return \Illuminate\Http\JsonResponse
     */
    public function online()
    {
        $employer = $this->auth->user();
        $expiresAt = Carbon::now()->addMinutes(5);
        \Cache::put('employer-is-online-' . $employer->id, true, $expiresAt);
        return response()->json(['is_online' => $employer->isOnline(), "code" => "200"], 200);
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to get a new token
     */
    public function logout()
    {
        $this->auth->logout();
        return response()->json(['message' => "Successfully logged out", "code" => "200"], 200);
    }

    /** get employer current plan
     * @param $employer
     * @return array|null|string
     */
    protected function getEmployerPlan($employer)
    {
        $employerPlan = $employer->subscriptions()
            ->where('employer_id', $employer->id)
            ->get()->last();
        if ($employerPlan) {
            return \Lang::get('app.plans.' . $employerPlan['plan_id'] . '.name');
        }
        return null;
    }

    /**
     * get employer profile pic url
     * @param $employer
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    protected function getEmployerPicUrl($employer)
    {
        $url = '';
        $photoPath = '/uploads/employer/photo/';
        $photoUrl = public_path() . $photoPath . $employer->pic_file_name;
        if (\File::exists($photoUrl) && $employer->pic_file_name) {
            $url = url($photoPath . $employer->pic_file_name);
        }
        return $url;
    }
}
