<?php
namespace App\Http\Controllers\API\Employer\Seeker;

use App\Http\Controllers\Controller;
use App\Seeker;
/**
 * Class EmployerSeekerController
 * @package App\Http\Controllers\API\Employer\Vacant
 */
class EmployerSeekerController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * EmployerFavoriteSeekerController constructor.
     */
    public function __construct()
    {
        $this->auth = \Auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }
    /**
     * get FavoriteSeekers list
     * @return string
     */
    public function getFavoriteSeekers()
    {
        $employer = $this->employer;
        $seekerIds = $employer->favoriteSeekers->pluck('seeker_id');
        $seekers = Seeker::whereIn('ID', $seekerIds)->paginate(10);
        return response()->json([
            'count' => $seekers->count(),
            'total' => $seekers->total(),
            'next' => $seekers->nextPageUrl() ?: '',
            'previous' => $seekers->previousPageUrl() ?: '',
            'code' => "200",
            'data' => $seekers->items()
        ], 200);
    }
    /**
     * add seeker to favorite
     * @param $id seekerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function favoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        /*
         * Check if the seeker in applied list
         */
        $employer = $this->employer;
        $checkSeekerApplied = $this->CheckSeekerApplied($employer, $id);
        if ($checkSeekerApplied) {
            $employer->favoriteSeekers()->create(['seeker_id' => $seeker->ID]);
            return response()->json(['message' => "The Job Seeker Correctly added to Favorite List", 'code' => "201"],
                201);
        }
        return response()->json(['message' => "Seeker Doesn't Applied For  Employer vacancies", 'code' => "422"], 422);
    }
    /**
     * Remove seeker from employer favorite
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function unFavoriteSeeker($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $employer = $this->employer;
        $employer->favoriteSeekers()->where('seeker_id', '=', $seeker->ID)->delete();
        return response()->json(['message' => "The Job Seeker Correctly removed from Favorite List", 'code' => "200"],
            200);
    }
    /**
     * Check if the seeker in applied list
     * @param $employer
     * @param $id
     * @return bool
     */
    protected function CheckSeekerApplied($employer, $id)
    {
        $data = $employer->vacancies()->whereHas('Applied_seeker', function ($query) use ($id) {
            $query->where('seeker_id', $id);
        })->get();
        if ($data->count() == 0) {
            return false;
        }
        return true;
    }
}
