<?php
namespace App\Http\Controllers\API\Employer\Applied;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vacant;
use App\Area;

/**
 * Class EmployerAppliedVacantController
 * @package App\Http\Controllers\API\Employer\Applied
 */
class EmployerAppliedVacantController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;

    /**
     * EmployerVacantController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $id)
    {
        $vacant = Vacant::find($id);
        if (!$vacant) {
            return response()->json(['message' => 'This vacant does not exist', 'code' => "404"], 404);
        }
        /* check if the employer owns this vacant */
        if ($vacant->employer_id === $this->employer->id) {
            /* find all Applied seekers */
            $seekers = $vacant->Applied_seeker();
            /*Gender filter */
            if ($request->input('gender')) {
                $seekers = $seekers->where('gender', '=', $request->input('gender'));
            }
            /* Area filter */
            if ($request->input('area')) {
                $cities = Area::find($request->input('area'))->city;
                foreach ($cities as $city) {
                    $array[] = $city->ID;
                }
                $seekers = $seekers->wherein('city_id', $array);
            }
            /* City filter */
            if ($request->input('city')) {
                $seekers = $seekers->where('city_id', '=', $request->input('city'));
            }
            /* Nationality filter */
            if ($request->input('nationality')) {
                $saudiOptions = $this->getSaudiOptions();
                if ($request->input('nationality') == '1') {
                    $seekers = $seekers->wherein('nationality', $saudiOptions);
                } elseif ($request->input('nationality') == '2') {
                    $seekers = $seekers->whereNotIn('nationality', $saudiOptions);
                }
            }
            $seekers = $seekers->paginate(6);
            $data = $seekers->each(function ($seeker) use ($id) {
                $this->imageLink($seeker);
                $seeker->is_favorited = ($seeker->isFavorited($this->employer)) ? 'true' : 'false';
                $seeker->is_nominated = ($this->checkCandidate($this->employer, $seeker->ID, $id)) ? 'true' : 'false';
                $seeker->candidate_id = $this->getCandidate($this->employer, $seeker->ID, $id);
            });
            return response()->json(["data" => $data, "code" => "200"], 200);
        }
        return response()->json(["message" => trans('app.Unauthorized'), "code" => "401"], 401);
    }

    /** helper function to transform image name to
     * full link
     * @param $seeker
     * @return mixed
     */
    protected function imageLink($seeker)
    {
        $photoPath = '/uploads/seeker/photo/';
        $photoUrl = public_path() . $photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            $seeker->pic_file_name = url($photoPath . $seeker->pic_file_name);
        }
        return $seeker;
    }

    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function checkCandidate($employer, $seekerId, $vacantId)
    {
        $isCandidate = false;
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            $isCandidate = true;
        }
        return $isCandidate;
    }

    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function getCandidate($employer, $seekerId, $vacantId)
    {
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            return $data[0]->pivot->id;
        }
        return 0;
    }

    /**
     * get all available saudi options
     * @return array
     */
    private function getSaudiOptions()
    {
        return ['سعودي', 'السعودية', 'المملكة العربية السعودية', 'Saudi Arabian', 'Saudi Arabia'];
    }
}

