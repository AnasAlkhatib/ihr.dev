<?php
namespace App\Http\Controllers\API\Employer\Applied;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vacant;


/**
 * Class GetAllAppliedApplicantsController
 * @package App\Http\Controllers\API\Employer\Applied
 */
class GetAllAppliedApplicantsController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/employer/allapplicants",
     *      summary="Get All Employer Applied Applicants",
     *      tags={"Employer Vacant"},
     *      description="Get All Employer Applied Applicants",
     *      produces={"application/json"},
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Ok.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Token Error",
     *     )
     * )
     */


    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;

    /**
     * EmployerVacantController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request)
    {

        $vacancies = Vacant::where('employer_id', '=', $this->employer->id)->get();
        // $applicants_count = 0;
        $applicants_list[] = '';
        foreach ($vacancies as $vacancy) {
            if ($vacancy->Applied_seeker()->count() > 0) {
                $applicants = $vacancy->Applied_seeker()->get();
                foreach ($applicants as $applicant) {
                    $applicant->pic_file_name = $this->imageLink($applicant);
                    $applicant->is_favorited = ($applicant->isFavorited($this->employer)) ? 'true' : 'false';
                    $applicant->is_nominated = ($this->checkCandidate($this->employer, $applicant->ID,
                        $applicant->pivot->vacant_id)) ? 'true' : 'false';
                    $applicant->is_online = $applicant->isOnline();
                    $applicant->candidate_id = $this->getCandidate($this->employer, $applicant->ID, $applicant->pivot->vacant_id);
                    array_push($applicants_list, $applicant);
                }
            }

        }
        array_shift($applicants_list);
        return response()->json(["applicants" => $applicants_list, "code" => "200"], 200);
    }

    /** helper function to transform image name to
     * full link
     * @param $seeker
     * @return mixed
     */
    protected function imageLink($seeker)
    {
        $photoPath = '/uploads/seeker/photo/';
        $photoUrl = public_path() . $photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            $seeker->pic_file_name = url($photoPath . $seeker->pic_file_name);
        }
        return $seeker;
    }

    /**
     * get all available saudi options
     * @return array
     */
    private function getSaudiOptions()
    {
        return ['سعودي', 'السعودية', 'المملكة العربية السعودية', 'Saudi Arabian', 'Saudi Arabia'];
    }
}

