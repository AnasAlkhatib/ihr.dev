<?php
namespace App\Http\Controllers\API\Employer\Candidate;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Employer\EmployerCreateCandidateRequest as CreateRequest;
use App\Http\Requests\API\Employer\EmployerUpdateCandidateRequest as UpdateRequest;
/**
 * Class EmployerCandidateController
 * @package App\Http\Controllers\API\Employer\Candidate
 */
class EmployerCandidateController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * EmployerCandidateController constructor.
     */
    public function __construct()
    {
        $this->auth = \Auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }
    /**
     * Show all Candidates
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $seekers = $this->employer->seekers()
            ->paginate(10);
        return response()->json([
            'count' => $seekers->count(),
            'total' => $seekers->total(),
            'next' => $seekers->nextPageUrl() ?: '',
            'previous' => $seekers->previousPageUrl() ?: '',
            'code' => "200",
            'data' => $this->transform($seekers->items())
        ], 200);
    }
    /**
     * Get candidate by id
     * @param $id
     */
    public function show($id)
    {
        $candidate = $this->employer->seekers()->wherePivot('id', $id)->get();
        if ($candidate->count() == 0) {
            return response()->json(["message" => "This Candidate does not exist", "code" => "404"], 404);
        }
        return response()->json(["data" => $this->transform($candidate), "code" => '200'], 200);
    }
    /**
     *  Create new candidate
     * @param $request
     */
    public function store(CreateRequest $request)
    {
        $seeker_id = (int)$request->input('seeker_id');
        $vacant_id = (int)$request->input('vacant_id');
        $vacant = \App\Vacant::find($vacant_id);
        /*
        *  Check if the employer owned this vacancy
        */
        if ($this->employer->id != $vacant->employer_id) {
            return response()->json(['message' => 'This vacancy not owned by this employer', 'code' => "422"], 422);
        }
        /*
         * Check if the Seeker  Applied For Employer vacancies
         */
        $checkSeekerApplied = $this->CheckSeekerApplied($this->employer, $seeker_id);
        if (!$checkSeekerApplied) {
            return response()->json([
                'message' => "This Seeker Doesn't Applied for any employer vacancy",
                'code' => "422"
            ], 422);
        }
        /*
         * Prevent duplicate
         */
        $isCandidate = $this->checkCandidate($this->employer, $seeker_id, $vacant_id);
        if ($isCandidate) {
            return response()->json(['message' => "Seeker Already nominated for this vacant ", 'code' => "422"], 422);
        }
        $collection = collect($request->except(['token', 'seeker_id', '_method', 'locale']));
        if ($request->has('required_documents')) {
            $required_documents = $this->commaToArray($request->input('required_documents'));
            $collection = $collection->merge(['required_documents' => serialize($required_documents)]);
        }
        if ($request->has('offer_vacancies')) {
            $offer_vacancies = $this->commaToArray($request->input('offer_vacancies'));
            $collection = $collection->merge(['offer_vacancies' => serialize($offer_vacancies)]);
        }
        $attributes = array_filter($collection->all());
        $this->employer->seekers()->attach($seeker_id, $attributes);
        return response()->json(["message" => "Candidate Correctly added", "code" => "201"], 201);
    }
    /**
     * update   candidate
     * @param $request
     * @param $id
     */
    public function update(UpdateRequest $request, $id)
    {
        $seeker_id = (int)$request->input('seeker_id');
        $vacant_id = (int)$request->input('vacant_id');
        $vacant = \App\Vacant::find($vacant_id);
        /*
        *  Check if the employer owned this vacancy
        */
        if ($this->employer->id != $vacant->employer_id) {
            return response()->json(['message' => 'This vacancy not owned by this employer', 'code' => "422"], 422);
        }
        /*
         * Check if the Seeker  Applied For Employer vacancies
         */
        $checkSeekerApplied = $this->CheckSeekerApplied($this->employer, $seeker_id);
        if (!$checkSeekerApplied) {
            return response()->json([
                'message' => "This Seeker Doesn't Applied for any employer vacancy",
                'code' => "422"
            ], 422);
        }
        $collection = collect($request->except(['token', 'seeker_id', '_method', 'locale']));
        if ($request->has('required_documents')) {
            $required_documents = $this->commaToArray($request->input('required_documents'));
            $collection = $collection->merge(['required_documents' => serialize($required_documents)]);
        }
        if ($request->has('offer_vacancies')) {
            $offer_vacancies = $this->commaToArray($request->input('offer_vacancies'));
            $collection = $collection->merge(['offer_vacancies' => serialize($offer_vacancies)]);
        }
        $attributes = array_filter($collection->all());
        \DB::table('seeker_statuses')
            ->where('id', $id)
            ->where('seeker_id', $seeker_id)
            ->where('vacant_id', $vacant_id)
            ->update($attributes);
        return response()->json(["message" => "Candidate Correctly Updated", "code" => "200"], 200);
    }
    /**
     * delete candidate
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $candidate = $this->employer->seekers()->wherePivot('id', $id)->get();
        if ($candidate->count() == 0) {
            return response()->json(['message' => 'This Candidate does not exist', 'code' => "404"], 404);
        }
        $this->employer->seekers()->newPivotStatement()->where('id', $id)->delete();
        return response()->json(['message' => trans('app.deleted_successfully'), "code" => "200"], 200);
    }
    /**
     * Get All Required Documents
     * @return \Illuminate\Http\JsonResponse
     */
    public function requiredDocuments()
    {
        $documents = collect([
            ['text' => trans('app.job_guarantee'), 'id' => 'job_guarantee'],
            ['text' => trans('app.criminal_evidence'), 'id' => 'criminal_evidence'],
            ['text' => trans('app.proof_of_residence'), 'id' => 'proof_of_residence'],
            ['text' => trans('app.medical_examination'), 'id' => 'medical_examination'],
        ]);
        return response()->json(["data" => $documents, "code" => '200'], 200);
    }
    /**
     * Check if the seeker in applied list
     * @param $employer
     * @param $id
     * @return bool
     */
    protected function CheckSeekerApplied($employer, $id)
    {
        $data = $employer->vacancies()->whereHas('Applied_seeker', function ($query) use ($id) {
            $query->where('seeker_id', $id);
        })->get();
        if ($data->count() == 0) {
            return false;
        }
        return true;
    }
    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    protected function checkCandidate($employer, $seekerId, $vacantId)
    {
        $isCandidate = false;
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            $isCandidate = true;
        }
        return $isCandidate;
    }
    /**
     * @param $string
     * @param string $separator
     * @return array
     */
    protected function commaToArray($string, $separator = ',')
    {
        $values = explode($separator, $string);
        foreach ($values as $key => $val) {
            $values[$key] = trim($val);
        }
        return array_diff($values, array(""));
    }
    /**
     * transform  $data
     * @param $data
     * @return mixed
     */
    private function transform($data)
    {
        $array = collect($data);
        $array->each(function ($item) {
            $item->pic_file_name = $this->imageLink($item);
            if (!$item->pivot->offer_vacancies == null) {
                $item->pivot->offer_vacancies = implode(",", unserialize($item->pivot->offer_vacancies));
            }
            if (!$item->pivot->required_documents == null) {
                $item->pivot->required_documents = implode(",", unserialize($item->pivot->required_documents));
            }
        });
        return $array;
    }
    /**
     * helper function to transform image name to
     * full link
     * @param $seeker
     * @return mixed
     */
    protected function imageLink($seeker)
    {
        $photoPath = '/uploads/seeker/photo/';
        $photoUrl = public_path() . $photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            return url($photoPath . $seeker->pic_file_name);
        } else {
            return '';
        }
    }
}
