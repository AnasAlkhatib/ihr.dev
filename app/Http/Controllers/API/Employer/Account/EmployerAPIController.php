<?php
namespace App\Http\Controllers\API\Employer\Account;

use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Employer\EmployerUpdateProfileRequest as UpdateProfile;
use App\Http\Requests\API\Employer\EmployerChangePasswordRequest as ChangePassword;
use App\Http\Requests\API\Employer\EmployerUpdateProfilePictureRequest as UpdateProfilePicture;
use Carbon\Carbon;
use Config;
use App\Util\Traits\Messagable;
/**
 * Class EmployerAPIController
 * @package App\Http\Controllers\API\Employer\Account
 */
class EmployerAPIController extends Controller
{
    use Messagable;
    /**
     * @var string
     */
    protected $path = '/uploads/employer/photo/';
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;
    /**
     * EmployerAPIController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth', ['except' => ['companyType']]);
    }
    /** collection of Company Types
     * @return \Illuminate\Http\JsonResponse
     */
    public function companyType()
    {
        $companyTypes = collect(
            [
                ['name' => trans('app.private_sector'), 'value' => 'private_sector'],
                ['name' => trans('app.public_sector'), 'value' => 'public_sector'],
                ['name' => trans('app.non_profit_organization'), 'value' => 'non_profit_organization'],
                ['name' => trans('app.recruitment_agency'), 'value' => 'recruitment_agency'],
            ]);
        return response()->json(["data" => $companyTypes, "code" => "200"], 200);
    }
    /** Update Employer Profile
     * @param UpdateProfile $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(UpdateProfile $request)
    {
        $employer = $this->employer;
        $data = array_map('trim', $request->except(['token', '_method', 'active', 'password']));
        if (array_key_exists('email', $data)) {
            $data['email'] = strtolower($data['email']);
        }
        foreach ($data as $field => $value) {
            $employer->$field = $value;
        }
        $employer->save();
        return response()->json(['message' => 'The employer has been updated', 'code' => "200"], 200);
    }
    /**
     * change employer password
     * @param ChangePassword $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePassword $request)
    {
        $employer = $this->employer;
        $employer->password = bcrypt($request->new_password);
        if ($employer->save()) {
            $token = $this->auth->refresh();
            $employer->token = $token;
            return response()->json(['newToken' => $token, 'code' => "200"], 200);
        }
    }
    /**
     * Update employer profile picture
     * @param UpdateProfilePicture $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfilePicture(UpdateProfilePicture $request)
    {
        $employer = $this->employer;
        if ($request->hasFile('file')) {
            if ($employer->pic_file_name) {
                \File::delete(public_path() . $this->path . $employer->pic_file_name);
            }
            $file = $request->file('file');
            $name = $this->uploadPhoto($file);
            $employer->pic_file_name = $name;
            $employer->save();
            return response()->json([
                'message' => "Profile Picture correctly added",
                "pic_url" => url($this->path . $employer->pic_file_name),
                "code" => "201"
            ], 201);
        }
    }
    /**
     * upload and resize img
     *
     * @param  int $file
     * @return array
     */
    protected function uploadPhoto($file)
    {
        $config = config::get('profile-picture');
        $name = sha1(Carbon::now()) . '.' . $file->guessExtension();
        Image::make($file->getRealPath())
            ->fit($config['width'], $config['height'], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save('uploads/employer/photo/' . $name);
        return $name;
    }
}
