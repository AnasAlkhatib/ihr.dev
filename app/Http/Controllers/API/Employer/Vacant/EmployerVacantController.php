<?php
namespace App\Http\Controllers\API\Employer\Vacant;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Employer\EmployerCreateVacantRequest as CreateVacantRequest;
use App\Http\Requests\API\Employer\EmployerUpdateVacantRequest as UpdateVacantRequest;
use App\Jobs\PermissionPublishEmployerVacant;
use App\Util\Traits\ReferenceIDTrait;
use App\VacantApi As Vacant;

/**
 * Class EmployerVacantController
 * @package App\Http\Controllers\API\Employer\Vacant
 */
class EmployerVacantController extends Controller
{
    use ReferenceIDTrait;
    /**
     * @var \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $auth;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $employer;

    /**
     * EmployerVacantController constructor.
     * @param \Auth $auth
     */
    public function __construct(\Auth $auth)
    {
        $this->auth = $auth::guard("api-employer");
        $this->employer = $this->auth->user();
        $this->middleware('jwt.auth');
    }

    /**
     * get all employer vacancies
     * with applied seeker count
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $vacancies = Vacant::where('employer_id', '=', $this->employer->id)->get();
        $applicants_count = 0;
        foreach ($vacancies as $vacancy) {
            $applicants_count += $vacancy->Applied_seeker()->count();
            $vacancy['total_applicants_applied'] = $vacancy->Applied_seeker()->count();
            $vacancy->tagged;

        }
        return response()->json(["data" => $vacancies, 'applicants_count' => $applicants_count, "code" => "200"], 200);
    }


    public function applicants()
    {
        $vacancies = Vacant::where('employer_id', '=', $this->employer->id)->get();
        // $applicants_count = 0;
        $applicants_list[] = '';
        foreach ($vacancies as $vacancy) {
            if ($vacancy->Applied_seeker()->count() > 0) {
                $applicants = $vacancy->Applied_seeker()->get();
                foreach ($applicants as $applicant) {
                    $applicant->pic_file_name = $this->imageLink($applicant);
                    $applicant->is_favorited = ($applicant->isFavorited($this->employer)) ? 'true' : 'false';
                    $applicant->is_nominated = ($this->checkCandidate($this->employer, $applicant->ID,
                        $applicant->pivot->vacant_id)) ? 'true' : 'false';
                    $applicant->is_online = $applicant->isOnline();
                    $applicant->candidate_id = $this->getCandidate($this->employer, $applicant->ID, $applicant->pivot->vacant_id);
                    $applicant->vacancy_title = $vacancy->job_title;
                    array_push($applicants_list, $applicant);
                }
            }

        }
        array_shift($applicants_list);
        return response()->json(["applicants" => $applicants_list, "code" => "200"], 200);
    }


    /**
     * Get  employer vacancy by id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $vacant = Vacant::find($id);
        if (!$vacant) {
            return response()->json(['message' => 'This vacant does not exist', 'code' => "404"], 404);
        }
        /* check if the employer owns this vacant */
        if ($vacant->employer_id === $this->employer->id) {
            $vacant->tagged;
            return response()->json(["data" => $vacant, "code" => "200"], 200);
        }
        return response()->json(["message" => trans('app.Unauthorized'), "code" => "401"], 401);
    }

    /** Add new vacant
     * @param CreateVacantRequest $request
     * @return Vacant
     */
    public function store(CreateVacantRequest $request)
    {
        /* Get all values except language ,company_location , token */
        $values = array_map('trim', $request->except(['company_location', 'language', 'tags', 'token', '_method']));
        $values['company_location'] = implode(',', $this->commaToArray($request->input("company_location")));
        $values['language'] = implode(',', $this->commaToArray($request->input("language")));
        $values['employer_id'] = $this->employer->getkey();
        $values['ref_num'] = $this->generateReferenceID(\App\Vacant::class, 'ref_num', $length = 9);
        $vacant = Vacant::create($values);
        if ($vacant) {
            $vacantWeb = \App\Vacant::find($vacant->ID);
            $this->dispatch(new PermissionPublishEmployerVacant($this->employer, $vacantWeb));
        }
        $tags = $this->commaToArray($request->input('tags'));
        $vacant->tag($tags);
        $vacant->tagged;
        return response()->json(["data" => $vacant, 'message' => trans('app.created_successfully'), "code" => "200"],
            200);
    }

    /**
     * @param UpdateVacantRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateVacantRequest $request, $id)
    {
        $vacant = Vacant::find($id);
        if (!$vacant) {
            return response()->json(['message' => 'This vacant does not exist', 'code' => "404"], 404);
        }
        /* check if the employer owns this vacant */
        if ($vacant->employer_id === $this->employer->id) {
            $values = array_map('trim', $request->except('tags', 'locale', '_method', 'token'));
            foreach ($values as $field => $value) {
                $vacant->$field = $value;
            }
            if ($request->input('tags')) {
                $vacant->untag();
                $tags = $this->commaToArray($request->input('tags'));
                $vacant->tag($tags);
            }
            $vacant->save();
            $vacant->tagged;
            return response()->json([
                "data" => $vacant,
                'message' => trans('app.updated_successfully'),
                "code" => "200"
            ],
                200);
        }
        return response()->json(["message" => trans('app.Unauthorized'), "code" => "401"], 401);
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $vacant = Vacant::find($id);
        /* check if the employer owns this vacant */
        if ($vacant->employer_id === $this->employer->id) {
            $vacant->delete();
            return response()->json(['message' => trans('app.deleted_successfully'), "code" => "200"], 200);
        }
        return response()->json(["message" => trans('app.Unauthorized'), "code" => "401"], 401);
    }

    /**
     * get all employer active Vacancies
     * @return \Illuminate\Http\JsonResponse
     */
    public function getActiveVacancies()
    {
        $vacancies = Vacant::orderBy('ID', 'desc')
            ->where('active', 1)
            ->where('employer_id', $this->employer->id)
            ->get();
        return response()->json(["data" => $vacancies, "code" => "200"], 200);
    }

    /**
     * @param $string
     * @param string $separator
     * @return array
     */
    function commaToArray($string, $separator = ',')
    {
        $values = explode($separator, $string);
        foreach ($values as $key => $val) {
            $values[$key] = trim($val);
        }
        return array_diff($values, array(""));
    }

    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function checkCandidate($employer, $seekerId, $vacantId)
    {
        $isCandidate = false;
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            $isCandidate = true;
        }
        return $isCandidate;
    }

    function imageLink($seeker)
    {
        $photoPath = '/uploads/seeker/photo/';
        $photoUrl = public_path() . $photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            $seeker->pic_file_name = url($photoPath . $seeker->pic_file_name);
        }
        return $seeker->pic_file_name;
    }

    /**
     * @param $employer
     * @param $seekerId
     * @param $vacantId
     * @return bool
     */
    private function getCandidate($employer, $seekerId, $vacantId)
    {
        $data = $employer->seekers()
            ->wherePivot('seeker_id', $seekerId)
            ->wherePivot('vacant_id', $vacantId)
            ->get();
        if ($data->count() > 0) {
            return $data[0]->pivot->id;
        }
        return 0;
    }

}
