<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\LanguageRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
/**
 * Class LanguageController
 * @package App\Http\Controllers\API
 */
class LanguageController extends Controller
{
    /** git All seeker languages
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $language = $seeker->language->all();
        if (!$language) {
            return response()->json(['message' => 'This seeker does not have languages ', 'code' => "404"], 404);
        }
        return response()->json(['data' => $language, 'code' => "200"], 200);
    }
    /**
     * Store  New Seeker Language
     * @param LanguageRequest $request
     * @param $seekerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LanguageRequest $request, $seekerId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        $seeker->language()->create($values);
        return response()->json(['message' => "Language correctly added", 'code' => "201"], 201);
    }
    /**
     * Show Seeker Language by $languageId
     * @param $id
     * @param $languageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $languageId)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $language = $seeker->language->find($languageId);
        if (!$language) {
            return response()->json(['message' => 'Language does not exist for This Seeker', 'code' => "404"], 404);
        }
        return response()->json(['data' => $language, 'code' => "200"], 200);
    }
    /**
     * Update Seeker Language by $languageId
     * @param LanguageRequest $request
     * @param $seekerId
     * @param $languageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LanguageRequest $request, $seekerId, $languageId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $language = $seeker->language->find($languageId);
        if (!$language) {
            return response()->json(['message' => 'This Language does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        foreach ($values as $field => $value) {
            $language->$field = $value;
        }
        $language->save();
        return response()->json(['message' => 'The language has been updated'], 200);
    }
    /**
     * Delete Seeker Language by $languageId
     * @param $seekerId
     * @param $languageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seekerId, $languageId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $language = $seeker->language->find($languageId);
        if (!$language) {
            return response()->json(['message' => 'This Language does not exist', 'code' => "404"], 404);
        }
        $language->delete();
        return response()->json(['message' => 'The Language has been deleted', 'code' => "200"], 200);
    }
}
