<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\ResumeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use App\Seeker;
use File;
use Carbon\Carbon;
/**
 * Class ResumeController
 * @package App\Http\Controllers\API
 */
class ResumeController extends Controller
{
    /**
     * @var string
     */
    private $path = '/uploads/seeker/resume/';
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /* find the seeker */
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        /* find the seeker cv  */
        if (!$seeker->cv_file_name) {
            return response()->json(['message' => 'This seeker does not have any Resume', 'code' => "404"], 404);
        }
        $path = public_path() . $this->path . $seeker->cv_file_name;
        /* find the seeker cv path on the server */
        try {
            $file = File::get($path);
        } catch (FileNotFoundException $e) {
            /* delete the cv from seeker object if not exist on the server  */
            $this->destroy($id);
            return response()->json(['error' => "File does not exist at path", 'code' => "500"], 500);
        }
        return response()->json([
            'message' => "CV Correctly Retrieved",
            "cv_url" => url($this->path . $seeker->cv_file_name),
            'code' => "200"
        ], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function store(ResumeRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if ($request->hasFile('file')) {
            if ($seeker->cv_file_name) {
                File::delete(public_path() . $this->path . $seeker->cv_file_name);
            }
            $file = $request->file('file');
            $name = sha1(Carbon::now()) . '.' . $file->guessExtension();
            $file->move(public_path() . $this->path, $name);
            $seeker->cv_file_name = $name;
            $seeker->save();
            return response()->json([
                'message' => "Resume correctly added",
                "cv_url" => url($this->path . $seeker->cv_file_name),
                "code" => "201"
            ], 201);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ResumeRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if ($request->hasFile('file')) {
            if ($seeker->cv_file_name) {
                File::delete(public_path() . $this->path . $seeker->cv_file_name);
            }
            $file = $request->file('file');
            $name = sha1(Carbon::now()) . '.' . $file->guessExtension();
            $upload = $file->move(public_path() . $this->path, $name);
            $seeker->cv_file_name = $name;
            $seeker->save();
            return response()->json([
                'message' => "Resume correctly updated",
                "resume_file_name" => $seeker->cv_file_name,
                'code' => "200"
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if (!$seeker->cv_file_name) {
            return response()->json(['message' => 'This seeker does not have any Resume', 'code' => "404"], 404);
        }
        File::delete(public_path() . $this->path . $seeker->cv_file_name);
        $seeker->cv_file_name = "";
        $seeker->save();
        return response()->json(['message' => 'The Resume have been deleted ', 'code' => "200"], 200);
    }
}
