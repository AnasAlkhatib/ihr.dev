<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\EducationLevels;
/**
 * Class EducationLevelsController
 * @package App\Http\Controllers\API
 */
class EducationLevelsController extends Controller
{
    /**
     * Get all  Education Levels
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $educationLevels=new EducationLevels();
        $educationlevelsresult = $educationLevels->all();

        if (!$educationlevelsresult) {
            return response()->json(["message" => 'This seeker does not have Education', "code" => "404"], 404);
        }
        foreach($educationlevelsresult as $level)
            $level->code=trans('app.'.$level->code);
        return response()->json(["data" => $educationlevelsresult, "code" => "200"], 200);
    }

}
