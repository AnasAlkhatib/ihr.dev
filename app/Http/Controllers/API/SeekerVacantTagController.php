<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\SeekerVacantTagRequest;
use App\Http\Controllers\Controller;
Use App\SeekerVacantTag;
use App\Seeker;
/**
 * Class SeekerVacantTagController
 * @package App\Http\Controllers\API
 */
class SeekerVacantTagController extends Controller
{
    /**
     * Get Seeker Vacant Tags By seeker ID
     * @param $seeker_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($seeker_id)
    {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $seekerVacantTag = $seeker->vacantTag()->get();
        if ($seekerVacantTag->isEmpty()) {
            return response()->json(['message' => 'This seeker does not have Vacant Tags', 'code' => "404"], 404);
        }
        return response()->json(['data' => $this->transform($seekerVacantTag), 'code' => "200"], 200);
    }
    /**
     *  Create or Update Seeker Vacant Tag
     * @param SeekerVacantTagRequest $request
     * @param $seeker_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SeekerVacantTagRequest $request, $seeker_id)
    {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $tags = serialize(array_map('trim', explode(',', $request->input('tags'))));
        $seekerVacantTag = SeekerVacantTag::where('seeker_id', '=', $seeker_id)->first();
        /* If No Record Create New One */
        if (is_null($seekerVacantTag)) {
            $seekerVacantTag = SeekerVacantTag::create(['seeker_id' => $seeker_id, 'tags' => $tags]);
            if ($seekerVacantTag) {
                return response()->json(['message' => 'The Seeker Vacant Tag has been Created', 'code' => "201"], 201);
            }
        }
        /* Update If Record Exist */
        $seekerVacantTag->seeker_id = $seeker_id;
        $seekerVacantTag->tags = $tags;
        if ($seekerVacantTag->save()) {
            return response()->json(['message' => 'The Seeker Vacant Tag has been Updated', 'code' => "200"], 200);
        }
    }
    /**
     * Delete  Vacant Tag
     * @param $seeker_id
     * @param $tagID
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seeker_id, $tagID)
    {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $seekerVacantTag = $seeker->vacantTag()->find($tagID);
        if (!$seekerVacantTag) {
            return response()->json(['message' => 'This Vacant Tags does not exist', 'code' => "404"], 404);
        }
        $seekerVacantTag->delete();
        return response()->json(['message' => 'The Vacant Tags has been deleted', 'code' => "200"], 200);
    }
    /**
     * transform  $data to be compatible with iphone
     * @param $data
     * @return mixed
     */
    protected function transform($data)
    {
        $data->each(function ($item) {
            $item->setAttribute('ID', $item->id);
            unset($item->id);
            $item->tags = unserialize($item->tags);
        });
        return $data;
    }
}
