<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Settings;

/**
 * Class SettingsController
 * @package App\Http\Controllers\API
 */
class SettingsController extends Controller
{
    /**
     * get all settings
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $settings = Settings::all();
        return response()->json(['data' => $settings, 'code' => "200"], 200);
    }

    /**
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByKey($key)
    {
        $result = Settings::where(['key' => $key])->first();
        if (!$result) {
            return response()->json(['message' => 'This Settings does not exist', 'code' => "404"], 404);
        }
        return response()->json(['data' => $result->value, 'code' => "200"], 200);
    }
}
