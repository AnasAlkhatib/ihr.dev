<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VacantApi as Vacant;
use Carbon\Carbon;
/**
 * Class VacantController
 * @package App\Http\Controllers\API
 */
class VacantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacants = Vacant::orderBy('ID', 'desc')->where('active', '=', 1)->Paginate(10);
        return response()->json([
            'count' => $vacants->count(),
            'total' => $vacants->total(),
            'next' => $vacants->nextPageUrl() ?: '',
            'previous' => $vacants->previousPageUrl() ?: '',
            'code' => "200",
            'data' => $vacants->items()
        ], 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vacant = Vacant::find($id);
        if (!$vacant) {
            return response()->json(['message' => 'This vacant does not exist', 'code' => "404"], 404);
        }
        return response()->json(['data' => $vacant, 'code' => "200"], 200);
    }
    /** Vacant advanced search
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        /* build the main query */
        $query = Vacant::query()->where('active', '=', 1);
        /* if we have job title */
        $jobTitle = trim($request->input('job_title'));
        if ($jobTitle) {
            $query->where('job_title', 'LIKE', '%' . $jobTitle . '%');
        }
        /* If we have company location */
        $companyLocation = trim($request->input('company_location'));
        if ($companyLocation) {
            $query->where(function ($query) use ($companyLocation) {
                $query->orWhere('company_location', 'LIKE', '%' . $companyLocation . '%');
            });
        }
        /* If we have gender */
        $gender = trim($request->input('gender'));
        if ($gender) {
            $query->where(function ($query) use ($gender) {
                $query->whereIn('gender', [$gender, 'both']);
            });
        }
        $values = $request->only([
            'company_industry',
            'job_rules',
            'job_status',
            'education',
            'exp_level'
        ]);
        $searchTerms = array_map('trim', $values);
        /* if we have values to search */
        if (array_filter($searchTerms)) {
            foreach ($searchTerms as $key => $value) {
                if ($value) {
                    $query->where($key, '=', $value);
                }
            }
        }
        $data = $query->orderBy('created_at', 'desc')
            ->Paginate(10);
        return response()->json([
            'count' => $data->count(),
            'total' => $data->total(),
            'next' => $data->nextPageUrl() ?: '',
            'previous' => $data->previousPageUrl() ?: '',
            'code' => "200",
            'data' => $data->items()
        ], 200);
    }

    public function getbyplan(Request $request)
    {

        $today_date=Carbon::now();
        /* build the main query */
        $query = Vacant::query()->where('active', '=', 1);
        $query->join('plan_subscriptions','vacancies.employer_id','=','plan_subscriptions.employer_id');
        $query->where('plan_subscriptions.plan_id','=',$request->input('plan_id'));
        $query->where('plan_subscriptions.ends_at','>',$today_date->toDateTimeString());


        $data = $query->orderBy('created_at', 'desc')
            ->Paginate(10);
        return response()->json([
            'count' => $data->count(),
            'total' => $data->total(),
            'next' => $data->nextPageUrl() ?: '',
            'previous' => $data->previousPageUrl() ?: '',
            'code' => "200",
            'data' => $data->items()
        ], 200);
    }

}