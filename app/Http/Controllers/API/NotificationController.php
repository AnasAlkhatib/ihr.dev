<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\NotificationRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
use App\Notification;
/**
 * Class NotificationController
 * @package App\Http\Controllers\API
 */
class NotificationController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $notification = $seeker->notification->all();
        if (!$notification) {
            return response()->json(['message' => 'This seeker does not have notification', 'code' => "404"], 404);
        }
        return response()->json(['data' => $notification, 'code' => "200"], 200);
    }
    /**
     * @param NotificationRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(NotificationRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->all());
        $seekerNotification = $seeker->notification->last();
        if (is_null($seekerNotification)) {
            $seeker->notification()->create($values);
            return response()->json([
                'message' => "device notification correctly added for seeker id: {$seeker->ID}",
                'code' => "201"
            ], 201);
        }
        $notification = $seeker->notification->find($seekerNotification->ID);
        foreach ($values as $field => $value) {
            $notification->$field = $value;
        }
        $notification->save();
        return response()->json([
            'message' => "device notification correctly Updated for seeker id: {$seeker->ID}",
            'code' => "200"
        ], 200);
    }
    /**
     * @param $id
     * @param $notification_id
     * @return \Illuminate\Http\JsonResponse
     */
//    public function show($id, $notification_id)
//    {
//        $seeker = Seeker::find($id);
//        if (!$seeker) {
//            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
//        }
//        $notification = $seeker->notification->find($notification_id);
//        if (!$notification) {
//            return response()->json([
//                'message' => 'This notification device does not exist for this seeker',
//                'code' => "404"
//            ], 404);
//        }
//        return response()->json(['data' => $notification], 200);
//    }
    /**
     * @param NotificationRequest $request
     * @param $id
     * @param $notification_id
     * @return \Illuminate\Http\JsonResponse
     */
//    public function update(NotificationRequest $request, $id, $notification_id)
//    {
//        $seeker = Seeker::find($id);
//        if (!$seeker) {
//            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
//        }
//        $notification = $seeker->notification->find($notification_id);
//        if (!$notification) {
//            return response()->json(['message' => 'This device notification does not exist', 'code' => "404"], 404);
//        }
//        $values = array_map('trim', $request->except('_method'));
//        foreach ($values as $field => $value) {
//            $notification->$field = $value;
//        }
//        $notification->save();
//        return response()->json(['message' => 'The notification device has been updated'], 200);
//    }
    /**
     * @param $id
     * @param $notification_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, $notification_id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $notification = $seeker->notification->find($notification_id);
        if (!$notification) {
            return response()->json(['message' => 'This device notification does not exist', 'code' => "404"], 404);
        }
        $notification->delete();
        return response()->json(['message' => 'The device notification has been deleted', 'code' => "200"], 200);
    }
}
