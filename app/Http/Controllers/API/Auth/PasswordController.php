<?php
namespace App\Http\Controllers\API\Auth;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/**
 * Class PasswordController
 * @package App\Http\Controllers\API\Auth
 */
class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Seeker Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;
    /**
     * @var string
     */
    protected $broker = 'seeker';
    /**
     *  Create a new password controller instance.
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware());
    }
    /**
     * @param Request $request
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return $this->getSendResetLinkEmailFailureResponse($validator->errors()->all()[0]);
        }
        $broker = $this->getBroker();
        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
                return $this->getSendResetLinkEmailInvalidUserResponse($response);
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }
    /**
     * Success Response
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json(['status' => 'Success', 'message' => trans($response), 'code' => "200"], 200);
    }
    /**
     * User not found
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSendResetLinkEmailInvalidUserResponse($response)
    {
        return response()->json(['status' => 'Not Found', 'message' => trans($response), 'code' => "404"], 404);
    }
    /**
     * Email validation
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSendResetLinkEmailFailureResponse($response)
    {
        return response()->json(['status' => 'Error', 'message' => trans($response), 'code' => "422"], 422);
    }
    /** Set the email subject
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : trans('system_message.reset_password.subject');
    }
}
