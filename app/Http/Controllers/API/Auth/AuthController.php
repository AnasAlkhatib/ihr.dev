<?php
namespace App\Http\Controllers\API\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

/**
 * Class AuthController
 * @package App\Http\Controllers\API
 */
class AuthController extends Controller
{
    /**
     * @var
     */
    protected $auth;

    /**
     * AuthController constructor.
     * @param Auth $auth
     */
    function __construct(Auth $auth)
    {
        $this->auth = $auth::guard("api");
        $this->middleware('jwt.auth', ['except' => ['login']]);
        $this->middleware('locale');
    }

    /**
     * API Login, on success return JWT Auth token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = $this->auth->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
//        if ($this->auth->user()->active) {
//            return response()->json(compact('token'));
//        }
//        return response()->json(['error' => trans('app.not_activated'), 'code' => 400], 400);
        return response()->json(compact('token'));
    }

    /**
     * get auth user from provided token
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        $user = $this->auth->user();
        $data = $this->imageLink($user);
        $user->current_job = $this->getCurrentJob($user);
        $user->is_online = $user->isOnline();
        if ($user->country_id != 0) {
            if (\App::getLocale() == 'en') {
                $user->country_name = $user->country->find($user->country_id)->name;
            } else {
                $user->country_name = $user->country->find($user->country_id)->name_ar;
            }
        }
        if ($user->residence != 0 && $user->country_id != 0) {
            if (\App::getLocale() == 'en') {
                $user->residence_name = $user->country->find($user->residence)->name;
            } else {
                $user->residence_name = $user->country->find($user->residence)->name_ar;
            }
        } else if ($user->residence != 0 && $user->country_id == 0) {
            if (\App::getLocale() == 'en') {
                $user->residence_name = \App\Country::find($user->residence)->name;
            } else {
                $user->residence_name = \App\Country::find($user->residence)->name_ar;
            }
        }
        return response()->json(['data' => $data, "code" => "200"], 200);
    }

    /**
     * helper function to transform image name to
     * full link
     * @param $seeker
     * @return mixed
     */
    protected function imageLink($seeker)
    {
        $photoPath = '/uploads/seeker/photo/';
        $photoUrl = public_path() . $photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            $seeker->pic_file_name = url($photoPath . $seeker->pic_file_name);
        }
        else
            $seeker->pic_file_name = url($photoPath . 'd0c8afb28d0ac14513111eeb2aef8ca65a04dbf9.png');
        return $seeker;
    }

    /**
     * get job seeker current job
     * @param $seeker
     * @return bool|null
     */
    protected function getCurrentJob($seeker)
    {
        $currentJob = null;
        $array = array();
        $experience = $seeker->experience->all();
        if ($experience) {
            foreach ($experience as $exp) {
                if (strtotime($exp->end_date) < 0 || $exp->end_date == null) {
                    $array['job_title'] = $exp->job_title;
                }
            }
            if (!empty($array)) {
                $currentJob = $array['job_title'];
            }
        }
        return $currentJob;
    }

    /**
     * set seeker online
     * @return \Illuminate\Http\JsonResponse
     */
    public function online()
    {
        $seeker = $this->auth->user();
        $expiresAt = Carbon::now()->addMinutes(5);
        \Cache::put('seeker-is-online-' . $seeker->ID, true, $expiresAt);
        return response()->json(['is_online' => $seeker->isOnline(), "code" => "200"], 200);
    }

    /**
     * Log out
     * Invalidate the token, so Seeker cannot use it anymore
     * They have to get a new token
     */
    public function logout()
    {
        $this->auth->logout();
        return response()->json(['message' => "Successfully logged out", "code" => "200"], 200);
    }
}
