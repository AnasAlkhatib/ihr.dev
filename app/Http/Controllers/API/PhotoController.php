<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\PhotoRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
use File;
use Config;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
/**
 * Class PhotoController
 * @package App\Http\Controllers\API
 */
class PhotoController extends Controller
{
    private $path = '/uploads/seeker/photo/';
    /**
     * show seeker photo
     */
    public function show($id)
    {
        /* find the seeker */
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        /* find the seeker pic  */
        if (!$seeker->pic_file_name) {
            return response()->json(['message' => 'This seeker does not have any photo', 'code' => "404"], 404);
        }
        $path = public_path() . $this->path . $seeker->pic_file_name;
        /* find the seeker pic path on the server */
        try {
            $file = File::get($path);
        } catch (FileNotFoundException $e) {
            /* delete the pic from seeker object if not exist on the server  */
            $this->destroy($id);
            return response()->json(['error' => "File does not exist at path", 'code' => "500"], 500);
        }
        $type = File::mimeType($path);
        $response = response()->make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
    /**
     * Store a newly created resource in storage.
     * @param PhotoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PhotoRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if ($request->hasFile('file')) {
            if ($seeker->pic_file_name) {
                File::delete(public_path() . $this->path . $seeker->pic_file_name);
            }
            $file = $request->file('file');
            $name = $this->uploadPhoto($file);
            $seeker->pic_file_name = $name;
            $seeker->save();
            return response()->json([
                'message' => "Photo correctly added",
                "pic_url" => url($this->path . $seeker->pic_file_name),
                "code" => "201"
            ], 201);
        }
    }
    /**
     * Update the specified resource in storage.
     * @param PhotoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PhotoRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if ($request->hasFile('file')) {
            if ($seeker->pic_file_name) {
                File::delete(public_path() . $this->path . $seeker->pic_file_name);
            }
            $file = $request->file('file');
            $name = $this->uploadPhoto($file);
            $seeker->pic_file_name = $name;
            $seeker->save();
            return response()->json([
                'message' => "Photo correctly updated",
                "pic_url" => url($this->path . $seeker->pic_file_name),
                'code' => "200"
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        if (!$seeker->pic_file_name) {
            return response()->json(['message' => 'This seeker does not have any photo', 'code' => "404"], 404);
        }
        File::delete(public_path() . $this->path . $seeker->pic_file_name);
        $seeker->pic_file_name = "";
        $seeker->save();
        return response()->json(['message' => 'The Photo have been deleted ', 'code' => "200"], 200);
    }
    /**
     * upload and resize img
     *
     * @param  int $file
     * @return array
     */
    protected function uploadPhoto($file)
    {
        $config = config::get('profile-picture');
        $name = sha1(Carbon::now()) . '.' . $file->guessExtension();
        Image::make($file->getRealPath())
            ->fit($config['width'], $config['height'], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save('uploads/seeker/photo/' . $name);
        return $name;
    }
}
