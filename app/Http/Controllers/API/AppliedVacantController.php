<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\AppliedVacantRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
use App\Vacant;
use App\AppliedVacant;

class AppliedVacantController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id) {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => trans('app.seeker_not_exist'), 'code' => "404"], 404);
        }
        $vacantApplied = $seeker->vacantApplied->all();
        if (!$vacantApplied) {
            return response()->json(['message' => 'This seeker does not have applied jobs ', 'code' => "404"], 404);
        }
        return response()->json(['data' => $vacantApplied, 'code' => "200"], 200);
    }

    public function show($id, $vacant_id) {

        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => trans('app.seeker_not_exist'), 'code' => "404"], 404);
        }
        $vacantApplied = $seeker->vacantApplied->find($vacant_id);
        if (!$vacantApplied) {
            return response()->json(['message' => 'This vacant does not assign as Applied for this seeker', 'code' => "404"], 404);
        }
        return response()->json(['data' => $vacantApplied, 'code' => "200"], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppliedVacantRequest $request, $seeker_id) {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => trans('app.seeker_not_exist'), 'code' => "404"], 404);
        }


        $vacant=new Vacant();
        $vacantrecord=$vacant::find($request->vacant_id);

        $level=0;
        switch ($vacantrecord->education)
        {
            case ''            : $level=0; break;
            case 'high_school' : $level=1; break;
            case 'diploma'     : $level=2; break;
            case 'bachelor'    : $level=3; break;
            case 'high_diploma': $level=4; break;
            case 'master'      : $level=5 ;break;
            case 'doctorate'   : $level=6; break;
        }

        if($seeker->edu_level_id<$level)
            return response()->json(['message' => trans('app.education_error_'.$vacantrecord->education), 'code' => "422"], 422);

        //check gender
        if($seeker->gender!=$vacantrecord->gender && $vacantrecord->gender!=''&& $vacantrecord->gender!='both')
        {
            return response()->json(['message' => trans('app.error_'.$vacantrecord->gender), 'code' => "422"], 422);
        }

        $values = array_map('trim', $request->all());
        $seeker->appliedVacant()->create($values);
        return response()->json(['message' => trans('app.succ_applied_for_job'), 'code' => "201"], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($seeker_id, $applied_id) {

        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => trans('app.seeker_not_exist'), 'code' => "404"], 404);
        }

        $applied=AppliedVacant::where([
            ['vacant_id','=',$applied_id],
            ['seeker_id','=',$seeker_id]
        ])->first();

        if (!$applied) {
            return response()->json(['message' => trans('app.applied_job_not_exist'), 'code' => "404"], 404);
        }
        $applied->delete();
        return response()->json(['message' => trans('app.applied_job_deleted'), 'code' => "200"], 200);
    }

}