<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Area;

/**
 * Class CityController
 * @package App\Http\Controllers\API
 */
class CityController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $Area = Area::find($id);
        if (!$Area) {
            return response()->json(['message' => 'This Area does not exist', 'code' => "404"], 404);
        }
        $cities = $Area->city->all();
        if (!$cities) {
            return response()->json(['message' => 'This Area does not have cities ', 'code' => "404"], 404);
        }
        return response()->json(['data' => $Area->city, 'code' => "200"], 200);
    }
}
