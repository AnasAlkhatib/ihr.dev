<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\ExperienceRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
/**
 * Class ExperienceController
 * @package App\Http\Controllers\API
 */
class ExperienceController extends Controller
{
    /**
     * get all seeker experience
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $experience = $seeker->experience->all();
        if (!$experience) {
            return response()->json(['message' => 'This seeker does not have experience ', 'code' => "404"], 404);
        }
        return response()->json(['data' => $experience, 'code' => "200"], 200);
    }
    /**
     * store  new seeker experience
     * @param ExperienceRequest $request
     * @param $seekerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExperienceRequest $request, $seekerId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        $seeker->Experience()->create($values);
        return response()->json(['message' => "experience correctly added", 'code' => "201"], 201);
    }
    /**
     *  Show   seeker experience by $id
     * @param $id
     * @param $experienceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $experienceId)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $experience = $seeker->experience->find($experienceId);
        if (!$experience) {
            return response()->json(['message' => 'This Experience does not exist for this Seeker', 'code' => "404"],
                404);
        }
        return response()->json(['data' => $experience, 'code' => "200"], 200);
    }
    /**
     * Update seeker experience by $experienceId
     * @param ExperienceRequest $request
     * @param $seekerId
     * @param $experienceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExperienceRequest $request, $seekerId, $experienceId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $experience = $seeker->experience->find($experienceId);
        if (!$experience) {
            return response()->json(['message' => 'This experience does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        foreach ($values as $field => $value) {
            $experience->$field = $value;
        }
        $experience->save();
        return response()->json(['message' => 'The experience has been updated', 'code' => "200"], 200);
    }
    /**
     * Delete seeker experience
     * @param $seekerId
     * @param $experienceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seekerId, $experienceId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $experience = $seeker->experience->find($experienceId);
        if (!$experience) {
            return response()->json(['message' => 'This experience does not exist', 'code' => "404"], 404);
        }
        $experience->delete();
        return response()->json(['message' => 'The experience has been deleted', 'code' => "200"], 200);
    }
}
