<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\UpdateEducationRequest as UpdateRequest;
use App\Http\Requests\StoreEducationRequest as StoreRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
use App\Education;
/**
 * Class EducationController
 * @package App\Http\Controllers\API
 */
class EducationController extends Controller
{
    /**
     * Git all Seeker Education
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(["message" => trans('app.seeker_not_exist'), "code" => "404"], 404);
        }
        $education = $seeker->education->all();
        if (!$education) {
            return response()->json(["message" => trans('app.seeker_has_no_education'), "code" => "404"], 404);
        }
        return response()->json(["data" => $education, "code" => "200"], 200);
    }
    /**
     * Store  New Seeker Education
     * @param StoreRequest $request
     * @param $seekerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request, $seekerId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(["message" => trans('app.seeker_not_exist'), "code" => "404"], 404);
        }
        $values = array_map("trim", $request->except("_method", "locale"));

        $seeker->education()->create($values);

        $next_education = Education::where(['seeker_id'=>$seekerId])->max('edu_level_id');
        //update seeker record
        if($next_education)
        {
            $seeker->edu_level_id=$next_education;
            $seeker->save();
        }
        else {
            $seeker->edu_level_id = 0;
            $seeker->save();
        }

        return response()->json(["message" => trans('app.education_added'), "code" => "201"], 201);
    }
    /**
     * Show Seeker Education by $educationId
     * @param $id
     * @param $educationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $educationId)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(["message" => trans('app.seeker_not_exist'), "code" => "404"], 404);
        }
        $education = $seeker->education->find($educationId);
        if (!$education) {
            return response()->json(["message" => trans('app.education_not_exist_for_seeker'), "code" => "404"],
                404);
        }
        return response()->json(["data" => $education, "code" => '200'], 200);
    }
    /**
     * Update Seeker Education by $educationId
     * @param UpdateRequest $request
     * @param $seekerId
     * @param $educationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $seekerId, $educationId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(["message" => trans('app.seeker_not_exist'), "code" => "404"], 404);
        }
        $education = $seeker->education->find($educationId);
        if (!$education) {
            return response()->json(["message" =>trans('app.education_not_exist'), "code" => "404"], 404);
        }
        $values = array_map("trim", $request->except("_method", "locale"));
        foreach ($values as $field => $value) {
            $education->$field = $value;
        }
        $education->save();

        //update seeker record

        $next_education = Education::where(['seeker_id'=>$seekerId])->max('edu_level_id');

        if($next_education)
        {
            $seeker->edu_level_id=$next_education;
            $seeker->save();
        }
        else {
            $seeker->edu_level_id = 0;
            $seeker->save();
        }



        return response()->json(["message" =>trans('app.education_update'), "code" => '200'], 200);
    }
    /**
     * Delete Seeker Education by $educationId
     * @param $seekerId
     * @param $educationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seekerId, $educationId)
    {

        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(["message" => trans('app.seeker_not_exist'), "code" => "404"], 404);
        }
        $education = $seeker->education->find($educationId);
        if (!$education) {
            return response()->json(["message" => trans('app.education_not_exist'), "code" => "404"], 404);
        }
        $education->delete();


        $next_education = Education::where(['seeker_id'=>$seekerId])->max('edu_level_id');
        //update seeker record
        if($next_education)
        {
            $seeker->edu_level_id=$next_education;
            $seeker->save();
        }
        else {
            $seeker->edu_level_id = 0;
            $seeker->save();
        }

        return response()->json(["message" => trans('app.education_deleted'), "code" => "200"], 200);
    }
}
