<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Area;

class AreaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $areas = Area::all();
        if (count($areas) > 0) {
            foreach ($areas as $area) {
                $data [] = array('area_id' => $area->ID, 'area_name' => $area->name, 'area_name_en' => $area->name_en,
                    'cities' => $area->find($area->ID)->city()->get(['ID', 'name', 'name_en']));
            }
            return response()->json(["data" => $data, "code" => "200"], 200);
        }
    }

}
