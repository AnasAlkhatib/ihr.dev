<?php
namespace App\Http\Controllers\API;

use Validator;
use App\Vacant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\VacantPushNotification;
class TestPushController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required|in:1,2',
            'device_token' => 'required|string',
            'vacant_id' => 'required|exists:vacancies,ID',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors(), 'code' => "422"], 422);
        }
        $vacantId = $request->input('vacant_id');
        $vacant = Vacant::find($vacantId);
        $seekersData = [
            [
                'device_type' => $request->input('device_type'),
                'device_token' => $request->input('device_token'),
            ]
        ];
        dispatch(new VacantPushNotification($seekersData, $vacant));
        return response()->json(['message' => 'Notification  Pushed successfully', 'code' => "200"], 200);
    }
}