<?php
namespace App\Http\Controllers\API;

use App\Http\Requests\CourseRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
/**
 * Class CourseController
 * @package App\Http\Controllers\API
 */
class CourseController extends Controller
{
    /**
     * git all seeker Courses
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $courses = $seeker->course->all();
        if (!$courses) {
            return response()->json(['message' => 'This seeker does not have Courses', 'code' => "404"], 404);
        }
        return response()->json(['data' => $courses, 'code' => "200"], 200);
    }
    /**
     * Store New seeker Course
     * @param CourseRequest $request
     * @param $seekerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseRequest $request, $seekerId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        $seeker->course()->create($values);
        return response()->json(['message' => "Course Correctly added ", 'code' => "201"],
            201);
    }
    /**
     * Show Seeker Course by $courseId
     * @param $id
     * @param $courseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $courseId)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $course = $seeker->course->find($courseId);
        if (!$course) {
            return response()->json(['message' => 'This Course does not exist for this seeker', 'code' => "404"], 404);
        }
        return response()->json(['data' => $course, "code" => "200"], 200);
    }
    /**
     * Update Seeker Course by $courseId
     * @param CourseRequest $request
     * @param $seekerId
     * @param $courseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CourseRequest $request, $seekerId, $courseId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $course = $seeker->course->find($courseId);
        if (!$course) {
            return response()->json(['message' => 'This Course does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'locale'));
        foreach ($values as $field => $value) {
            $course->$field = $value;
        }
        $course->save();
        return response()->json(['message' => 'The course has been updated', 'code' => "200"], 200);
    }
    /**
     * Delete Seeker Course by $courseId
     * @param $seekerId
     * @param $courseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seekerId, $courseId)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $course = $seeker->course->find($courseId);
        if (!$course) {
            return response()->json(['message' => 'This course does not exist', 'code' => "404"], 404);
        }
        $course->delete();
        return response()->json(['message' => 'The course has been deleted', 'code' => "200"], 200);
    }
}
