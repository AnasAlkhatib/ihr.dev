<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\News;
use File;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Cache::remember('news', 15 / 60, function () {
            $data = News::orderBy('ID', 'desc')->simplePaginate(10);
            $data->each(function ($news) {
                $thumbnail_path = public_path() . '/uploads/news/thumbnail/' . $news->news_thumbnail;
                if (File::exists($thumbnail_path) && $news->news_img != '') {
                    $news->setAttribute('img_url', url("/uploads/news/thumbnail/" . $news->news_thumbnail));
                } else {
                    $news->setAttribute('img_url', '');
                }
            });
            return $data;
        });
        if (count($news) > 0) {
            return response()->json([
                'data' => $news->items(),
                'code' => "200",
                'next' => $news->nextPageUrl() ?: '',
                'previous' => $news->previousPageUrl() ?: '',
            ], 200);
        }
        return response()->json(['message' => 'There is no news to display', 'code' => "404"], 404);
    }
    public function show($id)
    {
        $news = News::find($id);
        if (!$news) {
            return response()->json(['message' => 'This News does not exist', 'code' => "404"], 404);
        }
        $thumbnail_path = public_path() . '/uploads/news/thumbnail/' . $news->news_thumbnail;
        if (File::exists($thumbnail_path) && $news->news_img != '') {
            $news->img_url = url("/uploads/news/thumbnail/" . $news->news_thumbnail);
        } else {
            $news->img_url = '';
        }
        return response()->json(['data' => $news, 'code' => "200"], 200);
    }
}
