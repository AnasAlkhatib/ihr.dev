<?php
namespace App\Http\Controllers\API;

use App\FavoriteVacant;
use App\Http\Requests\FavoriteVacantRequest;
use App\Http\Controllers\Controller;
use App\Seeker;
/**
 * Class FavoriteVacantController
 * @package App\Http\Controllers\API
 */
class FavoriteVacantController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $vacantFavorite = $seeker->vacantFavorite->all();
        if (!$vacantFavorite) {
            return response()->json(['message' => 'This seeker does not have favorite jobs ', 'code' => "404"], 404);
        }
        return response()->json(['data' => $vacantFavorite, 'code' => "200"], 200);
    }
    /**
     * @param $id
     * @param $vacant_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $vacant_id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $vacantFavorite = $seeker->vacantFavorite->find($vacant_id);
        if (!$vacantFavorite) {
            return response()->json([
                'message' => 'This vacant does not assign as favorite for this seeker',
                'code' => "404"
            ], 404);
        }
        return response()->json(['data' => $vacantFavorite, 'code' => "200"], 200);
    }
    /**
     * @param FavoriteVacantRequest $request
     * @param $seeker_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FavoriteVacantRequest $request, $seeker_id)
    {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->all());
        $seeker->favoriteVacant()->create($values);
        return response()->json(['message' => "Favorite Job correctly added", 'code' => "201"], 201);
    }
    /**
     * @param $seeker_id
     * @param $favorite_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($seeker_id, $favorite_id)
    {
        $seeker = Seeker::find($seeker_id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }

        $favorite=FavoriteVacant::where([
                                         ['vacant_id','=',$favorite_id],
                                         ['seeker_id','=',$seeker_id]
                                        ])->first();

        if (!$favorite) {
            return response()->json(['message' => 'This Favorite Job does not exist', 'code' => "404"], 404);
        }
        $favorite->delete();
        return response()->json(['message' => 'The Favorite Job has been deleted', 'code' => "200"], 200);
    }
}
