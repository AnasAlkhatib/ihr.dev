<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Vacant;
use App\Seeker;
use App\City;
class RecommendedJobController extends Controller
{
    protected $filtered_data = array();
    /**
     * Display a listing of the resource.
     * @toDo refactor this class
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // find seeker by id 
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        // get all(vacancies) based on Seeker gender and both genders 
        $vacancies = Vacant::orderBy('ID', 'desc')->where('active', '=', 1)->whereIn('gender',
            [$seeker->gender, 'both'])->get();
        // location filter
        $this->filtered_data = $this->location_filter($vacancies, $this->seeker_location($seeker));
        $this->filtered_data = $this->experience_count_filter($this->filtered_data,
            $this->seeker_experience_count($seeker));
        if ($this->seeker_experience($seeker)) {
            $exp_filter = $this->experience_filter($this->filtered_data, $this->seeker_experience($seeker));
//if exp job-title existing vacant database
            if (!$exp_filter) {
                $this->filtered_data = $exp_filter;
            }
        }
        $this->filtered_data = $this->education_filter($this->filtered_data, $this->seeker_education($seeker));
//        $data = $this->filtered_data->each(function ($vacant) {
//            $vacant->exp_level = trans('app.' . $vacant->exp_level);
//            $vacant->job_status = trans('app.' . $vacant->job_status);
//            $vacant->education = trans('app.' . $vacant->education);
//            $vacant->language = $this->LanguageTransform($vacant->language);
//        })->values()->all();
        $data = $this->filtered_data;
        if (count($data) > 0) {
            return response()->json(['data' => $data, 'code' => "200"], 200);
        }
//   return response()->json(['data' => $data, 'code' => "200"], 200);
        return response()->json(['message' => 'No recommended job', 'code' => "404"], 404);
    }
    /*
     * Seeker location 
     * @return string 
     */
    function seeker_location($seeker)
    {
        return City::find($seeker->city_id)->area->name;
    }
    /*
     * Seeker education 
     * @return string 
     */
    function seeker_education($seeker)
    {
        $data = ['high_school'];
        $education = $seeker->education->all();
        if ($education) {
            foreach ($education as $key => $value) {
                $edu_level = $value->edu_level;
                switch ($edu_level) {
                    case trans('app.high_school'):
                        array_push($data, 'high_school');
                        break;
                    case trans('app.diploma'):
                        array_push($data, 'high_school', 'diploma');
                        break;
                    case trans('app.bachelor'):
                        array_push($data, 'high_school', 'diploma', 'bachelor');
                        break;
                    case trans('app.high_diploma'):
                        array_push($data, 'high_school', 'diploma', 'bachelor', 'high_diploma');
                        break;
                    case trans('app.master'):
                        array_push($data, 'high_school', 'diploma', 'bachelor', 'high_diploma', 'master');
                        break;
                    case trans('app.doctorate'):
                        array_push($data, 'high_school', 'diploma', 'bachelor', 'high_diploma', 'master', 'doctorate');
                        break;
                }
            }
        }
        return array_unique($data);
    }
    /*
     * Seeker Last experience
     * @return string 
     */
    function seeker_experience($seeker)
    {
        $experience = $seeker->experience->all();
        if ($experience) {
            return $seeker->experience->last()->job_title;
        }
    }
    /*
     * Seeker Last experience years Count 
     * @return int 
     */
    function seeker_experience_count($seeker)
    {
        $year = 0;
        $experience = $seeker->experience->all();
        if ($experience) {
            $start_date = strtotime($seeker->experience->last()->start_date);
            if ($seeker->experience->last()->end_date != null) {
                $end_date = strtotime($seeker->experience->last()->end_date);
            } else {
                $end_date = strtotime(date('Y-m-d', time()));
            }
            $days = ($end_date - $start_date) / 86400;
            $year = ceil($days / 360);
        }
        return $year;
    }
    /*
     * Vacancies location filter 
     * @return array
     */
    function location_filter($data, $seeker_location)
    {
        $filter = $data->filter(function ($vacant) use ($seeker_location) {
            $company_location = explode(',', $vacant->company_location);
            if (in_array($seeker_location, $company_location)) {
                return $vacant;
            }
        });
        return $filter;
    }
    /*
     * Vacancies Experience Filter 
     * @return array
     */
    function experience_filter($data, $experience)
    {
        $filter = $data->filter(function ($vacant) use ($experience) {
            for ($i = 0; $i < $vacant->count(); $i++) {
                if ($vacant->job_title == $experience) {
                    return $vacant;
                }
                return false;
            }
        });
        return $filter;
    }
    /*
     * seeker experience count filter 
     * @return array
     */
    function experience_count_filter($data, $years)
    {
        $filter = $data->filter(function ($vacant) use ($years) {
            if ($vacant->exp_years <= $years) {
                return $vacant;
            }
        });
        return $filter;
    }
    /*
     * Vacancies Experience Filter 
     * @return array
     */
    function education_filter($data, $education)
    {
        $filter = $data->filter(function ($vacant) use ($education) {
            if (in_array($vacant->education, $education)) {
                return $vacant;
            }
        });
        return $filter;
    }
    /*
     * clean up string 
     * @return array
     */
    function clean_string($string)
    {
        return trim(str_replace("-", "", $string));
    }
//    public function LanguageTransform($language)
//    {
//        $data = array();
//        $vacantLanguage = explode(',', $language);
//        foreach ($vacantLanguage as $value) {
//            $data[] = trans('app.' . $value);
//        }
//        return implode(',', $data);
//    }
}
