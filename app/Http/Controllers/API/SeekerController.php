<?php
namespace App\Http\Controllers\API;

use App\Country;
use App\Seeker;
use App\Vacant;
use App\Http\Requests\SeekerSetLocaleRequest;
use App\Jobs\SendSeekerActivationCode;
use App\Http\Controllers\Controller;
use App\Http\Requests\SeekerRequest;
use App\Http\Requests\SendSeekerCvRequest;
use App\Jobs\GenerateSeekerCv;
/**
 * Class SeekerController
 * @package App\Http\Controllers\API
 */
class SeekerController extends Controller
{
    /**
     * @var string
     */
    private $photoPath = '/uploads/seeker/photo/';
    /**
     * SeekerController constructor.
     */
    public function __construct()
    {
        $this->middleware('locale');
    }
    /**
     * @param SeekerRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @toDo Remove sms activation function to own class
     * @toDo Correct status code response to match Standard
     */
    public function store(SeekerRequest $request)
    {
        $values = $request->except(['user_name', 'password']);
        $values['password'] = trim(bcrypt($request->input("password")));
        $activation_code = $this->generateCode(5);
        $values['activation_code'] = $activation_code;
        $result = array_map('trim', $values);
        $seeker = Seeker::create($result);
        if ($seeker) {
            $this->dispatch(new SendSeekerActivationCode($seeker));
            //send sms activation code
            if (\App::environment('production')) {
                $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
                        [
                            'api_key' => env('NEXMO_API_KEY'),
                            'api_secret' => env('NEXMO_API_SECRET'),
                            'to' => '966' . ltrim($values['mobile'], '0'),
                            'from' => 'IHR App',
                            'type' => 'unicode',
                            'text' => trans('app.successful_registration_message',
                                ['activation_code' => $activation_code])
                        ]
                    );
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $res = json_decode(curl_exec($ch));
                if (($res->messages[0]->status == 0)) {
                    return response()->json(
                        [
                            'message' => trans('app.successful_signing_up'),
                            'data' => $seeker->attributesToArray(),
                            'code' => "201"
                        ],
                        201
                    );
                } else {
                    \Log::info('sms was not send  : ' . $values['mobile'] . ':error code: ' . $res->messages[0]->status);
                    return response()->json(
                        [
                            'message' => trans('app.sms_activation_error'),
                            'error' => 3,
                            'data' => $seeker->attributesToArray(),
                            'code' => "404"
                        ],
                        201);
                }
            } else {
                return response()->json(
                    [
                        'message' => trans('app.successful_signing_up'),
                        'data' => $seeker->attributesToArray(),
                        'code' => "201"
                    ],
                    201
                );
            }
        } /* if seeker created */
    }
    /**
     * @param $seekerId
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function activateAccountmobile($seekerId, $code)
    {
        $seeker = Seeker::find($seekerId);
        if (!$seeker) {
            return response()->json(['message' => trans('app.account_not_found'), 'code' => "3"], 201);
        } else {
            //check if seeker already active
            if ($seeker->active == 1) {
                return response()->json(['message' => trans('app.account_already_active'), 'code' => "2"], 201);
            } elseif ($seeker->activation_code == $code) {
                $seeker->active = 1;
                $seeker->activation_code = '';
                $seeker->save();
                return response()->json([
                    'message' => trans('app.account_has_been_activated'),
                    'data' => $seeker->active,
                    'code' => "1"
                ], 200);
            } else {
                return response()->json(['message' => trans('app.wrong_activation_code'), 'code' => "4"], 200);
            }
        }
    }
    /**
     * @param integer $seekerid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resendActivationCode($seekerid)
    {
        $seeker = Seeker::find($seekerid);
        if (!$seeker) {
            return response()->json(['message' => trans('app.account_not_found'), 'code' => "404"], 404);
        } else {
            if ($seeker->active == 1) {
                return response()->json(['message' => trans('app.account_already_active'), 'code' => "404"], 404);
            }
        }
        //now we resend the activation code by sms
        $activation_code = $this->generateCode(5);
        $seeker->activation_code = $activation_code;
        $seeker->save();
        /* send sms only on production */
        if (\App::environment('production')) {
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
                    [
                        'api_key' => env('NEXMO_API_KEY'),
                        'api_secret' => env('NEXMO_API_SECRET'),
                        'to' => '966' . ltrim($seeker->mobile, '0'),
                        'from' => 'IHR App',
                        'type' => 'unicode',
                        'text' => trans('app.new_activation_code', ['activation_code' => $activation_code])
                    ]
                );
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = json_decode(curl_exec($ch));
            if (($res->messages[0]->status == 0)) {
                return response()->json(
                    ['message' => trans('app.activation_code_resend'), 'code' => "201"],
                    201
                );
            } else {
                \Log::info('sms was not send  : ' . $seeker->mobile . ':error code: ' . $res->messages[0]->status);
                return response()->json(
                    ['message' => trans('app.wrong_activation_code'), 'code' => "404"],
                    201
                );
            }
        }/* send only on production */
    }
    /**
     * activation account web page
     * @param $code
     * @param Seeker $seeker
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activateAccount($code, Seeker $seeker)
    {
        if ($seeker->accountIsActive($code)) {
            session(['message' => trans('activate.success'), 'alert-class' => 'alert-success']);
        } elseif ($seeker->Activated($code)) {
            session(['message' => trans('activate.activated'), 'alert-class' => 'alert-success']);
        } else {
            session(['message' => trans('activate.error'), 'alert-class' => 'alert-error']);
        }
        return view('activate.message');
    }
    /**
     * Display the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This seeker does not exist', 'code' => "404"], 404);
        }
        /*
         * Modify the seeker object by adding full photo path if exist
         */
        $photoUrl = public_path() . $this->photoPath . $seeker->pic_file_name;
        if (\File::exists($photoUrl) && $seeker->pic_file_name) {
            $seeker->pic_file_name = url($this->photoPath . $seeker->pic_file_name);
        }
        if ($seeker->country_id != 0) {
            if (\App::getLocale() == 'en') {
                $seeker->country_name = $seeker->country->find($seeker->country_id)->name;
            } else {
                $seeker->country_name = $seeker->country->find($seeker->country_id)->name_ar;
            }
        }


        if ($seeker->residence != 0 && $seeker->country_id != 0 ) {
            if (\App::getLocale() == 'en') {
                $seeker->residence_name = $seeker->country->find($seeker->residence)->name;
            } else {
                $seeker->residence_name = $seeker->country->find($seeker->residence)->name_ar;
            }
        }
        else if ($seeker->residence != 0 && $seeker->country_id == 0 ){
            if (\App::getLocale() == 'en') {
                $seeker->residence_name =Country::find($seeker->residence)->name;
            } else {
                $seeker->residence_name = Country::find($seeker->residence)->name_ar;
            }

           }

        return response()->json(['data' => $seeker, 'code' => "200"], 200);
    }
    /**
     * @param SeekerRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SeekerRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This Seeker does not exist', 'code' => "404"], 404);
        }
        $values = array_map('trim', $request->except('_method', 'password', 'active'));
        foreach ($values as $field => $value) {
            $seeker->$field = $value;
        }
        $seeker->save();
        return response()->json(['data' => $seeker, 'message' => 'The seeker has been updated', 'code' => "200"], 200);
    }
    /**
     * @param $id
     * @param SendSeekerCvRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendCv($id, SendSeekerCvRequest $request)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This Seeker does not exist', 'code' => "404"], 404);
        }
        $recipient = $request->input("email");
        $this->dispatch(new GenerateSeekerCv($seeker, $recipient));
        return response()->json(['message' => 'The Seeker CV sent Successfully', 'code' => "200"], 200);
    }
    /**
     * get all available vacant tags
     * @return \Illuminate\Http\JsonResponse
     */
    public function vacantTags()
    {
        $tags = Vacant::existingTags()->pluck('name');
        return response()->json(['data' => $tags, 'code' => "200"], 200);
    }
    /**
     * @param SeekerSetLocaleRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSeekerLocale(SeekerSetLocaleRequest $request, $id)
    {
        $seeker = Seeker::find($id);
        if (!$seeker) {
            return response()->json(['message' => 'This Seeker does not exist', 'code' => "404"], 404);
        }
        $locale = trim($request->input('locale'));
        $seeker->locale = $locale;
        $seeker->save();
        return response()->json([
            'data' => $seeker->locale,
            'message' => 'The Seeker Locale has Been Updated',
            'code' => "200"
        ], 200);
    }
    /**
     * @param int $length
     * @return string
     */
    private function generateCode($length = 16)
    {
        $num = range(0, 9);
        $alf = range('a', 'z');
        $_alf = range('A', 'Z');
        $symbols = array_merge($num, $alf, $_alf);
        shuffle($symbols);
        $code_array = array_slice($symbols, 0, (int)$length);
        $code = implode("", $code_array);
        return $code;
    }
}