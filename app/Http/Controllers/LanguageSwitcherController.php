<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\Session;
/**
 * Class LanguageSwitcherController
 * @package App\Http\Controllers
 */
class  LanguageSwitcherController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if (array_key_exists($request->input('locale'), Config::get('app.supported_languages'))) {
            Session::set('locale', $request->input('locale'));
        }
        $locale_session = Session::has('locale');
        if ($locale_session) {
            return response()->json(['message' => 'Language Changed successfully', 'status_code' => 200], 200);
        } else {
            return response()->json(['message' => ' The action Not Implemented', 'status_code' => 501], 501);
        }
    }
}
