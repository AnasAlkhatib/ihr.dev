<?php
namespace App\Http\Middleware;

use Closure;
use View;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Session;
/**
 * Class LocalizationMiddleware
 * @package App\Http\Middleware
 */
class LocalizationMiddleware
{
    /**
     * Localization Middleware constructor
     *
     * LocalizationMiddleware constructor.
     * @param Application $app
     * @param Auth $auth
     */
    protected $app;
    /**
     * LocalizationMiddleware constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $languages = Config::get('app.supported_languages');
        if (Session::has('locale') && array_key_exists(Session::get('locale'), $languages)) {
            $this->app->setLocale(Session::get('locale'));
            $locale = Session::get('locale');
        } else {
            $this->app->setLocale(Config::get('app.locale'));
            $locale = (Config::get('app.locale'));
        }
        View::share(['locale' => $locale]);
        return $next($request);
    }
}
