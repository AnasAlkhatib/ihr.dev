<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {
    protected $primaryKey = 'ID';
    protected $hidden = [ 'created_at', 'updated_at'];
    protected $fillable = ['area_id','name', 'name_en'];

    public function area() {
        return $this->belongsTo('App\Area');
    }

}
