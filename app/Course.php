<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Course
 * @package App
 */
class Course extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'ID';
    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    /**
     * @var array
     */
    protected $fillable = ['course_name', 'course_date', 'center_name', 'course_hours'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seeker()
    {
        return $this->belongsTo('App\Seeker');
    }
}
